﻿        <%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchForm.ascx.cs" Inherits="UserControl_SearchForm" %>
        <script type="text/javascript">
            $(window).load(function () {

            });

            $(document).ready(function () {
                $("#gc_BtnShowSearchID").click(function (ev) {
                    $("#gc_gobal_SearchID").toggle("slow");
                });


                $("#gc_BtnSearchID").click(function (ev) {//only testing get webmethod from aspx.
                    var dataString = $("#gc_txt_Search_ID").val();
                    $("#gc_gbHTitle").html("Kết quả tìm kiếm");
                    $("#gc_gobal_PagerTopID").hide();
                    $("#gc_ClassInformationInnerID").html("");
                    $("#gc_gobal_ListDataID").html("");

                    $('#gc_gb_MessageInfomation').html("<img src='../UserControl/Images/loading.gif'/>&nbsp;<strong>Đang tải dữ liệu...</strong> ");
                    cur_gb_GridDataIndex = 0;
                    var ClassId = $("#gc_cmbLopHocID_ID").val();
                    gb_strFilter = " (StudentIdHoTen like N'%" + dataString + "%')";
                    gb_strFilter += " oR (TeacherIdHoTen like N'%" + dataString + "%')";
                    gb_strFilter += " oR (NVTuVanIdHoTen like N'%" + dataString + "%')";
                    gb_strFilter += " oR (ThayGiaoTuVanIdHoTen like N'%" + dataString + "%')";
                    gb_strFilter += " oR (NhanVienCapNhat like N'%" + dataString + "%')";
                    gb_strFilter += " oR (username like N'%" + dataString + "%')";
                    //gb_PageSize only get max object 50
                    var gb_PageSizeInner = 50;
                    WSgcGobal_StyleSheet.GetDataSearch(ClassId, cur_gb_GridDataIndex, gb_PageSizeInner, gb_strSort, gb_strFilter, ' ', cur_gb_Account, gc_fn_LayoutListData);
                });

        
            });

         </script>
         
         <!-- gc Search Information : first time is disable-->
        <div class="content gc-gb-notshowatInit" id="gc_gobal_SearchID">
            <div id='gc_Search_ContentID' class="gc_SearchForm" >
	           <div class="login_sign" id="gc_SearchFormID">
                    <div class="input">
                        <label>Gõ vào từ khóa tìm kiếm</label>
                        <input type="text"  autocomplete="off"   value="Tìm kiếm..." id="gc_txt_Search_ID" name="search" class="form-control"  />
                        <input type="text" id="gc_txt_Search_ID_ID" style='display:none;'  />
                    </div>
                </div>
                <div class="inputBtnBottomV2">
                        <input type="button"  value="Tìm ngay" id="gc_BtnSearchID"  class="btn btn-primary btn-sm" />
                        <input type="button"  value="Đóng tìm kiếm" id="gc_BtnSearchCancelBtnID"  class="btn btn-info btn-sm" />
                </div>
            </div>
        </div>
        <!-- End gc Search Information -->
