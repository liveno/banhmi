﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DialogOK.ascx.cs" Inherits="UserControl_FormDialogOK" %>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#gc_DialogOKCancelBtnID").click(function (ev) {
                    $.colorbox.close();
                });
            });

         </script>

        <div id='gc_FormDialogOKID' style='min-height:100px;min-width:300px; margin-top:40px; padding:10px 20px 20px 10px; background:#fff;'>
	        <div class="login_sign" id="gc_DialogOKID">
                <div class="input">
                    <label id="gc_gb_MsgBoxLabel">Bạn đang thực hiện xóa dữ liệu này đúng không?</label>
                </div>

            </div>
            <div class="inputBtnBottom">
                    <input type="button"  value="Thực hiện" id="gc_DialogOKOKBtnID"  class="btn btn-warning btn-sm " />
                    <input type="button"  value="Quay về" id="gc_DialogOKCancelBtnID"  class="btn btn-info btn-sm" />
            </div>
        </div>