﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Contain_Right.ascx.cs" Inherits="UserControl_Contain_Right" %>
<script type='text/javascript'>
    function clickD(id)
    {
        var divS = $get(id+"_P");

        var page = divS.innerHTML;
        window.location.href = page;
    }
    function divClick(id, page)
    {
        var divS = $get(id);

        window.location.href = page;
    }
</script>
<div class="Menu_Right" style="z-index:10;">
    <div class="RightImage" style="width:280px;">
        <div style="border:solid 1px #ccc; float:left;"><div  class="RightImageBtn" onclick='divClick(this.id,"Download.aspx")'> TẢI PHẦN MỀM</div></div>
        <div style="width:3px;float:left;">&nbsp;</div>
        <div style="border:solid 1px #ccc; float:left;"><div  class="RightImageBtn" onclick='divClick(this.id,"CustomerSend.aspx")'> GHI NHẬN PHẢN HỒI</div></div>
    </div>
    <div style="clear:both;"></div>
    <div style="height:5px;"></div>
    
    <div class="RightTitle">
    </div>
    <div class="RightContain">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight:bold; color:#fff;font-size:12px;">Công ty Cổ phần Công nghệ Ánh Xuân</span> được thành lập từ đội ngũ những nhà khoa học đã và đang giảng dạy từ Đại Học Bách Khoa Tp. Hồ Chí Minh. Với kinh nghiệm thực hiện và triển khai nhiều dự án từ nhà nước cho đến doanh nghiệp tư nhân.<br /><br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Công nghệ hiện đại và sự phục vụ tận tâm chăm sóc khách hàng là tiêu chí hàng đầu 
         trong quá trình phát triển công ty. Chúng tôi mong muốn đem lại sự thuận tiện về dịch vụ chăm sóc, chất lượng sản phẩm cao nhất cho khách hàng.
         <br />
         <br />
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tư vấn, thiết kế, triển khai giải pháp phần cứng, phần mềm và quảng cáo thương mại với các lĩnh vực chính:
          <br />
           <br />
           <div class='giaiPhapContentWrapper'>
         <div class='giaiPhapContent'>• Giải pháp GIS </div>
         <div class='giaiPhapContent'>• Giải pháp ERP cho doanh nghiệp </div>
         <div class='giaiPhapContent'>• Giải pháp Hệ thống nhúng </div>
         <div class='giaiPhapContent'>• Giải pháp Hệ thống tự động công nghiệp </div>
         <div class='giaiPhapContent'>• Giải pháp Mạng </div>
         </div>
    </div>
    <div class="RightFooter_1">
    </div>
</div>
<div class="DivClear"></div>