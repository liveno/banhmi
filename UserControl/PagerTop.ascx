﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PagerTop.ascx.cs" Inherits="UserControl_PagerTop" %>

<div class="LegBottomAndSlide" id="gc_gobal_PagerTopID"> 
                <!--include a div which we'll use to illustrate jPaginator doing its job-->
	<div id="gc_ShowNumObjectID" class="ShowTitleNumOBject"></div>		   
    <div id="pagination"> 
			
	    <!-- optional left control buttons--> 					
	    <a class="control" id="max_backward"></a>
				
	    <a class="control" id="over_backward"></a> 
								
	    <div class='paginator_p_wrap'> 
				
		    <div class='paginator_p_bloc'> 
					
			    <!--<div class='paginator_p'></div> // page number : dynamically added --> 
						
		    </div> 
					
	    </div> 
				
	    <!-- optional right control buttons--> 
	    <a class="control" id="over_forward"></a>
				
	    <a class="control" id="max_forward"></a> 
										
	    <!-- slider --> 
	    <div class='paginator_slider' class='ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'> 
					
		    <a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a> 
					
	    </div>
				
    </div>
</div>