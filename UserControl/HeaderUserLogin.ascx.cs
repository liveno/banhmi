﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using zgc0LibAdmin;

public delegate void SendMessageToThePageHandler(string messageToThePage);

public partial class HeaderUserLogin : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        zgc0GlobalDict gcdict = new zgc0GlobalDict();
        object gcUserInfo = Session[gcdict.strDict["AccountInfoCol"]];
        if (gcUserInfo != null)
        {
            lblWelcome.Text = "Xin chào: Anh/chị " + (string)gcUserInfo ;
        }

        System.Text.StringBuilder strBuild = new System.Text.StringBuilder();
        strBuild.AppendLine("<script type='text/javascript'>");
        //strBuild.AppendLine("var menuQLNVCID='" + this.menuQLNV.ClientID + "';");
        //strBuild.AppendLine("var menuQLKHCID='" + this.menuQLKH.ClientID + "';");
        //strBuild.AppendLine("var menuQLBCCID='" + this.menuQLBC.ClientID + "';");
        //strBuild.AppendLine("var menuQLXECID='" + this.menuQLXE.ClientID + "';");
        //strBuild.AppendLine("var menuQLKHOCID='" + this.menuQLKHO.ClientID + "';");

        strBuild.AppendLine("</script>");
        this.Page.RegisterStartupScript("gcmenuP", strBuild.ToString()); 
    }
    
}
