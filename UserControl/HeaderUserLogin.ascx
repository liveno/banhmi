﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderUserLogin.ascx.cs" Inherits="HeaderUserLogin" %>

<%@ Register Src="~/UserControl/Flash_Banner.ascx" TagName="FlashBanner" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/Header_Tag.ascx" TagName="Header_Tag" TagPrefix="uc2" %>

<script type='text/javascript'>
<!--
function clickDivButton(id)
{
    curTCID = id;
    //gcGobalService.getStartButton(updateDivStartJS);
}
function updateDivStartJS(result)
{
    updateDivStart(result);
}
function btnReturnHome()
{
    window.location = "../ZGCLOGINUSER/zgc0LoginUser.aspx";
}

function btnKH(id)
{
BuildDivMeNuSelect(id);
}
function btngcHelper(id)
{
}

$(document).ready(function () {
    $("#gcLogoutBtnID").click(function () {
        window.location = "../Default.aspx";
    });
});
-->
</script>


<asp:Label style='display:none;' Width='1px' ID='lHideP2' Text='10|100' runat='server' ></asp:Label> 
<div class="qsfWrap" style="display:none">
        <div style="width:auto; height:60px; background-color:#3c3945;"> 
            <div style="height:35px;background-repeat:no-repeat" runat="server" id="StartDiv" class='gcFunctionGobal'                       onclick='clickDivButton(this.id)' ></div> 
        <div class="divLink" style="position: relative;float:right;margin:-60px 0px 0 0; cursor: pointer; color: White; font-family: Tahoma; font-weight: bold;">
        <asp:Label ID='lblWelcome' CssClass="divLink" runat='server' Text='Xin chào:'></asp:Label>|<a class="divLink" href="../Common/Logout.aspx">Thoát </a>
        </div>
        <div id='divbtnInBienNhan' style="margin:-30px 0px 0 0;float:right" class='gcNameTitleNew' runat='server' onclick='btnReturnHome();'>Về trang chủ</div> 
        <div id='divzgc0Helper' style='margin:-30px 100px 0 0;float:right;' class='gcNameTitleNew' runat='server' onclick='btnzgc0Helper(this.id);'>Giúp đỡ?</div> 
        
        
        </div>
</div>

<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="background-color: #064355;border-color: #064355;">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Anh ngữ AMA Vũng Tàu</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <li class="active"><a href="#">Trang chủ</a></li>
        </ul>
        <ul class="nav navbar-nav" style="float: right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tùy chọn <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
            <li><a href="#">Giúp đỡ</a></li>
            <li class="divider"></li>
            <li class="dropdown-header">Tài khoản : <%=Session["gcUserName"]%></li>
            <li><a href="#">Thông tin</a></li>
            <li><a href="#" id="gcLogoutBtnID">Đăng xuất</a></li>
            </ul>
        </li>
        </ul>
    </div>
        
    </div>
</nav>