﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftSideBar.ascx.cs" Inherits="UserControl_Share_SideBar" %>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../UserControl/Plugin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>
                    Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
            </div>
        </div>
        <!-- search form -->
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<%=((Session["Action"] + "" == "QLAdmin") ? "active" : "") %>"><a
                href="/QLAdmin/Default.aspx"><i class="fa fa-dashboard"></i><span>Doanh thu</span>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
