﻿        <%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.ascx.cs" Inherits="UserControl_ChangePassword" %>
        <script type="text/javascript">
            $(window).load(function () {

                $("#gc_gb_DIVChangePasswordID").hide();
            });

            $(document).ready(function () {
                $("#gc_gb_DIVChangePasswordID").hide();
                $("#gc_ChangePassWordCancelBtnID").click(function (ev) {
                    $.colorbox.close();
                });

                $("#gc_ChangePassWordOKBtnID").click(function (ev) {
                    var gc_user_edit_OldPassWordID = $("#gc_user_edit_OldPassWordID").val();
                    var gc_user_edit_NewPassWordID = $("#gc_user_edit_NewPassWordID").val();

                    WSgcGobal_StyleSheet.UpdatePassword(gc_user_edit_OldPassWordID, gc_user_edit_NewPassWordID, gc_fn_ChangePassSucss, gc_fn_ChangePassFail);

                    $('#gc_gb_MessageInfomation').html("<i class='glyphicon glyphicon-ok '></i>&nbsp;<strong>Đã thay đổi mật khẩu thành công.</strong> ");
                    $.colorbox.close();
                });
            });
            function gc_fn_ChangePassSucss(result) {
                $("#gc_gb_MsgChangePassWordLabel").html("Thay đổi mật khẩu thành công.");
            }
            function gc_fn_ChangePassFail() {
                $('#gc_gb_MessageInfomation').html("<i class='glyphicon glyphicon-ok '></i>&nbsp;<strong>Thay đổi mật khẩu thất bại.</strong> ");
                $("#gc_gb_MsgChangePassWordLabel").html("thay đổi mật khẩu thất bại.");
            }
         </script>

        <div style="display:none;">
            <div id='gc_gb_DIVChangePasswordID' style='min-height:100px;min-width:300px; margin-top:40px; padding:10px 20px 20px 10px; background:#fff; '>
                <div id="gc_gb_DIVChangePasswordInnerWrapID" style='min-height:100px;min-width:300px; margin-top:40px; padding:10px 20px 20px 10px; background:#fff; '>
	            <div class="login_sign" id="gc_gb_DIVChangePasswordInnerID">
                    <div class="input">
                        <label>Thay đổi mật khẩu</label>
                        <label class="alert alert-success" id="gc_gb_MsgChangePassWordLabel"></label>
                    </div>
                    <div class="alert alert-success alert-info-gc-small">
                        <div class="input">
                            <label>Nhập mật khẩu cũ</label>
                            <input type="password" autocomplete="off" id="gc_user_edit_OldPassWordID" class="input-large"  />
                        
                        </div>
                    </div>
                    <div class="alert alert-warning alert-info-gc-small">
                        <div class="input">
                            <label>Nhập mật khẩu mới</label>
                            <input type="password" autocomplete="off" id="gc_user_edit_NewPassWordID" class="input-large"  />
                        </div>
                    </div>
                    

                </div>
                <div class="inputBtnBottom">
                        <input type="button"  value="Thay đổi" id="gc_ChangePassWordOKBtnID"  class="btn btn-warning btn-sm" />
                        <input type="button"  value="Quay về" id="gc_ChangePassWordCancelBtnID"  class="btn btn-info btn-sm" />
                </div>
                </div>
            </div>
        </div>