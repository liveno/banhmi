﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PagerBottom.ascx.cs" Inherits="UserControl_PagerBottom" %>

<div class="LegBottomAndSlide" id="gc_gobal_PagerTopID"> 
                <!--include a div which we'll use to illustrate jPaginator doing its job-->
			   
    <div id="bottom_pager"> 
			
	    <!-- optional left control buttons--> 					
	    <a class="control" id="bt_max_backward"></a>
				
	    <a class="control" id="bt_over_backward"></a> 
								
	    <div class='paginator_p_wrap'> 
				
		    <div class='paginator_p_bloc'> 
					
			    <!--<div class='paginator_p'></div> // page number : dynamically added --> 
						
		    </div> 
					
	    </div> 
				
	    <!-- optional right control buttons--> 
	    <a class="control" id="bt_over_forward"></a>
				
	    <a class="control" id="bt_max_forward"></a> 
										
	    <!-- slider --> 
	    <div class='paginator_slider' class='ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'> 
					
		    <a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a> 
					
	    </div>
				
    </div>
</div>