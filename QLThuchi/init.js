﻿

function fun_BreakControlEnter(id) {
    return false;
};
function pAsg() {
};
function FocusCmbText(strId, e) {
};

function view($that, $container, o, on) {
    var parentWidth = $container.parent().innerWidth();
    var html = $(on).html();
    //var $title = $('<h3 style="font-size: 18px; color: rgb(58, 135, 173);"/>').text($that.find('a').text());
    $('#gc_gbHTitle').text($that.find('a').text());
    $container.prepend($(on)).css({ "width": parentWidth + "px" });
    var $listButton = $container.find('.inputBtnBottomV2');
    $listButton.find('.btn.btn-warning.btn-sm').hide();
    var $inputFirst = $container.find('div.input').find('input:first'); //.not('#fo28605i');
    $inputFirst.each(function () {
        if ($that.attr('id') == "fo28605i") {
            $that.css({ 'margin-right': '5px' });
        }
        else {
            $that.css({ 'margin-right': '35px' });
        }
    });
};

/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
* Các hàm ngắt kernel.js                                                *
_______________________________________________________________________*/

/************************************************************************
* Xử lý khi nhấn nút đóng ở report                                      *
*************************************************************************/
function pCDT(s, obj) {
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 2) < 10 ? ('0' + (s._e.length - 2)) : (s._e.length - 2))] = cur_gb_AccountId;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 5) < 10 ? ('0' + (s._e.length - 5)) : (s._e.length - 5))] = cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account;
    else if (obj.type == 'u') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = "isnull($x, '') + N'~" + (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account + "'";
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 7) < 10 ? ('0' + (s._e.length - 7)) : (s._e.length - 7))] = cur_gb_Account;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 8) < 10 ? ('0' + (s._e.length - 8)) : (s._e.length - 8))] = 0;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 13) < 10 ? ('0' + (s._e.length - 13)) : (s._e.length - 13))] = dateToString(new Date(), 'iso');
    obj.d['f' + ((s._e.length - 15) < 10 ? ('0' + (s._e.length - 15)) : (s._e.length - 15))] = cur_gb_AccountId;
    if (s.id == 'assignClass' || s.id == 'assignStudent' || s.id == 'studentEnrollAL'
        || s.id == 'enrollment') {
        obj.a = 'AddOrUpdateClassStudent';
    }
};
function pCb() {
    if ($('#classDetail').length > 0 && $('#classDetail_i').length > 0) {
        $('#classDetail_i').find('.jtable tbody tr:first').click();
    }
};
///************************************************************************
// Xử lý ngắt chỉnh display jtable                                          *
//*************************************************************************/
function pCW(v, _s, ll, i, ii, ic, f0){
    // Menu Student
    if (_s.id == 'studentList') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f01; showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Birthday'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    }
    return v;
};
function postonItemClick(itemLI, tBox, index) {
    if((tBox == "dmstudentList_Stausi" || tBox == "dmstudentList_Sexi" || tBox == "dmstudentList_AOFirsti"
            || tBox == "dmstudentList_StudentGroupi" || tBox == "dmstudentList_Sourcei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
    }
};
/************************************************************************
* Xử lý gán Phieunhapkhoid cho input_detail (ngắt kernel.js)            *
*************************************************************************/
function pAssId(s) {
    //Class
    if (s.id == 'assignRoom') {
        $('#fassignRoom01h').val(cur_gb_KeyId);
    }
    
};
/************************************************************************
* Xử lý validate dữ liệu trước khi insert or update (ngắt kernel.js)    *
*************************************************************************/
function pVd(s) {
    var flag = false;
    //Student
    if (s.id == 'studentAdd') {
        if($("#fstudentAdd01i").val() == "" || $("#fstudentAdd03i").val() == "" || $("#fstudentAdd07h").val() == ""
            || $("fstudentAdd17h").val() == ""){
            modal.open({ content: "Bạn vui lòng nhập đủ thông tin" });
            flag = true;
        }
    }
    return flag;
};

/************************************************************************
* Xử lý sau khi insert update dữ liệu thành công                        *
*************************************************************************/
function p_us_o(s, t, r, o) {
    if(r.Result == "OK"){
        if(s.id == "studentAdd" && o.type == "i"){
            cur_gb_cur_ListDataShow = o.d;
            cur_gb_cur_ListDataShow.sN = o.d.f01;
            cur_gb_cur_ListDataShow.f00 = r.Records;
            showPopupStudentDetail("", r.Records, "#newStudent");
            $("#newStudent").click();
        }
    }
};

function reportRequest(obj, opt){
    modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $.ajax({
        type: "POST",
        url: '../zgcReport/Report.ashx',
        crossDomain: true,
        data: JSON.stringify({ obj: obj }),
        success: function (msg) {
            modal.close();
            var $divall = $("<div></div>");
            var $div = $("<div class='input'></div>");
            var $divClear = $("<div style='clear: both; height: 20px'></div>");
            var $btnActive = $("<input type='button'  value='Đóng' id='gc_btnActiveForm' onclick='closeReport()' class='btn btn-info btn-sm' />");
            var $btnExport = $("<input type='button'  value='Export' id='gc_btnExportForm' onclick='convertTableToExcel()' class='btn btn-info btn-sm' />");
            var $btnPrint = $("<input type='button'  value='In' id='gc_btnPrintForm' onclick='PrintReport()'  class='btn btn-info btn-sm' />");
            var $btnNewTab = $("<a type='button' target='_blank' href='../QLHocVien/DefaultQLHocVien.aspx'  class='btn btn-info btn-sm' >New tab</a>");
            $div.append($btnActive);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnExport);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnPrint);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnNewTab);
            var $divcontent = $("<div id='gc_DivReceiptContent'></div>");
            $divcontent.html(msg);
            $divall.append($div);
            $divall.append($divClear);
            $divall.append($divcontent);
            modal.open({ content: $divall.html() });
            if(opt != null && typeof opt.success == "function"){
                opt.success(obj, opt, $divall);
            }
        },
        error: function () {
            alert("Error");
        }
    });
}

function closeReport(){
    modal.close();
}
function selectOption(elId, filter, table, field){
    $('#' + elId).click(function (ev) {
        test(elId, ev, filter, table, field);
    });
    $('#' + elId).keyup(function (ev) {
        if ($('#' + elId).val() == ''){
            //$('#' + elId).val('Select...');
            $('#' + elId.substr(0, elId.length - 1) + 'h').val('');
        }
        if($('#' + elId).val() == '' && $('#' + elId.substring(0, elId.length - 1) + 'h').val() == "" &&
            typeof window[elId.split("_")[0] + "_selectOption"] == 'function'){
            window[elId.split("_")[0] + "_selectOption"]();
        }
        test(elId, ev, filter, table, field);
    });
};



