﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<%@ Register Src="~/UserControl/Header.ascx" TagName="Header" TagPrefix="gc_header" %>
<%@ Register Src="~/UserControl/Footer.ascx" TagName="Footer" TagPrefix="gc_footer" %>
<%@ Register Src="~/UserControl/PagerTop.ascx" TagName="PagerTop" TagPrefix="gc_pagertop" %>
<%@ Register Src="~/UserCtrl-ThuChi/MainMenu.ascx" TagName="MainMenu" TagPrefix="gc_MainMenu" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anh ngữ AMA Vũng Tàu</title>
    <!-- Le styles -->
    <link href="../UserControl/bootstrap/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../UserControl/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="../UserControl/bootstrap/assets/css/docs.css" rel="stylesheet" />
    <link href="../UserControl/bootstrap/assets/js/google-code-prettify/prettify.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../UserControl/style_home.css" />
    <link rel="stylesheet" href="../UserControl/colorbox-master/css/colorbox.css" />
    <link rel="stylesheet" type="text/css" href="../UserControl/styles-12.css" />
    <link rel="stylesheet" href="../UserControl/datepicker/css/datepicker.css" />
    <link rel="stylesheet" href="../UserControl/jPager/css/custom-pager.css" />
    <link rel="stylesheet" href="../UserControl/jPager/css/jPaginator.css" />
    <link href="../UserControl/bootstrap/assets/css/bootstrap-select.css" rel="stylesheet" />
    <!-- jTable style file -->
    <link href="../jsGobalLib/Scripts/jtable/themes/lightcolor/green/jtable.min.css"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../JSGobalLib/jquery-ui-themes-1.10.3/themes/ui-lightness/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="../JSGobalLib/jquery-ui-themes-1.10.3/themes/ui-lightness/jquery.ui.theme.css" />
    <link href="../UserControl/bootstrap/assets/css/bootstrap-multiselect.css" rel="stylesheet" />
    <style type="text/css">
        .rptTable
        {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        
        .login_sign select
        {
            float: left;
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .hide-important
        {
            display: none !important;
        }
        
        #assign-work .row
        {
            margin-top: 4px;
            margin-bottom: 4px;
        }
    </style>
    <script type="text/javascript" src="../jsGobalLib/xdate.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery.validate.js"></script>
    <script type="text/javascript" src="../UserControl/colorbox-master/jquery.colorbox.js"></script>
    <script type="text/javascript" src="../UserControl/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="../UserControl/jPager/js/jPaginator-min.js"></script>
    <script type="text/javascript" src="../jsGobalLib/PageInitSize.js"></script>
    <script src='../Common/gc_ShowDivData.js' type='text/javascript'></script>
    <script type="text/javascript" src="../common/gcGlobalForm.js"></script>
    <script type="text/javascript" src="../common/gcGlobal.js"></script>
    <script type="text/javascript" src="../common/Kernel.js"></script>
    <script type="text/javascript" src="../js/o.js"></script>
    <script src="../UserControl/bootstrap/assets/js/bootstrap-select.js"></script>
    <script src="../UserControl/bootstrap/assets/js/bootstap-multiselect.js"></script>
    <!-- Report js -->
    <script src="../Common/report.js"></script>
    <script src="../jsGobalLib/Scripts/jtablesite.js" type="text/javascript"></script>
    <!-- A helper library for JSON serialization -->
    <script type="text/javascript" src="../jsGobalLib/Scripts/jtable/external/json2.js"></script>
    <!-- Core jTable script file -->
    <script type="text/javascript" src="../jsGobalLib/Scripts/jtable/jquery.jtable.js"></script>
    <!-- ASP.NET Web Forms extension for jTable -->
    <script type="text/javascript" src="../jsGobalLib/Scripts/jtable/extensions/jquery.jtable.aspnetpagemethods.js"></script>
    <script src="../Common/report.js"></script>
    <script src="../Common/KernelCell.js"></script>
    <script src="../JS/dic.js"></script>
    <script src="init.js"></script>
    <script src="thuChi.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            cur_gb_Account = '<%= Session["gcUserName"] %>'; // có thời gian mã hóa tên của user để che lại
            cur_gb_GroupRight = '<%= Session["gcRightGroup"] %>';
            cur_gb_BranchId = '<%= Session["gcBranchId"] %>';
            cur_gb_AccountId = '<%= Session["gcAccountId"] %>';
            cur_gb_MaCanBoId = '<%= Session["gcMaCanBoId"] %>';
            cur_gb_CtyId = '<%= Session["gcCtyId"] %>';
            cur_gb_DepartmentId = '<%= Session["gcDepartmentId"] %>';
            cur_gb_Today = '<%= Session["curday"] %>';
            cur_gb_ElementId = '';
            $("#gc_gobal_PagerTopID").hide();
            //click vao cái đầu tiên
            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });
            setClassAll('FilterReceiptDay');
            $("#NgayBatDauIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            $("#PhieuThuButton").click();
        });
    </script>
</head>
<body>
    <a id='URLDownloadFile' style='display: none;' href=''></a>
    <form action="ClassInfo.aspx?paramExportFile=ExportToWord" method="post" id="downloaddata"
    style="display: none; visibility: hidden">
    <input type="submit" value="download" style="display: none; visibility: hidden" />
    </form>
    <form id="loginForm" runat="server">
    <!-- Webservice Information -->
    <asp:ScriptManager ID='gcSM' runat='server' EnablePageMethods='true' ScriptMode='Release'>
        <Services>
            <asp:ServiceReference Path="~/WSASMXGobal/Kernel.asmx" />
        </Services>
    </asp:ScriptManager>
    <!-- End webservice Information -->
    <div id="container">
        <div class="container">
            <!-- gc Header form : first time is enable-->
            <gc_MainMenu:MainMenu ID='gc_parent_MenuID' runat="server" />
            <div style="clear: both;">
            </div>
            <!-- End gc Header form -->
            <!-- This contains the hidden content for inline calls -->
            <div class="content" id="FilterReceiptDay">
                <div id='Div2' class="gc_SearchForm">
                    <div class="login_sign" id="Div3">
                        <div class="input">
                            <label>
                                Từ Ngày</label>
                            <input type="text" autocomplete="off" id="NgayBatDauIdNew" name='datepicker' />
                            <label>
                                Tới ngày</label>
                            <input type="text" autocomplete="off" id="NgayKetThucIdNew" name='datepicker' />
                            <label>
                                Nhập vào Mã phiếu</label>
                            <input type="text" autocomplete="off" id="MaCTSearch" />
                        </div>
                        <%--<div class="input">
                            <label>
                                Chọn nhân viên:</label>
                            <input type="text" autocomplete="off" value="Chọn danh sách..." id="mlNhanVienIdi"
                                name="combo" class="brwidthinput400 input-large" />
                            <input type="text" id="mlNhanVienIdh" style='display: none;' />
                        </div>--%>
                        <div class="col-lg-4">
                            <select id="typeID" class="selectpicker show-tick form-control" data-live-search="true"
                                name='master'>
                                <option id="Option2" value="LocHoaDonPhieuThu">1. Tìm phiếu</option>
                                <option id="Option3" value="LocHoaDonPhieuChi">2. Báo cáo</option>
                            </select>
                        </div>
                        <div class="col-lg-4" style="display: none">
                            <input type="button" value="In Báo Cáo" id="LocHoaDonPhieuThu" class="btn btn-primary btn-sm" />
                            <input type="button" value="In Báo Cáo" id="LocHoaDonPhieuChi" class="btn btn-primary btn-sm" />
                            <input type="button" value="In Báo Cáo" id="BaoCaoThu" class="btn btn-primary btn-sm" />
                            <input type="button" value="In Báo Cáo" id="BaoCaoChi" class="btn btn-primary btn-sm" />
                            <input type="button" value="In Báo Cáo" id="ChuyenSoLieuDauKy" class="btn btn-primary btn-sm" />
                        </div>
                        <input type="button" value="Tìm" id="InBillBtn" class="btn btn-primary btn-sm" />
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <!-- End gc Header form -->
                <!-- gc Search Filter : first time is enable-->
                <!-- End gc Search Filter -->
                <!-- gc Information : for notify successfull update, insert, error, first time is disable-->
                <div class="content" id="gc_gobal_InformationID">
                </div>
                <!-- End gc Information -->
                <!-- End gc Main Form -->
                <div class="content" id="gc_gobal_MainFormID">
                    <h2 id="gc_gbHTitle">
                    </h2>
                    <!--tabs-->
                    <!--/tabs-->
                    <!-- Pager at the top -->
                    <!-- End pager at the top -->
                    <div style="clear: both;">
                    </div>
                    <!-- Pager at the top -->
                    <gc_pagertop:PagerTop ID="gc_pagertopID" runat="server" />
                    <!-- End pager at the top -->
                    <div id="gc_gobal_ListDataID">
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
                <!---content-->
                <!-- End gc Main Form -->
                <div style="clear: both;">
                </div>
                <div id="report" style="display: none">
                </div>
                <!-- gc Tag Main form : first time is enable-->
                <div class="content" id="gc_gobal_TagMainFormID">
                </div>
                <!-- End Tag Main form form -->
                <!-- gc Footer form : first time is enable-->
                <div class="content" id="gc_gobal_FooterMainFormID">
                </div>
                <!-- End gc Footer form -->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/ #container-->
    </form>
    <!-- Footer Information -->
    <gc_footer:Footer ID='gc_footer_ID' runat="server" />
    <!-- End footer Information -->
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../jsGobalLib/widgets.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script type="text/javascript" src="../jsGobalLib/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script type="text/javascript" src="../jsGobalLib/canvas-to-blob.min.js"></script>
    <!--
     <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script type="text/javascript" src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
    <script type="text/javascript" src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> 
    -->
    <script type="text/javascript" src="../UserControl/bootstrap/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../UserControl/bootstrap/assets/js/holder.js"></script>
</body>
</html>
