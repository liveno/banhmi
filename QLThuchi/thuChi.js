﻿
/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
* Các hàm ready                                                         *
_______________________________________________________________________*/
$(document).ready(function () {
    $("#InBillBtn").click(function (ev) {
        if ($("#typeID option:selected").val() == 'LocHoaDonPhieuThu') {
            $("#LocHoaDonPhieuThu").click();
        }
    });
    /************************************************************************
    * Xử lý khi click Kết phiếu                                             *
    *************************************************************************/
    $("#PhieuThuButton").click(function (ev) {
        var fd = XDate.today().toString("MM/dd/yyyy");
        var td = fd;
        var stW = "";
        stW = fd == "" ? stW : (stW == "" ? (" (InvoiceDate >= '" + fd + "' ) ") : (stW + " and (InvoiceDate >= '" + fd + "' ) "));
        stW = td == "" ? stW : (stW == "" ? (" (InvoiceDate <= '" + td + "' ) ") : (stW + " and (InvoiceDate <= '" + td + "' ) "));
        kpThu("DANH SÁCH PHIẾU THU TRONG NGÀY", stW);
    });
    /************************************************************************
    * Xử lý khi In hóa đơn Tìm phiếu thu                                    *
    *************************************************************************/
    $("#LocHoaDonPhieuThu").click(function (ev) {
        var fd = d2e($("#NgayBatDauIdNew").val());
        var td = d2e($("#NgayKetThucIdNew").val());
        var iv = $("#MaCTSearch").val();
        var stW = "";
        stW = fd == "" ? stW : (stW == "" ? (" (InvoiceDate >= '" + fd + "' ) ") : (stW + " and (InvoiceDate >= '" + fd + "' ) "));
        stW = td == "" ? stW : (stW == "" ? (" (InvoiceDate <= '" + td + "' ) ") : (stW + " and (InvoiceDate <= '" + td + "' ) "));
        stW = iv == "" ? stW : (stW == "" ? (" (invoice like'%" + iv + "%') ") : (stW + " and (invoice like '%" + iv + "%' ) "));
        kpThu("DANH SÁCH PHIẾU THU", stW);
    });
});
function kpThu(title, stW) {
    modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $("#gc_gbHTitle").html(title);
    request({
        a: 'pGetGBL_TRAINNING_COURSE_CLASS_STUDENT', c: '', d: '', type: 'p', cl: '*', si: 1, mr: 10000, se: ' InvoiceDate', f: stW
    }, {
        success: function (o, op, r) {
            modal.close();
            if (r.Result == "OK") {
                var $el = $('<table style="margin: 0 auto; width: 120%"/>').addClass('table');
                $el.append("<thead><tr><th>No.</th><th>InvoiceDate</th><th>Invoice</th><th>Student</th><th>Type</th><th>Payment</th>" +
                    "<th>Class</th><th>Note</th><th>Description</th><th>Employee</th><th>Unit</th><th>Amount</th><th width='10%'>FinishPT</th><th>TT</th><th>TV</th></tr></thead>");
                var $tbody = $('<tbody />');
                for (var i = 0; i < r.Records.length; i++) {
                    $tbody.append('<tr data-id="' + r.Records[i][1] + '"><td>' + (i + 1) + '</td>' +
                                            '<td>' + (r.Records[i][12] == null ? '' : _parseDate(r.Records[i][12] + "").format("dd/MM/yyyy")) + '</td>' +
                                            '<td>' + (r.Records[i][4] == null ? '' : r.Records[i][4]) + '</td>' +
                                            '<td>' + (r.Records[i][58] == null ? '' : r.Records[i][58]) + '</td>' +
                                            '<td>' + (r.Records[i][73] == null ? '' : r.Records[i][73]) + '</td>' +
                                            '<td>' + (r.Records[i][65] == null ? '' : r.Records[i][65]) + '</td>' +
                                            '<td>' + (r.Records[i][66] == null ? '' : r.Records[i][66]) + '</td>' +
                                            '<td>' + (r.Records[i][11] == null ? '' : r.Records[i][11]) + '</td>' +
                                            '<td>' + (r.Records[i][17] == null ? '' : r.Records[i][17]) + '</td>' +
                                            '<td>' + (r.Records[i][63] == null ? '' : r.Records[i][63]) + '</td>' +
                                            '<td>' + (r.Records[i][60] == null ? '' : r.Records[i][60]) + '</td>' +
                                            '<td>' + (r.Records[i][5] == null ? '' : c(r.Records[i][5])) + '</td>' +
                                            '<td><input type="text" autocomplete="off" class="form-control finishPT" value="' +
                                                (r.Records[i][42] == null ? '' : _parseDate(r.Records[i][42] + "").format("dd/MM/yyyy")) + '"></td>' +
                                            '<td>' + (r.Records[i][47] == null ? 0 : r.Records[i][47]) + '</td>' +
                                            '<td><input type="button" class="btn btn-warning btn-sm kpThu" value="Kp" data-id="' + r.Records[i][1] + '"></td>' +
                                            '</tr>');
                }
                $('#gc_gobal_ListDataID').html('');
                $('#gc_gobal_ListDataID').append($el.append($tbody));
                $el.find(".finishPT").datepicker({ format: 'dd/mm/yyyy' }); //.change(function () { dmstudentList_selectOption(); }).on('changeDate', dmstudentList_selectOption);
                $el.find(".kpThu").click(function () {
                    var $t = $(this);
                    var $tr = $(this).closest("tr");
                    var $in = $tr.find("td:nth-child(13) input");
                    var $c = $tr.find("td:nth-child(14)");
                    var id = $t.attr("data-id");
                    if ($in.val().length < 8 || $in.val().length > 10) {
                        showError("Bạn chưa nhập ngày kết phiếu.");
                    } else {
                        request({
                            a: 'UpdateGBL_TRAINNING_COURSE_CLASS_STUDENT',
                            c: { Id: id },
                            d: { Keep05: d2e($in.val()), isPrgWaitingConfirmStatus: '(isnull($x, 0) + 1)' }
                        }, {
                            success: function (o1, ot1, r1) {
                                if (r1.Records == "Không có quyền truy cập dữ liệu") {
                                    showError("Bạn không còn quyền truy cập dữ liệu ở phiên giao dịch hiện tại. </br> Vui lòng đăng nhập lại (2s sau trình duyệt tự chuyển).");
                                    setTimeout(function () { window.location = '../default.aspx'; }, 2000);
                                    return false;
                                }
                                if (r1.Result != "OK" || r1.Records == "-1") { $in.val(""); showError("Bạn kết phiếu thất bại."); }
                                else { $c.html(parseInt($c.html()) + 1); }
                            }
                        });
                    }
                });
            } else {
                showError("Lỗi hệ thống.");
            }
        }
    });
};