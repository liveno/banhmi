﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Tao DataBase Tu Dong</title>
    <link href="../UserControl/style_home.css" rel="stylesheet" type="text/css" />
    <link href="../UserControl/mailisa.css" rel="stylesheet" type="text/css" />
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services >
            <asp:ServiceReference  Path="../WSASMXGobal/zgc0GobalService.asmx" />
     </Services> 
    </asp:ScriptManager>
    
   
        <script type="text/javascript">
            function btnEditTable_onclick(id) {
                var ids = document.getElementById(id);
                var r = Math.random();
                var oWnd = radopen("diag_EditTable.aspx?_id="+id, "EditCreateString" ,"width=400px, height=400px" );          
                oWnd.SetSize(600,550);   
                oWnd.Center();   
            }
            
            function btnEditColumn_onclick(id) {
                var ids = document.getElementById(id);
                var r = Math.random();
                var oWnd = radopen("diag_EditColumn.aspx?_id="+id, "EditColumn" ,"width=450px, height=400px" );          
                oWnd.SetSize(700,550);   
                oWnd.Center();   
            }
            function btnConfigDb_Click()
            {
                zgc0GobalService.btnConfigDb();
            }
            function FuncDB()
            {
                zgc0GobalService.FuncDB();
            }
            function ObjectCSharp()
            {
                //window.open('default.aspx?ObjectCSharp=1','','','');
                zgc0GobalService.ObjectCSharp();
            }
            function ViewCSharp()
            {
                //window.open('default.aspx?CreateView=1','','','');
                zgc0GobalService.ViewCSharp();
            }
            function BuildAll() {
                $get("<%= E5.ClientID %>").value = 'Building....';
                zgc0GobalService.BuildAll(AfterBuildAll);
            }
            function AfterBuildAll(resutl) {
                $get("<%= E5.ClientID %>").value = resutl;
            }
            function Form()
            {
                $get("<%= E5.ClientID %>").value = 'Building....';
                zgc0GobalService.BuildForm(AfterBuildAll);
            }
            function Repair()
            {
//                window.open('default.aspx?Repair=1','','','');
                zgc0GobalService.Repair();
            }
            function BuildCore() {
                $get("<%= E5.ClientID %>").value = 'Building....';
                zgc0GobalService.BuildCore(AfterBuildAll);
            }
            function BuildCoreJS()
            {
                $get("<%= E5.ClientID %>").value = 'Building....';
                zgc0GobalService.BuildCoreJS(AfterBuildAll);
            }
            function SendSMS()
            {
                zgc0GobalService.SendSMS();
            }
            
        </script>
  
  
    <div >
      
       
        
        <div style="height:5px;"></div>
        
        <div id="divFormStore" visible="false" runat="server" style="border-color:Blue;border-style:dashed;border-width:thin;width:800px;padding:10px">
            <div style="width: 750px;font-size:medium;color:Blue;padding-left:10px;text-align:right;padding-right:5">
                Tên bảng: &nbsp; &nbsp;&nbsp;&nbsp;<b>tbl_Store_</b>
               
            </div>
            
        </div>
        
        <div style="border-bottom:solid 1px #ccc;"></div>
         <div style="padding:10px;">
            <div style='float:left; margin-left:10px;'> <input id='BtnUpdateP2' runat='server'  class='gcBtn' type='button' onclick='FuncDB();'  value='Tạo view/func Db' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button1' runat='server'  class='gcBtn' type='button' onclick='ObjectCSharp();'  value='Tạo obj C#/js/ws' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button2' runat='server'  class='gcBtn' type='button' onclick='ViewCSharp();'  value='Tạo view C#' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button3' runat='server'  class='gcBtn' type='button' onclick='BuildAll();'  value='Build All' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button4' runat='server'  class='gcBtn' type='button' onclick='Form();'  value='Tạo form' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button6' runat='server'  class='gcBtn' type='button' onclick='Repair();'  value='Tu chỉnh DB' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button7' runat='server'  class='gcBtn' type='button' onclick='BuildCore();'  value='Build Core' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button8' runat='server'  class='gcBtn' type='button' onclick='BuildCoreJS();'  value='Build Core JS' /></div>
            <div style='float:left; margin-left:10px;'> <input id='Button9' runat='server'  class='gcBtn' type='button' onclick='SendSMS();'  value='Send SMS' /></div>
            <asp:Button ID="testWebsite" runat="server" OnClick="TestWebsite" Text="Test Website" />
         </div>
         <div class='gcClear'></div>
         <div style="border-bottom:solid 1px #ccc;padding:3px;"></div>
         
        <div style="padding-top:10px">
            <asp:TextBox runat="server" ID="T1" Text="Test"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="T2" Text="333"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="T3" Text="4444"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="T4" Text="33333"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="E1" Text="6666"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="E2" Text="66666"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="E3" Text="7777"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="E4" Text="77770"  Width="300px" Height="30px"></asp:TextBox>
            <asp:Button runat="server" ID="btnTestKey" OnClick="onClickBtnTest" Text="Test Button"/>
        </div><br />
        
        <div style="padding-top:10px">
          
            <asp:TextBox runat="server" ID="U1" Text="7777"  Width="300px" Height="30px"></asp:TextBox>
            <asp:TextBox runat="server" ID="UN" Text="77770"  Width="300px" Height="30px"></asp:TextBox>
            <asp:Button runat="server" ID="Button5" OnClick="onClickGM" Text="Giai Ma"/>
        </div><br />

      
        <br />
            <asp:TextBox runat="server" ID="E5" Text="7777" spellcheck="false" TextMode="MultiLine" Rows="70" Width="917px" 
            Height="328px"></asp:TextBox>
            <br />
    </div>

    <div id='InforNews' runat="server" style="padding:20px;" class="InforNews">
    </div>
    </form>
</body>
</html>
