﻿
var phieu = {//gcGobal_INCOM_Receipt 
    _v: ['gcGobal_INCOM_Receipt', 'Id', 'Mã chứng từ ', 'Số CT', 'Mã số HĐ ', 'Ngày lập ', 'Chi nhánh ', 'Nhóm khách hàng ', 'Khách hàng ', 'T.Tiền', 'T.Thu', 'Nợ lại ', 'K.Hàng', 'Tiền sách ', 'P.Thu', 'Người thu ', 'Ngày giao hàng ', 'Thanh toán hết ', 'K.Mãi', 'Trạng thái ', 'isPrinted', 'Bank ', 'Card ', 'Danh mục thu ', 'Lớp học ', 'Hole ', 'Hole ', 'K.Hàng ', 'Hole ', 'Hole ', 'Hole ', 'Hole ', 'Hole ', 'isPrgAccountId', 'isPrgInUse', 'isPrgCreateDate', 'isPrgWaitingConfirmStatus', 'isPrgbAdminDeleted', 'isPrgbUserDeleted', 'isPrgbShow', 'isPrgOrdered', 'isPrgVNKoDau', 'isPrgSmField', 'isPrgPartComp', 'isPrgEncriptData', 'isPrgDescriptData', 'isPrgAccountUpdateId', ],
    _e: ['gcGobal_INCOM_Receipt', 'Id', 'MaCT', 'SoCT', 'MSHDId', 'NgayLap', 'BranchId', 'LoaiKhachId', 'KHId', 'TongTien', 'ThucThu', 'NoLai', 'DienGiai', 'VAT', 'PhuThu', 'NhanVienId', 'NgayHen', 'isFinished', 'DiscountCust', 'StatusId', 'isPrinted', 'BankReceiptId', 'CardPaymentId', 'OrderId', 'ClassId', 'Keep01', 'Keep02', 'Keep03', 'Keep04', 'Space01', 'Space02', 'Space03', 'SpaceId', 'isPrgAccountId', 'isPrgInUse', 'isPrgCreateDate', 'isPrgWaitingConfirmStatus', 'isPrgbAdminDeleted', 'isPrgbUserDeleted', 'isPrgbShow', 'isPrgOrdered', 'isPrgVNKoDau', 'isPrgSmField', 'isPrgPartComp', 'isPrgEncriptData', 'isPrgDescriptData', 'isPrgAccountUpdateId', ],
    _d: ["00,00,0,1,0,,,,", "01,01,0,4,0,,,,|02,02,0,4,0,,,,|03,03,0,1,1,gcGobal_INCOM_Contract,Name,47,", "04,04,0,5,0,,,,|05,05,0,1,1,gcGobal_COMP_Branch,NAME,48,|07,07,0,1,1,gcGobal_CUST_Customer,HoTen,50,|14,14,0,1,1,gcGobal_COMP_EmployeeLife,HoTen,51,", "06,06,0,1,1,gcGobal_CUST_CustomerType,Name,49,", "26,26,1,4,0,,,,", "11,11,0,4,0,,,,", "08,08,3,2,0,,,,|13,13,1,2,0,,,,|17,17,1,2,0,,,,|09,09,3,2,0,,,,|10,10,0,2,0,,,,", "12,12,0,2,0,,,,|20,20,0,1,0,,,,|21,21,0,1,0,,,,|23,23,0,1,1,gcGobal_TRAINNING_COURSE_CLASS,Name,54,", "15,15,0,5,0,,,,|18,18,0,1,1,gcGobal_INCOM_Status,Name,52,|22,22,0,1,1,gcGobal_LITERAL_DANHMUC_THU,Name,53,", "16,16,0,9,0,,,,", "19,19,0,1,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,2,0,,,,", "29,29,0,4,0,,,,", "30,30,0,5,0,,,,", "31,31,0,1,0,,,,", "32,32,0,1,0,,,,", "33,33,0,1,0,,,,", "34,34,0,5,0,,,,", "35,35,0,1,0,,,,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "38,38,0,1,0,,,,", "39,39,0,1,0,,,,", "40,40,0,4,0,,,,", "41,41,0,4,0,,,,", "42,42,0,4,0,,,,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "45,45,0,1,0,,,,", ],
    _f: [], _r: [], _coj: {}, __id: 73, _sy: 'dlg_cl', id: 'phieu', cssb: 'inputBtnBottomV2', $dw: null, dwcss: 'login_sign'
};
var phieuDetail = {//gcGobal_INCOM_Receipt_Detail 
    _v: ['gcGobal_INCOM_Receipt_Detail', 'Id', 'Phiếu thu tiền mặt ', 'Kho hàng  ', 'V.Tư ', 'SL', 'Số con/số cái ', 'Đ.Giá', 'ĐVT ', 'T.Tiền ', 'Nhân viên bán SP ', 'Hoa hồng NV ', 'Chiết khấu ', 'TraLai ', 'GiaMua ', 'Khuyến mãi', 'Phụ thu', 'Hoa hồng ', 'Hole ', 'Thêm vào ', 'Hole ', 'Hole ', 'Hole ', 'Hole ', 'isPrgAccountId', 'isPrgInUse', 'isPrgCreateDate', 'isPrgWaitingConfirmStatus', 'isPrgbAdminDeleted', 'isPrgbUserDeleted', 'isPrgbShow', 'isPrgOrdered', 'isPrgVNKoDau', 'isPrgSmField', 'isPrgPartComp', 'isPrgEncriptData', 'isPrgDescriptData', 'isPrgAccountUpdateId', ],
    _e: ['gcGobal_INCOM_Receipt_Detail', 'Id', 'PhieuThuTienMatId', 'KhoId', 'VatTuId', 'SoLuong', 'SoConSoCai', 'DonGia', 'DonViTinhId', 'ThanhTien', 'NhanVienBanSPId', 'ChietKhauNV', 'ChietKhauNVTV', 'TraLai', 'GiaMua', 'KhuyenMaiBSP', 'PhuThu', 'ChietKhauNgMoiGioi', 'PrintSLInfo', 'AddInfor', 'Space01', 'Space02', 'Space03', 'SpaceId', 'isPrgAccountId', 'isPrgInUse', 'isPrgCreateDate', 'isPrgWaitingConfirmStatus', 'isPrgbAdminDeleted', 'isPrgbUserDeleted', 'isPrgbShow', 'isPrgOrdered', 'isPrgVNKoDau', 'isPrgSmField', 'isPrgPartComp', 'isPrgEncriptData', 'isPrgDescriptData', 'isPrgAccountUpdateId', ],
    _d: ["00,00,0,1,0,,,,", "01,01,0,1,1,gcGobal_INCOM_Receipt,MaCT,38,", "02,02,0,1,1,gcGobal_STOCK_List,Name,39,|16,16,0,2,0,,,,", "03,03,1,1,1,gcGobal_STOCK_gcProductList,Name,40,|07,07,1,1,1,gcGobal_LITERAL_Unit,Name,41,|06,06,1,2,0,,,,|04,04,1,2,0,,,,|05,05,0,2,0,,,,", "08,08,1,2,0,,,,", "09,09,0,1,1,gcGobal_COMP_EmployeeLife,HoTen,42,|10,10,0,2,0,,,,|14,14,0,2,0,,,,|15,15,0,2,0,,,,", "11,11,0,2,0,,,,", "12,12,0,2,0,,,,", "13,13,0,2,0,,,,", "17,17,0,4,0,,,,", "18,18,0,4,0,,,,", "19,19,0,2,0,,,,", "20,20,0,4,0,,,,", "21,21,0,5,0,,,,", "22,22,0,1,0,,,,", "23,23,0,1,0,,,,", "24,24,0,1,0,,,,", "25,25,0,5,0,,,,", "26,26,0,1,0,,,,", "27,27,0,1,0,,,,", "28,28,0,1,0,,,,", "29,29,0,1,0,,,,", "30,30,0,1,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,4,0,,,,", "34,34,0,1,0,,,,", "35,35,0,1,0,,,,", "36,36,0,1,0,,,,", ],
    _f: [], _r: [], _coj: {}, __id: 74, _sy: 'dlg_cl', id: 'phieuDetail', cssb: 'inputBtnBottomV2', $dw: null, dwcss: 'login_sign'
};




var listlapPhieu = {
    title: 'Danh sách phiếu nhập',
    jElementTit: "#gc_gbHTitle",
    /* element, attribute, element1, attribute1, class1, id1, label, element2, attribute2, class2, id2, typedata,col, default */
    /* typeData [int: 1; float: 2; string: 4; date: 5; bool: 3, 6: money] */
    data: [
        ['h4', '', '', '', '', '', '', '', '', '', '', 4, 3, '_'],
        ['p', 'style="min-height: 80px"', 'span', '', '', '', 'K.Hàng: ', 'span', 'style="float: right"', '', '', 4, 27, '_'],
        ['p', '', 'span', '', '', '', 'T.Tiền: ', 'span', 'style="float: right"', '', '', 6, 9, '_'],
        ['p', '', 'span', '', '', '', 'P.Thu: ', 'span', 'style="float: right"', '', '', 6, 14, '_'],
        ['p', '', 'strong', '', '', '', 'T.Thu: ', 'strong', 'style="color: red;font-weight: bold; float: right;"', '', '', 6, 10, '_'],
        ['p', '', 'span', '', '', '', 'Người lập: ', 'strong', 'style="font-style:italic;color:red; float: right;"', '', '', 4, 41, '_']
    ],
    eventItem: function () {
        var that = this;
        /************************************************************************
        * Xử lý khi click vào Phiếu nhập kho                                    *
        *************************************************************************/
    },
    button: [['In phiếu', 'ItemPrintBill']],
    CapNhatPhuThuKMForm: function (o, key, index) {
        cur_gb_ReceitpID = cur_gb_cur_ListDataShow[index][1];
        gb_strFilterGrid = ' id = ' + cur_gb_ReceitpID;
        cur_gb_ElementId = 'phieu';
        _gb.pr(phieu).ff(true, phieu);
        _gb.rf(phieu);
        $('#phieuc_').hide();
        $("#phieui_").hide();
        $('#phieu_f').css({ 'margin-top': '8px' });
        $('#fphieu07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieu07i').val() == '') $('#fphieu07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fphieu07i").click(function () { $("#fphieu07i").select(); });
        $("#fphieu07i").keyup(function (ev) { formatTien(this); });
        cur_gb_ItemSelect = cur_gb_cur_ListDataShow[index];
        if (cur_gb_ItemSelect[4] < XDate.today() && cur_gb_GroupRight != 1) {
            $('#phieuu_').hide();
        }
        gb_curBoxDlg = $(o).colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#phieu", title: "Thông tin cập nhật" });
    },
    ItemPrintBill: function (o, key, index) {
        cur_gb_ReceitpID = cur_gb_cur_ListDataShow[index][1];
        var obj = new Object();
        obj.Name = 'Testing';
        obj.Id = cur_gb_ReceitpID;
        showReport(obj, 'Receipt', 'ActiveCurrentBill', 'PrintPhieuThu', "../zgcReport/ReportReceipt2.ashx");
    },
    showPopup: function ($that) {
        gb_strFilterGrid = " PhieuNhapKhoId=" + cur_gb_ReceitpID;
        _gb.pr(phieuDetail).ff(true, phieuDetail);
        _gb.rf(phieuDetail);
        $('#phieuDetail_f').css({ 'margin-top': '8px' });
        $('#phieuDetailc_').hide();
        $('#fphieuDetail02i').removeClass("brwidthinput200").css("width", "40%");
        $('#fphieuDetail01h').val(cur_gb_ReceitpID);
        $('#fphieuDetail03i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieuDetail03i').val() == '') $('#fphieuDetail03i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val(1);
        $("#fphieuDetail03i").click(function () { $("#fphieuDetail03i").select(); });
        $('#fphieuDetail06i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieuDetail06i').val() == '') $('#fphieuDetail06i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fphieuDetail06i").click(function () { $("#fphieuDetail06i").select(); });
        $('#fphieuDetail07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieuDetail07i').val() == '') $('#fphieuDetail07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fphieuDetail07i").click(function () { $("#fphieuDetail07i").select(); });
        autoFare($("#fphieuDetail06i"), $("#fphieuDetail03i"), $("#fphieuDetail07i"));
        gb_curBoxDlg = $that.colorbox({ inline: true, innerWidth: "96%", innerHeight: "90%", href: "#phieuDetail", title: "Cập nhật phiếu" });
    }
};



/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
* Các hàm ready                                                         *
_______________________________________________________________________*/
$(document).ready(function () {
    createDSHang();
    $('#nhanVienIdi').click(function (ev) {
        test('nhanVienIdi', ev, '', 'gcGobal_COMP_EmployeeLife', 'HoTen');
        $('#nhanVienIdi').select();
    });
    $('#nhanVienIdi').keyup(function (ev) {
        test('nhanVienIdi', ev, '', 'gcGobal_COMP_EmployeeLife', 'HoTen');
    });
    $('#hangHoaIdi').click(function (ev) {
        test('hangHoaIdi', ev, '', 'gcGobal_STOCK_gcProductList', 'Name~Code');
        $('#hangHoaIdi').select();
    });
    $('#hangHoaIdi').keyup(function (ev) {
        test('hangHoaIdi', ev, '', 'gcGobal_STOCK_gcProductList', 'Name~Code');
    });
    $('#khoIdi').click(function (ev) {
        test('khoIdi', ev, '', 'gcGobal_STOCK_List', 'Name~Code-0');
        $('#khoIdi').select();
    });
    $('#khoIdi').keyup(function (ev) {
        test('khoIdi', ev, '', 'gcGobal_STOCK_List', 'Name~Code-0');
    });
    $("#InBillBtn").click(function (ev) {
        if ($("#typeID option:selected").val() == 'LocHoaDon') {
            $("#LocHoaDon").click();
        }
        else if ($("#typeID option:selected").val() == 'btnNewKiemKho') {
            $("#btnNewKiemKho").click();
        }
        else if ($("#typeID option:selected").val() == 'TinhLaiKho') {
            $("#TinhLaiKho").click();
        }
        else if ($("#typeID option:selected").val() == 'DSTrongNgay') {
            $("#DSTrongNgay").click();
        }
        else if ($("#typeID option:selected").val() == 'DSChiTietTrongNgay') {
            $("#DSChiTietTrongNgay").click();
        }
    });
    $("#lapPhieu").click(function () {
        $("#rowLapPhieu").show();
        $("#rowDSPhieuDatHang").hide();
    });
    $("#dsPhieuDatHang").click(function () {
        $("#rowLapPhieu").hide();
        $("#rowDSPhieuDatHang").show();
        modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
        request({
            a: 'pGetgcGobal_INCOM_Receipt',
            c: '', d: '', type: 'p', cl: '*', si: 1, mr: 20, se: ' Id desc', f: " (isPrgWaitingConfirmStatus = 2) AND " +
            "(isPrgbUserDeleted is null or isPrgbUserDeleted = 0) and (isPrgOrdered is null OR isPrgOrdered = 0) "
        }, { success: function (o, op, r) { _gbc.it(r, $('#itemDSPhieuDatHang'), listlapPhieu, o); } });
    });
    /************************************************************************
    * Xử lý khi click vào thẻ tab ghi thu phiếu                             *
    *************************************************************************/
    $("#phieuThuDetail").click(function () {
        gb_strFilterGrid = "";
        gb_strFilter = '';
        gb_strSort = " ";
        var dateNow = new Date();
        //        var fTime = dateNow < Date.parse(XDate.today().toString("MM/dd/yyyy") + " 05:30")
        //                    dateNow >= Date.parse(XDate.today().toString("MM/dd/yyyy") + " 05:30")
        //                    && dateNow < Date.parse(XDate.today().toString("MM/dd/yyyy") + " 17:30");
        modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
        request({
            a: 'pGetgcGobal_INCOM_Receipt',
            c: '', d: '', type: 'p', cl: '*', si: 1, mr: 1, se: ' Id ', t: 'PT',
            f: "(isPrgOrdered is null OR isPrgOrdered = 0) and isPrgWaitingConfirmStatus = 1 AND ( NgayLap>=N'"
             + XDate.today().toString("MM/dd/yyyy") + " 03:00' and NgayLap<=N'"
             + XDate.today().toString("MM/dd/yyyy") + " 23:59' ) AND ( isPrgAccountId=" + cur_gb_AccountId + " )"
             + " AND ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1)) "
        }, { success: function (o, op, r) { createPhieuThu(o, op, r, $("#tab_2-2 .row .col-md-12")); } });
    });
    /************************************************************************
    * Xử lý khi click vào thẻ tab ghi thu phiếu                             *
    *************************************************************************/
    $("#phieuThu").click(function () {
        modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
        request({
            a: 'pGetgcGobal_INCOM_Receipt',
            c: '', d: '', type: 'p', cl: '*', si: 1, mr: 1, se: ' Id ',
            f: "(isPrgOrdered is null OR isPrgOrdered = 0) and isPrgWaitingConfirmStatus = 1 AND ( NgayLap>=N'"
             + XDate.today().toString("MM/dd/yyyy") + " 03:00' and NgayLap<=N'"
             + XDate.today().toString("MM/dd/yyyy") + " 23:59' ) AND ( isPrgAccountId=" + cur_gb_AccountId + " )"
             + " AND ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1)) "
        }, { success: function (o, op, r) {
            gb_strFilterGrid = " Id=" + cur_gb_ReceitpID;
            _gb.pr(phieu).ff(true, phieu);
            _gb.rf(phieu);
            $('#phieuc_').hide();
            $('#phieui_').hide();
            //$("#phieu .jtable-main-container").attr("style", "font-size: 25px!important");
            //$("#phieu .login_sign").attr("style", "font-size: 23px!important");
            //$("#phieu .login_sign").hide();
            $("#tab_1-1 .row .col-md-12").prepend($("#phieu")).show();
            $("#phieu").css({ "width": "100%" });
        }
        });
    });
    /************************************************************************
    * Xử lý khi click vào thẻ tab ghi thu phiếu                             *
    *************************************************************************/
    $("#printBill").click(function () {
        var obj = new Object();
        obj.Name = 'Testing';
        obj.Id = cur_gb_ReceitpID;
        showReport(obj, 'Receipt', 'ActiveCurrentBill', 'PrintPhieuThu', "../zgcReport/ReportReceipt1.ashx", null,
            { success: function (o, op, $el) { $("#gc_btnPrintForm").click(); } });
    });
    $("#phieuDetailConfirm").click(function () {
        request({
            a: 'UpdategcGobal_INCOM_Receipt',
            c: { Id: cur_gb_ReceitpID },
            d: { isPrgWaitingConfirmStatus: 2 }
        }, {
            success: function (o, ot, r) {
                $("#phieuThuDetail").click();
            }
        });
    });
});
/************************************************************************
* Xử lý khi popup được load xong                                        *
*************************************************************************/
$(document).bind('cbox_complete', function () {
    if (cur_gb_ElementId == 'phieu') {
        var $form1 = $('#phieu');
        var $form2 = $('#phieuDetail');
        var $divtab = $("<div class='br-phanloai'></div>");
        var $divtab1 = $("<ul class='nav nav-tabs'> </ul>");
        var $div1 = $("<li class='" + ($form1.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Thông tin chung</a></li>");
        var $div2 = $("<li class='" + ($form2.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Chi tiết</a></li>");
        $divtab.append($divtab1);
        $divtab1.append($div1);
        $divtab1.append($div2);
        if ($form1.length > 0) {
            form2Click($div2, $form2.selector.split('#')[1]);
            $form1.prepend($divtab);
        }
        else if ($form2.length > 0) {
            form1Click($div1, $form1.selector.split('#')[1]);
            $form2.prepend($divtab);
        }
    }
});
/************************************************************************
* Xử lý khi thay đổi giá, sl thì thành tiền thay đổi                    *
*************************************************************************/
function autoFare($donGia, $soLuong, $thanhTien) {
    $donGia.keyup(function () {
        formatTien(this);
        if ($soLuong.val().length > 0 && $donGia.val().length) {
            var productSl = gcRev($soLuong.val(), ',');
            var productDg = gcRev($donGia.val(), ','); ;
            var productTt = parseFloat(productSl) * parseFloat(productDg);
            if (parseFloat(productSl) < 10000)
                $thanhTien.val(gcFormatStr(productTt.toString()));
        }
    });
    $soLuong.keyup(function (e) {
        formatTien(this);
        if ($soLuong.val().length > 0 && $donGia.val().length) {
            var productSl = gcRev($soLuong.val(), ',');
            var productDg = gcRev($donGia.val(), ','); ;
            var productTt = parseFloat(productSl) * parseFloat(productDg);
            if (parseFloat(productSl) < 10000)
                $thanhTien.val(gcFormatStr(productTt.toString()));
        }
    });
};
/************************************************************************
* Xử lý khi nhấn nút form phụ                                         *
*************************************************************************/
function form2Click($el, el) {
    $el.click(function () {
        cur_gb_ElementId = 'phieu';
        cur_gb_KeyObj = cur_gb_ReceitpID;
        gb_strFilterGrid = " PhieuNhapKhoId = " + cur_gb_ReceitpID;
        _gb.pr(window[el]).ff(true, window[el]);
        _gb.rf(window[el]);
        $('#phieuDetailc_').hide();
        $('#phieuDetail_f').css({ 'margin-top': '8px' });
        $('#fphieuDetail02i').removeClass("brwidthinput200").css("width", "40%");
        $('#fphieuDetail01h').val(cur_gb_ReceitpID);
        $('#fphieuDetail03i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieuDetail03i').val() == '') $('#fphieuDetail03i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val(1);
        $("#fphieuDetail03i").click(function () { $("#fphieuDetail03i").select(); });
        $('#fphieuDetail06i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieuDetail06i').val() == '') $('#fphieuDetail06i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fphieuDetail06i").click(function () { $("#fphieuDetail06i").select(); });
        $('#fphieuDetail07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieuDetail07i').val() == '') $('#fphieuDetail07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fphieuDetail07i").click(function () { $("#fphieuDetail07i").select(); });
        autoFare($("#fphieuDetail06i"), $("#fphieuDetail03i"), $("#fphieuDetail07i"));
        if (cur_gb_ItemSelect[4] < XDate.today() && cur_gb_GroupRight != 1) {
            $('#phieuDetailu_').hide();
            $('#phieuDetaili_').hide();
        }
        gb_curBoxDlg = $(this).colorbox({ inline: true, innerWidth: "96%", innerHeight: "90%", href: '#' + el + '', title: "Cập nhật phiếu" });
    });
    //autoFare($('#f' + el + '06i'), $('#f' + el + '03i'), $('#f' + el + '07i'));
};
/************************************************************************
* Xử lý khi nhấn nút form chính                                         *
*************************************************************************/
function form1Click($el, el) {
    $el.click(function () {
        cur_gb_ElementId = 'phieu';
        gb_strFilterGrid = ' id = ' + cur_gb_ReceitpID;
        cur_gb_KeyObj = cur_gb_ReceitpID;
        _gb.pr(window[el]).ff(true, window[el]);
        _gb.rf(window[el]);
        $('#' + el + 'c_').hide();
        $("#" + el + "i_").hide();
        $('#' + el + '_f').css({ 'margin-top': '8px' });
        $('#fphieu07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if ($('#fphieu07i').val() == '') $('#fphieu07i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fphieu07i").click(function () { $("#fphieu07i").select(); });
        $("#fphieu07i").keyup(function (ev) { formatTien(this); });
        if (cur_gb_ItemSelect[4] < XDate.today() && cur_gb_GroupRight != 1) {
            $('#phieuu_').hide();
        }
        gb_curBoxDlg = $(this).colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: '#' + el + '', title: "Thông tin cập nhật" });
    });
};
/************************************************************************
* Xử lý khi nhấn in hóa đơn (trong rp in phiếu)                         *
*************************************************************************/
function PrintPhieuThu() {
    request({
        a: 'UpdategcGobal_INCOM_Receipt',
        c: { Id: cur_gb_ReceitpID },
        d: { isPrgOrdered: 1 }
    }, {
        success: function (o, ot, r) {
            GobalPrintNew('gc_DivReceiptContent');
            $("#dsPhieuDatHang").click();
            modalcenter.close();
        }
    });
};
function tinhLaiKho(title, stW, kCode) {
    modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $("#gc_gbHTitle").html(title);
    request({
        a: 'pGetgcGobal_STOCK_gcStock_' + kCode + '_Today', c: '', d: '', type: 'p', cl: '*', si: 1, mr: 10000, se: 'ProductIdName', f: stW
    }, {
        success: function (o, op, r) {
            modal.close();
            if (r.Result == "OK") {
                var $el = $('<table style="margin: 0 auto; width: 120%"/>').addClass('table');
                $el.append("<thead><tr><th>STT</th><th>Ngày</th><th>Loại</th><th>Tên hàng</th><th>ĐVT</th><th>Giá mua</th><th>SL cuối ngày</th><th>SL thực tế</th><th>TV</th></tr></thead>");
                var $tbody = $('<tbody />');
                for (var i = 0; i < r.Records.length; i++) {
                    $tbody.append('<tr data-id="' + r.Records[i][1] + '"><td>' + (i + 1) + '</td>' +
                                            '<td>' + (r.Records[i][2] == null ? '' : _parseDate(r.Records[i][2] + "").format("dd/MM/yyyy")) + '</td>' +
                                            '<td>' + (r.Records[i][33] == null ? '' : r.Records[i][33]) + '</td>' +
                                            '<td>' + (r.Records[i][32] == null ? '' : r.Records[i][32]) + '</td>' +
                                            '<td>' + (r.Records[i][34] == null ? '' : r.Records[i][34]) + '</td>' +
                                            '<td>' + (r.Records[i][11] == null ? '' : c(r.Records[i][11])) + '</td>' +
                                            '<td>' + (r.Records[i][18] == null ? '' : c(r.Records[i][18])) + '</td>' +
                                            '<td><input type="text" autocomplete="off" class=" finishPT" value="' +
                                                (r.Records[i][21] == null ? '' : c(r.Records[i][18] + r.Records[i][21])) + '"></td>' +
                                            '<td><input type="button" class="btn btn-warning btn-sm kpThu" value="XN" data-id="' + r.Records[i][1] + '"></td>' +
                                            '</tr>');
                }
                $('#gc_gobal_ListDataID').html('');
                $('#gc_gobal_ListDataID').append($el.append($tbody));
                $el.find(".kpThu").click(function () {
                    var $t = $(this);
                    var $tr = $(this).closest("tr");
                    var $in = $tr.find("td:nth-child(8) input");
                    var $c = $tr.find("td:nth-child(14)");
                    var id = $t.attr("data-id");
                    if (!$.isNumeric(gcRev($in.val(), ','))) {
                        showError("Bạn chưa nhập SL chênh lệch.");
                    } else {
                        modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
                        request({
                            a: 'UpdategcGobal_STOCK_gcStock_' + kCode + '_Today',
                            c: { Id: id },
                            d: { Miss: "case when $x is null then 0 else 0 end + (" + $in.val().ra(',', '') + " - ed)",
                                mMiss: "case when $x is null then 0 else 0 end + (isnull(inv,0)* (" + $in.val().ra(',', '') + " - ed))",
                                Missv: "case when $x is null then 0 else 0 end + isnull(inv,0) "
                            }
                        }, {
                            success: function (o1, ot1, r1) {
                                modal.close();
                                if (r1.Records == "Không có quyền truy cập dữ liệu") {
                                    showError("Bạn không còn quyền truy cập dữ liệu ở phiên giao dịch hiện tại. </br> Vui lòng đăng nhập lại (2s sau trình duyệt tự chuyển).");
                                    setTimeout(function () { window.location = '../default.aspx'; }, 2000);
                                    return false;
                                }
                                if (r1.Result != "OK" || r1.Records == "-1") { $in.val(""); showError("Bạn xác nhận thất bại."); }
                                else { }
                            }
                        });
                    }
                });
            } else {
                showError("Lỗi hệ thống.");
            }
        }
    });
};
function createDSHang() {
    var $tab1 = $("#tab_1 .row");
    var $tab2 = $("#tab_2 .row");
    var $tab3 = $("#tab_3 .row");
    var $tab4 = $("#tab_4 .row");
    var $tab5 = $("#tab_5 .row");
    var product =
        "<div class=\"color-palette-set\" data-id=\"|0|\" data-donvitinhid=\"|1|\" data-giaban=\"|5|\"> " +
        "   <div class=\"bg-item-name bg-item-name-box\"> " +
        "       <div class=\"bg-item-index bg-item-index-box\"> " +
        "           |4|</div> " +
        "       <div class=\"bg-item-name-text\"> " +
        "           |3|</div> " +
        "   </div> " +
        "   <div class=\"bg-item-fare bg-item-fare-box\"> " +
        "       |2|K " +
        "   </div> " +
        "</div> ";
    $tab1.empty();
    for (var i = 0; i < diemTamSang.length; i++) {
        var item = new String(product).f2(diemTamSang[i][0], diemTamSang[i][2], c(diemTamSang[i][3] / 1000.0), diemTamSang[i][1]/*.toUpperCase()*/, i + 1, diemTamSang[i][3]);
        var $item = $("<div />").addClass("col-sm-3 col-md-3 col-lg-2 item").append(item);
        $tab1.append($item);
    }
    $tab2.empty();
    for (var i = 0; i < comTruaChieu.length; i++) {
        var item = new String(product).f2(comTruaChieu[i][0], comTruaChieu[i][2], c(comTruaChieu[i][3] / 1000.0), comTruaChieu[i][1]/*.toUpperCase()*/, i + 1, comTruaChieu[i][3]);
        var $item = $("<div />").addClass("col-sm-3 col-md-3 col-lg-2 item").append(item);
        $tab2.append($item);
    }
    $tab3.empty();
    for (var i = 0; i < nuocLon.length; i++) {
        var item = new String(product).f2(nuocLon[i][0], nuocLon[i][2], c(nuocLon[i][3] / 1000.0), nuocLon[i][1]/*.toUpperCase()*/, i + 1, nuocLon[i][3]);
        var $item = $("<div />").addClass("col-sm-3 col-md-3 col-lg-2 item").append(item);
        $tab3.append($item);
    }
    $tab4.empty();
    for (var i = 0; i < nuocChai.length; i++) {
        var item = new String(product).f2(nuocChai[i][0], nuocChai[i][2], c(nuocChai[i][3] / 1000.0), nuocChai[i][1]/*.toUpperCase()*/, i + 1, nuocChai[i][3]);
        var $item = $("<div />").addClass("col-sm-3 col-md-3 col-lg-2 item").append(item);
        $tab4.append($item);
    }
    $tab5.empty();
    for (var i = 0; i < cafeSinhTo.length; i++) {
        var item = new String(product).f2(cafeSinhTo[i][0], cafeSinhTo[i][2], c(cafeSinhTo[i][3] / 1000.0), cafeSinhTo[i][1]/*.toUpperCase()*/, i + 1, cafeSinhTo[i][3]);
        var $item = $("<div />").addClass("col-sm-3 col-md-3 col-lg-2 item").append(item);
        $tab5.append($item);
    }
    $("#tabContentLeft").find(".item").click(function () {
        var $itemInfo = $(this).find(".color-palette-set");
        $("#fphieuDetail03h").val($itemInfo.attr("data-id"));
        $("#fphieuDetail04i").val(1);
        $("#fphieuDetail06i").val($itemInfo.attr("data-giaban"));
        $("#fphieuDetail07h").val($itemInfo.attr("data-donvitinhid"));
        $("#fphieuDetail08i").val($itemInfo.attr("data-giaban"));
        $("#phieuDetaili_").click();
    });
};
function createPhieuThu(o, op, r, $el) {
    cur_gb_ReceitpID = r.Records[0][1];
    if (cur_gb_ReceitpID == "-1") {
        request({
            a: 'InsertgcGobal_INCOM_Receipt',
            d: {
                MaCT: r.Records[0][2],
                SoCT: "($x select 'PT' + convert(nvarchar, right(100000000+isnull(max(replace(SoCT, 'PT', '')), 0) + 1, 8)) from gcGobal_INCOM_Receipt where SoCT like 'PT%')",
                NgayLap: dateToString(new Date(), 'iso'),
                TongTien: 0,
                ThucThu: 0,
                PhuThu: 10,
                isPrgWaitingConfirmStatus: 1,
                isPrgOrdered: 0,
                isPrgAccountId: cur_gb_AccountId,
                isPrgVNKoDau: cur_gb_Account,
                isPrgCreateDate: dateToString(new Date(), 'iso'),
                isPrgAccountUpdateId: cur_gb_AccountId,
                isPrgSmField: (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + ' | ' + cur_gb_Account,
                isPrgPartComp: cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId
            }
        }, {
            success: function (obj, opt, r) {
                if (r.Result == 'OK') {
                    cur_gb_ReceitpID = r.Records;
                    gb_strFilterGrid = " PhieuThuTienMatId=" + cur_gb_ReceitpID;
                    $("#fphieuDetail01h").val(cur_gb_ReceitpID);
                } else
                    alert('Hệ thống hiện đang bảo trì mong ban thông cảm!');
            }
        });
    }
    gb_strFilterGrid = " PhieuThuTienMatId=" + cur_gb_ReceitpID;
    //gb_strFilterGrid = " id> 1566329";
    _gb.pr(phieuDetail).ff(true, phieuDetail);
    _gb.rf(phieuDetail);
    $("#phieuDetail .login_sign").hide();
    //$("#phieuDetail .jtable-main-container").attr("style", "font-size: 23px!important");
    $("#fphieuDetail01h").val(cur_gb_ReceitpID);
    $("#fphieuDetail02h").val(cur_gb_KhoHangId);
    $el.prepend($("#phieuDetail")).show();
    $("#phieuDetail").css({ "width": "100%" });
};