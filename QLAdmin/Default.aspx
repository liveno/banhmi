﻿<%@ Page Language="C#" MasterPageFile="../UserControl/Share/Site.Master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Default" %>

<%@ Register Src="~/UserControl/PagerTop.ascx" TagName="PagerTop" TagPrefix="gc_pagertop" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Style" runat="server">
    <style>
        .color-palette
        {
            height: 35px;
            line-height: 35px;
            text-align: center;
        }
        .color-palette-set
        {
            margin-bottom: 15px;
        }
        .color-palette span
        {
            display: none;
            font-size: 12px;
        }
        .color-palette:hover span
        {
            display: block;
        }
        .color-palette-box h4
        {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Doanh thu
                <%--<small>Control panel</small>--%>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Doanh thu</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Main row -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" id="ChiNhanhTong">
                    <div class="box">
                        <div class="box-body">
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row (main row) -->
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12" id="ChiNhanh1">
                    <div class="box">
                        <div class="box-body">
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6 col-sm-12 col-xs-12" id="ChiNhanh2">
                    <div class="box">
                        <div class="box-body">
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row (main row) -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script src="../UserControl/Plugin/hightchart/highcharts.js" type="text/javascript"></script>
    <script src="../UserControl/Plugin/hightchart/exporting.js" type="text/javascript"></script>
    <script src="dashboard.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).load(function () {
            cur_gb_Account = '<%= Session["gcUserName"] %>'; // có thời gian mã hóa tên của user để che lại
            cur_gb_GroupRight = '<%= Session["gcRightGroup"] %>';
            cur_gb_BranchId = '<%= Session["gcBranchId"] %>';
            cur_gb_AccountId = '<%= Session["gcAccountId"] %>';
            cur_gb_MaCanBoId = '<%= Session["gcMaCanBoId"] %>';
            cur_gb_CtyId = '<%= Session["gcCtyId"] %>';
            cur_gb_DepartmentId = '<%= Session["gcDepartmentId"] %>';
            cur_gb_Today = '<%= Session["curday"] %>';
            cur_gb_ElementId = '<%= Session["curday"] %>';
            cur_gb_ChucVuId = '<%= Session["gcChucVuId"] %>';
            cur_gb_KhoHangId = '<%= Session["gcKhoId"] %>';
            cur_gb_cur_ListDataShow = '';

            $("#gc_gobal_PagerTopID").hide();
            //    $('.selectpicker').selectpicker({
            //        'selectedText': 'cat'
            //    });
            setClassAll('FilterReceiptDay');
            $("#NgayBatDauIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy"));
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LeftSideBar" runat="server">
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../UserControl/Plugin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle"
                        alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        Alexander Pierce</p>
                    <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
                </div>
            </div>
            <!-- search form -->
            <div class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..." />
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="<%=((Session["Action"] + "" == "QLAdmin") ? "active" : "") %>"><a href="/QLAdmin/Default.aspx">
                    <i class="fa fa-dashboard"></i><span>Doanh thu</span> </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
</asp:Content>
