﻿/*
* Author: Abdullah A Almsaeed
* Date: 4 Jan 2014
* Description:
*      This is a demo file used only for the main dashboard (default.aspx)
**/
$(function () {
    Highcharts.setOptions({
        lang: {
            decimalPoint: '.',
            thousandsSep: ','
        }
    });
    //main: refhresh every 10seconds
    main();
    setInterval(function () {
        main();
    }, 300 * 1000); //30s
    //-------------------end -----------------------
});


function main() {

    var today = XDate.today().toString("MM/dd/yyyy");
    var last7Day = XDate.today().addDays(-7);
    var last7DayStr = last7Day.toString("MM/dd/yyyy");

    //Chart Last week
    request({
        a: 'GetProcedure',
        c: ["Select NgayLap, ThanhTienCa1CN1, ThanhTienCa2CN1, ThanhTienCN1 " +
                " , ThanhTienCa1CN2, ThanhTienCa2CN2, ThanhTienCN2, ThanhTien " +
                " from XIKE_Report_DoanhThu where NgayLap > '" + last7DayStr + "' " +
                " and NgayLap <= '" + today + "' order by NgayLap "]
    }, { success: function (o, op, r) {
        if (r.Result == "OK") {
            var k = 0;
            var i = 0;
            var dataCa1CN1 = [];
            var dataCa2CN1 = [];
            var dataCN1 = [];
            var dataCa1CN2 = [];
            var dataCa2CN2 = [];
            var dataCN2 = [];
            var data = [];
            var items = [];
            var temp = "";
            var tempDay = XDate.today().addDays(-7);
            for (var j = 0; j < 7; j++) {
                items.push(tempDay.addDays(1).toString("dd/MM/yyyy"));
            }
            tempDay = XDate.today().addDays(-7);
            while (i < 7 && k < r.Records.length) {
                console.log(k);
                if (_parseDate(r.Records[k][0] + "").format("dd/MM/yyyy") == tempDay.addDays(1).toString("dd/MM/yyyy")) {
                    dataCa1CN1.push(r.Records[k][1]);
                    dataCa2CN1.push(r.Records[k][2]);
                    dataCN1.push(r.Records[k][3]);
                    dataCa1CN2.push(r.Records[k][4]);
                    dataCa2CN2.push(r.Records[k][5]);
                    dataCN2.push(r.Records[k][6]);
                    data.push(r.Records[k][7]);
                    k++;
                }
                else {
                    dataCa1CN1.push(0);
                    dataCa2CN1.push(0);
                    dataCN1.push(0);
                    dataCa1CN2.push(0);
                    dataCa2CN2.push(0);
                    dataCN2.push(0);
                    data.push(0);
                }
                i++;
            }
            $('#ChiNhanhTong .box-body').highcharts({
                //chart: {
                 //   type: 'column'
                //},
                title: {
                    text: 'Doanh thu trong 7 ngày'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: items
                },
                yAxis: {
                    min: 0,
                    labels: {
                        format: '{value:,.0f} '
                    },
                    title: {
                        text: 'Tổng tiền (VND)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:,.0f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true
                        }
                    },
					line: {
                        pointPadding: 0.1,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                series: [
                    { name: "Chi nhánh 1", data: dataCN1 },
                    { name: "Chi nhánh 2", data: dataCN2, color: "rgba(255,188,117,1)" },
                    { name: "Tổng", data: data, color: "red" }
                ]
            });
            $('#ChiNhanh1 .box-body').highcharts({
                //chart: {
                //    type: 'column'
                //},
                title: {
                    text: 'Doanh thu chi nhánh 1 trong 7 ngày'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: items
                },
                yAxis: {
                    min: 0,
                    labels: {
                        format: '{value:,.0f} '
                    },
                    title: {
                        text: 'Tổng tiền (VND)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:,.0f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true
                        }
                    },
					line: {
                        pointPadding: 0.1,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                series: [
                    { name: "Ca 1", data: dataCa1CN1 },
                    { name: "Ca 2", data: dataCa2CN1, color: "#90ed7d" }
                ]
            });
            $('#ChiNhanh2 .box-body').highcharts({
                //chart: {
                 //   type: 'column'
                //},
                title: {
                    text: 'Doanh thu chi nhánh 2 trong 7 ngày'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: items
                },
                yAxis: {
                    min: 0,
                    labels: {
                        format: '{value:,.0f} '
                    },
                    title: {
                        text: 'Tổng tiền (VND)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:,.0f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true
                        }
                    },
					line: {
                        pointPadding: 0.1,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: false,
                        }
                    }
                },
                series: [
                    { name: "Ca 1", data: dataCa1CN2 },
                    { name: "Ca 2", data: dataCa2CN2, color: "#90ed7d" }
                ]
            });

        }
    }
    });
}