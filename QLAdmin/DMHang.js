var diemTamSang = 
    [
        [2894, "Bánh bao 12k", 6, 12000],
        [2893, "Bánh bao 15K", 6, 15000],
        [693, "Cháo thịt bằm", 68, 15000],
        [1662, "Cháo bò bằm", 68, 25000],
        [2898, "Bún riêu cua", 68, 17000],
        [2936, "Cơm chiên dương châu", 68, 20000],
        [750, "Bánh cuốn", 68, 20000],
        [1654, "Bánh mì chả", 68, 12000],
        [1678, "Bánh mì thịt ốp la", 68, 15000],
        [1670, "Hủ tíu heo", 68, 22000],
        [1672, "Nui heo", 68, 22000],
        [1671, "Bánh canh heo", 68, 22000],
        [640, "Phở bò", 68, 30000],
	    [638, "Bún bò Huế", 68, 30000]
    ];
var comTruaChieu = 
    [
        [678, "Cơm cá kho 20K", 68, 20000],
        [1681, "Cơm đậu hũ nhồi thịt", 68, 20000],
        [1680, "Cơm cá lóc kho tiêu", 68, 22000],
        [1679, "Cơm thịt kho tiêu", 68, 22000],
        [686, "Cơm thịt kho đậu hũ", 68, 22000],
        [1675, "Cơm Thịt kho trứng ", 68, 22000],
        [2941, "Cơm Sườn chiên", 68, 25000],
        [694, "Cơm thịt heo quay", 68, 25000],
        [684, "Cơm đùi gà chiên", 68, 25000],
        [2939, "Cơm gà kho", 68, 25000],
        [2942, "Cơm sườn non", 68, 28000],
        [1694, "Canh khổ qua nhồi thịt", 68, 10000],
        [762, "Cơm trắng", 68, 5000]
    ];
var nuocLon = 
    [
        [94, "Pepsi lon", 55, 12000],
        [106, "Sting lon", 55, 12000],
        [96, "Coca cola lon", 55, 12000],
        [2113, "Coca Light lon", 55, 12000],
        [92, "7up lon", 55, 12000],
        [97, "Mirinda Xá xị lon", 55, 12000],
        [3673, "Revive lon", 55, 12000],
        [1744, "Twister lon", 55, 12000],
	    [769, "CC Lemon lon", 55, 12000],
        [102, "Bí đao Wonderfarm", 55, 12000],
        [90, "Redbull Thái lon", 55, 13000],
        [100, "Yến Ngân Nhĩ L1 lon", 55, 12000],
        [104, "Bia 333", 55, 13000],
        [105, "Bia Tiger", 55, 16000],
        [103, "Bia Heineken", 55, 20000]
    ];
var nuocChai = 
    [
        [1, "Aquafina 500ml", 40, 6000],
        [5, "La Vie 500ml", 40, 6000],
        [95, "Pepsi chai", 40, 10000],
        [91, "Sting chai", 40, 10000],
        [124, "Coca cola chai", 40, 10000],
        [110, "7up chai", 40, 10000],
        [111, "Mirinda Xá xị chai", 40, 10000],
        [1732, "Revive chai", 40, 10000],
        [1743, "Twister chai", 40, 10000],
        [1729, "C2", 40, 10000],
        [115, "Dr. Thanh", 40, 12000],
        [114, "Trà xanh không độ", 40, 10000],
        [121, "Number One", 40, 12000],
        [2175, "Trà Ô Long Nhỏ chai", 40, 10000],
    ];
var cafeSinhTo = 
    [
        [130, "Khăn lạnh", 6, 2000],
        [602, "Nước Sâm", 62, 5000],
        [1711, "Mía", 62, 6000],
        [1713, "Sữa đậu nành", 62, 6000],
        [1712, "Rau Má", 62, 8000],
        [594, "Cà phê đá", 62, 10000],
        [595, "Cà phê sữa đá", 62, 12000],
        [1710, "Sữa nóng", 62, 10000],
        [619, "Sinh tố bơ", 62, 20000],
        [624, "Sinh tố mãng cầu", 62, 20000],
        [3024, "Sinh tố dâu", 62, 22000],
        [621, "Sinh tố bơ sầu riêng", 62, 22000],
        [618, "Sinh tố đu đủ", 62, 18000],
        [615, "Nước ép chanh dây", 62, 15000],
	    [605, "Nước ép cam vắt", 62, 20000],
        [611, "Nước ép thơm", 62, 18000],
        [610, "Nước ép cà rốt", 62, 18000],
    ];