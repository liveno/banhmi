﻿/************************************************************************
* Xử lý chuyển table html thành file excel                              *
*************************************************************************/
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" ' +
        'xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
        '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets>' +
        '<x:ExcelWorksheet><x:Name>{worksheet}</x:Name>' +
        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
        '</xml><![endif]--></head><body>' +
        '<table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return function (table, name) {
        if (!table.nodeType) table = $(table)//table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.html() }
        window.location.href = uri + base64(format(template, ctx))
    };
})();
/************************************************************************
* Xử lý gửi request lên server                                          *
*************************************************************************/
//function request(obj, opt) {
//    $.ajax({
//        url: '../WSASMXGobal/Kernel.asmx/P',
//        type: "POST",
//        crossDomain: true,
//        data: JSON.stringify({ obj: obj }),
//        contentType: "application/json; charset=utf-8",
//        success: function (r) {
//            if (typeof opt.success == 'function')
//                opt.success(obj, opt, parseJsResponse(r));
//        }
//    });
//};
function fun_BreakControlEnter(id) {
    return false;
};
function pAsg() {
};
function FocusCmbText(strId, e) {
};
/************************************************************************
* Xử lý hiển thị nội dung                                               *
*************************************************************************/
function view($that, $container, o, on, title) {
    var parentWidth = $container.parent().innerWidth();
    //var $title = $('<h3 style="font-size: 18px; color: rgb(58, 135, 173);"/>').text($that.find('a').text());
    if (title == null || title == "") $('#gc_gbHTitle').text($that.find('a').text());
    else $('#gc_gbHTitle').text(title);
    $container.prepend($(on)).css({ "width": parentWidth + "px" }).show();
    var $listButton = $container.find('.inputBtnBottomV2');
    $listButton.find('.btn.btn-warning.btn-sm').hide();
    var $inputFirst = $container.find('div.input').find('input:first'); //.not('#fo28605i');
    //    $inputFirst.each(function () {
    //        if ($that.attr('id') == "fo28605i") {
    //            $that.css({ 'margin-right': '5px' });
    //        }
    //        else {
    //            $that.css({ 'margin-right': '35px' });
    //        }
    //    });
};

/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
* Các hàm ngắt kernel.js                                                *
_______________________________________________________________________*/
/************************************************************************
* Xử lý load và tự fill thông tin của stock_input (ngắt kernel.js)      *
*************************************************************************/
/************************************************************************
* Xử lý load và tự fill thông tin của stock_input (ngắt kernel.js)      *
*************************************************************************/
function gR(s) {
}
/************************************************************************
* Xử lý ngắt để chỉnh data trước khi gửi request lên server             *
*************************************************************************/
function pCDT(s, obj) {
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 2) < 10 ? ('0' + (s._e.length - 2)) : (s._e.length - 2))] = cur_gb_AccountId;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 5) < 10 ? ('0' + (s._e.length - 5)) : (s._e.length - 5))] = cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account;
    else if (obj.type == 'u') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = "isnull($x, '') + N'~" + (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account + "'";
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 7) < 10 ? ('0' + (s._e.length - 7)) : (s._e.length - 7))] = cur_gb_Account;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 8) < 10 ? ('0' + (s._e.length - 8)) : (s._e.length - 8))] = 0;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 13) < 10 ? ('0' + (s._e.length - 13)) : (s._e.length - 13))] = dateToString(new Date(), 'iso');
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 15) < 10 ? ('0' + (s._e.length - 15)) : (s._e.length - 15))] = cur_gb_AccountId;

    // Chỉnh sửa dữ liệu
    if (s.id == "phieuDetail") {
        obj.a = "AddOrUpdateReceiptDetail";
    }
};
/************************************************************************
* Xử lý khi jtable load xong dữ liệu                                    *
*************************************************************************/
function pCb() {
    if ($('#phieu').length > 0) {
        $('#phieu_g').find('.jtable tbody tr:first').click();
    } else if ($('#daLap').length > 0) {
        $('#daLap_g').find('.jtable tbody tr:first').click();
    } else if ($('#chuaDuyet').length > 0) {
        $('#chuaDuyet_g').find('.jtable tbody tr:first').click();
    } else if ($('#daDuyet').length > 0) {
        $('#daDuyet_g').find('.jtable tbody tr:first').click();
    } else if ($('#hoanThanh').length > 0) {
        $('#hoanThanh_g').find('.jtable tbody tr:first').click();
    } else if ($('#timPhieu').length > 0) {
        $('#timPhieu_g').find('.jtable tbody tr:first').click();
    }
};
/************************************************************************
* Xử lý ngắt chỉnh display jtable                                       *
*************************************************************************/
function pCW(v, _s, ll, i, ii, ic, f0) {
    // Menu Student
    if (_s.id == 'studentList') {
        if (_s._e[ic + 1] == 'Name') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if (_s._e[ic + 1] == 'isPrgCreateDate') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if (_s._e[ic + 1] == 'Birthday') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    }
    return v;
};
/************************************************************************
* Xử lý khi click chọn item combobox                                    *
*************************************************************************/
function postonItemClick(itemLI, tBox, index) {
    if (tBox == "flapPhieuChi05i") {
        $("#flapPhieuChi04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "fdaLapChi05i") {
        $("#fdaLapChi04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fchuaDuyetChi05i") {
        $("#fchuaDuyetChi04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fdaDuyetChi05i") {
        $("#fdaDuyetChi04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fhoanThanhChi05i") {
        $("#fhoanThanhChi04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "ftimPhieu05i") {
        $("#ftimPhieu04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "flapPhieuNhapQuy05i") {
        $("#flapPhieuNhapQuy04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "fdaLapNhapQuy05i") {
        $("#fdaLapNhapQuy04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fchuaDuyetNhapQuy05i") {
        $("#fchuaDuyetNhapQuy04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fdaDuyetNhapQuy05i") {
        $("#fdaDuyetNhapQuy04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fhoanThanhNhapQuy05i") {
        $("#fhoanThanhNhapQuy04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "ftimPhieuNhapQuy05i") {
        $("#ftimPhieuNhapQuy04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "flapPhieuQuyetToan05i") {
        $("#flapPhieuQuyetToan04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "flapPhieuQuyetToan20i") {
        if (cur_gb_DataDivShow[index][3] != null) {
            $("#flapPhieuQuyetToan04i").val(cur_gb_DataDivShow[index][3]);
            $("#flapPhieuQuyetToan05i").val(cur_gb_DataDivShow[index][3]);
        }
        if (cur_gb_DataDivShow[index][4] != null) $("#flapPhieuQuyetToan07i").val(c(cur_gb_DataDivShow[index][4]));
        if (cur_gb_DataDivShow[index][5] != null) $("#flapPhieuQuyetToan05h").val(cur_gb_DataDivShow[index][5]);
        autoFare("lapPhieuQuyetToan");
    } else if (tBox == "flapPhieuTamUng05i") {
        $("#flapPhieuTamUng04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "fdaLapTamUng05i") {
        $("#fdaLapTamUng04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fchuaDuyetTamUng05i") {
        $("#fchuaDuyetTamUng04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fdaDuyetTamUng05i") {
        $("#fdaDuyetTamUng04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "fhoanThanhTamUng05i") {
        $("#fhoanThanhTamUng04i").val(cur_gb_DataDivShow[index][2]);
    }
    else if (tBox == "ftimPhieuTamUng05i") {
        $("#ftimPhieuTamUng04i").val(cur_gb_DataDivShow[index][2]);
    }
};
/************************************************************************
* Xử lý ngặt chỉnh obj trước khi load jtable                            *
*************************************************************************/
function pJtablCustomizeObj(obj, s) {

}
/************************************************************************
* Xử lý ngặt chỉnh cho phép delete hay ko                               *
*************************************************************************/
function pGe_o(s, sr) {
    var allow = false;
    var sR = "";
    if (s.id == "phieu") {
        allow = true;
    }
    else if (s.id == "phieuDetail") {
        sR += ",d: {width: '1%',sorting: false, type: 'img',title: '', display: function (data) { var $img=$(\"<button class='btn-sm' type='button' title='Xóa'></button>\"); $img.append(\"<i class='fa fa-fw fa-trash-o'></i>\"); $img.click(function(){ del(data.record,'" + s._e[0] + "'," + s.id + "," + s._e.length + ")}); return $img; } } ";
        allow = true;
    }
    return { allow: allow, sr: sR };
}
/************************************************************************
* Xử lý gán value cho 1 số input sau khi insert, update                 *
*************************************************************************/
function pAssId(s) {
    if (s.id == 'phieuDetail') {
        $("#fphieuDetail01h").val(cur_gb_ReceitpID);
        $("#fphieuDetail02h").val(cur_gb_KhoHangId);
    }
};
function pRm(s, obj) {
    if (s.id == "phieuDetail") {
        obj.a = "DeleteReceiptDetail";
        obj.c.PhieuThuTienMatId = cur_gb_ReceitpID;
    }
}
/************************************************************************
* Xử lý gửi request lên server lấy data và show report                  *
*************************************************************************/
function showReport(obj, typeReport, fNameClose, fNamePrint, url, fNameXacNhan) {
    modalcenter.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $.ajax({
        type: "POST",
        url: url,
        data: { typeReport: typeReport, obj: obj },
        success: function (msg) {
            var $divall = $("<div></div>");
            var $div = $("<div class='input'></div>");
            var $btnActive = $("<input type='button'  value='Đóng' id='gc_btnActiveForm' onclick='" + fNameClose + "()' class='btn btn-info btn-sm' />");
            var $btnPrint = $("<input type='button'  value='In' id='gc_btnPrintForm' onclick='" + fNamePrint + "()'  class='btn btn-info btn-sm' />");
            $div.append($btnActive);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnPrint);
            if (fNameXacNhan != null) {
                var $btnXN = $("<input type='button'  value='Xác nhận' id='gc_btnPrintForm1' onclick='" + fNameXacNhan + "()'  class='btn btn-info btn-sm' />");
                $div.append($("<label>&nbsp;</label>"));
                $div.append($btnXN);
            }
            var $divcontent = $("<div id='gc_DivReceiptContent'></div>");
            $divcontent.html(msg);
            $divcontent.find("table.rptTable .rptTDHeader:first").html("STT");
            $divall.append($div);
            $divall.append($divcontent);
            modalcenter.close();
            modal.open({ content: $divall.html() });
        },
        error: function (xhr, ErrorText, thrownError) {
            $("#gc_gobal_ListDataID").text("Error" + xhr.status + xhr.responseText);
        }
    });
};
/************************************************************************
* Xử lý khi nhấn nút in trong report                                    *
*************************************************************************/
function PrintReport() {
    GobalPrintNew('gc_DivReceiptContent');
    modal.close();
};
/************************************************************************
* Xử lý gửi request lên server và show thông tin report                 *
*************************************************************************/
function reportRequest(obj, opt) {
    modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $.ajax({
        type: "POST",
        url: '../zgcReport/Report.ashx',
        crossDomain: true,
        data: JSON.stringify({ obj: obj }),
        success: function (msg) {
            modal.close();
            var $divall = $("<div></div>");
            var $div = $("<div class='input'></div>");
            var $divClear = $("<div style='clear: both; height: 20px'></div>");
            var $btnActive = $("<input type='button'  value='Đóng' id='gc_btnActiveForm' onclick='closeReport()' class='btn btn-info btn-sm' />");
            var $btnExport = $("<input type='button'  value='Xuất' id='gc_btnExportForm' onclick='convertTableToExcel()' class='btn btn-info btn-sm' />");
            var $btnPrint = $("<input type='button'  value='In' id='gc_btnPrintForm' onclick='PrintReport()'  class='btn btn-info btn-sm' />");
            var $btnNewTab = $("<a type='button' target='_blank' href='../QLHocVien/DefaultQLHocVien.aspx'  class='btn btn-info btn-sm' >Tab mới</a>");
            $div.append($btnActive);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnExport);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnPrint);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnNewTab);
            var $divcontent = $("<div id='gc_DivReceiptContent'></div>");
            $divcontent.html(msg);
            $divcontent.find("table.rptTable .rptTDHeader:first").html("STT");
            $divall.append($div);
            $divall.append($divClear);
            $divall.append($divcontent);
            modal.open({ content: $divall.html() });
            if (opt != null && typeof opt.success == "function") {
                opt.success(obj, opt, modal.$content());
            }
        },
        error: function () {
            alert("Error");
        }
    });
}
/************************************************************************
* Xử lý khi nhấn nút export trong report                                *
*************************************************************************/
function convertTableToExcel() {
    tableToExcel('#gc_DivReceiptContent', 'W3C Example Table');
};
/************************************************************************
* Xử lý tắt popup show report                                           *
*************************************************************************/
function closeReport() {
    modal.close();
};
/************************************************************************
* Xử lý điều kiện filter                                                *
*************************************************************************/
function selectOption(elId, filter, table, field) {
    $('#' + elId).click(function (ev) {
        test(elId, ev, filter, table, field);
    });
    $('#' + elId).keyup(function (ev) {
        if ($('#' + elId).val() == '') {
            $('#' + elId.substr(0, elId.length - 1) + 'h').val('');
        }
        if ($('#' + elId).val() == '' && $('#' + elId.substring(0, elId.length - 1) + 'h').val() == "" &&
            typeof window[elId.split("_")[0] + "_selectOption"] == 'function') {
            window[elId.split("_")[0] + "_selectOption"]();
        }
        test(elId, ev, filter, table, field);
    });
};
$(document).bind('cbox_closed', function () {
    gb_strFilterGrid = " ";
    if (cur_gb_ElementId == 'lapPhieu') {
        $("#LapPhieu").click();
    } else if (cur_gb_ElementId == 'chuaDuyet') {
        $("#DSPhieuChuaDuyet").click();
    } else if (cur_gb_ElementId == 'daDuyet') {
        $("#DSPhieuDaDuyet").click();
    } else if (cur_gb_ElementId == 'daDuyetChi') {
        $("#DSPhieudaDuyetChi").click();
    } else if (cur_gb_ElementId == 'hoanThanhChi') {
        $("#DSPhieuDahoanThanhChi").click();
    } else if (cur_gb_ElementId == 'lapPhieuNhapQuy') {
        $("#lapPhieuNhapQuy").click();
    } else if (cur_gb_ElementId == 'daLapNhapQuy') {
        $("#DSPhieudaLapNhapQuy").click();
    } else if (cur_gb_ElementId == 'chuaDuyetNhapQuy') {
        $("#DSPhieuchuaDuyetNhapQuy").click();
    } else if (cur_gb_ElementId == 'daDuyetNhapQuy') {
        $("#DSPhieudaDuyetNhapQuy").click();
    } else if (cur_gb_ElementId == 'hoanThanhNhapQuy') {
        $("#DSPhieuDahoanThanhNhapQuy").click();
    } else if (cur_gb_ElementId == 'lapPhieuQuyetToan') {
        $("#lapPhieuQuyetToan").click();
    } else if (cur_gb_ElementId == 'daLapQuyetToan') {
        $("#DSPhieudaLapQuyetToan").click();
    } else if (cur_gb_ElementId == 'chuaDuyetQuyetToan') {
        $("#DSPhieuchuaDuyetQuyetToan").click();
    } else if (cur_gb_ElementId == 'daDuyetQuyetToan') {
        $("#DSPhieudaDuyetQuyetToan").click();
    } else if (cur_gb_ElementId == 'hoanThanhQuyetToan') {
        $("#DSPhieuDahoanThanhQuyetToan").click();
    } else if (cur_gb_ElementId == 'choTPQuyetToan') {
        $("#DSPhieuchoTPQuyetToan").click();
    } else if (cur_gb_ElementId == 'lapPhieuTamUng') {
        $("#lapPhieuTamUng").click();
    } else if (cur_gb_ElementId == 'daLapTamUng') {
        $("#DSPhieudaLapTamUng").click();
    } else if (cur_gb_ElementId == 'chuaDuyetTamUng') {
        $("#DSPhieuchuaDuyetTamUng").click();
    } else if (cur_gb_ElementId == 'daDuyetTamUng') {
        $("#DSPhieudaDuyetTamUng").click();
    } else if (cur_gb_ElementId == 'hoanThanhTamUng') {
        $("#DSPhieuDahoanThanhTamUng").click();
    } else if (cur_gb_ElementId == 'choTPTamUng') {
        $("#DSPhieuchoTPTamUng").click();
    }
});


