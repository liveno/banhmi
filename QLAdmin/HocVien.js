﻿
function request(obj, opt) {
    $.ajax({
        url: '../WSASMXGobal/Kernel.asmx/P',
        type: "POST",
        crossDomain: true,
        data: JSON.stringify({ obj: obj }),
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            if (typeof opt.success == 'function')
                opt.success(obj, opt, parseJsResponse(r));
        },
    });
}

function view1($that, $container, o, on) {
    // move html to content in right of screen and style width
    var parentWidth = $container.parent().innerWidth();
    var html = $(on).html();
    //var $title = $('<h3 style="font-size: 18px; color: rgb(58, 135, 173);"/>').text($that.find('a').text());
    $container.prepend($(on)).css({ "width": parentWidth + "px" });
    //$(on).before($title);


    // Hide button "Dong cua so"
    var $listButton = $container.find('.inputBtnBottomV2');
    $listButton.find('.btn.btn-warning.btn-sm').hide();

    // Margin input
    var $inputFirst = $container.find('div.input').find('input:first'); //.not('#fo28605i');
    //$inputFirst.each(function () {
    //    if ($that.attr('id') == "fo28605i") {
    //        $that.css({ 'margin-right': '5px' });
    //    }
    //    else {
    //        $that.css({ 'margin-right': '35px' });
    //    }
    //});
}

/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
* Các hàm ngắt kernel.js                                                *
_______________________________________________________________________*/
/************************************************************************
* Xử lý load và tự fill thông tin của stock_input (ngắt kernel.js)      *
*************************************************************************/
/************************************************************************
* Xử lý load và tự fill thông tin của stock_input (ngắt kernel.js)      *
*************************************************************************/
function gR(s) {
//    var sf = [];
//    var sn = [];
//    var sfk = [];
//    for (var i = 1; i < s._e.length; i++) {
//        sf.push(s._e[i]);
//        sfk.push([1, 0]);
//    }
//    var j = 1;
//    for (i = 0; i < s._d.length; i++) {
//        var rd = s._d[i].split('|');
//        for (u = 0; u < rd.length; u++) {
//            sfk[parseInt(rd[u].split(',')[0])][0] = rd[u].split(',')[3];
//            sfk[parseInt(rd[u].split(',')[0])][1] = 0;
//            if (rd[u].split(',')[5] != '') {
//                sfk[parseInt(rd[u].split(',')[0])][1] = j;
//                sn.push(s._e[parseInt(rd[u].split(',')[0]) + 1] + rd[u].split(',')[6].split('~')[0]);
//                j++;
//            }
//        }
//    }
//    if (s.__id == 374) {
//        request({
//            a: 'fGetgcGobal_SUPP_Supplier',
//            c: {
//                Id: cur_gb_ReceitpID
//            },
//            _f: sf.concat(sn).join()
//        }, {
//            success: function(obj, opt, r) {
//                if (r.Result == 'OK') {
//                    for (var i = 0; i < sf.length; i++) {
//                        var val = r.Records[0][i];
//                        if (sfk[i][0] == 5 && val != null) {
//                            val = _parseDate(val).format('dd/MM/yyyy HH:mm:ss');
//                        }
//                        $('#fo374' + (i < 10 ? ('0' + i) : i) + 'i').val(val);
//                        $('#fo374' + (i < 10 ? ('0' + i) : i) + 'h').val(val);
//                        if ($('#fo374' + (i < 10 ? ('0' + i) : i) + 'h').length > 0) {
//                            $('#fo374' + (i < 10 ? ('0' + i) : i) + 'i').val(r.Records[0][sf.length + sfk[i][1] - 1]);
//                        }
//                    }
//                }
//            }
//        });
//    } else if (s.__id == 318) {
//        request({
//            a: 'fGetgcGobal_SUPP_Supplier_DS_KhuyenMai_TruocBa',
//            c: {
//                Id: cur_gb_ReceitpID
//            },
//            _f: sf.concat(sn).join()
//        }, {
//            success: function(obj, opt, r) {
//                if (r.Result == 'OK') {
//                    for (var i = 0; i < sf.length; i++) {
//                        var val = r.Records[0][i];
//                        if (sfk[i][0] == 5 && val != null) {
//                            val = _parseDate(val).format('dd/MM/yyyy HH:mm:ss');
//                        }
//                        $('#fo318' + (i < 10 ? ('0' + i) : i) + 'i').val(val);
//                        $('#fo318' + (i < 10 ? ('0' + i) : i) + 'h').val(val);
//                        if ($('#fo318' + (i < 10 ? ('0' + i) : i) + 'h').length > 0) {
//                            $('#fo318' + (i < 10 ? ('0' + i) : i) + 'i').val(r.Records[0][sf.length + sfk[i][1] - 1]);
//                        }
//                    }
//                }
//            }
//        });
//    }
};
/************************************************************************
* Xử lý khi nhấn nút đóng ở report                                      *
*************************************************************************/
function pCDT(s, obj) {
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 2) < 10 ? ('0' + (s._e.length - 2)) : (s._e.length - 2))] = cur_gb_AccountId;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 5) < 10 ? ('0' + (s._e.length - 5)) : (s._e.length - 5))] = cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account;
    else if (obj.type == 'u') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = "isnull($x, '') + N'~" + (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account + "'";
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 7) < 10 ? ('0' + (s._e.length - 7)) : (s._e.length - 7))] = cur_gb_Account;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 8) < 10 ? ('0' + (s._e.length - 8)) : (s._e.length - 8))] = 0;
    if(obj.type == 'i') obj.d['f' + ((s._e.length - 13) < 10 ? ('0' + (s._e.length - 13)) : (s._e.length - 13))] = dateToString(new Date(), 'iso');
    obj.d['f' + ((s._e.length - 15) < 10 ? ('0' + (s._e.length - 15)) : (s._e.length - 15))] = cur_gb_AccountId;
    if (s.id == 'assignClass' || s.id == 'assignStudent' || s.id == 'studentEnrollAL'
        || s.id == 'enrollment') {
        obj.a = 'AddOrUpdateClassStudent';
        // Keep01 phần loại type ghi danh vào lớp thường hay là lớp AL: 1->lớp thường; 2->Lớp AL
        if(s.id == "studentEnrollAL"){
            obj.d.f37 = 2;
        } else if(s.id == "enrollment"){
            obj.d.f37 = 1;
        }
    } else if (s.id == 'scheduleList'){
        var sn = '';
        sn = obj.d['f02'] ? (sn += 'Mon') : sn;
        sn = obj.d['f03'] ? (sn == '' ? (sn + 'Tue') : (sn + '/Tue')) : sn;
        sn = obj.d['f04'] ? (sn == '' ? (sn + 'Wed') : (sn + '/Wed')) : sn;
        sn = obj.d['f05'] ? (sn == '' ? (sn + 'Thu') : (sn + '/Thu')) : sn;
        sn = obj.d['f06'] ? (sn == '' ? (sn + 'Fri') : (sn + '/Fri')) : sn;
        sn = obj.d['f07'] ? (sn == '' ? (sn + 'Sat') : (sn + '/Sat')) : sn;
        sn = obj.d['f08'] ? (sn == '' ? (sn + 'Sun') : (sn + '/Sun')) : sn;
        sn += (' ' + $('#fscheduleList09i').val());
        obj.d['f01'] = sn;
    } else if (s.id == 'scheduleAdvanceDetail'){
        obj.d['f03'] = $('#fscheduleAdvanceDetail02i').val();
        obj.a = 'AddOrUpdateScheduleAdvanceDetail';
    } else if (s.id == 'shiftList'){
        obj.d['f02'] = $('#fshiftList03i').val() + '-' + $('#fshiftList04i').val();
    }else if (s.id == 'addMarkStudent'){
        obj.a = 'AddOrUpdateStudentAddMark';
    }else if (s.id == 'doiDiemStudent'){
        obj.a = 'AddOrUpdateDoiDiem';
    } else if(s.id == "studentAdd"){
        var d = $("#studentAddDay").val();
        var m = $("#studentAddMonth").val();
        var y = $("#studentAddYear").val();
        if(d != "" && m != "" && y != "")
            obj.d['f09'] = m + "/" + d + "/" + y;
        else 
            obj.d['f09'] = "";
        $("#studentAddDay").val("");
        $("#studentAddMonth").val("");
        $("#studentAddYear").val("");
    } else if(s.id == "studentDetail"){
        var d = $("#studentDetailDay").val();
        var m = $("#studentDetailMonth").val();
        var y = $("#studentDetailYear").val();
        if(d != "" && m != "" && y != "")
            obj.d['f09'] = m + "/" + d + "/" + y;
        else 
            obj.d['f09'] = "";
        $("#studentDetailDay").val("");
        $("#studentDetailMonth").val("");
        $("#studentDetailYear").val("");
    } else if(s.id == "cancelDate"){
        obj.a = 'AddOrUpdateCancelDate';
    }
};
function pCb() {
    if ($('#classDetail').length > 0 && $('#classDetail_i').length > 0) {
        $('#classDetail_i').find('.jtable tbody tr:first').click();
    }
    else if ($('#studentDetail').length > 0 && $('#studentDetail_i').length > 0) {
        $('#studentDetail_i').find('.jtable tbody tr:first').click();
    }
    else if($('#studentAction').length > 0){
        $('#studentAction_i').find('.jtable thead tr th:nth-child(8)').attr('style', 'width: 30%');
    } else if($('#studentList').length > 0){
        $('#studentList_i').find('.jtable thead tr th:nth-child(2)').attr('style', 'width: 4%');
        $('#studentList_i').find('.jtable thead tr th:nth-child(5)').attr('style', 'width: 20%');
        //$('#studentList_i').find('.jtable thead tr th:nth-child(4)').attr('style', 'width: 15%');
        //$('#studentList_i').find('.jtable thead tr th:nth-child(11)').attr('style', '');
        $('#studentList_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } else if($('#inquires').length > 0){
        $('#inquires_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } else if($('#placementTest').length > 0){
        $('#placementTest_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(3)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
            var td2 = $(this).find('td:nth-child(2)');
            if(td2.text() != '') td2.text(td2.text().substring(0, 10));
        });
    } else if($('#enrollmentList').length > 0){
        $('#studentList_i').find('.jtable thead tr th:nth-child(8)').attr('style', 'width: 15%');
        $('#enrollmentList_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } else if($('#upcomingClassEnrollList').length > 0){
        $('#upcomingClassEnrollList_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(5)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } else if($('#courseEnrollList').length > 0){
        $('#courseEnrollList_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(5)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    }  else if($('#studentStudying').length > 0){
        $('#studentStudying_i').find('.jtable thead tr th:nth-child(2)').attr('style', 'width: 4%');
        $('#studentStudying_i').find('.jtable thead tr th:nth-child(5)').attr('style', 'width: 20%');
        //$('#studentStudying_i').find('.jtable thead tr th:nth-child(4)').attr('style', 'width: 15%');
        //$('#studentStudying_i').find('.jtable thead tr th:nth-child(11)').attr('style', '');
        $('#studentStudying_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    }   else if($('#studentOld').length > 0){
        $('#studentOld_i').find('.jtable thead tr th:nth-child(2)').attr('style', 'width: 4%');
        $('#studentOld_i').find('.jtable thead tr th:nth-child(5)').attr('style', 'width: 20%');
        //$('#studentOld_i').find('.jtable thead tr th:nth-child(4)').attr('style', 'width: 15%');
        //$('#studentOld_i').find('.jtable thead tr th:nth-child(11)').attr('style', '');
        $('#studentOld_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    }   else if($('#studentNot').length > 0){
        $('#studentNot_i').find('.jtable thead tr th:nth-child(2)').attr('style', 'width: 4%');
        $('#studentNot_i').find('.jtable thead tr th:nth-child(5)').attr('style', 'width: 20%');
        //$('#studentNot_i').find('.jtable thead tr th:nth-child(4)').attr('style', 'width: 15%');
        //$('#studentNot_i').find('.jtable thead tr th:nth-child(11)').attr('style', '');
        $('#studentNot_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    }
    // Menu upcomming Class
    else if($('#studentAnalysis').length > 0){
        $('#studentAnalysis_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(5)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    }
    // Menu Class
    else if($('#studentSpecialNote').length > 0){
        $('#studentSpecialNote_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(3)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } else if($('#searchBirthday').length > 0){
        $('#searchBirthday_i').find('.jtable thead tr th:nth-child(2)').attr('style', 'width: 4%');
        $('#searchBirthday_i').find('.jtable thead tr th:nth-child(5)').attr('style', 'width: 20%');
        $('#searchBirthday_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(4)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    }
    // Menu AL
    else if ($('#classAL').length > 0 && $('#classAL_i').length > 0) {
        $('#classAL_i').find('.jtable tbody tr:first').click();
    } else if($('#ALList').length > 0){
        $('#ALList_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(2)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } else if($('#listAttendanceAL').length > 0){
        $('#listAttendanceAL_i').find('.jtable tbody tr').each(function(){
            var td = $(this).find('td:nth-child(2)');
            td.attr('style', td.find('a').attr('style') + '; border-bottom: 3px solid #bebebe ; text-align: left; font-size : 16px; color: black');
        });
    } 
};
///************************************************************************
// Xử lý ngắt chỉnh display jtable                                          *
//*************************************************************************/
function pCW(v, _s, ll, i, ii, ic, f0){
    // Menu Student
    if (_s.id == 'studentList') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { var ex = ''; if(data.record.f08 == 1) ex = '<span style=\"float: right; color: red\">VIP</span>'; else if(data.record.f08 == 2) ex = '<span style=\"float: right; color: red\">ĐVC</span>'; else if(data.record.f08 == 3) ex = '<span style=\"float: right; color: red\">NT</span>'; else if(data.record.f08 == 4) ex = '<span style=\"float: right; color: red\">NNMs.X</span>'; else if(data.record.f08 == 5) ex = '<span style=\"float: right; color: orange\">BMs.X</span>'; else if(data.record.f08 == 6) ex = '<span style=\"float: right; color: black\">HTP</span>'; else if(data.record.f08 == 7) ex = '<span style=\"float: right; color: orange\">CGV</span>'; $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + ex + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f01; showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Birthday'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'studentStudying') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { var ex = ''; if(data.record.f08 == 1) ex = '<span style=\"float: right; color: red\">VIP</span>'; else if(data.record.f08 == 2) ex = '<span style=\"float: right; color: red\">ĐVC</span>'; else if(data.record.f08 == 3) ex = '<span style=\"float: right; color: red\">NT</span>'; else if(data.record.f08 == 4) ex = '<span style=\"float: right; color: red\">NNMs.X</span>'; else if(data.record.f08 == 5) ex = '<span style=\"float: right; color: orange\">BMs.X</span>'; else if(data.record.f08 == 6) ex = '<span style=\"float: right; color: black\">HTP</span>'; else if(data.record.f08 == 7) ex = '<span style=\"float: right; color: orange\">CGV</span>'; $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + ex + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f01; showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Birthday'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'studentOld') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { var ex = ''; if(data.record.f08 == 1) ex = '<span style=\"float: right; color: red\">VIP</span>'; else if(data.record.f08 == 2) ex = '<span style=\"float: right; color: red\">ĐVC</span>'; else if(data.record.f08 == 3) ex = '<span style=\"float: right; color: red\">NT</span>'; else if(data.record.f08 == 4) ex = '<span style=\"float: right; color: red\">NNMs.X</span>'; else if(data.record.f08 == 5) ex = '<span style=\"float: right; color: orange\">BMs.X</span>'; else if(data.record.f08 == 6) ex = '<span style=\"float: right; color: black\">HTP</span>'; else if(data.record.f08 == 7) ex = '<span style=\"float: right; color: orange\">CGV</span>'; $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + ex + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f01; showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'District'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 61);
        } else if(_s._e[ic + 1] == 'GenderId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 62);
        } else if(_s._e[ic + 1] == 'Birthday'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 63);
        } else if(_s._e[ic + 1] == 'StatusId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 64);
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'studentNot') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { var ex = ''; if(data.record.f08 == 1) ex = '<span style=\"float: right; color: red\">VIP</span>'; else if(data.record.f08 == 2) ex = '<span style=\"float: right; color: red\">ĐVC</span>'; else if(data.record.f08 == 3) ex = '<span style=\"float: right; color: red\">NT</span>'; else if(data.record.f08 == 4) ex = '<span style=\"float: right; color: red\">NNMs.X</span>'; else if(data.record.f08 == 5) ex = '<span style=\"float: right; color: orange\">BMs.X</span>'; else if(data.record.f08 == 6) ex = '<span style=\"float: right; color: black\">HTP</span>'; else if(data.record.f08 == 7) ex = '<span style=\"float: right; color: orange\">CGV</span>'; $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + ex + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f01; showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'District'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 61);
        } else if(_s._e[ic + 1] == 'GenderId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 62);
        } else if(_s._e[ic + 1] == 'Birthday'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 63);
        } else if(_s._e[ic + 1] == 'StatusId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 64);
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'enrollment') {
        if(_s._e[ic + 1] == 'Comment'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { if(data.record.f|2| ==null) return ''; else { var e = (data.record.f|2| + '').split('~'); var e2 = e[e.length - 1].split(' | '); return e2.length > 2 ? e2[2] : ''; }}  } ").f2(f0, ll, 52);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'isPrgSmField'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|.split('~')[data.record.f|2|.split('~').length - 1].split(' | ')[0]+'') }  } ").f2(f0, ll, 52);
        } else if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) {$sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f18 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupClassDetail(ev, data.record.f18, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if (_s.id == 'studentAction') {
        if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy HH:mm:ss') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'studentEnrollAL') {
        if(_s._e[ic + 1] == 'Comment'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { if(data.record.f|2| ==null) return ''; else { var e = (data.record.f|2| + '').split('~'); var e2 = e[e.length - 1].split(' | '); return e2.length > 2 ? e2[2] : ''; }}  } ").f2(f0, ll, 52);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'isPrgSmField'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|.split('~')[data.record.f|2|.split('~').length - 1].split(' | ')[0]+'') }  } ").f2(f0, ll, 52);
        }
    }
    else if(_s.id == 'inquires'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%',display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f30 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f25; showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '12%',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss'):_parseDate(data.record.f|2|+'').format('MM/dd//yyyy HH:mm:ss') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Enroll'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 39);
        } else if(_s._e[ic + 1] == 'CourseId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 32);
        } else if(_s._e[ic + 1] == 'ScheduleId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 41);
        } else if(_s._e[ic + 1] == 'Customer'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|+'') }  } ").f2(f0, ll, 31)
        } else if(_s._e[ic + 1] == 'Comment'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '30%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, ii[1]);
        }
    } else if(_s.id == 'placementTest'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%',display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f36 + ';color: black; font-size: 14px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f32; showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if(_s.id == 'enrollmentList'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '30%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f57; showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 78);
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 75);
        } else if(_s._e[ic + 1] == 'FinishDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'').substring(0,1)) }  } ").f2(f0, ll, 79);
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 80);
            //v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss'):_parseDate(data.record.f|2|+'').format('MM/dd//yyyy HH:mm:ss') }  } ").f2(f0, _gb.ll(ic, _s), 80);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Description'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '30%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, ii[1]);
        }
    } else if(_s.id == 'upcomingClassEnrollList'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 75);
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 80);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if(_s.id == 'courseEnrollList'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '15%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 80);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if(_s.id == 'attendanceAL'){
        if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'HourIn'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) + ':' + ((data.record.f|3| ==null)?'':(data.record.f|3|+''))}  } ").f2(f0, ll, ii[1], (parseInt(ii[1]) + 1) + "");
        } else if(_s._e[ic + 1] == 'HourOut'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) + ':' + ((data.record.f|3| ==null)?'':(data.record.f|3|+'')) }  } ").f2(f0, ll, ii[1], (parseInt(ii[1]) + 1) + "");
        } else if(_s._e[ic + 1] == 'Date'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 54);
        }
    } else if (_s.id == 'studentTakeCare') {
        if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy HH:mm:ss') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'quaStudent') {
        if(_s._e[ic + 1] == 'isPrgPartComp'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { if(data.record.f|2| ==null) return ''; else { var e = (data.record.f|2| + '').split('~'); var e2 = e[e.length - 1].split(' | '); return e2.length > 2 ? e2[2] : ''; }}  } ").f2(f0, ll, 17);
        } else if(_s._e[ic + 1] == 'isPrgSmField'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%',display: function (data) { return (data.record.f|2| ==null)?'':(data.record.f|2|.split('~')[data.record.f|2|.split('~').length - 1].split(' | ')[0]+'') }  } ").f2(f0, ll, 17);
        }
    }
    // Menu Upcomming Class
    else if(_s.id == 'studentAnalysis'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 75);
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 80);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if(_s.id == 'upcommingList'){
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { cur_gb_cur_ListDataShow = data.record; $sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupUpcommingClassDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'MinimumStudent'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, _gb.ll(ic, _s), 47);
        }
    } else if(_s.id == 'upcommingAnalysis'){
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { cur_gb_cur_ListDataShow = data.record; $sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupClassDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'MinimumStudent'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, _gb.ll(ic, _s), 47);
        } else if(_s._e[ic + 1] == 'ScheduleId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'FinishDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'CashCollection'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, _gb.ll(ic, _s), 48);
        } 
    } else if(_s.id == 'studentAnalysis'){
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 75);
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 80);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    }
    // Menu Class 
    else if (_s.id == 'classList') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { cur_gb_cur_ListDataShow = data.record; $sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupClassDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'TotalTime'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%', display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 46);
        } else if(_s._e[ic + 1] == 'Comment'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%', display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, ll, 47);
        } else if(_s._e[ic + 1] == 'StartRegisterDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, ll, 53);
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'FinishDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'ScheduleId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'StatusId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { var el = ''; if(data.record.f|3| == 2){ el = '<strong style=\"color:red;\">' + data.record.f|2| + '</strong>'; } else if(data.record.f|3| == 3){ el = '<strong style=\"color:blue;\">' + data.record.f|2| + '</strong>';} else if (data.record.f|2| !=null) {  el = '<strong>' + data.record.f|2| + '</strong>';}  return el; }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '', ii[1]);
        }
    } else if (_s.id == 'studentSpecialNote') {
        if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'TotalTime'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%', display: function (data) { return ((data.record.f|2| ==null)?'':data.record.f|2|+'') }  } ").f2(f0, ll, 46);
        } else if(_s._e[ic + 1] == 'Comment'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%', display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, ll, 47);
        } else if(_s._e[ic + 1] == 'StartRegisterDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(f0, ll, 53);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { cur_gb_cur_ListDataShow = data.record; $sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f18 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupClassDetail(ev, data.record.f18, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if (_s.id == 'classEventList') {
        if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { cur_gb_cur_ListDataShow = data.record; $sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupClassDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll,  (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'Customer'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 33);
        } else if(_s._e[ic + 1] == 'FromDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'ToTime'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'searchBirthday') {
        if(_s._e[ic + 1] == 'Name'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Birthday'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    } else if (_s.id == 'classTakeCare') {
        if(_s._e[ic + 1] == 'isPrgCreateDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy HH:mm:ss') }  } ").f2(f0, ll, ii[1]);
        }
    }
    // Menu AL
    else if(_s.id == 'ALList'){
        if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { cur_gb_cur_ListDataShow = data.record; $sDetail = $('<a href=\"javascript:;\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupClassALDetail(ev, data.record.f00, data.record.f18, this)}); return $sDetail }  } ").f2(f0, ll,  (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'TotalMonth'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 87);
        } else if(_s._e[ic + 1] == 'HourOut'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) + ':' + ((data.record.f|3| ==null)?'':(data.record.f|3|+'')) }  } ").f2(f0, ll, ii[1], (parseInt(ii[1]) + 1) + "");
        } else if(_s._e[ic + 1] == 'StartDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'FinishDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'InvoiceDate'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '25%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'ReasonId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 89);
        } else if(_s._e[ic + 1] == 'CourseId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '15%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if(_s.id == 'listAttendanceAL'){
        if(_s._e[ic + 1] == 'ClassId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        } else if(_s._e[ic + 1] == 'HourIn'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) + ':' + ((data.record.f|3| ==null)?'':(data.record.f|3|+''))}  } ").f2(f0, ll, ii[1], (parseInt(ii[1]) + 1) + "");
        } else if(_s._e[ic + 1] == 'HourOut'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) + ':' + ((data.record.f|3| ==null)?'':(data.record.f|3|+'')) }  } ").f2(f0, ll, ii[1], (parseInt(ii[1]) + 1) + "");
        } else if(_s._e[ic + 1] == 'Date'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if(_s._e[ic + 1] == 'Keep01'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 54);
        } else if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '20%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f61 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    } else if (_s.id == 'scheduleAdvanceList' && _s._e[ic + 1] == 'Name') {
        v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%', display: function (data) { $sDetail = $('<a style=\"font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupScheduleAdvance(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
    } else if (_s.id == 'assignStudent' ){ 
        if( _s._e[ic + 1] == 'Comment') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '10%', display: function (data) { $sDetail = $('<a style=\"font-size: 16px\" data-id=\"' + data.record.f00 + '\">Edit</a>'); $sDetail.click(function(ev){showPopupEditClassStudent(ev, data.record, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if( _s._e[ic + 1] == 'InvoiceId') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 75);
        } else if(_s._e[ic + 1] == 'LevelId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, 77);
        } else if(_s._e[ic + 1] == 'SpeakingId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 80);
        } else if(_s._e[ic + 1] == 'isPrgAccountId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '3%', display: function (data) { return ((data.record.f|2| ==null)?'':(data.record.f|2|+'')) }  } ").f2(f0, ll, 61);
        } else if(_s._e[ic + 1] == 'StudentId'){
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '30%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f81 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f01 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){cur_gb_cur_ListDataShow = data.record; cur_gb_cur_ListDataShow.sN = data.record.f57; console.log(data.record); showPopupStudentDetail(ev, data.record.f01, this)}); return $sDetail }  } ").f2(f0, ll, (parseInt(ii[7]) - 1) + '');
        }
    }
    return v;
};
function postonItemClick(itemLI, tBox, index) {
    // Gán điểm cho học viên Trong popup edit Class
    if (tBox == 'dmTypei') {
        gb_strFilterGrid = ' classid = ' + cur_gb_KeyId + ' and typeid = ' + $('#dmTypeh').val();
        request({
            a: 'pGetGBL_TRAINNING_COURSE_CLASS_STUDENT_MARK', c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: gb_strFilterGrid
        }, {
            success: function(o, op, r){
                if(r.Result == "OK"){
                    var $el = $('<table style="margin: 0 auto; width: 96%"/>');
                    $el.append('<thead><tr><th>No.</th><th>Student</th><th>Type</th><th>Speaking</th><th>Listening</th><th>Reading</th><th>Grammer</th><th>Writing</th></tr></thead>');
                    var $tbody = $('<tbody />');
                    for(var i = 0; i < r.Records.length; i++){
                        $tbody.append('<tr data-id="' + r.Records[i][1] + '"><td>' + (i + 1) + '</td>' +
                            '<td>' + (r.Records[i][27] == null ? '' : r.Records[i][27]) + '</td>' + 
                            '<td>' +  (r.Records[i][28] == null ? '' : r.Records[i][28]) + '</td>' + 
                            '<td><input type="text" autocomplete="off" class=" speaking" value="' +  ((r.Records[i][5] == null || r.Records[i][5] == "") ? '%' : r.Records[i][5]) + '"></td>' +
                            '<td><input type="text" autocomplete="off" class=" listening" value="' +  ((r.Records[i][6] == null || r.Records[i][6] == "") ? '%' : r.Records[i][6]) + '"></td>' +
                            '<td><input type="text" autocomplete="off" class=" reading" value="' +  ((r.Records[i][7] == null || r.Records[i][7] == "") ? '%' : r.Records[i][7]) + '"></td>' +
                            '<td><input type="text" autocomplete="off" class=" grammer" value="' +  ((r.Records[i][8] == null || r.Records[i][8] == "") ? '%' : r.Records[i][8]) + '"></td>' +
                            '<td><input type="text" autocomplete="off" class=" writing" value="' +  ((r.Records[i][9] == null || r.Records[i][9] == "") ? '%' : r.Records[i][9]) + '"></td>' +
                            '</tr>');
                    }
                    if(r.Records.length > 0) $tbody.append('<tr><td></td><td><input type="button" class="btn btn-primary btn-sm" id="updateMark" value="Update"></td>' + 
                            '<td></td>' + 
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '</tr>');
                    $('#studentMarkDivData').html('');
                    $('#studentMarkDivData').append($el.append($tbody));
                    $('#updateMark').click(function(){
                        var d = [];
                        var c = []
                        $('#studentMarkDivData table tbody tr').each(function(i, el){
                            if(typeof $(el).attr('data-id') != 'undefined'){
                                c.push({Id: $(el).attr('data-id')});
                                d.push({
                                    Speaking: $(el).find('input.speaking').val(),
                                    Listening: $(el).find('input.listening').val(),
                                    Reading: $(el).find('input.reading').val(),
                                    Grammer: $(el).find('input.grammer').val(),
                                    Writing: $(el).find('input.writing').val()
                                });
                            }
                        });
                        request({a: 'UpdateMarks', c: c, d: d }, {
                            success: function(o1, op1, r1){
                                if(r1.Result == "OK"){
                                    alert('Bạn đã cập nhập điểm thành công');
                                } else {
                                    alert('Bạn đã cập nhập điểm thất bại');
                                }
                            }
                        });
                    });
                }
            }
        });
    }
    // Menu Student: Student List
    else if((tBox == "dmstudentList_Stausi" || tBox == "dmstudentList_Sexi" || tBox == "dmstudentList_AOFirsti"
            || tBox == "dmstudentList_StudentGroupi" || tBox == "dmstudentList_Sourcei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
    }
    // Menu Student: Student Studying
    else if((tBox == "dmstudentStudying_Stausi" || tBox == "dmstudentStudying_Sexi" || tBox == "dmstudentStudying_AOFirsti"
            || tBox == "dmstudentStudying_StudentGroupi" || tBox == "dmstudentStudying_Sourcei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
    }
    // Menu Student: Student old
    else if((tBox == "dmstudentOld_Stausi" || tBox == "dmstudentOld_Sexi" || tBox == "dmstudentOld_AOFirsti"
            || tBox == "dmstudentOld_StudentGroupi" || tBox == "dmstudentOld_Sourcei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
    }
    // Menu Student: Student not
    else if((tBox == "dmstudentNot_Stausi" || tBox == "dmstudentNot_Sexi" || tBox == "dmstudentNot_AOFirsti"
            || tBox == "dmstudentNot_StudentGroupi" || tBox == "dmstudentNot_Sourcei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
    }
    else if (tBox == "fquaStudent02i") {
        if (cur_gb_DataDivShow[index][4] != null) $("#fquaStudent06i").val(cur_gb_DataDivShow[index][4]);
        if (cur_gb_DataDivShow[index][5] != null) $("#fquaStudent06h").val(cur_gb_DataDivShow[index][5]);
    }
    // Menu Student: Enrollment
    else if(tBox == "fenrollment18i"){
        if(cur_gb_DataDivShow[index][4] != null) $("#fenrollment35i").val(cur_gb_DataDivShow[index][4]);
        $("#fenrollment35i").trigger('keyup');
    }
    // Menu Student: Enrollment
    else if(tBox == "fenrollment05i"){
        if(cur_gb_DataDivShow[index][3] != null) $("#fenrollment06i").val(cur_gb_DataDivShow[index][3]);
    }
    // Menu Student: Enrollment AL
    else if(tBox == "fstudentEnrollAL07i"){
        if(cur_gb_DataDivShow[index][3] != null) $("#fstudentEnrollAL35i").val(cur_gb_DataDivShow[index][3]);
        $("#fstudentEnrollAL35i").trigger('keyup');
    }
    // Menu Student: Enrollment AL
    else if(tBox == "fstudentEnrollAL05i"){
        if(cur_gb_DataDivShow[index][3] != null) $("#fstudentEnrollAL06i").val(cur_gb_DataDivShow[index][3]);
    }
    // Menu Student: Inquiry
    else if((tBox == "dminquires_Typei" || tBox == "dminquires_Sexi" || tBox == "dminquires_AOFirsti"
            || tBox == "dminquires_StudentGroupi" || tBox == "dminquires_StuIdi")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Student: Placement Test
    else if((tBox == "dmplacementTest_Resulti" || tBox == "dmplacementTest_AOFirsti"
            || tBox == "dmplacementTest_StudentGroupi" || tBox == "dmplacementTest_StuIdi")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Student: Enrollment List
    else if((tBox == "dmenrollmentList_Stausi" || tBox == "dmenrollmentList_Sexi" || tBox == "dmenrollmentList_AOFirsti"
            || tBox == "dmenrollmentList_StudentGroupi" || tBox == "dmenrollmentList_StuIdi")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Student: Attendent AL
    else if((tBox == "dmattendanceAL_Classi")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Class: Class List
    else if((/*tBox == "dmupcommingList_Stausi" || */tBox == "dmupcommingList_Coursei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Class: Class List
    else if((tBox == "dmclassList_Stausi" || tBox == "dmclassList_Coursei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Class: assign TA
    else if(tBox == "fassignTA03i"){
        if(cur_gb_DataDivShow[index][3] != null){
            $("#fassignTA11i").val(c(cur_gb_DataDivShow[index][3]));
        }
    }
    // Menu Class: Add Class
    else if(tBox == "faddClass01i"){
        if(cur_gb_DataDivShow[index][3] != null){
            $("#faddClass13i").val(c(cur_gb_DataDivShow[index][3]));
        }
        if(cur_gb_DataDivShow[index][4] != null){
            $("#faddClass06i").val(c(cur_gb_DataDivShow[index][4]));
        }
    }
    // Menu Class: Add Class
    else if((tBox == "dmaddClass_Stausi" || tBox == "dmaddClass_Coursei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Class: List Student of Class
    else if(tBox == "fassignStudent09i"){
            if(cur_gb_DataDivShow[index][1] == '1' || cur_gb_DataDivShow[index][1] == '2' || cur_gb_DataDivShow[index][1] == '3'){
                $("#fassignStudent26i").hide();
                $("#lassignStudent26i").removeClass("brlabel125").hide();
                $("#r11assignStudent").hide();
                $("#r18assignStudent").hide();
                $("#r17assignStudent").hide();
                $("#fassignStudent26i").trigger("keyup");
            } else if(cur_gb_DataDivShow[index][1] == '4' || cur_gb_DataDivShow[index][1] == '6'){
                $("#fassignStudent26i").show();
                $("#lassignStudent26i").addClass("brlabel125").show();
                $("#r11assignStudent").hide();
                $("#r18assignStudent").show();
                $("#r17assignStudent").hide();
                $("#fassignStudent26i").trigger("keyup");
            } else if(cur_gb_DataDivShow[index][1] == '5' || cur_gb_DataDivShow[index][1] == '7'){
                $("#fassignStudent26i").show();
                $("#lassignStudent26i").addClass("brlabel125").show();
                $("#r11assignStudent").show();
                $("#r18assignStudent").show();
                $("#r17assignStudent").show();
                $("#fassignStudent26i").trigger("keyup");
            }
    }
    // Menu Class: Search Birthday
    else if((tBox == "dmsearchBirthday_AOFirsti")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    // Menu Active Learning: AL Students
    else if((tBox == "dmALList_GroupCoursei" || tBox == "dmALList_Coursei")
            && typeof window[tBox.split("_")[0] + "_selectOption"] == 'function'){
            window[tBox.split("_")[0] + "_selectOption"]();
        
    }
    
};
/************************************************************************
* Xử lý gán Phieunhapkhoid cho input_detail (ngắt kernel.js)            *
*************************************************************************/
function pAssId(s) {
    //Class
    if (s.id == 'assignRoom') {
        $('#fassignRoom01h').val(cur_gb_KeyId);
    } else if (s.id == 'assignTeacher') {
        $('#fassignTeacher01h').val(cur_gb_KeyId);
    }else if (s.id == 'studentMark') {
        $('#fstudentMark01h').val(cur_gb_KeyId);
    }else if (s.id == 'assignEvent') {
        $('#fassignEvent01h').val(cur_gb_KeyId);
    }else if (s.id == 'assignStudent') {
        $('#fassignStudent02h').val(cur_gb_KeyId);
    }else if (s.id == 'assignTA') {
        $('#fassignTA01h').val(cur_gb_KeyId);
    } else if (s.id == 'cancelDate') {
        $('#fcancelDate01h').val(cur_gb_KeyId);
    } else if (s.id == 'teacherCancel') {
        $('#fteacherCancel01h').val(cur_gb_KeyId);
    } else if (s.id == 'tACancel') {
        $('#ftACancel01h').val(cur_gb_KeyId);
    } else if (s.id == 'classMarkType') {
        $('#fclassMarkType01h').val(cur_gb_KeyId);
    } else if (s.id == 'survey') {
        $('#fsurvey01h').val(cur_gb_KeyId);
    } else if (s.id == 'classTakeCare') {
        $('#fclassTakeCare01h').val(cur_gb_KeyId);
    }  
    // Add Class
    else if (s.id == 'addClass') {
        $("#faddClass18i").prop("checked", true);
        $("#faddClass19i").prop("checked", true);
        $("#faddClass20i").prop("checked", true);
        $("#faddClass21i").prop("checked", true);
        $("#faddClass22i").prop("checked", true);
        $("#faddClass23i").prop("checked", true);
        $("#faddClass02i").val("Not Ready");
        $("#faddClass02h").val("1");
        $("#faddClass14i").val("1");
        $("#faddClass16i").val("25");
    }
    else if (s.id == 'studentAction') {
        $('#fstudentAction01h').val(cur_gb_KeyId);
    }else if (s.id == 'enrollment') {
        $('#fenrollment01h').val(cur_gb_KeyId);
        $('#fenrollment35i').val('0');
        $('#fenrollment04i').val('0');
        $('#fenrollment09h').val('1');
        $("#fenrollment11i").val((new XDate()).toString("dd/MM/yyyy"));
        $('#fenrollment05i').val("VND");
        $('#fenrollment05h').val("1");
        $('#fenrollment06i').val("0.00004636");
        $('#fenrollment13i').val(cur_gb_Account);
        $('#fenrollment13h').val(cur_gb_AccountId);
    }else if (s.id == 'classStudent') {
        $('#fclassStudent01h').val(cur_gb_KeyId);
    }else if (s.id == 'studentEntranceTest') {
        $('#fstudentEntranceTest01h').val(cur_gb_KeyId);
    }else if (s.id == 'studentEnrollAL') {
        $('#fstudentEnrollAL01h').val(cur_gb_KeyId);
        $('#fstudentEnrollAL09h').val('1');
        $("#fstudentEnrollAL11i").val((new XDate()).toString("dd/MM/yyyy"));
        $('#fstudentEnrollAL05i').val("VND");
        $('#fstudentEnrollAL05h').val("1");
        $('#fstudentEnrollAL06i').val("0.00004636");
        $('#fstudentEnrollAL13i').val(cur_gb_Account);
        $('#fstudentEnrollAL13h').val(cur_gb_AccountId);
    }else if (s.id == 'assignClass') {
        $('#fassignClass01h').val(cur_gb_KeyId);
    }else if (s.id == 'quaStudent') {
        $('#fquaStudent01h').val(cur_gb_KeyId);
        $("#fquaStudent04i").val((new XDate()).toString("dd/MM/yyyy HH:mm:ss"));
        $("#fquaStudent03h").val(8);
    }else if (s.id == 'addMarkStudent') {
        $('#faddMarkStudent01h').val(cur_gb_KeyId);
    }else if (s.id == 'doiDiemStudent') {
        $('#fdoiDiemStudent01h').val(cur_gb_KeyId);
    }else if (s.id == 'studentTakeCare') {
        $('#fstudentTakeCare01h').val(cur_gb_KeyId);
    }else if (s.id == 'attendanceAL') {
        $('#fattendanceAL01h').val(cur_gb_KeyId);
    }
    //schedule
    else if (s.id == 'scheduleAdvanceDetail') {
        $('#fscheduleAdvanceDetail01h').val(cur_gb_KeyId);
    }
    //
    
};
/************************************************************************
* Xử lý validate dữ liệu trước khi insert or update (ngắt kernel.js)    *
*************************************************************************/
function pVd(s) {
    var flag = false;
    //Student
    if (s.id == 'studentAdd') {
        if($("#fstudentAdd01i").val() == "" || $("#fstudentAdd03i").val() == "" || $("#fstudentAdd07h").val() == ""
            || $("fstudentAdd17h").val() == ""){
            modal.open({ content: "Bạn vui lòng nhập đủ thông tin" });
            flag = true;
        }
    } 
    else if (s.id == 'enrollment') {
        if($("#fenrollment03i").val() == "" || $("#fenrollment04i").val() == "" || $("#fenrollment36h").val() == ""
            || $("#fenrollment11i").val() == "" || $("#fenrollment13h").val() == "" || $("#fenrollment15h").val() == ""
            || $("#fenrollment18h").val() == ""){
            modal.open({ content: "Bạn vui lòng nhập đủ thông tin" });
            flag = true;
        }
    }
    else if (s.id == 'studentEnrollAL') {
        if($("#fstudentEnrollAL03i").val() == "" || $("#fstudentEnrollAL04i").val() == "" || $("#fstudentEnrollAL36h").val() == ""
            || $("#fstudentEnrollAL11i").val() == "" || $("#fstudentEnrollAL13h").val() == "" || $("#fstudentEnrollAL15h").val() == ""
            || $("#fstudentEnrollAL20i").val() == "" || $("#fstudentEnrollAL21i").val() == "" || $("#fstudentEnrollAL19i").val() == ""
            || $("#fstudentEnrollAL28i").val() == "" || $("#fstudentEnrollAL07h").val() == ""){
            modal.open({ content: "Bạn vui lòng nhập đủ thông tin" });
            flag = true;
        }
    }
    return flag;
};

/************************************************************************
* Xử lý sau khi insert update dữ liệu thành công                        *
*************************************************************************/
function p_us_o(s, t, r, o) {
    if(r.Result == "OK"){
        if(s.id == "studentAdd" && o.type == "i"){
            cur_gb_cur_ListDataShow = o.d;
            cur_gb_cur_ListDataShow.sN = o.d.f01;
            cur_gb_cur_ListDataShow.f00 = r.Records;
            showPopupStudentDetail("", r.Records, "#newStudent");
            $("#newStudent").click();
        }
    }
};

function reportRequest(obj, opt){
    modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $.ajax({
        type: "POST",
        url: '../zgcReport/Report.ashx',
        crossDomain: true,
        data: JSON.stringify({ obj: obj }),
        success: function (msg) {
            modal.close();
            var $divall = $("<div></div>");
            var $div = $("<div class='input'></div>");
            var $divClear = $("<div style='clear: both; height: 20px'></div>");
            var $btnActive = $("<input type='button'  value='Đóng' id='gc_btnActiveForm' onclick='closeReport()' class='btn btn-info btn-sm' />");
            var $btnExport = $("<input type='button'  value='Export' id='gc_btnExportForm' onclick='convertTableToExcel()' class='btn btn-info btn-sm' />");
            var $btnPrint = $("<input type='button'  value='In' id='gc_btnPrintForm' onclick='PrintReport()'  class='btn btn-info btn-sm' />");
            var $btnNewTab = $("<a type='button' target='_blank' href='../QLHocVien/DefaultQLHocVien.aspx'  class='btn btn-info btn-sm' >New tab</a>");
            $div.append($btnActive);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnExport);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnPrint);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnNewTab);
            var $divcontent = $("<div id='gc_DivReceiptContent'></div>");
            $divcontent.html(msg);
            $divall.append($div);
            $divall.append($divClear);
            $divall.append($divcontent);
            modal.open({ content: $divall.html() });
            if(opt != null && typeof opt.success == "function"){
                opt.success(obj, opt, modal.$content());
            }
        },
        error: function () {
            alert("Error");
        }
    });
}
function closeReport(){
    modal.close();
}
function selectOption(elId, filter, table, field){
    $('#' + elId).click(function (ev) {
        $('#' + elId).select();
        test(elId, ev, filter, table, field);
    });
    $('#' + elId).keyup(function (ev) {
        if ($('#' + elId).val() == ''){
            //$('#' + elId).val('Select...');
            $('#' + elId.substr(0, elId.length - 1) + 'h').val('');
        }
        if($('#' + elId).val() == '' && $('#' + elId.substring(0, elId.length - 1) + 'h').val() == "" &&
            typeof window[elId.split("_")[0] + "_selectOption"] == 'function'){
            window[elId.split("_")[0] + "_selectOption"]();
        }
        test(elId, ev, filter, table, field);
    });
};



