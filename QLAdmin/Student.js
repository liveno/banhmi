﻿var studentList = {//GBL_TRAINING_STUDENT 
	_v: ['GBL_TRAINING_STUDENT','Id','Full Name','Email ','Phone','Branches','Contact Name','District','Gender','Student Group','Birthday','Nick Name','Address','Birth Place','School/Company','Profession','Status','Reason Study','AO Firstname','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,3,1,0,,,,", "38,38,3,5,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,","05,05,2,4,0,,,,|06,06,1,4,0,,,,","07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|08,08,2,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,","09,09,1,5,0,,,,|10,10,2,4,0,,,,","11,11,2,4,0,,,,|12,12,2,4,0,,,,","13,13,2,4,0,,,,|14,14,2,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,|16,16,2,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,|18,18,2,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","19,19,2,4,0,,,,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentList', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentAction = {//GBL_TRAINING_STUDENT_ACTION 
	_v: ['GBL_TRAINING_STUDENT_ACTION','Id','StudentId','Type','AO','Enroll','Register course','Schedule','Customer','Comment','Info','Code','isPrgAccountId','isPrgInUse','Date time','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_ACTION','Id','StudentId','TypeId','AOId','Enroll','CourseId','ScheduleId','Customer','Comment','Info','Code','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,26,", "13,13,3,5,0,,,,", "02,02,1,1,1,GBL_TRAINING_STUDENT_ACTION_TYPE,Name,27,|03,03,1,1,1,gcGobal_ACCOUNT_Account,Username,28,","04,04,1,9,0,,,,|05,05,1,1,1,GBL_TRAINNING_COURSE,Name,29,","06,06,1,1,1,GBL_TRAINNING_SCHEDULE,Name,30,|07,07,1,4,0,,,,","08,08,1,4,0,,,,","09,09,0,4,0,,,,", "10,10,0,4,0,,,,", "11,11,0,1,0,,,,", "12,12,0,1,0,,,,", "14,14,0,1,0,,,,", "15,15,0,1,0,,,,", "16,16,0,1,0,,,,", "17,17,0,1,0,,,,", "18,18,0,1,0,,,,", "19,19,0,4,0,,,,", "20,20,0,4,0,,,,", "21,21,0,4,0,,,,", "22,22,0,1,0,,,,", "23,23,0,1,0,,,,", "24,24,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 376, _sy: 'dlg_cl', id: 'studentAction', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentEntranceTest = {//GBL_TRAINING_STUDENT_ENTRANCETEST 
	_v: ['GBL_TRAINING_STUDENT_ENTRANCETEST','Id','StudentId','Type','Listening','Reading','Writing','Speaking','Written GE','Sra','Grammar','Overall','Use Of English','Final Result','DateTest','Deadline For Payment','Teacher','Prefer Schedule','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_ENTRANCETEST','Id','StudentId','TypeId','Listening','Reading','Writing','Speaking','WrittenGE','Sra','Grammar','Overall','UseOfEnglish','FinalResultId','DateTest','DeadlineForPayment','TeacherId','PreferSchedule','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,33,", "02,02,1,1,1,GBL_TRAINING_STUDENT_ENTRANCETEST_TYPE,Name,34,","03,03,1,4,0,,,,|04,04,1,4,0,,,,","05,05,1,4,0,,,,|06,06,1,4,0,,,,","07,07,1,4,0,,,,|08,08,1,4,0,,,,","09,09,1,4,0,,,,|10,10,1,4,0,,,,","11,11,1,4,0,,,,|12,12,1,1,1,GBL_TRAINNING_COURSE,Name,35,","13,13,1,5,0,,,,|14,14,1,5,0,,,,","15,15,1,1,1,GBL_TRAINNING_TEACHER,Name,36,|16,16,1,4,0,,,,","17,17,1,4,0,,,,","18,18,0,1,0,,,,", "19,19,0,1,0,,,,", "20,20,0,5,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,1,0,,,,", "24,24,0,1,0,,,,", "25,25,0,1,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,1,0,,,,", "30,30,0,1,0,,,,", "31,31,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 351, _sy: 'dlg_cl', id: 'studentEntranceTest', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };

//var enrollment = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
//	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice Name','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date of pay','isPT','Employee','Schedule','Type Payment','Description','Cupon','Class Name','Date From','Total Time','Total Month','Date To','Reason','Student Special','Comment','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Level','Type ','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,58,|02,02,2,1,1,gcGobal_INCOM_Receipt,SoCT~Space01~DienGiai,59,|03,03,1,4,0,,,,","04,04,1,2,0,,,,|05,05,2,1,1,gcGobal_LITERAL_Currency,Name,60,","06,06,2,2,0,,,,|07,07,1,1,1,GBL_TRAINNING_COURSE,Name,61,(CourseTypeId = 1)|23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,","08,08,2,4,0,,,,|36,36,2,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,|09,09,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","10,10,1,4,0,,,,|11,11,1,5,0,,,,","12,12,2,9,0,,,,|13,13,2,1,1,gcGobal_ACCOUNT_Account,UserName,63,(macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))","14,14,2,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","16,16,1,4,0,,,,|17,17,2,4,0,,,,","18,18,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name,66,(courseid = $0)~#fenrollment07h|24,24,2,4,0,,,,", "19,19,1,5,0,,,,|20,20,2,2,0,,,,", "22,22,1,5,0,,,,","21,21,0,2,0,,,,","25,25,0,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "28,28,0,5,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "35,35,0,1,0,,,,", "37,37,0,2,0,,,,", "38,38,0,2,0,,,,","39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "41,41,0,5,0,,,,", "42,42,0,1,1,gcGobal_INCOM_Receipt,MaCT,75,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "45,45,0,5,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,0,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
//	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'enrollment', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var enrollment = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice Name','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date of pay','Xuan','Employee','Schedule','Type Payment','Description','Cupon','Class Name','Date From','Total Time','Total Month','Date To','Reason','Student Special','Modified-By','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Price','Type ','Keep01','Discount','Keep03','Keep04','Finish PT','Keep06','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','Modified Date','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "45,45,3,5,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,58,|02,02,0,2,1,gcGobal_INCOM_Receipt,MaCT,59,|03,03,1,4,0,,,,|35,35,2,1,0,,,,|05,05,1,1,1,gcGobal_LITERAL_Currency,Name~Space01-0,60,", "07,07,2,1,1,GBL_TRAINNING_COURSE,Name,61,(CourseTypeId = 1)|38,38,2,4,0,,,,|06,06,2,2,0,,,,","08,08,2,4,0,,,,|04,04,1,2,0,,,,","23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,", "10,10,1,4,0,,,,|36,36,1,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,","09,09,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,2,9,0,,,,|11,11,1,5,0,,,,", "24,24,2,4,0,,,,|13,13,2,1,1,gcGobal_ACCOUNT_Account,UserName,63,((isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))", "18,18,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name~StatusIdName~PricePerStudent,66,(statusid != 4 and statusid != 5 and courseid = $0)~#fenrollment07h|14,14,0,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","16,16,1,4,0,,,,", "17,17,0,4,0,,,,","19,19,2,5,0,,,,|20,20,2,2,0,,,,", "22,22,2,5,0,,,,|41,41,0,5,0,,,,","21,21,0,2,0,,,,","25,25,3,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "28,28,0,5,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "37,37,0,2,0,,,,", "39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "42,42,0,2,0,,,,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,3,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'enrollment', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };

//var enrollmentList = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
//	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date','isPT','AO Employee','Schedule','Type Payment','Description','Cupon','Sex','Phone','Total Time','Total Month','Status','Reason','Student Special','Comment','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Price','Level ','AO First','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_d: ["00,00,0,1,0,,,,", "11,11,1,5,0,,,,", "02,02,2,1,1,gcGobal_INCOM_Receipt,SoCT~Space01~DienGiai,59,|03,03,1,4,0,,,,|01,01,1,1,1,GBL_TRAINING_STUDENT,Name,58,","04,04,1,2,0,,,,|05,05,2,1,1,gcGobal_LITERAL_Currency,Name,60,","16,16,1,4,0,,,,","10,10,1,4,0,,,,","06,06,2,2,0,,,,|07,07,1,1,1,GBL_TRAINNING_COURSE,Name,61,|23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,","08,08,2,4,0,,,,|09,09,2,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,2,9,0,,,,","14,14,2,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","17,17,2,4,0,,,,","18,18,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name,66,|24,24,2,4,0,,,,", "19,19,1,5,0,,,,|20,20,2,2,0,,,,", "22,22,1,5,0,,,,","21,21,0,2,0,,,,","25,25,0,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "28,28,0,5,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "36,36,1,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,", "37,37,1,2,0,,,,", "13,13,1,1,1,gcGobal_ACCOUNT_Account,Id,63,", "38,38,0,2,0,,,,","39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "41,41,0,5,0,,,,", "42,42,0,1,1,gcGobal_INCOM_Receipt,MaCT,75,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "45,45,0,5,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,0,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
//	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'enrollmentList', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var enrollmentList = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice Name','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date','Xuan','Employee','Schedule','Type Payment','Description','Cupon','Sex','Phone','Total Time','Total Month','Status','Reason','Student Special','Modified By','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Price','Type ','AO First','Discount','Keep03','Keep04','Finish PT','Keep06','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','Modified Date','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "45,45,0,5,0,,,,|11,11,1,5,0,,,,|03,03,1,4,0,,,,", "01,01,1,1,1,GBL_TRAINING_STUDENT,Name,58,|02,02,0,2,1,gcGobal_INCOM_Receipt,MaCT,59,|35,35,0,1,0,,,,|04,04,1,2,0,,,,","16,16,1,4,0,,,,|17,17,2,4,0,,,,", "10,10,1,4,0,,,,", "07,07,1,1,1,GBL_TRAINNING_COURSE,Name,61,(CourseTypeId = 1)|38,38,2,4,0,,,,","08,08,2,4,0,,,,|05,05,0,1,1,gcGobal_LITERAL_Currency,Name,60,","06,06,0,2,0,,,,|23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,", "18,18,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name~StatusIdName~PricePerStudent,66,(courseid = $0)~#fenrollment07h", "19,19,1,5,0,,,,", "22,22,1,5,0,,,,", "36,36,1,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,","09,09,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,2,9,0,,,,", "24,24,2,4,0,,,,", "14,14,0,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","20,20,2,2,0,,,,", "41,41,2,5,0,,,,","21,21,0,2,0,,,,","25,25,0,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "28,28,0,5,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "37,37,1,2,0,,,,|13,13,1,1,1,gcGobal_ACCOUNT_Account,UserName,63,(macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))", "39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "42,42,0,2,0,,,,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,0,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'enrollmentList', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };

var studentAdd = {//GBL_TRAINING_STUDENT 
	_v: ['GBL_TRAINING_STUDENT','Id','Full Name (<span style="color: red">*</span>)','Email ','Phone 1 (<span style="color: red">*</span>)','Branches','Phone 2','District','Gender (<span style="color: red">*</span>)','Type','Birthday','Nick Name','Address','Birth Place','School/Company','Profession','Status','Reason Study','AO Firstname (<span style="color: red">*</span>)','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','Customer','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,3,1,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|05,05,2,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,", "07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|09,09,3,5,0,,,,","11,11,2,4,0,,,,|06,06,1,4,0,,,,", "17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,((isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))|15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,","08,08,1,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,|35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,","10,10,0,4,0,,,,","19,19,2,4,0,,,,","12,12,0,4,0,,,,","13,13,0,4,0,,,,|14,14,0,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","16,16,0,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","18,18,0,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "38,38,3,5,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentAdd', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
//var studentDetail = {//GBL_TRAINING_STUDENT 
//	_v: ['GBL_TRAINING_STUDENT','Id','Full Name','Email ','Phone 1','Branches','Phone 2','District','Gender','Student Group','Birthday','Nick Name','Address','Birth Place','School/Company','Profession','Status','Reason Study','AO Firstname','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_d: ["00,00,3,1,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,","05,05,2,4,0,,,,|06,06,1,4,0,,,,","07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|08,08,2,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,","09,09,1,5,0,,,,|10,10,2,4,0,,,,","11,11,2,4,0,,,,|12,12,2,4,0,,,,","13,13,1,4,0,,,,|14,14,2,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,|16,16,2,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,|18,18,2,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","19,19,2,4,0,,,,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "38,38,3,5,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
//	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentDetail', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentDetail = {//GBL_TRAINING_STUDENT 
	_v: ['GBL_TRAINING_STUDENT','Id','Full Name (<span style="color: red">*</span>)','Email ','Phone 1 (<span style="color: red">*</span>)','Branches','Phone 2','District','Gender (<span style="color: red">*</span>)','Type','Birthday','Nick Name','Address','Birth Place','School/Company','Profession','Status','Reason Study','AO Firstname (<span style="color: red">*</span>)','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','Customer','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,3,1,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|05,05,2,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,", "07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|09,09,3,5,0,,,,","11,11,2,4,0,,,,|06,06,1,4,0,,,,", "17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,((isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))|15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,","08,08,2,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,|35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,","10,10,0,4,0,,,,","19,19,2,4,0,,,,","12,12,0,4,0,,,,","13,13,0,4,0,,,,|14,14,0,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","16,16,0,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","18,18,0,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "38,38,3,5,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentDetail', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };

var classStudent = {//GBL_TRAINNING_COURSE_CLASS 
	_v: ['GBL_TRAINNING_COURSE_CLASS','Id','Course','Status','Schedules','Join Date','Class Name','Total Time','Start Register Date','Finish Date','Branch','Type','Old Class','Location','Price Per Student','Minimum students','Cash collection','Maximum students','Class Promotion','Speaking','Listening','Reading','Grammar','Writing','Oral','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS','Id','CourseId','StatusId','ScheduleId','StartDate','Name','TotalTime','StartRegisterDate','FinishDate','BranchId','TypeId','OldClassId','LocationId','PricePerStudent','MinimumStudent','CashCollection','MaximumStudent','IsPromotion','Speaking','Listening','Reading','Grammar','Writing','Oral','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "04,04,1,5,0,,,,", "01,01,1,1,1,GBL_TRAINNING_COURSE,Name,40,|05,05,1,4,0,,,,|02,02,1,1,1,GBL_TRAINNING_COURSE_CLASS_STATUS,Name,41,","03,03,1,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,42,","06,06,0,2,0,,,,","07,07,0,5,0,,,,|08,08,0,5,0,,,,","09,09,0,1,1,gcGobal_COMP_Branch,NAME,43,|10,10,0,1,1,GBL_TRAINNING_COURSE_CLASS_TYPE,Name,44,","11,11,0,1,1,GBL_TRAINNING_COURSE_CLASS,Name,45,","12,12,0,1,1,GBL_TRAINNING_COURSE_CLASS_LOCATION,Name,46,|13,13,0,2,0,,,,","14,14,0,1,0,,,,|15,15,0,2,0,,,,","16,16,0,1,0,,,,|17,17,0,9,0,,,,","18,18,0,9,0,,,,|19,19,0,9,0,,,,","20,20,0,9,0,,,,|21,21,0,9,0,,,,","22,22,0,9,0,,,,|23,23,0,9,0,,,,","24,24,0,4,0,,,,","25,25,0,1,0,,,,", "26,26,0,1,0,,,,", "27,27,0,5,0,,,,", "28,28,0,1,0,,,,", "29,29,0,1,0,,,,", "30,30,0,1,0,,,,", "31,31,0,1,0,,,,", "32,32,0,1,0,,,,", "33,33,0,4,0,,,,", "34,34,0,4,0,,,,", "35,35,0,4,0,,,,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "38,38,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 360, _sy: 'dlg_cl', id: 'classStudent', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', }
var assignClass = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Class Name','Start Date','Finish Date','Status ','Reason','Customer','Student Special','Invoice','Invoice Name','Invoice Date','Amount','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','ClassId','StartDate','FinishDate','StatusId','ReasonId','Customer','StudentSpecial','InvoiceId','Invoice','InvoiceDate','Amount','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,29,|02,02,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name,30,","03,03,1,5,0,,,,|04,04,1,5,0,,,,","05,05,1,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,31,|06,06,1,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,32,","07,07,1,4,0,,,,|08,08,1,4,0,,,,","09,09,1,1,1,gcGobal_INCOM_Receipt,SoCT~Space01,33,|10,10,1,4,0,,,,","11,11,1,5,0,,,,|12,12,1,2,0,,,,","13,13,1,4,0,,,,","14,14,0,1,0,,,,", "15,15,0,1,0,,,,", "16,16,0,5,0,,,,", "17,17,0,1,0,,,,", "18,18,0,1,0,,,,", "19,19,0,1,0,,,,", "20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,4,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,1,0,,,,", "26,26,0,1,0,,,,", "27,27,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'assignClass', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
//var studentEnrollAL = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
//	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice Name','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date of pay','isPT','Employee','Schedule','Type Payment','Description','Cupon','Class Name','Date From','Total Time','Total Month','Date To','Reason','Student Special','Comment','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Level','Type ','Keep01','Keep02','Keep03','Special Note','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
//	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,58,|02,02,2,1,1,gcGobal_INCOM_Receipt,SoCT~Space01~DienGiai,59,|03,03,1,4,0,,,,","04,04,1,2,0,,,,|05,05,2,1,1,gcGobal_LITERAL_Currency,Name,60,","06,06,2,2,0,,,,|07,07,1,1,1,GBL_TRAINNING_COURSE,Name,61,(CourseTypeId = 2)|23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,","35,35,0,1,0,,,,|13,13,2,1,1,gcGobal_ACCOUNT_Account,UserName,63,(macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))|34,34,2,4,0,,,,","18,18,3,1,1,GBL_TRAINNING_COURSE_CLASS,Name,66,|14,14,1,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,", "20,20,2,2,0,,,,|21,21,2,2,0,,,,", "19,19,1,5,0,,,,|22,22,1,5,0,,,,", "28,28,2,5,0,,,,|08,08,2,4,0,,,,", "11,11,1,5,0,,,,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,", "40,40,2,4,0,,,,|10,10,2,4,0,,,,","16,16,1,4,0,,,,|17,17,1,4,0,,,,","25,25,0,4,0,,,,", "09,09,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,0,9,0,,,,", "24,24,0,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "29,29,2,1,1,GBL_TRAINNING_COURSE,Name,68,|30,30,2,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,2,1,1,GBL_TRAINNING_COURSE,Name,70,|32,32,2,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "36,36,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,", "37,37,0,2,0,,,,", "38,38,0,2,0,,,,", "39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "41,41,0,5,0,,,,", "42,42,0,1,1,gcGobal_INCOM_Receipt,MaCT,75,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "45,45,0,5,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,0,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
//	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'studentEnrollAL', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentEnrollAL = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice Name','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date of pay','Xuan','Employee','Schedule','Type Payment','Description','Cupon','Class Name','Date From','Total Time','Total Month','Date To','Reason','Student Special','Modified-By','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Price','Type ','Keep01','Discount','Keep03','Keep04','Finish PT','Keep06','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','Modified Date','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "45,45,3,5,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,58,|02,02,0,2,1,gcGobal_INCOM_Receipt,MaCT,59,|03,03,1,4,0,,,,|35,35,2,1,0,,,,|05,05,1,1,1,gcGobal_LITERAL_Currency,Name~Space01-0,60,", "07,07,2,1,1,GBL_TRAINNING_COURSE,Name~Space01,61,(CourseTypeId = 2)|38,38,2,4,0,,,,|06,06,2,2,0,,,,","08,08,2,4,0,,,,|04,04,1,2,0,,,,","23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,", "10,10,1,4,0,,,,|36,36,1,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,","09,09,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,2,9,0,,,,|11,11,1,5,0,,,,", "24,24,2,4,0,,,,|13,13,2,1,1,gcGobal_ACCOUNT_Account,UserName,63,((isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))", "18,18,0,1,1,GBL_TRAINNING_COURSE_CLASS,Name~StatusIdName~PricePerStudent,66,(statusid != 4 and statusid != 5 and courseid = $0)~#fenrollment07h|14,14,0,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|17,17,2,4,0,,,,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","16,16,1,4,0,,,,","20,20,1,2,0,,,,|21,21,2,2,0,,,,", "19,19,2,5,0,,,,|22,22,2,5,0,,,,", "28,28,2,5,0,,,,|41,41,0,5,0,,,,","25,25,3,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "37,37,0,2,0,,,,", "39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "42,42,0,2,0,,,,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,3,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'studentEnrollAL', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var inquires = {//GBL_TRAINING_STUDENT_ACTION 
	_v: ['GBL_TRAINING_STUDENT_ACTION','Id','Student Name','Type','AO','Sex','Phone','Status','AO First','Comment','Info','Code','isPrgAccountId','isPrgInUse','Date time','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_ACTION','Id','StudentId','TypeId','AOId','Enroll','CourseId','ScheduleId','Customer','Comment','Info','Code','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "13,13,1,5,0,,,,", "02,02,1,1,1,GBL_TRAINING_STUDENT_ACTION_TYPE,Name,27,","01,01,1,1,1,GBL_TRAINING_STUDENT,Name,26,","04,04,1,9,0,,,,|05,05,1,1,1,GBL_TRAINNING_COURSE,Name,29,","06,06,1,1,1,GBL_TRAINNING_SCHEDULE,Name,30,","08,08,1,4,0,,,,", "07,07,1,4,0,,,,","03,03,1,1,1,gcGobal_ACCOUNT_Account,Id,28,","09,09,0,4,0,,,,", "10,10,0,4,0,,,,", "11,11,0,1,0,,,,", "12,12,0,1,0,,,,", "14,14,0,1,0,,,,", "15,15,0,1,0,,,,", "16,16,0,1,0,,,,", "17,17,0,1,0,,,,", "18,18,0,1,0,,,,", "19,19,0,4,0,,,,", "20,20,0,4,0,,,,", "21,21,0,4,0,,,,", "22,22,0,1,0,,,,", "23,23,0,1,0,,,,", "24,24,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 376, _sy: 'dlg_cl', id: 'inquires', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var placementTest = {//GBL_TRAINING_STUDENT_ENTRANCETEST 
	_v: ['GBL_TRAINING_STUDENT_ENTRANCETEST','Id','Student Name','Type','Listening','Reading','Writing','Speaking','Written GE','Sra','Grammar','Overall','Use Of English','Result','Date Test','Deadline For Payment','Teacher','Schedule','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_ENTRANCETEST','Id','StudentId','TypeId','Listening','Reading','Writing','Speaking','WrittenGE','Sra','Grammar','Overall','UseOfEnglish','FinalResultId','DateTest','DeadlineForPayment','TeacherId','PreferSchedule','Comment','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "13,13,1,5,0,,,,", "01,01,1,1,1,GBL_TRAINING_STUDENT,Name,33,", "02,02,0,1,1,GBL_TRAINING_STUDENT_ENTRANCETEST_TYPE,Name,34,", "07,07,1,4,0,,,,","03,03,1,4,0,,,,|04,04,1,4,0,,,,","05,05,1,4,0,,,,|06,06,1,4,0,,,,","08,08,1,4,0,,,,","09,09,1,4,0,,,,|10,10,0,4,0,,,,","11,11,0,4,0,,,,|12,12,1,1,1,GBL_TRAINNING_COURSE,Name,35,","14,14,0,5,0,,,,","15,15,1,1,1,GBL_TRAINNING_TEACHER,Name,36,|16,16,1,4,0,,,,","17,17,1,4,0,,,,","18,18,0,1,0,,,,", "19,19,0,1,0,,,,", "20,20,0,5,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,1,0,,,,", "24,24,0,1,0,,,,", "25,25,0,1,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,1,0,,,,", "30,30,0,1,0,,,,", "31,31,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 351, _sy: 'dlg_cl', id: 'placementTest', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var upcomingClassEnrollList = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date of pay','isPT','AO Employee','Schedule','Type Payment','Description','Cupon','Class join','Phone','Total Time','Total Month','Status','Reason','Student Special','Comment','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Level','Type ','AO First','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "11,11,1,5,0,,,,", "02,02,2,1,1,gcGobal_INCOM_Receipt,SoCT~Space01~DienGiai,59,|03,03,1,4,0,,,,|04,04,1,2,0,,,,|01,01,1,1,1,GBL_TRAINING_STUDENT,Name,58,","19,19,1,5,0,,,,|05,05,2,1,1,gcGobal_LITERAL_Currency,Name,60,","16,16,0,4,0,,,,","10,10,0,4,0,,,,","06,06,2,2,0,,,,|07,07,1,1,1,GBL_TRAINNING_COURSE,Name,61,|23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,","08,08,2,4,0,,,,|09,09,2,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,2,9,0,,,,","14,14,1,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","17,17,2,4,0,,,,","18,18,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name,66,|24,24,2,4,0,,,,", "20,20,2,2,0,,,,", "22,22,0,5,0,,,,","21,21,0,2,0,,,,","25,25,0,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "28,28,0,5,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "35,35,0,1,0,,,,", "36,36,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,", "37,37,1,2,0,,,,", "13,13,0,1,1,gcGobal_ACCOUNT_Account,Id,63,", "38,38,0,2,0,,,,","39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "41,41,0,5,0,,,,", "42,42,0,1,1,gcGobal_INCOM_Receipt,MaCT,75,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "45,45,0,5,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,0,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'upcomingClassEnrollList', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var courseEnrollList = {//GBL_TRAINNING_COURSE_CLASS_STUDENT 
	_v: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','Student Name','Invoice','Invoice','Amount','Unit','Exe. Rate','Course','Person Pay','Status ','Note','Date of pay','isPT','AO Employee','Schedule','Type Payment','Description','Cupon','Class join','Phone','Total Time','Total Month','Status','Reason','Student Special','Comment','Amount Learned','Amount Keep','Date Expired','Speaking','Reading','Listening','Writing','Grammer','Guaranteed Score','Level','Type ','AO First','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINNING_COURSE_CLASS_STUDENT','Id','StudentId','InvoiceId','Invoice','Amount','UnitId','ExecuteRate','CourseId','PersonPay','StatusId','Note','InvoiceDate','isPT','EmployeeId','ScheduleId','TypePaymentId','Description','Cupon','ClassId','StartDate','TotalTime','TotalMonth','FinishDate','ReasonId','StudentSpecial','Comment','AmountLearned','AmountKeep','DateExpired','SpeakingId','ReadingId','ListeningId','WritingId','GrammerId','GuaranteedScore','LevelId','TypeId','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "11,11,1,5,0,,,,", "02,02,2,1,1,gcGobal_INCOM_Receipt,SoCT~Space01~DienGiai,59,|03,03,1,4,0,,,,|04,04,1,2,0,,,,|01,01,1,1,1,GBL_TRAINING_STUDENT,Name,58,","19,19,0,5,0,,,,|05,05,2,1,1,gcGobal_LITERAL_Currency,Name,60,","16,16,0,4,0,,,,","10,10,0,4,0,,,,","06,06,2,2,0,,,,|07,07,1,1,1,GBL_TRAINNING_COURSE,Name,61,|23,23,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_REASON,Name,67,","08,08,2,4,0,,,,|09,09,2,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_STATUS,Name,62,","12,12,2,9,0,,,,","14,14,1,1,1,GBL_TRAINNING_SCHEDULE_ADVANCE,Name,64,|15,15,2,1,1,gcGobal_LITERAL_PaymentType,Name,65,","17,17,2,4,0,,,,","18,18,0,1,1,GBL_TRAINNING_COURSE_CLASS,Name,66,|24,24,2,4,0,,,,", "20,20,2,2,0,,,,", "22,22,0,5,0,,,,","21,21,0,2,0,,,,","25,25,0,4,0,,,,", "26,26,0,2,0,,,,", "27,27,0,2,0,,,,", "28,28,0,5,0,,,,", "29,29,0,1,1,GBL_TRAINNING_COURSE,Name,68,", "30,30,0,1,1,GBL_TRAINNING_COURSE,Name,69,", "31,31,0,1,1,GBL_TRAINNING_COURSE,Name,70,", "32,32,0,1,1,GBL_TRAINNING_COURSE,Name,71,", "33,33,0,1,1,GBL_TRAINNING_COURSE,Name,72,", "34,34,0,4,0,,,,", "35,35,0,1,0,,,,", "36,36,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_TYPE,Name,73,", "37,37,1,2,0,,,,", "13,13,0,1,1,gcGobal_ACCOUNT_Account,Id,63,", "38,38,0,2,0,,,,","39,39,0,1,1,GBL_TRAINNING_COURSE_CLASS_STUDENT_PHI,Name,74,", "40,40,0,4,0,,,,", "41,41,0,5,0,,,,", "42,42,0,1,1,gcGobal_INCOM_Receipt,MaCT,75,", "43,43,0,1,0,,,,", "44,44,0,1,0,,,,", "45,45,0,5,0,,,,", "46,46,0,1,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", "50,50,0,1,0,,,,", "51,51,0,4,0,,,,", "52,52,0,4,0,,,,", "53,53,0,4,0,,,,", "54,54,0,1,0,,,,", "55,55,0,1,0,,,,", "56,56,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 380, _sy: 'dlg_cl', id: 'courseEnrollList', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var quaStudent = {//GBL_TRAINING_STUDENT_QUA 
	_v: ['GBL_TRAINING_STUDENT_QUA','Id','Student','Production','Stock','Date','Count','Unit','Description','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','Create User','ModifiedDate','ModifiedUser','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_QUA','Id','StudentId','Productid','KhoId','NgayTang','SoLuong','DonViTinhId','Description','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,23,|02,02,1,1,1,gcGobal_STOCK_gcProductList,Name~GiaMua~DonViTinhIdName~DonViTinhId-0,24,|03,03,3,1,1,gcGobal_STOCK_List,Name,25,|04,04,3,5,0,,,,","05,05,1,1,0,,,,|06,06,1,1,1,gcGobal_LITERAL_Unit,Name,26,","07,07,1,4,0,,,,","08,08,0,1,0,,,,", "09,09,0,1,0,,,,", "10,10,0,5,0,,,,", "11,11,0,1,0,,,,", "12,12,0,1,0,,,,", "13,13,0,1,0,,,,", "14,14,0,1,0,,,,", "15,15,0,1,0,,,,", "16,16,3,4,0,,,,", "17,17,3,4,0,,,,", "18,18,3,4,0,,,,", "19,19,0,1,0,,,,", "20,20,0,1,0,,,,", "21,21,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 401, _sy: 'dlg_cl', id: 'quaStudent', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var addMarkStudent = {//GBL_TRAINING_STUDENT_ADDMARK 
	_v: ['GBL_TRAINING_STUDENT_ADDMARK','Id','Student','Class','Type','Date','Mark','Description','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_ADDMARK','Id','StudentId','ClassId','TypeId','NgayTang','Mark','Description','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,22,|02,02,0,1,1,GBL_TRAINNING_COURSE_CLASS,Name,23,|03,03,1,1,1,GBL_TRAINING_STUDENT_ADDMARK_TYPE,Name,24,","04,04,1,5,0,,,,|05,05,1,1,0,,,,","06,06,1,4,0,,,,","07,07,0,1,0,,,,", "08,08,0,1,0,,,,", "09,09,0,5,0,,,,", "10,10,0,1,0,,,,", "11,11,0,1,0,,,,", "12,12,0,1,0,,,,", "13,13,0,1,0,,,,", "14,14,0,1,0,,,,", "15,15,0,4,0,,,,", "16,16,0,4,0,,,,", "17,17,0,4,0,,,,", "18,18,0,1,0,,,,", "19,19,0,1,0,,,,", "20,20,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 402, _sy: 'dlg_cl', id: 'addMarkStudent', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var doiDiemStudent = {//GBL_TRAINING_STUDENT_DOIDIEM 
	_v: ['GBL_TRAINING_STUDENT_DOIDIEM','Id','Student','Production','Stock','Date','Count','Unit','Price','Total','GiamGia','PhuThu','ThucThu','Description','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_DOIDIEM','Id','StudentId','Productid','KhoId','NgayLap','SoLuong','DonViTinhId','Gia','TongTien','GiamGia','PhuThu','ThucThu','Description','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,28,|02,02,1,1,1,gcGobal_STOCK_gcProductList,Name,29,","03,03,1,1,1,gcGobal_STOCK_List,Name,30,|04,04,1,5,0,,,,","05,05,1,1,0,,,,|06,06,1,1,1,gcGobal_LITERAL_Unit,Name,31,","07,07,1,2,0,,,,|08,08,1,2,0,,,,","09,09,0,2,0,,,,", "10,10,0,2,0,,,,", "11,11,0,2,0,,,,", "12,12,1,4,0,,,,","13,13,0,1,0,,,,", "14,14,0,1,0,,,,", "15,15,0,5,0,,,,", "16,16,0,1,0,,,,", "17,17,0,1,0,,,,", "18,18,0,1,0,,,,", "19,19,0,1,0,,,,", "20,20,0,1,0,,,,", "21,21,0,4,0,,,,", "22,22,0,4,0,,,,", "23,23,0,4,0,,,,", "24,24,0,1,0,,,,", "25,25,0,1,0,,,,", "26,26,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 400, _sy: 'dlg_cl', id: 'doiDiemStudent', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };

var attendanceAL = {//GBL_TRAINING_STUDENT_ATTENDANCEAL 
	_v: ['GBL_TRAINING_STUDENT_ATTENDANCEAL','Id','Student','Class','ShowOnlineReport','Mon','Tue','Wed','Thu','Fri','Sat','Sun','Date ','Date To','Time In',':','Time Out',':','Subject','Status','Level','Instructor','Lesson','Work Sheet','Lesson ','Note','Teacher Comment ','Work Book','Duration','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_ATTENDANCEAL','Id','StudentId','ClassId','ShowOnlineReport','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday','Date','DateTo','HourIn','MinuteIn','HourOut','MinuteOut','SubjectId','StatusId','LevelId','InstructorId','LessonId','WorksSheetId','Name','Note','Comment','WorkBook','Keep01','Keep02','Keep03','Keep04','Keep05','Keep06','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "01,01,0,1,1,GBL_TRAINING_STUDENT,Name,48,|02,02,1,1,1,GBL_TRAINNING_COURSE_CLASS,Name,49,(id in (select classid from gbl_trainning_course_class_student where studentid = $0 and courseid in (select id from gbl_trainning_course where coursetypeid = 2)))~#fattendanceAL01h|03,03,0,9,0,,,,|11,11,1,5,0,,,,","04,04,0,9,0,,,,|05,05,0,9,0,,,,|06,06,0,9,0,,,,","07,07,0,9,0,,,,|08,08,0,9,0,,,,|09,09,0,9,0,,,,|10,10,0,9,0,,,,","12,12,0,5,0,,,,","13,13,1,1,0,,,,|14,14,2,1,0,,,,|15,15,1,1,0,,,,|16,16,2,1,0,,,,","27,27,3,2,0,,,,","17,17,2,1,1,GBL_TRAINNING_SUBJECT,Name,50,|18,18,2,1,1,GBL_TRAINING_STUDENT_ATTENDANCEAL_STATUS,Name,51,","19,19,0,1,0,,,,|21,21,0,1,1,GBL_TRAINNING_LESSON,Name,53,","20,20,2,1,1,gcGobal_ACCOUNT_Account,UserName,52,|22,22,2,1,1,GBL_TRAINNING_WORKSHEET,Name,54,","23,23,1,4,0,,,,|24,24,1,4,0,,,,","25,25,1,4,0,,,,|26,26,2,4,0,,,,", "28,28,0,2,0,,,,", "29,29,0,2,0,,,,", "30,30,0,4,0,,,,", "31,31,0,5,0,,,,", "32,32,0,1,0,,,,", "33,33,0,1,0,,,,", "34,34,0,1,0,,,,", "35,35,0,5,0,,,,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "38,38,0,1,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,4,0,,,,", "42,42,0,4,0,,,,", "43,43,0,4,0,,,,", "44,44,0,1,0,,,,", "45,45,0,1,0,,,,", "46,46,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 413, _sy: 'dlg_cl', id: 'attendanceAL', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentOld = {//GBL_TRAINING_STUDENT 
	_v: ['GBL_TRAINING_STUDENT','Id','Full Name','Email ','Phone','Branches','Contact Name','Take care 1','Take care 2','Student Group','Take care 3','Nick Name','Address','Birth Place','School/Company','Profession','Take care 4','Reason Study','AO Firstname','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,3,1,0,,,,", "38,38,3,5,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,","05,05,2,4,0,,,,|06,06,1,4,0,,,,","07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|08,08,2,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,","09,09,1,5,0,,,,|10,10,2,4,0,,,,","11,11,2,4,0,,,,|12,12,2,4,0,,,,","13,13,2,4,0,,,,|14,14,2,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,|16,16,2,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,|18,18,2,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","19,19,2,4,0,,,,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentOld', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentNot = {//GBL_TRAINING_STUDENT 
	_v: ['GBL_TRAINING_STUDENT','Id','Full Name','Email ','Phone','Branches','Contact Name','Take Care 1','Take Care 2','Student Group','Take Care 3','Nick Name','Address','Birth Place','School/Company','Profession','Take Care 4','Reason Study','AO Firstname','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,3,1,0,,,,", "38,38,3,5,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,","05,05,2,4,0,,,,|06,06,1,4,0,,,,","07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|08,08,2,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,","09,09,1,5,0,,,,|10,10,2,4,0,,,,","11,11,2,4,0,,,,|12,12,2,4,0,,,,","13,13,2,4,0,,,,|14,14,2,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,|16,16,2,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,|18,18,2,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","19,19,2,4,0,,,,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentNot', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };
var studentStudying = {//GBL_TRAINING_STUDENT 
	_v: ['GBL_TRAINING_STUDENT','Id','Full Name','Email ','Phone','Branches','Contact Name','District','Gender','Student Group','Birthday','Nick Name','Address','Birth Place','School/Company','Profession','Status','Reason Study','AO Firstname','Source Info','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT','Id','Name','Email','Phone','BranchId','ContactName','District','GenderId','GroupId','Birthday','Nickname','Address','Birthplace','SchoolCompany','ProfessionId','StatusId','StudyReasonId','AOFirstNameId','SourceId','Comment','TypeId','ClassId','DiscountId','Code','Note','Info','Subject','Office','Module','Tax','Fax','Org','AgreeReason','RegisterDate','Fee','CustomerId','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,3,1,0,,,,", "38,38,3,5,0,,,,", "01,01,1,4,0,,,,|02,02,2,4,0,,,,","03,03,1,4,0,,,,|04,04,0,1,1,gcGobal_COMP_Branch,NAME,51,","05,05,2,4,0,,,,|06,06,1,4,0,,,,","07,07,1,1,1,gcGobal_LITERAL_Gender,Name,52,|08,08,2,1,1,GBL_TRAINING_STUDENT_GROUP,Name,53,","09,09,1,5,0,,,,|10,10,2,4,0,,,,","11,11,2,4,0,,,,|12,12,2,4,0,,,,","13,13,2,4,0,,,,|14,14,2,1,1,GBL_DANHMUC_NGHENGHIEP,Name,54,","15,15,1,1,1,GBL_TRAINING_STUDENT_STATUS,Name,55,|16,16,2,1,1,GBL_DANHMUC_MUCDICHHOC,Name,56,","17,17,1,1,1,gcGobal_ACCOUNT_Account,Username,57,|18,18,2,1,1,GBL_DANHMUC_NGUONTHONGTIN,Name,58,","19,19,2,4,0,,,,","20,20,0,1,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "23,23,0,4,0,,,,", "24,24,0,4,0,,,,", "25,25,0,4,0,,,,", "26,26,0,4,0,,,,", "27,27,0,4,0,,,,", "28,28,0,4,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,4,0,,,,", "33,33,0,5,0,,,,", "34,34,0,2,0,,,,", "35,35,2,1,1,gcGobal_CUST_Customer,HoTen,59,", "36,36,0,1,0,,,,", "37,37,0,1,0,,,,", "39,39,0,1,0,,,,", "40,40,0,1,0,,,,", "41,41,0,1,0,,,,", "42,42,0,1,0,,,,", "43,43,0,1,0,,,,", "44,44,0,4,0,,,,", "45,45,0,4,0,,,,", "46,46,0,4,0,,,,", "47,47,0,1,0,,,,", "48,48,0,1,0,,,,", "49,49,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 377, _sy: 'dlg_cl', id: 'studentStudying', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };

var studentTakeCare = {//GBL_TRAINING_STUDENT_TAKECARE 
	_v: ['GBL_TRAINING_STUDENT_TAKECARE','Id','Student','Type','Status','Class','Comment','KieuChu','KieuChu1','KieuChu2','KieuSo','KieuSo1','KieuSo2','KieuTien','KieuTien1','KieuTien2','KieuNgay','KieuNgay1','KieuNgay2','KieuBit','KieuBit1','KieuBit2','isPrgAccountId','isPrgInUse','Date','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','username/userid/departmentid/agentid/compid','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_e: ['GBL_TRAINING_STUDENT_TAKECARE','Id','StudentId','TypeId','StatusId','ClassId','Comment','KieuChu','KieuChu1','KieuChu2','KieuSo','KieuSo1','KieuSo2','KieuTien','KieuTien1','KieuTien2','KieuNgay','KieuNgay1','KieuNgay2','KieuBit','KieuBit1','KieuBit2','isPrgAccountId','isPrgInUse','isPrgCreateDate','isPrgWaitingConfirmStatus','isPrgbAdminDeleted','isPrgbUserDeleted','isPrgbShow','isPrgOrdered','isPrgVNKoDau','isPrgSmField','isPrgPartComp','isPrgEncriptData','isPrgDescriptData','isPrgAccountUpdateId',], 
	_d: ["00,00,0,1,0,,,,", "23,23,3,5,0,,,,|01,01,0,1,1,GBL_TRAINING_STUDENT,Name,36,|02,02,1,1,1,GBL_TRAINING_STUDENT_TAKECARE_TYPE,Name,37,|03,03,1,1,1,GBL_TRAINING_STUDENT_TAKECARE_STATUS,Name,38,","04,04,0,1,1,GBL_TRAINNING_COURSE_CLASS,Name,39,","05,05,1,4,0,,,,","06,06,0,4,0,,,,", "07,07,0,4,0,,,,", "08,08,0,4,0,,,,", "09,09,0,1,0,,,,", "10,10,0,1,0,,,,", "11,11,0,1,0,,,,", "12,12,0,2,0,,,,", "13,13,0,2,0,,,,", "14,14,0,2,0,,,,", "15,15,0,5,0,,,,", "16,16,0,5,0,,,,", "17,17,0,5,0,,,,", "18,18,0,9,0,,,,", "19,19,0,9,0,,,,", "20,20,0,9,0,,,,", "21,21,0,1,0,,,,", "22,22,0,1,0,,,,", "24,24,0,1,0,,,,", "25,25,0,1,0,,,,", "26,26,0,1,0,,,,", "27,27,0,1,0,,,,", "28,28,0,1,0,,,,", "29,29,0,4,0,,,,", "30,30,0,4,0,,,,", "31,31,0,4,0,,,,", "32,32,0,1,0,,,,", "33,33,0,1,0,,,,", "34,34,0,1,0,,,,", ], 
	_f: [],_r: [], _coj: {},__id: 435, _sy: 'dlg_cl', id: 'studentTakeCare', cssb:'inputBtnBottomV2',$dw: null, dwcss: 'login_sign', };





$(document).ready(function () {
        
    var isSearchStudentName = false;
    $("#searchStudentNameButton").click(function(){
        //tableToExcel('jtable', 'W3C Example Table')
        isSearchStudentName = true;
        gb_strFilterGrid = " (name like N'%" + $("#searchStudentName").val() + "%' or Phone like N'%" + $("#searchStudentName").val() + "%') ";
        $('#dropdown-student ul li a.a2').click();
    });
    $("#searchStudentName").keyup(function(ev){
        if(ev.which == 13){
            $("#searchStudentNameButton").click();
        }
    });
    $('#dropdown-student ul li').click(function () {
        $("#gc_ReportForm").html('');
        $('.content2').hide();
        $('#student-button').show();
        $('.sidebar-content').empty();
        $('.btn-group button').removeClass('active');
        $(this).addClass('active');
        var o = {};
        var on = '';
        var f = true;
        switch ($(this).find('a').attr('class')) {
            case 'a1':
                o = studentAdd; on='#studentAdd';
                gb_strFilterGrid = " ";
                $('.btn-group button a.a1').parent().addClass('active');
                _gb.pr(o).ff(f, o);
                _gb.rf(o);
                $("#fstudentAdd17i").val(cur_gb_Account);
                $("#fstudentAdd17h").val(cur_gb_AccountId);
                $("#fstudentAdd15i").val("Contact");
                $("#fstudentAdd15h").val(1);
                $("#fstudentAdd08i").val("Bình thường");
                $("#fstudentAdd08h").val(8);
                $("#studentAddu_").hide();
                $("#fstudentAdd19i").remove();
                $areanote = $("<textarea rows='5' id='fstudentAdd19i' class='form-control' style='width: 545px!important'></textarea>")
                $("#r08studentAdd").append($areanote);
                var lD = "<option value=''>Select</option>", 
                lM = "<option value=''>Select</option>", 
                lY = "<option value=''>Select</option>";
                for(var i = 1; i < 32; i++)
                    lD += "<option value=" + i + ">" + i + "</option>";
                for(var i = 1; i < 13; i++)
                    lM += "<option value=" + i + ">" + i + "</option>";
                for(var i = 1940; i < 2015; i++)
                    lY += "<option value" + i + ">" + i + "</option>";
                var $day = $("<select id='studentAddDay'>" + lD + "</select>");
                var $month = $("<select id='studentAddMonth'>" + lM + "</select>");
                var $year = $("<select id='studentAddYear'>" + lY + "</select>");
                $("#lstudentAdd09i").show();
                $("#r03studentAdd").append($day).append($month).append($year);
                view($(this), $('.sidebar-content'), o, on);
                break;
            case 'a2':
                 o = studentList; 
                 on='#studentList';
                if(isSearchStudentName != true) gb_strFilterGrid = " ";
                else isSearchStudentName = false;
                $('.btn-group button a.a2').parent().addClass('active');
                _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                 +"  <div class='input'>"
                                +"  <label>From Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew' name='datepicker' class='form-control'/>"
                                +"  <label>To Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew' name='datepicker'  class='form-control'/>"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Status </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentList_Stausi' name='combo'  />"
                                +"  <input type='text' id='dmstudentList_Staush' style='display:none;'  />"
                                +"  <label >Sex </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentList_Sexi' name='combo' />"
                                +"  <input type='text' id='dmstudentList_Sexh' style='display:none;'  />"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentList_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dmstudentList_AOFirsth' style='display:none;'  />"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Student Group </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentList_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dmstudentList_StudentGrouph' style='display:none;'  />"
                                +"  <label >Source Of Info</label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentList_Sourcei' name='combo' />"
                                +"  <input type='text' id='dmstudentList_Sourceh' style='display:none;'  />"
                                +"  <label >Student Name</label>"
                                +"  <input type='text'  autocomplete='off'  class='form-control input-large' id='dmstudentList_StudentName' name='combo'  />"
                                +" <input style='margin-left: 20px' type='button' class='btn btn-primary btn-sm' id='exportStudentList' value='Export'>"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);
                $("#NgayBatDauIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentList_selectOption();}).on('changeDate', dmstudentList_selectOption);
                $("#NgayKetThucIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentList_selectOption();}).on('changeDate', dmstudentList_selectOption);
                $("#NgayBatDauIdNew").val(XDate.today().addMonths(-1).toString("dd/MM/yyyy"));
                $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy")); 
                selectOption('dmstudentList_Stausi', '', 'GBL_TRAINING_STUDENT_STATUS', 'Name');
                selectOption('dmstudentList_Sexi', '', 'gcGobal_LITERAL_Gender', 'Name');
                selectOption('dmstudentList_AOFirsti', '( (isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dmstudentList_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dmstudentList_Sourcei', '', 'GBL_DANHMUC_NGUONTHONGTIN', 'Name');
                $("#dmstudentList_StudentName").change(function(){dmstudentList_selectOption()});
                $("#exportStudentList").click(function(){
                    var fd = d2e($("#NgayBatDauIdNew").val());
                    var td = d2e($("#NgayKetThucIdNew").val());
                    var st = $("#dmstudentList_Staush").val();
                    var se = $("#dmstudentList_Sexh").val();
                    var ao = $("#dmstudentList_AOFirsth").val();
                    var sg = $("#dmstudentList_StudentGrouph").val();
                    var so = $("#dmstudentList_Sourceh").val();
                    var si = $("#dmstudentList_StudentName").val();
                    var stW = "";
                    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) >= '" + fd + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) >= '" + fd + "' ) "));
                    stW = td == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) <= '" + td + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) <= '" + td + "' ) "));
                    stW = st == "" ? stW : (stW == "" ? (" (statusid = " + st + " ) ") : (stW + " and (statusid = " + st + " ) "));
                    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
                    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
                    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
                    stW = so == "" ? stW : (stW == "" ? (" (SourceId = " + so + " ) ") : (stW + " and (SourceId = " + so + " ) "));
                    stW = si == "" ? stW : (stW == "" ? (" (Name like N'%" + si + "%') ") : (stW + " and (Name like N'%" + si + "%' ) "));
                    reportRequest({
                                _a: 'rp33',
                                _cf: {
                                    bShowIndexRow: false,
                                    mColMergForSubSumRow: 7,
                                    reportId: 15050801,
                                    strOrderBy: ' order by Name, Id, isPrgCreateDate ',
                                    hdValue: " ",
                                    fd: 2,
                                    classRowSumGobal: "dhu_rpt_TextCenterTotal",
                                    strWhere: stW
                                }
                            }, { success: function(o, op, $el){
                                $el.find("#gc_btnExportForm").click();
                                modal.close();
                            }});
                });
                break;
            case 'a3':
                o = inquires;
                on = '#inquires';
                gb_strFilterGrid = " ";
                 _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $('.btn-group button a.a3').parent().addClass('active');
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                +"  <div class='input'>"
                                +"  <label>From Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew' name='datepicker' class='form-control'/>"
                                +"  <label>To Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew' name='datepicker'  class='form-control'/>"
                                +"  <label >Type </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dminquires_Typei' name='combo'  />"
                                +"  <input type='text' id='dminquires_Typeh' style='display:none;'  />"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Sex </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dminquires_Sexi' name='combo' />"
                                +"  <input type='text' id='dminquires_Sexh' style='display:none;'  />"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dminquires_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dminquires_AOFirsth' style='display:none;'  />"
                                +"  <label >Student Group </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dminquires_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dminquires_StudentGrouph' style='display:none;'  />"
                                +"  <label >Student </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dminquires_StuIdi' name='combo'  />"
                                +"  <input type='text' id='dminquires_StuIdh' style='display:none;'  />"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);
                $("#NgayBatDauIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dminquires_selectOption();}).on('changeDate', dminquires_selectOption);
                $("#NgayKetThucIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dminquires_selectOption();}).on('changeDate', dminquires_selectOption);
                $("#NgayBatDauIdNew").val(XDate.today().addMonths(-1).toString("dd/MM/yyyy"));
                $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy")); 
                selectOption('dminquires_Typei', '', 'GBL_TRAINING_STUDENT_ACTION_TYPE', 'Name');
                selectOption('dminquires_Sexi', '', 'gcGobal_LITERAL_Gender', 'Name');
                selectOption('dminquires_AOFirsti', '((isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dminquires_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dminquires_StuIdi', '', 'GBL_TRAINING_STUDENT', 'Name');
                break;
            case 'a4':
                o = placementTest;
                on = '#placementTest';
                gb_strFilterGrid = " ";
                 _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $('.btn-group button a.a4').parent().addClass('active');
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                +"  <div class='input'>"
                                +"  <label >Status </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmplacementTest_Resulti' name='combo'  />"
                                +"  <input type='text' id='dmplacementTest_Resulth' style='display:none;'  />"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmplacementTest_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dmplacementTest_AOFirsth' style='display:none;'  />"
                                +"  <label >Student Type </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmplacementTest_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dmplacementTest_StudentGrouph' style='display:none;'  />"
                                +"  <label >Student </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmplacementTest_StuIdi' name='combo'  />"
                                +"  <input type='text' id='dmplacementTest_StuIdh' style='display:none;'  />"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);

                selectOption('dmplacementTest_Resulti', '', 'GBL_TRAINNING_COURSE', 'Name');
                selectOption('dmplacementTest_AOFirsti', '((isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dmplacementTest_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dmplacementTest_StuIdi', '', 'GBL_TRAINING_STUDENT', 'Name');
                break;
            case 'a5': 
                o = enrollmentList; on = '#enrollmentList';
                gb_strFilterGrid = " ";
                 _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $('.btn-group button a.a5').parent().addClass('active');
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                +"  <div class='input'>"
                                +"  <label>From Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew' name='datepicker' class='form-control'/>"
                                +"  <label>To Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew' name='datepicker'  class='form-control'/>"
                                +"  <label >Invoice </label>"
                                +"  <input type='text'  autocomplete='off'  class='form-control input-large' id='dmenrollmentList_Invoice'  />"
                                +"  <label >Sex </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmenrollmentList_Sexi' name='combo' />"
                                +"  <input type='text' id='dmenrollmentList_Sexh' style='display:none;'  />"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmenrollmentList_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dmenrollmentList_AOFirsth' style='display:none;'  />"
                                +"  <label  style='display:none;'>Status</label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' style='display:none;' class='form-control' id='dmenrollmentList_Stausi' name='combo' />"
                                +"  <input type='text' id='dmenrollmentList_Staush' style='display:none;'  />"
                                +"  <label >Student Group </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmenrollmentList_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dmenrollmentList_StudentGrouph' style='display:none;'  />"
                                +"  <label >Student </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmenrollmentList_StuIdi' name='combo'  />"
                                +"  <input type='text' id='dmenrollmentList_StuIdh' style='display:none;'  />"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);
                $("#NgayBatDauIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmenrollmentList_selectOption();}).on('changeDate', dmenrollmentList_selectOption);
                $("#NgayKetThucIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmenrollmentList_selectOption();}).on('changeDate', dmenrollmentList_selectOption);
                $("#NgayBatDauIdNew").val(XDate.today().addMonths(-1).toString("dd/MM/yyyy"));
                $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy")); 
                selectOption('dmenrollmentList_Stausi', '', 'GBL_TRAINING_STUDENT_STATUS', 'Name');
                selectOption('dmenrollmentList_Sexi', '', 'gcGobal_LITERAL_Gender', 'Name');
                selectOption('dmenrollmentList_AOFirsti', '', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dmenrollmentList_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dmenrollmentList_StuIdi', '', 'GBL_TRAINING_STUDENT', 'Name');
                $("#dmenrollmentList_Invoice").change(function(){dmenrollmentList_selectOption()});
                break;
            case 'a6':
                o = studentStudying;
                on = '#studentStudying';
                gb_strFilterGrid = " id in (select a.studentid from gbl_trainning_course_class_student a " +
                    "join gbl_trainning_course_class b on a.ClassId = b.id " +
                    "where (b.Id = 1612) or (b.StartDate >= '" + XDate.today().toString("MM/dd/yyyy") + "' or (b.startdate < '" 
                    + XDate.today().toString("MM/dd/yyyy") + "' and b.FinishDate >= '" + XDate.today().toString("MM/dd/yyyy") + "'))) ";
                _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $('.btn-group button a.a6').parent().addClass('active');
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                 +"  <div class='input'>"
                                +"  <label>From Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew' name='datepicker' class='form-control'/>"
                                +"  <label>To Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew' name='datepicker'  class='form-control'/>"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Status </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentStudying_Stausi' name='combo'  />"
                                +"  <input type='text' id='dmstudentStudying_Staush' style='display:none;'  />"
                                +"  <label >Sex </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentStudying_Sexi' name='combo' />"
                                +"  <input type='text' id='dmstudentStudying_Sexh' style='display:none;'  />"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentStudying_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dmstudentStudying_AOFirsth' style='display:none;'  />"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Student Group </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentStudying_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dmstudentStudying_StudentGrouph' style='display:none;'  />"
                                +"  <label >Source Of Info</label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentStudying_Sourcei' name='combo' />"
                                +"  <input type='text' id='dmstudentStudying_Sourceh' style='display:none;'  />"
                                +"  <label >Student Name</label>"
                                +"  <input type='text'  autocomplete='off'  class='form-control input-large' id='dmstudentStudying_StudentName' name='combo'  />"
                                //+" <input style='margin-left: 20px' type='button' class='btn btn-primary btn-sm' id='exportstudentStudying' value='Export'>"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);
                $("#NgayBatDauIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentStudying_selectOption();}).on('changeDate', dmstudentStudying_selectOption);
                $("#NgayKetThucIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentStudying_selectOption();}).on('changeDate', dmstudentStudying_selectOption);
                $("#NgayBatDauIdNew").val(XDate.today().addMonths(-1).toString("dd/MM/yyyy"));
                $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy")); 
                selectOption('dmstudentStudying_Stausi', '', 'GBL_TRAINING_STUDENT_STATUS', 'Name');
                selectOption('dmstudentStudying_Sexi', '', 'gcGobal_LITERAL_Gender', 'Name');
                selectOption('dmstudentStudying_AOFirsti', '( (isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dmstudentStudying_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dmstudentStudying_Sourcei', '', 'GBL_DANHMUC_NGUONTHONGTIN', 'Name');
                $("#dmstudentStudying_StudentName").change(function(){dmstudentStudying_selectOption()});
                break;
            case 'a7':
                o = studentOld;
                on = '#studentOld';
                gb_strFilterGrid = " id in (select a.studentid from gbl_trainning_course_class_student a " +
                    "join gbl_trainning_course_class b on a.ClassId = b.id " +
                    " where b.Id != 1612 group by a.studentid " +
                    " having max(b.StartDate) < '" + XDate.today().toString("MM/dd/yyyy") + "' and max(b.FinishDate) < '" 
                    + XDate.today().toString("MM/dd/yyyy") + "') ";
                _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $('.btn-group button a.a7').parent().addClass('active');
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                 +"  <div class='input'>"
                                +"  <label>From Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew' name='datepicker' class='form-control'/>"
                                +"  <label>To Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew' name='datepicker'  class='form-control'/>"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Status </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentOld_Stausi' name='combo'  />"
                                +"  <input type='text' id='dmstudentOld_Staush' style='display:none;'  />"
                                +"  <label >Sex </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentOld_Sexi' name='combo' />"
                                +"  <input type='text' id='dmstudentOld_Sexh' style='display:none;'  />"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentOld_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dmstudentOld_AOFirsth' style='display:none;'  />"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Student Group </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentOld_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dmstudentOld_StudentGrouph' style='display:none;'  />"
                                +"  <label >Source Of Info</label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentOld_Sourcei' name='combo' />"
                                +"  <input type='text' id='dmstudentOld_Sourceh' style='display:none;'  />"
                                +"  <label >Student Name</label>"
                                +"  <input type='text'  autocomplete='off'  class='form-control input-large' id='dmstudentOld_StudentName' name='combo'  />"
                                //+" <input style='margin-left: 20px' type='button' class='btn btn-primary btn-sm' id='exportstudentOld' value='Export'>"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);
                $("#NgayBatDauIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentOld_selectOption();}).on('changeDate', dmstudentOld_selectOption);
                $("#NgayKetThucIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentOld_selectOption();}).on('changeDate', dmstudentOld_selectOption);
                $("#NgayBatDauIdNew").val(XDate.today().addMonths(-1).toString("dd/MM/yyyy"));
                $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy")); 
                selectOption('dmstudentOld_Stausi', '', 'GBL_TRAINING_STUDENT_STATUS', 'Name');
                selectOption('dmstudentOld_Sexi', '', 'gcGobal_LITERAL_Gender', 'Name');
                selectOption('dmstudentOld_AOFirsti', '( (isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dmstudentOld_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dmstudentOld_Sourcei', '', 'GBL_DANHMUC_NGUONTHONGTIN', 'Name');
                $("#dmstudentOld_StudentName").change(function(){dmstudentOld_selectOption()});
                break;
            case 'a8':
                o = studentNot;
                on = '#studentNot';
                gb_strFilterGrid = " id not in (select studentid from gbl_trainning_course_class_student)";
                _gb.pr(o).ff(f, o);
                _gb.rf(o);
                view($(this), $('.sidebar-content'), o, on);
                $(on).find('.login_sign').hide();
                $('.btn-group button a.a8').parent().addClass('active');
                $filter = "<div id='' class='gc_SearchForm' >"
	                            +" <div class='login_sign' id=''>"
                                 +"  <div class='input'>"
                                +"  <label>From Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew' name='datepicker' class='form-control'/>"
                                +"  <label>To Date</label>"
                                +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew' name='datepicker'  class='form-control'/>"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Status </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentNot_Stausi' name='combo'  />"
                                +"  <input type='text' id='dmstudentNot_Staush' style='display:none;'  />"
                                +"  <label >Sex </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentNot_Sexi' name='combo' />"
                                +"  <input type='text' id='dmstudentNot_Sexh' style='display:none;'  />"
                                +"  <label >AO First </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...'  class='form-control' id='dmstudentNot_AOFirsti' name='combo'  />"
                                +"  <input type='text' id='dmstudentNot_AOFirsth' style='display:none;'  />"
                                +"      </div>"
                                +"  <div class='input'>"
                                +"  <label >Student Group </label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentNot_StudentGroupi' name='combo' />"
                                +"  <input type='text' id='dmstudentNot_StudentGrouph' style='display:none;'  />"
                                +"  <label >Source Of Info</label>"
                                +"  <input type='text'  autocomplete='off'  placeholder='Select...' class='form-control' id='dmstudentNot_Sourcei' name='combo' />"
                                +"  <input type='text' id='dmstudentNot_Sourceh' style='display:none;'  />"
                                +"  <label >Student Name</label>"
                                +"  <input type='text'  autocomplete='off'  class='form-control input-large' id='dmstudentNot_StudentName' name='combo'  />"
                                //+" <input style='margin-left: 20px' type='button' class='btn btn-primary btn-sm' id='exportstudentNot' value='Export'>"
                                +"      </div>"
                                +"  </div>"
                                +"  </div>";
                $(on).before($filter);
                $("#NgayBatDauIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentNot_selectOption();}).on('changeDate', dmstudentNot_selectOption);
                $("#NgayKetThucIdNew").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmstudentNot_selectOption();}).on('changeDate', dmstudentNot_selectOption);
                $("#NgayBatDauIdNew").val(XDate.today().addMonths(-1).toString("dd/MM/yyyy"));
                $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy")); 
                selectOption('dmstudentNot_Stausi', '', 'GBL_TRAINING_STUDENT_STATUS', 'Name');
                selectOption('dmstudentNot_Sexi', '', 'gcGobal_LITERAL_Gender', 'Name');
                selectOption('dmstudentNot_AOFirsti', '( (isprgbuserdeleted is null or isprgbuserdeleted = 0) and macanboid in (select id from gcGobal_COMP_EmployeeLife where departmentid = 25))', 'gcGobal_ACCOUNT_Account', 'UserName');
                selectOption('dmstudentNot_StudentGroupi', '', 'GBL_TRAINING_STUDENT_GROUP', 'Name');
                selectOption('dmstudentNot_Sourcei', '', 'GBL_DANHMUC_NGUONTHONGTIN', 'Name');
                $("#dmstudentNot_StudentName").change(function(){dmstudentNot_selectOption()});
                break;
            default:
                break;
        }
    }); 
});


function showPopupStudentDetail(ev, sId, that){
    cur_gb_ElementId = 'studentDetail';
    cur_gb_KeyId = sId;
    gb_strFilterGrid = ' id = ' + cur_gb_KeyId + ' ';
    _gb.pr(studentDetail).ff(true, studentDetail);
    _gb.rf(studentDetail);
    $('#studentDetailc_').hide();
    $('#studentDetail_f').css({ 'margin-top': '8px' });
    $("#fstudentDetail19i").remove();
    $areanote = $("<textarea rows='5' id='fstudentDetail19i' class='form-control' style='width: 545px!important'></textarea>")
    $("#r08studentDetail").append($areanote);
    var lD = "<option value=''>Select</option>", 
    lM = "<option value=''>Select</option>", 
    lY = "<option value=''>Select</option>";
    for(var i = 1; i < 32; i++)
        lD += "<option value=" + i + ">" + i + "</option>";
    for(var i = 1; i < 13; i++)
        lM += "<option value=" + i + ">" + i + "</option>";
    for(var i = 1940; i < 2015; i++)
        lY += "<option value" + i + ">" + i + "</option>";
    var $day = $("<select id='studentDetailDay'>" + lD + "</select>");
    var $month = $("<select id='studentDetailMonth'>" + lM + "</select>");
    var $year = $("<select id='studentDetailYear'>" + lY + "</select>");
    $("#lstudentDetail09i").show();
    $("#r03studentDetail").append($day).append($month).append($year);
    if(cur_gb_GroupRight != 1 && cur_gb_GroupRight != 2 && cur_gb_GroupRight != 3)
        $("#fstudentDetail17i").attr("readonly", "readonly");
    gb_curBoxDlg = $(that).colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentDetail", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
};
$(document).bind('cbox_complete', function () {
    if(cur_gb_ElementId == 'studentDetail'){
        var $form1 = $('#studentDetail');
        var $form2 = $('#studentAction');
        var $form3 = $('#enrollment');
        var $form4 = $('#classStudent');
        var $form5 = $('#studentEntranceTest');
        var $form6 = $('#o14');
        var $form7 = $('#studentEnrollAL');
        var $form8 = $('#attendanceAL');
        var $form9 = $('#o14');
        var $form10 = $('#assignClass');
        var $form11 = $('#quaStudent');
        var $form12 = $('#addMarkStudent');
        var $form13 = $('#doiDiemStudent');
        var $form14 = $('#studentTakeCare');
        var $divtab = $("<div class='br-phanloai'></div>");
        var $divtab1 = $("<ul class='nav nav-tabs'> </ul>");
        var $div1 = $("<li class='" + ($form1.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Student</a></li>");
        var $div2 = $("<li class='" + ($form2.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Action</a></li>");
        var $div3 = $("<li class='" + ($form3.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Enrollment</a></li>");
        var $div4 = $("<li class='" + ($form4.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>History Class</a></li>");
        var $div5 = $("<li class='" + ($form5.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Entrance Test</a></li>");
        var $div6 = $("<li class='" + ($form6.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Overview</a></li>");
        var $div7 = $("<li class='" + ($form7.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Enroll UE-AL</a></li>");
        var $div8 = $("<li class='" + ($form8.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Attendance UE-AL</a></li>");
        var $div9 = $("<li class='" + ($form9.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Survey</a></li>");
        var $div10 = $("<li class='" + ($form10.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Assign Class</a></li>");
        var $div11 = $("<li class='" + ($form11.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Gift</a></li>");
        var $div12 = $("<li class='" + ($form12.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Bonus Mark</a></li>");
        var $div13 = $("<li class='" + ($form13.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Change Mark</a></li>");
        var $div14 = $("<li class='" + ($form14.length > 0 ? 'active' : '') + "'><a role='tab' data-toggle='tab'  style=' cursor : pointer'>Take Care</a></li>");
        $divtab.append($divtab1);
        $divtab1.append($div1);
        $divtab1.append($div2);
        $divtab1.append($div3);
        $divtab1.append($div4);
        $divtab1.append($div5);
//        $divtab1.append($div6);
        $divtab1.append($div7);
        $divtab1.append($div8);
//        $divtab1.append($div9);
        //$divtab1.append($div10);
        $divtab1.append($div11);
        $divtab1.append($div12);
        $divtab1.append($div13);
        $divtab1.append($div14);
        if ($form1.length > 0) {
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form1.prepend($divtab);
        }
        else if ($form2.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form2.prepend($divtab);
        }
        else if ($form3.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form3.prepend($divtab);
        }
        else if ($form4.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form4.prepend($divtab);
        }
        else if ($form5.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form5.prepend($divtab);
        }
        else if ($form6.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form6.prepend($divtab);
        }
        else if ($form7.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form7.prepend($divtab);
        }
        else if ($form8.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form8.prepend($divtab);
            $filter = "<div id='' class='gc_SearchForm' >"
            +"      <div class='input'>"
            +"  <label>From Date</label>"
            +"  <input type='text'  autocomplete='off'  id='NgayBatDauIdNew1' name='datepicker' />"
            +"  <label>To Date</label>"
            +"  <input type='text'  autocomplete='off'  id='NgayKetThucIdNew1' name='datepicker' />"
            +"           <label>Class</label>"
            +"  <input type='text'  autocomplete='off'  placeholder='Select...' id='dmattendanceAL_Classi' name='combo' class='brwidthinput300 input-large'  />"
            +"  <input type='text' id='dmattendanceAL_Classh' style='display:none;'  />"
            +"      <div class='clear'></div>"
            +"      </div>"
            +"  </div>";
            $form8.find("#attendanceAL_f").prepend($filter);
            $form8.find("#attendanceAL_g").after('<div id="attendanceAL_TotalHour">');
            $("#NgayBatDauIdNew1").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmattendanceAL_selectOption();}).on('changeDate', dmattendanceAL_selectOption);
            $("#NgayKetThucIdNew1").datepicker({ format: 'dd/mm/yyyy' }).change(function(){dmattendanceAL_selectOption();}).on('changeDate', dmattendanceAL_selectOption);
            //$("#NgayBatDauIdNew1").val(XDate.today().addMonths(-2).toString("dd/MM/yyyy"));
            //$("#NgayKetThucIdNew1").val(XDate.today().toString("dd/MM/yyyy"));
            selectOption('dmattendanceAL_Classi', '(id in (select classid from GBL_TRAINNING_COURSE_CLASS_STUDENT ' 
                    + 'where keep01 = 2 and studentid = ' + cur_gb_KeyId + '))', 'GBL_TRAINNING_COURSE_CLASS', 'Name');
            var fd = d2e($("#NgayBatDauIdNew1").val());
            var td = d2e($("#NgayKetThucIdNew1").val());
            var se = $("#dmattendanceAL_Classh").val();
            var stW = " studentid = " + cur_gb_KeyId + " ";
            stW = fd == "" ? stW : (stW == "" ? (" (convert(date, Date) >= '" + fd + "' ) ") : (stW + " and (convert(date, Date) >= '" + fd + "' ) "));
            stW = td == "" ? stW : (stW == "" ? (" (convert(date, Date) <= '" + td + "' ) ") : (stW + " and (convert(date, Date) <= '" + td + "' ) "));
            stW = se == "" ? stW : (stW == "" ? (" (ClassId = " + se + " ) ") : (stW + " and (ClassId = " + se + " ) "));
            request({a: 'GetProcedure', c: [
                    " select CASE WHEN len(CONVERT(nvarchar, sum(isnull(TotalMinute, 0))/60)) = 1 THEN '0' "
                    + " + CONVERT(nvarchar, sum(isnull(TotalMinute, 0))/60) ELSE CONVERT(nvarchar, sum(isnull(TotalMinute, 0))/60)  "
                    + " END + ':' + CASE WHEN len(CONVERT(nvarchar, sum(isnull(TotalMinute, 0)) % 60))  "
                    + " = 1 THEN '0' + CONVERT(nvarchar, sum(isnull(TotalMinute, 0)) % 60)  "
                    + " ELSE CONVERT(nvarchar, sum(isnull(TotalMinute, 0)) % 60) end "
                    + " from zgcl_GBL_TRAINING_STUDENT_ATTENDANCEAL06_FULL "
                    + " where " + stW
                    + " group by studentid  " + (se == "" ? "" : ", ClassId ")
                    ]}, { success: function(o, op, r){ 
                        if(r.Result == "OK" && r.Records.length > 0){
                            $("#attendanceAL_TotalHour").html("<p>Total Learning Hours: <strong>" + r.Records[0][0] + "</strong></p>");
                        } else {
                            $("#attendanceAL_TotalHour").html("<p>Total Learning Hours: <strong>00:00</strong></p>")};
                        }
                    });
        }
        else if ($form9.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form9.prepend($divtab);
        }
        else if ($form10.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form10.prepend($divtab);
        }
        else if ($form11.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form11.prepend($divtab);
        }
        else if ($form12.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form12.prepend($divtab);
        }
        else if ($form13.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm14Click($div14, $form14.selector.split('#')[1]);
            $form13.prepend($divtab);
        }
        else if ($form14.length > 0) {
            studentForm1Click($div1, $form1.selector.split('#')[1]);
            studentForm2Click($div2, $form2.selector.split('#')[1]);
            studentForm3Click($div3, $form3.selector.split('#')[1]);
            studentForm4Click($div4, $form4.selector.split('#')[1]);
            studentForm5Click($div5, $form5.selector.split('#')[1]);
            studentForm6Click($div6, $form6.selector.split('#')[1]);
            studentForm7Click($div7, $form7.selector.split('#')[1]);
            studentForm8Click($div8, $form8.selector.split('#')[1]);
            studentForm9Click($div9, $form9.selector.split('#')[1]);
            studentForm10Click($div10, $form10.selector.split('#')[1]);
            studentForm11Click($div11, $form11.selector.split('#')[1]);
            studentForm12Click($div12, $form12.selector.split('#')[1]);
            studentForm13Click($div13, $form13.selector.split('#')[1]);
            $form14.prepend($divtab);
        }
    }
});
function studentForm1Click($el, fId){
    $el.click(function(){
        gb_strFilterGrid = ' id = ' + cur_gb_KeyId + ' ';
        cur_gb_ElementId = 'studentDetail';
        _gb.pr(studentDetail).ff(true, studentDetail);
        _gb.rf(studentDetail);
        $('#studentDetailc_').hide();
        $('#studentDetail_f').css({ 'margin-top': '8px' });
        $("#fstudentDetail19i").remove();
        $areanote = $("<textarea rows='5' id='fstudentDetail19i' class='form-control' style='width: 545px!important'></textarea>")
        $("#r08studentDetail").append($areanote);
        var lD = "<option value=''>Select</option>", 
        lM = "<option value=''>Select</option>", 
        lY = "<option value=''>Select</option>";
        for(var i = 1; i < 32; i++)
            lD += "<option value=" + i + ">" + i + "</option>";
        for(var i = 1; i < 13; i++)
            lM += "<option value=" + i + ">" + i + "</option>";
        for(var i = 1940; i < 2015; i++)
            lY += "<option value" + i + ">" + i + "</option>";
        var $day = $("<select id='studentDetailDay'>" + lD + "</select>");
        var $month = $("<select id='studentDetailMonth'>" + lM + "</select>");
        var $year = $("<select id='studentDetailYear'>" + lY + "</select>");
        $("#lstudentDetail09i").show();
        $("#r03studentDetail").append($day).append($month).append($year);
        if(cur_gb_GroupRight != 1 && cur_gb_GroupRight != 2 && cur_gb_GroupRight != 3)
            $("#fstudentDetail17i").attr("readonly", "readonly");
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentDetail", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm2Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(studentAction).ff(true, studentAction);
        _gb.rf(studentAction);
        $('#fstudentAction01h').val(cur_gb_KeyId);
        $('#studentActionc_').hide();
        $('#studentAction_f').css({ 'margin-top': '8px' });
        $("#fstudentAction08i").remove();
        $areanote = $("<textarea rows='5' id='fstudentAction08i' class='form-control' style='width: 545px!important'></textarea>")
        $("#r06studentAction").append($areanote);
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentAction", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm3Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' keep01 = 1 and studentId = ' + cur_gb_KeyId;
        _gb.pr(enrollment).ff(true, enrollment);
        _gb.rf(enrollment);
        //$('#enrollment .login_sign').hide();
        $('#fenrollment01h').val(cur_gb_KeyId);
        $('#fenrollment09h').val("1");
        $('#fenrollment05i').val("VND");
        $('#fenrollment05h').val("1");
        $('#fenrollment06i').val("0.00004636");
        $('#fenrollment13i').val(cur_gb_Account);
        $('#fenrollment13h').val(cur_gb_AccountId);
        $('#fenrollment35i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        $('#fenrollment04i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if($('#fenrollment35i').val() == '') $('#fenrollment35i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        if($('#fenrollment04i').val() == '') $('#fenrollment04i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fenrollment35i").click(function () { $("#fenrollment35i").select(); });
        $("#fenrollment04i").click(function () { $("#fenrollment04i").select(); });
        //$("#fenrollment04i").keyup(function (ev) { formatTien(this); autoFareKho("enrollment"); });
        $("#fenrollment35i").keyup(function (ev) { formatTien(this); autoFareKho("enrollment"); });
        $("#fenrollment38i").keyup(function (ev) { autoFareKho("enrollment"); });
        $("#fenrollment11i").val((new XDate()).toString("dd/MM/yyyy"));
        var $btn = $("<div />").append('<button type="button" id="tinhNgay" class="btn btn-warning btn-sm">Calculate</>');
        
//        $btn.click(function(){
//            var hours = $("#fenrollment20i").val();
//            $("#alertCheckCode").remove();
//            request({
//                a: 'pGetGBL_TRAINNING_COUPON', 
//                c: '', d: '', type: 'p', cl: 'Id, DiscountMoney, DiscountPer', si: 1, mr: 20, se: ' Id desc',
//                f: " fromdate <= '" + d2e(XDate.today().toString("dd/MM/yyyy")) + "' and todate >= '" 
//                    + d2e(XDate.today().toString("dd/MM/yyyy")) + "' and code = '" + code + "' and (maxusing - amount) > 0"
//            }, {
//                success: function(o, op, r){
//                    var $al = $("<div id='alertCheckCode'/>").addClass("alert alert-info alert-info-gc-small");
//                    if(r.Result == "OK" && r.Records.length > 0){
//                        var dis = 0;
//                        if(parseFloat(r.Records[0][2]) > 0){
//                            $("#freceipt20i").val(parseFloat(r.Records[0][2]));
//                            $("#freceipt13i").val(parseFloat($("#freceipt12i").val()) - parseFloat($("#freceipt20i").val()) 
//                                + parseFloat($("#freceipt17i").val()));
//                        } else {
//                            $("#freceipt20i").val(parseFloat($("#freceipt12i").val())*parseFloat(r.Records[0][3])/100);
//                            $("#freceipt13i").val(parseFloat($("#freceipt12i").val()) - parseFloat($("#freceipt20i").val()) 
//                                + parseFloat($("#freceipt17i").val()));
//                        }
//                        $al.text('Mã giảm đúng');
//                    } else {
//                        $al.text('Mã giảm sai');
//                    }
//                    $("#receipt_f .login_sign").before($al);
//                    var t = setTimeout(function(){
//                        $al.remove();
//                        clearTimeout(t);
//                    }, 5000);
//                }
//            });
//        });
        $("#fenrollment20i").after($btn);
        $('#enrollmentc_').hide();
        $('#enrollment_f').css({ 'margin-top': '8px' });
        $('#enrollment_f').prepend($("#enrollment_i"));
        $('#enrollment_f').find(".login_sign").css({ 'margin-top': '8px' });
        $('#enrollment_f').find(".jtable-bottom-panel").hide();
        $("#fenrollment16i").remove();
        $areanote = $("<textarea rows='5' id='fenrollment16i' class='form-control' style='width: 545px!important'></textarea>")
        $("#r11enrollment").append($areanote);
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#enrollment", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm4Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' id in (select classId from gbl_trainning_course_class_student where studentid = ' + cur_gb_KeyId + ' )';
        _gb.pr(classStudent).ff(true, classStudent);
        _gb.rf(classStudent);
        $('#classStudent .login_sign').hide();
        $('#fclassStudent01h').val(cur_gb_KeyId);
        $('#classStudentc_').hide();
        $('#classStudent_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#classStudent", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm5Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(studentEntranceTest).ff(true, studentEntranceTest);
        _gb.rf(studentEntranceTest);
        $('#fstudentEntranceTest01h').val(cur_gb_KeyId);
        $('#studentEntranceTestc_').hide();
        $('#studentEntranceTest_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentEntranceTest", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm6Click($el, fId){
//    $el.click(function(){
//        cur_gb_ElementId = 'studentDetail';
//        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
//        _gb.pr(studentEntranceTest).ff(true, studentEntranceTest);
//        _gb.rf(studentEntranceTest);
//        $('#fstudentEntranceTest01h').val(cur_gb_KeyId);
//        $('#studentEntranceTestc_').hide();
//        $('#studentEntranceTest_f').css({ 'margin-top': '8px' });
//        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentEntranceTest", title: "Update Info <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
//    });
};
function studentForm7Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        //gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        gb_strFilterGrid = '  keep01 = 2 and studentId = ' + cur_gb_KeyId;
        _gb.pr(studentEnrollAL).ff(true, studentEnrollAL);
        _gb.rf(studentEnrollAL);
        $('#fstudentEnrollAL01h').val(cur_gb_KeyId);
        $('#fstudentEnrollAL09h').val("1");
        $('#fstudentEnrollAL05i').val("VND");
        $('#fstudentEnrollAL05h').val("1");
        $('#fstudentEnrollAL06i').val("0.00004636");
        $('#fstudentEnrollAL13i').val(cur_gb_Account);
        $('#fstudentEnrollAL13h').val(cur_gb_AccountId);
        $('#fstudentEnrollAL35i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        $('#fstudentEnrollAL04i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if($('#fstudentEnrollAL35i').val() == '') $('#fstudentEnrollAL35i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        if($('#fstudentEnrollAL04i').val() == '') $('#fstudentEnrollAL04i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val('0');
        $("#fstudentEnrollAL35i").click(function () { $("#fstudentEnrollAL35i").select(); });
        $("#fstudentEnrollAL04i").click(function () { $("#fstudentEnrollAL04i").select(); });
        //$("#fstudentEnrollAL04i").keyup(function (ev) { formatTien(this); autoFareKho("studentEnrollAL"); });
        $("#fstudentEnrollAL35i").keyup(function (ev) { formatTien(this); autoFareKho("studentEnrollAL"); });
        $("#fstudentEnrollAL38i").keyup(function (ev) { autoFareKho("studentEnrollAL"); });
        $("#fstudentEnrollAL11i").val((new XDate()).toString("dd/MM/yyyy"));
        var $btn = $("<div />").append('<button type="button" id="tinhNgay" class="btn btn-warning btn-sm">Calculate</>');
        $btn.click(function(){
            var t = $("#fstudentEnrollAL19i").val();
            if(t.length > 9){
                var m = $("#fstudentEnrollAL21i").val();
                if($.isNumeric(m)){
                    if(m != ''){
                        var td = new XDate(d2e(t)).addMonths(parseInt(m)).toString("dd/MM/yyyy");
                        $("#fstudentEnrollAL22i").val(td);
                        $("#fstudentEnrollAL28i").val(td);
                    }
                }
                //var d = _parseDate(t.split('/')[2] + '-' + t.split('/')[1] + '-' + t.split('/')[0]).addMonths($("#fstudentEnrollAL21i").val()).toString("dd/MM/yyyy");
            }
        });
        $("#fstudentEnrollAL21i").after($btn);
        $('#studentEnrollALc_').hide();
        $('#studentEnrollAL_f').css({ 'margin-top': '8px' });
        $('#studentEnrollAL_f').prepend($("#studentEnrollAL_i"));
        $('#studentEnrollAL_f').find(".login_sign").css({ 'margin-top': '8px' });
        $('#studentEnrollAL_f').find(".jtable-bottom-panel").hide();
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentEnrollAL", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm8Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(attendanceAL).ff(true, attendanceAL);
        _gb.rf(attendanceAL);
        $('#fattendanceAL01h').val(cur_gb_KeyId);
        $("#fattendanceAL13i").attr("style", "width: 85px!important");
        $("#lattendanceAL14i").attr("style", "width: 10px!important;min-width: 10px!important");
        $("#fattendanceAL14i").attr("style", "width: 85px!important");
        $("#fattendanceAL15i").attr("style", "width: 85px!important");
        $("#lattendanceAL16i").attr("style", "width: 10px!important;min-width: 10px!important");
        $("#fattendanceAL16i").attr("style", "width: 85px!important");
        $('#attendanceALc_').hide();
        $('#attendanceAL_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#attendanceAL", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm9Click($el, fId){
//    $el.click(function(){
//        cur_gb_ElementId = 'studentDetail';
//        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
//        _gb.pr(studentEntranceTest).ff(true, studentEntranceTest);
//        _gb.rf(studentEntranceTest);
//        $('#fstudentEntranceTest01h').val(cur_gb_KeyId);
//        $('#studentEntranceTestc_').hide();
//        $('#studentEntranceTest_f').css({ 'margin-top': '8px' });
//        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentEntranceTest", title: "Update Info <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
//    });
};
function studentForm10Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(assignClass).ff(true, assignClass);
        _gb.rf(assignClass);
        $('#fassignClass01h').val(cur_gb_KeyId);
        $('#assignClassc_').hide();
        $('#assignClass_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#assignClass", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm11Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(quaStudent).ff(true, quaStudent);
        _gb.rf(quaStudent);
        $('#fquaStudent01h').val(cur_gb_KeyId);
        $('#quaStudentc_').hide();
        $('#quaStudent_f').css({ 'margin-top': '8px' });
        $('#fquaStudent02i').removeClass("brwidthinput200").css("width", "40%");
        $('#fquaStudent05i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);');
        if($('#fquaStudent05i').val() == '') $('#fquaStudent05i').attr('style', 'font-weight: bold; color: rgb(23, 194, 82);').val(1);
        $("#fquaStudent05i").click(function () { $("#fquaStudent05i").select(); });
        $("#fquaStudent05i").keyup(function (ev) { formatTien(this); });
        $("#fquaStudent04i").val((new XDate()).toString("dd/MM/yyyy HH:mm:ss"));
        $("#fquaStudent03h").val(8);
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#quaStudent", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm12Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(addMarkStudent).ff(true, addMarkStudent);
        _gb.rf(addMarkStudent);
        $('#faddMarkStudent01h').val(cur_gb_KeyId);
        $('#addMarkStudentc_').hide();
        $('#addMarkStudent_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#addMarkStudent", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm13Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(doiDiemStudent).ff(true, doiDiemStudent);
        _gb.rf(doiDiemStudent);
        $('#fdoiDiemStudent01h').val(cur_gb_KeyId);
        $('#doiDiemStudentc_').hide();
        $('#doiDiemStudent_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#doiDiemStudent", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function studentForm14Click($el, fId){
    $el.click(function(){
        cur_gb_ElementId = 'studentDetail';
        gb_strFilterGrid = ' studentId = ' + cur_gb_KeyId;
        _gb.pr(studentTakeCare).ff(true, studentTakeCare);
        _gb.rf(studentTakeCare);
        $('#fstudentTakeCare01h').val(cur_gb_KeyId);
        $('#studentTakeCarec_').hide();
        $('#studentTakeCare_f').css({ 'margin-top': '8px' });
        $("#fstudentTakeCare05i").remove();
        $areanote = $("<textarea rows='5' id='fstudentTakeCare05i' class='form-control' style='width: 545px!important'></textarea>")
        $("#r03studentTakeCare").append($areanote);
        gb_curBoxDlg = $el.colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#studentTakeCare", title: "Update Info <span style='color: WindowText'>" +  cur_gb_cur_ListDataShow.sN + "</span> <a href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'>New Tab</a>" });
    });
};
function dmstudentList_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew").val());
    var td = d2e($("#NgayKetThucIdNew").val());
    var st = $("#dmstudentList_Staush").val();
    var se = $("#dmstudentList_Sexh").val();
    var ao = $("#dmstudentList_AOFirsth").val();
    var sg = $("#dmstudentList_StudentGrouph").val();
    var so = $("#dmstudentList_Sourceh").val();
    var si = $("#dmstudentList_StudentName").val();
    var stW = "";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) >= '" + fd + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) <= '" + td + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) <= '" + td + "' ) "));
    stW = st == "" ? stW : (stW == "" ? (" (statusid = " + st + " ) ") : (stW + " and (statusid = " + st + " ) "));
    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = so == "" ? stW : (stW == "" ? (" (SourceId = " + so + " ) ") : (stW + " and (SourceId = " + so + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (Name like N'%" + si + "%' or Phone like N'%" + si + "%' ) ") : (stW + " and (Name like N'%" + si + "%' or Phone like N'%" + si + "%' ) "));
    $("#studentList_g").jtable('load', {
        obj: { a: 'pGet' + studentList._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dmstudentStudying_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew").val());
    var td = d2e($("#NgayKetThucIdNew").val());
    var st = $("#dmstudentStudying_Staush").val();
    var se = $("#dmstudentStudying_Sexh").val();
    var ao = $("#dmstudentStudying_AOFirsth").val();
    var sg = $("#dmstudentStudying_StudentGrouph").val();
    var so = $("#dmstudentStudying_Sourceh").val();
    var si = $("#dmstudentStudying_StudentName").val();
    var stW = " id in (select a.studentid from gbl_trainning_course_class_student a " +
                    "join gbl_trainning_course_class b on a.ClassId = b.id " +
                    "where (b.Id = 1612) or (b.StartDate >= '" + XDate.today().toString("MM/dd/yyyy") + "' or (b.startdate < '" 
                    + XDate.today().toString("MM/dd/yyyy") + "' and b.FinishDate >= '" + XDate.today().toString("MM/dd/yyyy") + "'))) ";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) >= '" + fd + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) <= '" + td + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) <= '" + td + "' ) "));
    stW = st == "" ? stW : (stW == "" ? (" (statusid = " + st + " ) ") : (stW + " and (statusid = " + st + " ) "));
    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = so == "" ? stW : (stW == "" ? (" (SourceId = " + so + " ) ") : (stW + " and (SourceId = " + so + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (Name like N'%" + si + "%') ") : (stW + " and (Name like N'%" + si + "%' ) "));
    $("#studentStudying_g").jtable('load', {
        obj: { a: 'pGet' + studentStudying._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dmstudentOld_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew").val());
    var td = d2e($("#NgayKetThucIdNew").val());
    var st = $("#dmstudentOld_Staush").val();
    var se = $("#dmstudentOld_Sexh").val();
    var ao = $("#dmstudentOld_AOFirsth").val();
    var sg = $("#dmstudentOld_StudentGrouph").val();
    var so = $("#dmstudentOld_Sourceh").val();
    var si = $("#dmstudentOld_StudentName").val();
    var stW = " id in (select a.studentid from gbl_trainning_course_class_student a " +
                    "join gbl_trainning_course_class b on a.ClassId = b.id " +
                    " where b.Id != 1612 group by a.studentid " +
                    " having max(b.StartDate) < '" + XDate.today().toString("MM/dd/yyyy") + "' and max(b.FinishDate) < '" 
                    + XDate.today().toString("MM/dd/yyyy") + "') ";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) >= '" + fd + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) <= '" + td + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) <= '" + td + "' ) "));
    stW = st == "" ? stW : (stW == "" ? (" (statusid = " + st + " ) ") : (stW + " and (statusid = " + st + " ) "));
    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = so == "" ? stW : (stW == "" ? (" (SourceId = " + so + " ) ") : (stW + " and (SourceId = " + so + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (Name like N'%" + si + "%') ") : (stW + " and (Name like N'%" + si + "%' ) "));
    $("#studentOld_g").jtable('load', {
        obj: { a: 'pGet' + studentOld._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dmstudentNot_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew").val());
    var td = d2e($("#NgayKetThucIdNew").val());
    var st = $("#dmstudentNot_Staush").val();
    var se = $("#dmstudentNot_Sexh").val();
    var ao = $("#dmstudentNot_AOFirsth").val();
    var sg = $("#dmstudentNot_StudentGrouph").val();
    var so = $("#dmstudentNot_Sourceh").val();
    var si = $("#dmstudentNot_StudentName").val();
    var stW = " id not in (select studentid from gbl_trainning_course_class_student)";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) >= '" + fd + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) <= '" + td + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) <= '" + td + "' ) "));
    stW = st == "" ? stW : (stW == "" ? (" (statusid = " + st + " ) ") : (stW + " and (statusid = " + st + " ) "));
    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = so == "" ? stW : (stW == "" ? (" (SourceId = " + so + " ) ") : (stW + " and (SourceId = " + so + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (Name like N'%" + si + "%') ") : (stW + " and (Name like N'%" + si + "%' ) "));
    $("#studentNot_g").jtable('load', {
        obj: { a: 'pGet' + studentNot._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dminquires_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew").val());
    var td = d2e($("#NgayKetThucIdNew").val());
    var st = $("#dminquires_Typeh").val();
    var se = $("#dminquires_Sexh").val();
    var ao = $("#dminquires_AOFirsth").val();
    var sg = $("#dminquires_StudentGrouph").val();
    var so = $("#dminquires_Sourceh").val();
    var si = $("#dminquires_StuIdh").val();
    var stW = "";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) >= '" + fd + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, isPrgCreateDate) <= '" + td + "' ) ") : (stW + " and (convert(date, isPrgCreateDate) <= '" + td + "' ) "));
    stW = st == "" ? stW : (stW == "" ? (" (Typeid = " + st + " ) ") : (stW + " and (typeid = " + st + " ) "));
    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (studentId = " + si + " ) ") : (stW + " and (studentId = " + si + " ) "));
    $("#inquires_g").jtable('load', {
        obj: { a: 'pGet' + inquires._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dmplacementTest_selectOption(){
    var st = $("#dmplacementTest_Resulth").val();
    var ao = $("#dmplacementTest_AOFirsth").val();
    var sg = $("#dmplacementTest_StudentGrouph").val();
    var si = $("#dmplacementTest_StuIdh").val();
    var stW = "";
    stW = st == "" ? stW : (stW == "" ? (" (FinalResultId = " + st + " ) ") : (stW + " and (FinalResultId = " + st + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (studentid = " + si + " ) ") : (stW + " and (studentid = " + si + " ) "));
    $("#placementTest_g").jtable('load', {
        obj: { a: 'pGet' + placementTest._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dmenrollmentList_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew").val());
    var td = d2e($("#NgayKetThucIdNew").val());
    var st = $("#dmenrollmentList_Staush").val();
    var se = $("#dmenrollmentList_Sexh").val();
    var ao = $("#dmenrollmentList_AOFirsth").val();
    var sg = $("#dmenrollmentList_StudentGrouph").val();
    var si = $("#dmenrollmentList_StuIdh").val();
    var iv = $("#dmenrollmentList_Invoice").val();
    var stW = "";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, invoicedate) >= '" + fd + "' ) ") : (stW + " and (convert(date, invoicedate) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, invoicedate) <= '" + td + "' ) ") : (stW + " and (convert(date, invoicedate) <= '" + td + "' ) "));
    stW = st == "" ? stW : (stW == "" ? (" (statusid = " + st + " ) ") : (stW + " and (statusid = " + st + " ) "));
    stW = se == "" ? stW : (stW == "" ? (" (GenderId = " + se + " ) ") : (stW + " and (GenderId = " + se + " ) "));
    stW = ao == "" ? stW : (stW == "" ? (" (AOFirstNameId = " + ao + " ) ") : (stW + " and (AOFirstNameId = " + ao + " ) "));
    stW = sg == "" ? stW : (stW == "" ? (" (GroupId = " + sg + " ) ") : (stW + " and (GroupId = " + sg + " ) "));
    stW = si == "" ? stW : (stW == "" ? (" (studentid = " + si + " ) ") : (stW + " and (studentid = " + si + " ) "));
    stW = iv == "" ? stW : (stW == "" ? (" (invoice like'%" + iv + "%') ") : (stW + " and (invoice like '%" + iv + "%' ) "));
    $("#enrollmentList_g").jtable('load', {
        obj: { a: 'pGet' + enrollmentList._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
};
function dmattendanceAL_selectOption(){
    var fd = d2e($("#NgayBatDauIdNew1").val());
    var td = d2e($("#NgayKetThucIdNew1").val());
    var se = $("#dmattendanceAL_Classh").val();
    var stW = " studentid = " + cur_gb_KeyId + " ";
    stW = fd == "" ? stW : (stW == "" ? (" (convert(date, Date) >= '" + fd + "' ) ") : (stW + " and (convert(date, Date) >= '" + fd + "' ) "));
    stW = td == "" ? stW : (stW == "" ? (" (convert(date, Date) <= '" + td + "' ) ") : (stW + " and (convert(date, Date) <= '" + td + "' ) "));
    stW = se == "" ? stW : (stW == "" ? (" (ClassId = " + se + " ) ") : (stW + " and (ClassId = " + se + " ) "));
    $("#attendanceAL_g").jtable('load', {
        obj: { a: 'pGet' + attendanceAL._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 100, se: ' id desc', f: stW }
        , jtStartIndex: 0
        , jtPageSize: 300
    });
    request({a: 'GetProcedure', c: [
                    " select CASE WHEN len(CONVERT(nvarchar, sum(isnull(TotalMinute, 0))/60)) = 1 THEN '0' "
                    + " + CONVERT(nvarchar, sum(isnull(TotalMinute, 0))/60) ELSE CONVERT(nvarchar, sum(isnull(TotalMinute, 0))/60)  "
                    + " END + ':' + CASE WHEN len(CONVERT(nvarchar, sum(isnull(TotalMinute, 0)) % 60))  "
                    + " = 1 THEN '0' + CONVERT(nvarchar, sum(isnull(TotalMinute, 0)) % 60)  "
                    + " ELSE CONVERT(nvarchar, sum(isnull(TotalMinute, 0)) % 60) end "
                    + " from zgcl_GBL_TRAINING_STUDENT_ATTENDANCEAL06_FULL "
                    + " where " + stW
                    + " group by studentid  " + (se == "" ? "" : ", ClassId ")
                    ]}, { success: function(o, op, r){ 
                        if(r.Result == "OK" && r.Records.length > 0){
                            $("#attendanceAL_TotalHour").html("<p>Total Learning Hours: <strong>" + r.Records[0][0] + "</strong></p>");
                        } else {
                            $("#attendanceAL_TotalHour").html("<p>Total Learning Hours: <strong>00:00</strong></p>")};
                        }
                    });
};
function autoFareKho(on) {
    if (($.isNumeric(gcRev($("#f" + on + "35i").val(), ',')) || $("#f" + on + "35i").val() == '')
            && ($.isNumeric(gcRev($("#f" + on + "04i").val(), ',')) || $("#f" + on + "04i").val() == '')) {
        var tt = gcRev($("#f" + on + "35i").val(), ',') == '' ? 0 : parseFloat(gcRev($("#f" + on + "35i").val(), ','));
        if($("#f" + on + "38i").val() == ''){
            if(tt > 1000)
                if(tt%1000 >= 500)
                    tt = (parseFloat(parseInt(tt/1000)) * 1000) + 1000;
                else
                    tt = (parseFloat(parseInt(tt/1000)) * 1000);
            $("#f" + on + "04i").val(gcFormatStr(tt.toString()));
        }
        else {
            var d = $("#f" + on + "38i").val().split(',');
            var ptr = tt;
            for(var i = 0; i < d.length; i++){
                if($.isNumeric(gcRev($("#f" + on + "38i").val(), ','))){
                    if (d[i] != ''){
                        var km = parseFloat(d[i]);
                        ptr = ptr - (km < 100 ? parseInt(km * ptr / 100) : km);
                    }
                } else {
                    $("#f" + on + "04i").val(gcFormatStr(tt.toString()));
                    alert('Bạn nhập dữ liệu không đúng');
                    return false;
                }
            }
            if(ptr > 1000)
                if(ptr%1000 >= 500)
                    ptr = (parseFloat(parseInt(ptr/1000)) * 1000) + 1000;
                else
                    ptr = (parseFloat(parseInt(ptr/1000)) * 1000);
            $("#f" + on + "04i").val(gcFormatStr(ptr.toString()));
        }
    }
};
function pgrs_studentDetail(r){
    if(r["f09"] != null){
        var dr = _parseDate(r["f09"]).format("dd/MM/yyyy").split("/");
        $("#studentDetailDay").val(parseInt(dr[0]));
        $("#studentDetailMonth").val(parseInt(dr[1]));
        $("#studentDetailYear").val(parseInt(dr[2]));
    }
};
function pgrs_studentAdd(r){
    if(r["f09"] != null){
        var dr = _parseDate(r["f09"]).format("dd/MM/yyyy").split("/");
        $("#studentAddDay").val(parseInt(dr[0]));
        $("#studentAddMonth").val(parseInt(dr[1]));
        $("#studentAddYear").val(parseInt(dr[2]));
    }
};


