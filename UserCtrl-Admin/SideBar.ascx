﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideBar.ascx.cs" Inherits="UserCtrl_Admin_SideBar" %>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="../QLAdmin/Default.aspx">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                    <small class="label pull-right bg-green">new</small>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
            </li>
            <li class="">
                <a href="../ZGCLOGINUSER/zgc0LoginUser.aspx">
                    <i class="fa fa-list"></i>
                    <span>Manager</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
