﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MainMenu.ascx.cs" Inherits="UserControl_HocVien_MainMenu" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#student-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-student ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#upcomming-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-upcomming ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#classes-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-classes ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#activelearning-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-activelearning ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#ftdp-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-ftdp ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#categorizes-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-categorizes ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#report-button a[data-rel='menu']").closest('button').click(function () {
            $("#dropdown-report ul li ." + $(this).find('a').attr('class')).closest('li').click();
        });
        $("#gc_BtnInDuLieuID").click(function (ev) {
            $("#gc_ReportForm").toggle('show');
        });
        //                $("#dropdown-database").mouseover(function () {
        //                    $(this).addClass('open');
        //                    $("#dropdown-account").removeClass('open');
        //                });
        //                $("#dropdown-account").mouseover(function () {
        //                    $(this).addClass('open');
        //                    $("#dropdown-database").removeClass('open');
        //                });

        $("#sdafsdfsadfsadf").click(function (ev) {
            $.colorbox.close();
        });



        $("#gc_gb_menu_CreateAccountMenuID").click(function (ev) {
            $("#gc_gb_MsgChangePassWordLabel").val('');
            gb_curBoxDlg = $("#gc_gb_menu_CreateAccountMenuID").colorbox({ inline: true, href: "#gc_gb_DIVCreateUserInnerWrapID", title: "Tạo tài khoản" });
        });


        $("#gc_gb_menu_UpdateAcountMenuID").click(function (ev) {
            $("#gc_gb_MsgChangePassWordLabel").val('');
            gb_curBoxDlg = $("#gc_gb_menu_UpdateAcountMenuID").colorbox({ inline: true, href: "#gc_gb_DIVChangePasswordInnerWrapID", title: "Thay đổi mật khẩu" });
        });

        $("#gc_usctrl_menu_UpdateCourseID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%",
                href: "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_TRAINNING_COURSE.aspx?_showHeader=0", title: "Cập nhật khóa học"
            });
        });
        $("#gc_usctrl_menu_UpdateClassID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%",
                href: "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_TRAINNING_COURSE_CLASS.aspx?_showHeader=0", title: "Cập nhật lớp học"
            });
        });
        $("#gc_usctrl_menu_UpdateProgID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%",
                href: "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_LITERAL_CHUONGTRINHHOC.aspx?_showHeader=0", title: "Cập nhật chương trình học"
            });
        });
        $("#gc_usctrl_menu_UpdateStudentID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_CUST_Customer.aspx?_showHeader=0", title: "Cập nhật Học viên"
            });
        });
        $("#gc_usctrl_menu_UpdateTeacherID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_zgcl_gcGobal_TRAINNING_TEACHER.aspx?_showHeader=0", title: "Cập nhật dữ Giáo viên"
            });
        });
        $("#gc_usctrl_menu_UpdateEmployID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_COMP_EmployeeLife.aspx?_showHeader=0", title: "Cập nhật Nhân viên"
            });
        });

        $("#gc_usctrl_menu_UpdateLEPHIID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_LITERAL_LEPHI.aspx?_showHeader=0", title: "DANH SÁCH LỆ PHÍ	"
            });
        });
        $("#gc_usctrl_menu_UpdateGIAMGIAID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_LITERAL_GIAMGIA.aspx?_showHeader=0", title: "Cập nhật GIẢM HỌC PHÍ"
            });
        });
        $("#gc_usctrl_menu_UpdateKHANANGPHONGID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_LITERAL_KHANANGPHONG.aspx?_showHeader=0", title: "Cập nhật KHẢ NĂNG PHÒNG "
            });
        });
        $("#gc_usctrl_menu_UpdateTHULAOGIAOVIENID").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_LITERAL_THULAOGIAOVIEN.aspx?_showHeader=0", title: "Cập nhật THÙ LAO GIÁO VIÊN"
            });
        });


        $("#gc_usctrl_menu_GIOHOC").click(function () {
            $(this).colorbox({
                iframe: true, innerWidth: "90%", innerHeight: "95%", href:
                "../zgcDirADMIN_SERV_PRODUCT/ADMIN_gcGobal_LITERAL_GIOHOC.aspx?_showHeader=0", title: "Cập nhật"
            });
        });



    });

</script>

<div class="gc-gb-right-toolbar">
    <div class="button-container ">

        <img class="button-arrow" src="../UserControl/images/bird.png">

        <div class="gc-gb-quick">
            <a href="#top">
                <img src="../UserControl/images/quick1_01.png" style="position: absolute; top: 20px; right: 2px;" />
            </a>
            <a class="a1">
                <img src="../UserControl/images/quick1_04.png" style="position: absolute; top: 60px; right: 2px;" />
            </a>
            <a href="Default.aspx">
                <img src="../UserControl/images/quick1_06.png" style="position: absolute; top: 100px; right: 2px;" />
            </a>

        </div>
    </div>
</div>

<div class="content" id="gc_gobal_HeaderMainFormID">
    <div id='Div1' class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <a href="../ZGCLOGINUSER/zgc0LoginUser.aspx" class="btn btn-success"><i class="glyphicon glyphicon-home"></i>Trang chủ</a>
                    <div class="btn-group " id='dropdown-student'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Students <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="a1"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Add Student</a></li>
                            <li><a href="#" class="a2"><i class="i"></i>Student List</a></li>
                            <li><a href="#" class="a3"><i class="i"></i>Inquiries</a></li>
                            <li><a href="#" class="a4"><i class="i"></i>Placement Tests</a></li>
                            <li><a href="#" class="a5"><i class="i"></i>Enrollment List</a></li>
                            <li><a href="#" class="a6"><i class="i"></i>Studying student</a></li>
                            <li><a href="#" class="a7"><i class="i"></i>Old Student</a></li>
                            <li><a href="#" class="a8"><i class="i"></i>Potential Customer</a></li>

                        </ul>

                    </div>
                    <div class="btn-group" id='dropdown-upcomming'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Upcomming<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <%--<li><a href="#" class="a1"><i class="i"></i>Add New Upcoming Class</a></li>--%>
                            <li><a href="#" class="a2"><i class="i"></i>Upcoming Class List</a></li>
                            <li><a href="#" class="a3"><i class="i"></i>Enrolled Student Analysis</a></li>
                            <li><a href="#" class="a4"><i class="i"></i>Up-coming class Analysis</a></li>
                        </ul>

                    </div>
                    <div class="btn-group " id='dropdown-classes'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Classes <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a  href="#" class="a1"><i class="i"></i>Class List</a></li>
                            <li><a  href="#" class="a2"><i class="i"></i>Class Notes</a></li>
                            <li><a  href="#" class="a3"><i class="i"></i>Special Note for Students</a></li>
                            <li><a href="#" class="a4"><i class="i"></i>Add New Non-Invoice Class </a></li>
                            <li><a href="#" class="a5"><i class="i"></i>Events and Tasks in Class</a></li>
                            <li><a href="#" class="a6"><i class="i"></i>Track Classes</a></li>
                            <li><a href="#" class="a7"><i class="i"></i>Online Learning Report</a></li>
                            <li><a href="#" class="a8"><i class="i"></i>Search Birthdays</a></li>
                        </ul>

                    </div>
                    <div class="btn-group " id='dropdown-activelearning'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Active Learning <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="a1"><i class="i"></i>AL Students</a></li>
                            <li><a href="#" class="a2"><i class="i"></i>Attendance</a></li>
                            <li><a href="#" class="a3"><i class="i"></i>Manage AL Students</a></li>
                            <li><a href="#" class="a4"><i class="i"></i>Feedbacks and Requests</a></li>
                            <li><a href="#" class="a5"><i class="i"></i>Events and Tasks</a></li>
                            <li><a href="#" class="a6"><i class="i"></i>Marks and Tests</a></li>
                        </ul>

                    </div>
                    <div class="btn-group " id='dropdown-ftdp'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>FTDP<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="a1"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Teaching Hours</a></li>
                            <li><a href="#" class="a2"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Teacher Infomation</a></li>
                            <li><a href="#" class="a3"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Check Teacher Availability</a></li>
                            <li><a href="#" class="a4"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Manage Classes and Schools</a></li>
                            <li><a href="#" class="a5"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Add Teacher</a></li>
                        </ul>

                    </div>
                    <div class="btn-group " id='dropdown-teacher'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Teachers <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="a1"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Check Teaching Hours</a></li>
                            <li><a href="#" class="a2"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Add Teaching Hours</a></li>
                            <li><a href="#" class="a3"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Check Teacher Availability</a></li>
                            <li><a href="#" class="a4"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Teacher Information</a></li>
                            <li><a href="#" class="a5"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Tracking In-Progress Classes</a></li>
                            <li><a href="#" class="a6"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Add Teacher</a></li>
                            <li><a href="#" class="a7"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Check TA Availability</a></li>
                            <li><a href="#" class="a8"><i class="glyphicon glyphicon-pencil gc-th-color"></i>Add TA</a></li>
                            <li><a href="#" class="a9"><i class="glyphicon glyphicon-pencil gc-th-color"></i>TA Infomation</a></li>
                        </ul>

                    </div>
                    <div class="btn-group " id='dropdown-categorizes'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Categorizes <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="a1"><i class="i"></i>Room Availabilty</a></li>
                            <li><a href="#" class="a2"><i class="i"></i>Rooms</a></li>
                            <li><a href="#" class="a3"><i class="i"></i>Schedules</a></li>
                            <li><a href="#" class="a15"><i class="i"></i>Schedule Details</a></li>
                            <li><a href="#" class="a4"><i class="i"></i>Time and Shifts</a></li>
                            <li><a href="#" class="a5"><i class="i"></i>Group Course</a></li>
                            <li><a href="#" class="a6"><i class="i"></i>Course</a></li>
                            <li><a href="#" class="a7"><i class="i"></i>Course Levels</a></li>
                            <li><a href="#" class="a8"><i class="i"></i>Schools</a></li>
                            <li><a href="#" class="a9"><i class="i"></i>Source of Information</a></li>
                            <li><a href="#" class="a10"><i class="i"></i>Subjects</a></li>
                            <li><a href="#" class="a11"><i class="i"></i>Lesson</a></li>
                            <li><a href="#" class="a12"><i class="i"></i>Worksheet</a></li>
                            <li><a href="#" class="a13"><i class="i"></i>Surveys</a></li>
                            <li><a href="#" class="a14"><i class="i"></i>Feedback and Service Types</a></li>
                            <li><a href="#" class="a16"><i class="i"></i>Type Test</a></li>
                            <li><a href="#" class="a17"><i class="i"></i>Day Off</a></li>
                        </ul>

                    </div>


                    <div class="btn-group " id='dropdown-report'>

                        <button type="button" class="btn btn-navy  dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-info-sign"></i>Reports <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <%--<li><a href="#" class="a1"><i class="i"></i>Detail Enrollments</a></li>
                            <li><a href="#" class="a2"><i class="i"></i>Monthly Sales Tracking</a></li>
                            <li><a href="#" class="a3"><i class="i"></i>Daily Sales Tracking</a></li>
                            <li><a href="#" class="a4"><i class="i"></i>Overall Classes</a></li>
                            <li><a href="#" class="a5"><i class="i"></i>Teaching Hours of Teachers</a></li>--%>
                            <li><a href="#" class="a6"><i class="i"></i>SMS Service</a></li>
                            <li><a  id="class_report_btn" ><i class="i"></i>Class Report</a></li>
                            <li><a  id="student_report_btn" ><i class="i"></i>Student Report</a></li>
                            <li><a  id="teacher_report_btn" ><i class="i"></i>Teacher Report</a></li>
                            <li><a  id="saler_report_btn" ><i class="i"></i>Saler Report</a></li>
                        </ul>

                    </div>
                    <div class="btn-group " id='Div21'>

                        <a  class="btn btn-warning dropdown-toggle" href='../QLHocVien/DefaultQLHocVien.aspx' target='_blank'><i class="glyphicon glyphicon-info-sign"></i>New Tab</a><span class="caret"></span>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Menu con -->
<!-- Student -->
<div class="clear"></div>
<div class="content2" id="student-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group " id='Div2'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a class="a1" data-rel="menu">Add Student</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div3'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=" "></i><a class="a2" data-rel="menu">Students List</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div4'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a class="a3" data-rel="menu">Inquiries</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div5'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a class="a4" data-rel="menu">Placement Tests</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div6'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a class="a5" data-rel="menu">Enrollment List</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div7'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a class="a6" data-rel="menu">Studying Student</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div8'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a7" data-rel="menu">Old Student</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div9'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a class="a8" data-rel="menu">Potential Customer</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div58'>
                        <input type="text" id="searchStudentName" class="form-control" style="width: 150px" />
                            <i class=""></i><a style="color: Blue" class="a9 btn br-button" id="searchStudentNameButton">Search</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Upcomming -->
<div class="content2" id="upcomming-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <%--<div class="btn-group " id='Div10'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a1" data-rel="menu">Add New Upcomming Class</a>
                        </button>
                    </div>--%>
                    <div class="btn-group " id='Div11'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a2" data-rel="menu">Upcomming Class List</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div12'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a3" data-rel="menu">Enrolled Student Analysis </a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div13'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a4" data-rel="menu">Up-coming class Analysis</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Classes -->
<div class="content2" id="classes-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group " id='Div14'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a1" data-rel="menu">Class List</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div15'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a2" data-rel="menu">Class Notes</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div16'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a3" data-rel="menu">Special Note for Students</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div17'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a4" data-rel="menu">Add New Class</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div18'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a5" data-rel="menu">Events & Tasks</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div19'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a6" data-rel="menu">Track Classes</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div20'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a7" data-rel="menu">Ol/Learning Report</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div59'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a8" data-rel="menu">Search Birthdays</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div60'>
                        <input type="text" id="searchClassName" class="form-control" style="width: 150px" />
                            <i class=""></i><a style="color: Blue" class="a9 btn br-button" id="searchClassNameButton">Search</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Activelearing -->
<div class="content2" id="activelearning-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group " id='Div22'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a1" data-rel="menu">AL Students</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div62'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a2" data-rel="menu">Attendance</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div23'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a3" data-rel="menu">Manage AL Students</a>
                        </button>
                    </div>
                    
                    <div class="btn-group " id='Div24'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a4" data-rel="menu">Feedbacks and Requests</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div25'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a5" data-rel="menu">Events and Tasks</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div26'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a6" data-rel="menu">Marks and Tests</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div61'>
                        <input type="text" id="searchStudentNameAL" class="form-control" style="width: 150px" />
                            <i class=""></i><a style="color: Blue" class="a9 btn br-button" id="searchStudentNameALButton">Search</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--FTDP menu -->
<div class="content2" id="ftdp-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group " id='Div27'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a1" data-rel="menu">Teaching Hours</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div28'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a2" data-rel="menu">Teacher Infomation</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div29'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a3" data-rel="menu">Check Teacher Availability</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div30'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a4" data-rel="menu">Manage Classes and Schools</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div31'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a5" data-rel="menu">Add Teacher</a>
                        </button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!--Teacher menu -->
<div class="content2" id="teacher-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group " id='Div32'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a1" data-rel="menu">Check Teaching Hours</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div33'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a2" data-rel="menu">Add Teaching Hours</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div34'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a3" data-rel="menu">Check Teacher Availability</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div35'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a4" data-rel="menu">Teacher Information </a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div36'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a5" data-rel="menu">Tracking In-Progress Classes</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div37'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a6" data-rel="menu">Add Teacher</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div55'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a7" data-rel="menu">Check TA Availability</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div56'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a8" data-rel="menu">Add TA</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div57'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a9" data-rel="menu">TA Infomation</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Categorizes -->
<div class="content2" id="categorizes-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group " id='Div38'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a1" data-rel="menu">Room Availability</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div39'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a2" data-rel="menu">Rooms</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div40'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a3" data-rel="menu">Schedules</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div53'>
                        <button type="button" class="btn btn-menu  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a15" data-rel="menu">Schedule Details</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div41'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a4" data-rel="menu">Time & Shifts</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div42'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a5" data-rel="menu">Group Course</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div43'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a6" data-rel="menu">Course</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div44'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a7" data-rel="menu">Course Levels</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div45'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a8" data-rel="menu">Schools</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div46'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a9" data-rel="menu">SOF</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div47'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            </i><a href="#" class="a10" data-rel="menu">Subjects</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div48'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            </i><a href="#" class="a11" data-rel="menu">Lesson</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div49'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a12" data-rel="menu">Worksheet</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div50'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            </i><a href="#" class="a13" data-rel="menu">Surveys</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div51'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a14" data-rel="menu">Feedback & Service</a>
                        </button>
                    </div>
                    <div class="btn-group " id='Div54'>
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <a href="#" class="a16" data-rel="menu">Type Test</a>
                        </button>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content2" id="report-button" style="display: none">
    <div class="gc_gobal_HeaderMainForm">
        <div class="inputBtnBottomVHead">
            <div class="btn-toolbar" style="margin-bottom: 9px">
                <div class="btn-group">
                    <div class="btn-group ">
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a5" data-rel="menu">SMS Service</a>
                        </button>
                    </div>
                    <div class="btn-group " >
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a6" data-rel="menu">Class Report</a>
                        </button>
                    </div>
                    <div class="btn-group ">
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a7" data-rel="menu">Student Report</a>
                        </button>
                    </div>
                    <div class="btn-group ">
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a8" data-rel="menu">Teacher Report</a>
                        </button>
                    </div>
                    <div class="btn-group ">
                        <button type="button" class="btn br-button  dropdown-toggle" data-toggle="dropdown">
                            <i class=""></i><a href="#" class="a9" data-rel="menu">Saler</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
