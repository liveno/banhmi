﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="UserCtrl_Admin_Footer" %>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; @DateTime.Now.Year <a href="#">GCORE TEAM</a>.</strong> All rights reserved.
</footer>
