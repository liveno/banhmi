﻿
var d2 = {
    title: 'Danh sách phiếu',
    jElementTit: "#gc_gbHTitle",
    /* element, attribute, element1, attribute1, class1, id1, label, element2, attribute2, class2, id2, typedata,col, default */
    /* typeData [int: 1; float: 2; string: 4; date: 5; bool: 3, 6: money] */
    data: [
        ['h3', '', '', '', '', '', '', '', '', '', '', 4, 2, '_'],
        ['h2', '', '', '', '', '', '', '', '', '', '', 4, 3, '_'],
        ['p', '', 'strong', '', '', '', 'Tiền biêu: ', 'strong', 'style="color: red;"', '', '', 6, 8, '_'],
        ['p', '', 'strong', '', '', '', 'Thanh toán: ', 'strong', 'style="color:blue;"', '', '', 6, 10, '_'],
        ['p', '', '', '', '', '', 'Kho xuất: ', 'strong', 'style="color: rgb(204, 0, 255);"', '', '', 4, 41, '_'],
        ['p', '', '', '', '', '', 'Khách hàng: ', 'strong', '', '', '', 5, 39, '_']
    ],
    eventItem: function () {
        var that = this;
        /************************************************************************
        * Xử lý khi click vào Phiếu nhập kho                                    *
        *************************************************************************/
        $(".item").click(function (ev) {
            var $self = $(this);
            cur_gb_ReceitpID = cur_gb_cur_ListDataShow[$self.find('div:first').attr('data-index')][1];
            $self.addClass("itemcolor-theme2");
            that.showPopup($self);
            if (cur_gb_ReceitpID == "-1") {
                request({
                    a: 'InsertgcGobal_STOCK_gcProduct_Output',
                    d: {
                        Ten: $self.find('h3').text(),
                        SoCT: $self.find('h2').text(),
                        NgayLap: dateToString(new Date(), 'iso'),
                        KhoNhapId: 1,
                        KhoXuatId: 1,
                        isPrgOrdered: 0,
                        isPrgAccountId: cur_gb_AccountId,
                        isPrgVNKoDau: cur_gb_Account,
                        isPrgCreateDate: dateToString(new Date(), 'iso'),
                        isPrgAccountUpdateId: cur_gb_AccountId,
                        isPrgSmField: (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId,
                        isPrgPartComp: cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId
                    }
                }, {
                    success: function (obj, opt, r) {
                        if (r.Result == 'OK') {
                            cur_gb_cur_ListDataShow[$self.find('div:first').attr('data-index')][1] = r.Records;
                            cur_gb_ReceitpID = r.Records;
                            gb_strFilterGrid = " PhieuXuatKhoId=" + cur_gb_ReceitpID;
                            $('#fo21501i').val(cur_gb_ReceitpID);
                            $('#fo21501h').val(cur_gb_ReceitpID);
                        } else
                            alert('Hệ thống hiện đang bảo trì mong ban thông cảm!');
                        $("#profileTab").click();
                    }
                });
            }
        });
    },
    button: [['Cập nhật', 'CapNhatPhuThuKMForm'], ['In phiếu', 'ItemPrintBill']],
    CapNhatPhuThuKMForm: function (o, key, index) {
        gb_strFilterGrid = '';
        cur_gb_ReceitpID = key;
        _gb.pr(o214).ff(false, o214);
        _gb.rf(o214);
        $('#o214c_').hide();
        $('#o214_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $(o).colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#o214", title: "Thông tin cập nhật" });
    },
    ItemPrintBill: function (o, key, index) {
        modalcenter.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
        cur_gb_ReceitpID = key;
        var obj = new Object();
        obj.Name = 'Testing';
        obj.Id = key;
        showReport(obj, 'Receipt', 'ActiveCurrentBill', 'PrintPhieuNKToPrinterP258', "../zgcReport/ReportOutput.ashx");
    },
    showPopup: function ($that) {
        gb_strFilterGrid = " PhieuXuatKhoId=" + cur_gb_ReceitpID;
        _gb.pr(o215).ff(true, o215);
        _gb.rf(o215);
        autoFare($("#fo21506i"), $("#fo21503i"), $("#fo21507i"));
        $('#o215c_').hide();
        $('#o215_f').css({ 'margin-top': '8px' });
        $('#r00o215').hide();
        $('#lo21501i').removeClass('brlabel125').hide();
        $('#fo21501i').hide();
        $('#fo21501i').val(cur_gb_ReceitpID);
        $('#fo21501h').val(cur_gb_ReceitpID);
        var $divClose = $("<h1 style='line-height: 32px;'>Tiền hàng:<span  id='gcGobal_STOCK_gcProduct_Output_Detail_ToTalMoney' style='font-size:18px;' class='label label-danger'> </span>  »»»Tiền thối:<span  id='gcGobal_STOCK_gcProduct_Output_Detail_ThoiLaiMoney' style='font-size:18px;' class='label label-info'> </span></h1>");
        $('#r00o215').before($divClose);
        gb_curBoxDlg = $that.colorbox({ inline: true, innerWidth: "96%", innerHeight: "90%", href: "#o215", title: "Cập nhật phiếu" });
    }
};

// TSCD_Nhập
var d9 = {
    title: 'Danh sách phiếu',
    jElementTit: "#gc_gbHTitle",
    /* element, attribute, element1, attribute1, class1, id1, label, element2, attribute2, class2, id2, typedata,col, default */
    /* typeData [int: 1; float: 2; string: 4; date: 5; bool: 3, 6: money] */
    data: [
        ['h3', '', '', '', '', '', '', '', '', '', '', 4, 2, '_'],
        ['h2', '', '', '', '', '', '', '', '', '', '', 4, 3, '_'],
        ['a', '', '', '', '', '', 'Ngày lập: ', '', '', '', '', 5, 4, '_'],
        ['p', '', '', '', '', '', 'Giá TSBĐ ', 'strong', 'style="color: rgb(204, 0, 255);"', '', '', 6, 11, '_'],
        ['p', '', '', '', '', '', 'NCC: ', 'strong', '', '', '', 4, 51, '_'],
        ['p', '', '', '', '', '', 'Người lập: ', 'span', 'style="font-style:italic;color:red;"', '', '', 4, 42, '_']
    ],
    eventItem: function () {
        var that = this;
        /************************************************************************
        * Xử lý khi click vào Phiếu nhập kho                                    *
        *************************************************************************/
        $(".item").click(function (ev) {
            var $self = $(this);
            cur_gb_ReceitpID = cur_gb_cur_ListDataShow[$self.find('div:first').attr('data-index')][1];
            $self.addClass("itemcolor-theme2");
            that.showPopup($self);
            if (cur_gb_ReceitpID == "-1") {
                request({
                    a: 'InsertgcGobal_TSCD_NhapTaiSan',
                    d: {
                        MaCT: $self.find('h3').text(),
                        SoCT: $self.find('h2').text(),
                        NgayLap: dateToString(new Date(), 'iso'),
                        isPrgOrdered: 0,
                        isPrgAccountId: cur_gb_AccountId,
                        isPrgVNKoDau: cur_gb_Account,
                        isPrgCreateDate: dateToString(new Date(), 'iso'),
                        isPrgAccountUpdateId: cur_gb_AccountId,
                        isPrgSmField: (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId,
                        isPrgPartComp: cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId
                    }
                }, {
                    success: function (obj, opt, r) {
                        if (r.Result == 'OK') {
                            cur_gb_cur_ListDataShow[$self.find('div:first').attr('data-index')][1] = r.Records;
                            cur_gb_ReceitpID = r.Records;
                            gb_strFilterGrid = " NhapTaiSanId=" + cur_gb_ReceitpID;
                            $('#fo33601i').val(cur_gb_ReceitpID);
                            $('#fo33601h').val(cur_gb_ReceitpID);
                        } else
                            alert('Hệ thống hiện đang bảo trì mong ban thông cảm!');
                    }
                });
            }
        });
    },
    button: [['Cập nhật', 'CapNhatPhuThuKMForm'], ['In phiếu', 'ItemPrintBill']],
    CapNhatPhuThuKMForm: function (o, key, index) {
        gb_strFilterGrid = '';
        cur_gb_ReceitpID = key;
        _gb.pr(o335).ff(false, o335);
        _gb.rf(o335);
        $('#o335c_').hide();
        $('#o335_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $(o).colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#o335", title: "Thông tin cập nhật" });
    },
    ItemPrintBill: function (o, key, index) {
        cur_gb_ReceitpID = key;
        var obj = new Object();
        obj.Name = 'Testing';
        obj.Id = key;
        showReport(obj, 'Receipt', 'ActiveCurrentBill', 'PrintPhieuNKToPrinterP258', "../zgcReport/ReportInput.ashx");
    },
    showPopup: function ($that) {
        gb_strFilterGrid = " NhapTaiSanId=" + cur_gb_ReceitpID;
        _gb.pr(o336).ff(true, o336);
        _gb.rf(o336);
        autoFare($("#fo33606i"), $("#fo33603i"), $("#fo33607i"));
        $('#o336c_').hide();
        $('#o336_f').css({ 'margin-top': '8px' });
        $('#r00o336').hide();
        $('#lo33601i').removeClass('brlabel125').hide();
        $('#fo33601i').hide();
        $('#fo33601i').val(cur_gb_ReceitpID);
        $('#fo33601h').val(cur_gb_ReceitpID);
        var $divClose = $("<h1 style='line-height: 32px;'>Tiền hàng:<span  id='gcGobal_TSCD_NhapTaiSan_Detail_ToTalMoney' style='font-size:18px;' class='label label-danger'> </span>  »»»Tiền thối:<span  id='gcGobal_TSCD_NhapTaiSan_Detail_ThoiLaiMoney' style='font-size:18px;' class='label label-info'> </span></h1>");
        $('#r00o336').before($divClose);
        gb_curBoxDlg = $that.colorbox({ inline: true, innerWidth: "96%", innerHeight: "90%", href: "#o336", title: "Cập nhật phiếu" });
    }
};
// TSCD_ThanhLy
var d10 = {
    title: 'Danh sách phiếu',
    jElementTit: "#gc_gbHTitle",
    /* element, attribute, element1, attribute1, class1, id1, label, element2, attribute2, class2, id2, typedata,col, default */
    /* typeData [int: 1; float: 2; string: 4; date: 5; bool: 3, 6: money] */
    data: [
        ['h3', '', '', '', '', '', '', '', '', '', '', 4, 2, '_'],
        ['h2', '', '', '', '', '', '', '', '', '', '', 4, 3, '_'],
        ['p', '', 'strong', '', '', '', 'Tiền biêu: ', 'strong', 'style="color: red;"', '', '', 6, 8, '_'],
        ['p', '', '', '', '', '', 'Kho nhập: ', 'strong', 'style="color: rgb(204, 0, 255);"', '', '', 4, 42, '_'],
        ['p', '', '', '', '', '', 'NCC: ', 'strong', '', '', '', 5, 39, '_'],
        ['p', '', '', '', '', '', 'Người lập: ', '', '', '', '', 4, 32, 'NO']
    ],
    eventItem: function () {
        var that = this;
        /************************************************************************
        * Xử lý khi click vào Phiếu nhập kho                                    *
        *************************************************************************/
        $(".item").click(function (ev) {
            var $self = $(this);
            cur_gb_ReceitpID = cur_gb_cur_ListDataShow[$self.find('div:first').attr('data-index')][1];
            $self.addClass("itemcolor-theme2");
            that.showPopup($self);
            if (cur_gb_ReceitpID == "-1") {
                request({
                    a: 'InsertgcGobal_TSCD_THANHLY',
                    d: {
                        MaCT: $self.find('h3').text(),
                        SoCT: $self.find('h2').text(),
                        NgayLap: dateToString(new Date(), 'iso'),
                        isPrgOrdered: 0,
                        isPrgAccountId: cur_gb_AccountId,
                        isPrgVNKoDau: cur_gb_Account,
                        isPrgCreateDate: dateToString(new Date(), 'iso'),
                        isPrgAccountUpdateId: cur_gb_AccountId,
                        isPrgSmField: (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId,
                        isPrgPartComp: cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId
                    }
                }, {
                    success: function (obj, opt, r) {
                        if (r.Result == 'OK') {
                            cur_gb_cur_ListDataShow[$self.find('div:first').attr('data-index')][1] = r.Records;
                            cur_gb_ReceitpID = r.Records;
                            gb_strFilterGrid = " NhapTaiSanId=" + cur_gb_ReceitpID;
                            $('#fo34201i').val(cur_gb_ReceitpID);
                            $('#fo34201h').val(cur_gb_ReceitpID);
                        } else
                            alert('Hệ thống hiện đang bảo trì mong ban thông cảm!');
                    }
                });
            }
        });
    },
    button: [['Cập nhật', 'CapNhatPhuThuKMForm'], ['In phiếu', 'ItemPrintBill']],
    CapNhatPhuThuKMForm: function (o, key, index) {
        gb_strFilterGrid = '';
        cur_gb_ReceitpID = key;
        _gb.pr(o341).ff(false, o341);
        _gb.rf(o341);
        $('#o341c_').hide();
        $('#o341_f').css({ 'margin-top': '8px' });
        gb_curBoxDlg = $(o).colorbox({ inline: true, innerHeight: "90%", innerWidth: "96%", href: "#o341", title: "Thông tin cập nhật" });
    },
    ItemPrintBill: function (o, key, index) {
        cur_gb_ReceitpID = key;
        var obj = new Object();
        obj.Name = 'Testing';
        obj.Id = key;
        showReport(obj, 'Receipt', 'ActiveCurrentBill', 'PrintPhieuNKToPrinterP258', "../zgcReport/ReportInput.ashx");
    },
    showPopup: function ($that) {
        gb_strFilterGrid = " NhapTaiSanId=" + cur_gb_ReceitpID;
        _gb.pr(o342).ff(true, o342);
        _gb.rf(o342);
        autoFare($("#fo34206i"), $("#fo34203i"), $("#fo34207i"));
        $('#o342c_').hide();
        $('#o342_f').css({ 'margin-top': '8px' });
        $('#r00o342').hide();
        $('#lo34201i').removeClass('brlabel125').hide();
        $('#fo34201i').hide();
        $('#fo34201i').val(cur_gb_ReceitpID);
        $('#fo34201h').val(cur_gb_ReceitpID);
        var $divClose = $("<h1 style='line-height: 32px;'>Tiền hàng:<span  id='gcGobal_TSCD_THANHLY_Detail_ToTalMoney' style='font-size:18px;' class='label label-danger'> </span>  »»»Tiền thối:<span  id='gcGobal_TSCD_THANHLY_Detail_ThoiLaiMoney' style='font-size:18px;' class='label label-info'> </span></h1>");
        $('#r00o342').before($divClose);
        gb_curBoxDlg = $that.colorbox({ inline: true, innerWidth: "96%", innerHeight: "90%", href: "#o342", title: "Cập nhật phiếu" });
    }
};