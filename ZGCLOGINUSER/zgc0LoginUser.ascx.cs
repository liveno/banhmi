using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using System.Threading;
using System.IO;
using zgc0LibAdmin;
public partial class zgc0LoginUserCX : System.Web.UI.UserControl
{
    string _id = "-1";
    public int STT = 1;
	protected void Page_Load(object sender, EventArgs e)
	{
		//---------------------------------------------------------;
		//Đăng ký biến;
		System.Text.StringBuilder strBuild = new System.Text.StringBuilder();
		strBuild.AppendLine("var curObjP9000 = null;");
		strBuild.Append("var curObjIdP9000 = null, curRowIndexP9000 = null;");
		strBuild.AppendLine("var otP9000 = {lErrorCID : '" + this.lError.ClientID + "'");
		
        //strBuild.Append(",BtnAddP9000CID : '" + this.BtnAddP9000.ClientID + "'");
        //strBuild.Append(",BtnThongKeP9000CID : '" + this.BtnThongKeP9000.ClientID + "'");
        //strBuild.Append(",BtnInfoP9000CID : '" + this.BtnInfoP9000.ClientID + "'");
	
	
		strBuild.Append("};");
		//strBuild.Append("var MainFormCID = '" + this.MainForm.ClientID + "';");
		//strBuild.Append("var ChildFormP34CID = '" + this.ChildFormP34.ClientID + "';");
		//strBuild.Append("var ChildFormP25CID = '" + this.ChildFormP25.ClientID + "';");
		ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "P9000", strBuild.ToString(), true); 
		if(!Page.IsPostBack)
		{
			_id = (Request.QueryString["_myId"]==null)?"-1":Request.QueryString["_myId"];
			if(_id=="-1") _id = (Session["myId"]==null)? "-1": (string)Session["myId"];
			//---------------------------------------------------
			//Kiểm tra số nguyên
			//End Kiểm tra số nguyên
        //lError.Text = FormatDiv("", "Điền thông tin vào mẫu bên dưới");
		}
	}
    protected void SendSMS_Click(object sender, EventArgs e)
    {
        string sqlStr = "server=112.213.84.113\\MSSQL2K8;database=mailisa;uid=lisa2;pwd=chimai";
        string query = "select * from gcGobal_STOCK_gcService";

        DataTable myDataTable = zgc0HelperSecurity.GetDataTableNew(query, zgc0GlobalStr.getSqlStr());
       
        if (myDataTable.Rows.Count > 0)
        {
            for(int m=0;m<myDataTable.Rows.Count; m++)
            //for (int m = 0; m < 10; m++)
            {
                //-----------------------------------------------------
                double gia = (double)myDataTable.Rows[m]["GiaDichVu"];
                double giamoi = 0;
                double trugia = 0;
                string tenmoi = "Gia công " + myDataTable.Rows[m]["Name"].ToString();
                string codemoi = "GC-"+ myDataTable.Rows[m]["Code"].ToString();
                string storecode = codemoi;
                //-------------------------------------------------------------------------
                string name = myDataTable.Rows[m]["Code"].ToString();
                if (name.Length < 4)
                    continue;
                string leftname = name.Substring(0, 4);
                
                name = tenmoi;
                codemoi = codemoi.ToLower();
                if (leftname.Contains("AMKN") || leftname.Contains("AMKT") || leftname.Contains("AGD") || leftname.Contains("AG2M") || leftname.Contains("AB3L"))
                {
                    if (codemoi.Contains("10x10"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 2700;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 2700;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 2700;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("10x15"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 3150;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 3150;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 3150;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("9x12"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 1890;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 1890;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 1890;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("12x12"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 3150;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 3150;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 3150;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }

                    else if (codemoi.Contains("13x18"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 6300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 6300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 6300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("15x15"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 6300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 6300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 6300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("15x21"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 10800;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 10800;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 10800;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("15x23"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 10800;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 10800;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 10800;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }

                    else if (codemoi.Contains("20x25"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 14400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 14400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 14400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("20x20"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 11700;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 11700;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 11700;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("20x30"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 15300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 15300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 15300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("25x25"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 18000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 18000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 18000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("25x30"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 21600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 21600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 21600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("25x35"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 24300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 24300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 24300;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("25x38"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 27000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 27000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 27000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    
                    else if (codemoi.Contains("30x30"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 26100;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 26100;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 26100;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("30x35"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 1050;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 1050;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 1050;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("30x40"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 33750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 33750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 33750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("30x45"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 38200;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 38200;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 18750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    
                    else if (codemoi.Contains("35x35"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("30x50"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("35x40"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("35x45"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 38250;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("35x50"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 24750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 24750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 24750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("40x40"))
                    {
                       
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("10  t"))
                        {
                            trugia = 10 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15  t"))
                        {
                            trugia = 15 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20  t"))
                        {
                            trugia = 20 * 45000;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("40x45"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("10  t"))
                        {
                            trugia = 10 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15  t"))
                        {
                            trugia = 15 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                         else if (name.Contains("20  t"))
                        {
                            trugia = 20 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("40x50"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 50400;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("40x55"))
                    {
                        if (name.Contains("10"))
                        {
                            trugia = 10 * 33750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15"))
                        {
                            trugia = 15 * 33750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20"))
                        {
                            trugia = 20 * 33750;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("40x60"))
                    {
                        
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 66600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 66600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 66600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    else if (codemoi.Contains("50x50"))
                    {
                        if (name.Contains("10 t"))
                        {
                            trugia = 10 * 66600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("15 t"))
                        {
                            trugia = 15 * 66600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                        else if (name.Contains("20 t"))
                        {
                            trugia = 20 * 66600;
                            giamoi = (gia - trugia) + (gia - trugia) * 0.2;
                        }
                    }
                    //-------------------------------------------------------
                    string s = String.Format("insert into gcGobal_STOCK_gcService (Code,Name,LoaiDichVuId,GiaDichVu,GiaNhanh,GiaVon,DiscountNVLam,DiscountNVTuVan,DiscountNguoiMoigioi,DiscountKH) values (N'{0}',N'{1}',8,{2},0,0,0,0,0,0)", storecode, tenmoi, giamoi);
                    zgc0HelperSecurity.ExecuteNonQuery(s, zgc0GlobalStr.getSqlStr());
                }
                //------------------------------------------------------
            }
            //--------------------------------------
        }
}

        /*
        vn.won.sms.api.WONSMSSolution won = new vn.won.sms.api.WONSMSSolution();
       // won.sendSMS("mailisa", "ml@12345", "brand", "MAILISA", "84942212127", "Mung le 2-9, \" Mailisa giam giam 50% hoc phi  va cac dich vu tai tao da toan than, triet long vinh vien: nach, tay, chan, bikini; phun mau, xoa sua may, moi, mi, dieu tri cac van de ve da: mun, nam, lao hoa, lo chan long lon\". Ap dung trong thang 9 tren toan he thong, chi tiet tai mailisa.com.", "0", "0", "");
        string sqlStr = "server=112.213.84.113\\MSSQL2K8;database=mailisa;uid=lisa2;pwd=chimai";
L1:
        string query = "select * from SMS_ViewCustomer_CUTALL where mobile not in (select distinct mobile from SMS_Store)";

        DataTable myDataTable = new DataTable();
        using (SqlConnection connection = new SqlConnection(sqlStr))
        {
            connection.Open();
            SqlDataAdapter MySqlDataAdapter = new SqlDataAdapter();
            MySqlDataAdapter.SelectCommand = new SqlCommand(query, connection);
            MySqlDataAdapter.SelectCommand.CommandTimeout = 36000;
            try
            {
                MySqlDataAdapter.Fill(myDataTable);
            }
            catch (Exception exc)
            {
                
            }
            finally { ; }
        }
        if(myDataTable.Rows.Count>0)
        {
            int m = 0; 
            while(m<myDataTable.Rows.Count)
            {
                int first = m;
                int end = first+99;
                string phones = "";
                string sqlTest = "";
                while (first<end&& first<myDataTable.Rows.Count)
                {
                    string phone = myDataTable.Rows[first][0].ToString();
                    sqlTest += String.Format("INSERT INTO SMS_Store       (mobile)   VALUES     ('{0}');", phone);
                    phone = "84" + phone.Substring(1,phone.Length-1);
                    phones+= phone+",";
                    first++;
                }
                phones = phones.Substring(0, phones.Length-1);

                using (SqlConnection connection2 = new SqlConnection(sqlStr))
                {
                    connection2.Open();
                    SqlCommand cmd = new SqlCommand(sqlTest, connection2);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception exc)
                    {

                    }
                }
                try
                {
                    ;// won.sendSMS("mailisa", "ml@12345", "brand", "MAILISA", phones, "Mung le 2-9, \" Mailisa giam giam 50% hoc phi va cac dich vu tai tao da toan than, triet long vinh vien: nach, tay, chan, bikini; phun mau, xoa sua may, moi, mi, dieu tri cac van de ve da: mun, nam, lao hoa, lo chan long lon\". Ap dung trong thang 9 tren toan he thong, chi tiet tai mailisa.com.", "1", "0", "");
                }
                catch
                {
                    goto L1;
                    
                }
               
                m+=99;
            }
            string test = "";
        }
        //zgc0HelperSecurity.GetDataTableNew(
        */
   
}

