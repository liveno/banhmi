<%@ Page Language='C#' AutoEventWireup='true' CodeFile='zgc0LoginUser.aspx.cs' Inherits='zgc0LoginUser' %>

<%@ Register Src="~/UserControl/HeaderUserLogin.ascx" TagName="HeaderUserLogin" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/Header_Tag.ascx" TagName="Header_Tag" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>
<%@ Register Src="zgc0LoginUser.ascx" TagName="formmain" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head id='Head1' runat='server'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anh ngữ AMA Vũng Tàu</title>
    <uc2:Header_Tag ID="Header_tag1" runat="server" />
    <style type='text/css'>
        .multiPage
        {
            position: relative;
            margin-top: 26px;
            border: 1px solid #94A7B5;
            padding: 4px;
            padding-left: 0;
            width: auto;
            height: 100%;
        }
        .brbanner-wrap .brbanner-btn .brbtn
        {
            background: none repeat scroll 0 0 #0f0f0f;
            color: #fff;
            display: block;
            font: 20px/1.2em "Oswald" ,sans-serif;
            padding: 5px 0;
            text-align: center;
            text-transform: uppercase;
        }
        .brbtn-link
        {
            border-color: transparent;
            border-radius: 0;
            color: #f3cc52;
            cursor: pointer;
        }
        .brbtn, .brreply a
        {
            background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
            border: medium none;
            border-radius: 0;
            box-shadow: none;
            box-sizing: border-box;
            color: #f3cc52;
            font: 11px/1.2em "Oswald" ,sans-serif;
            height: auto;
            letter-spacing: 0;
            padding: 2px;
            text-decoration: none;
            text-shadow: none;
            text-transform: uppercase;
            transition: all 0.3s ease 0s;
        }
    </style>
    <script type="text/javascript" src="../jsGobalLib/xdate.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery.validate.js"></script>
    <script type="text/javascript" src="../UserControl/colorbox-master/jquery.colorbox.js"></script>
    <script src="../UserControl/bootstrap/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../UserControl/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="../UserControl/jPager/js/jPaginator-min.js"></script>
    <script type="text/javascript" src="../jsGobalLib/PageInitSize.js"></script>
    <script src='../Common/gcPROCESS.js' type='text/javascript'></script>
    <script src='../Common/gcCONFIG.js' type='text/javascript'></script>
    <script src='../Common/gcGlobal.js' type='text/javascript'></script>
    <script src='../Common/gcGlobalForm.js' type='text/javascript'></script>
</head>
<body runat='server' onmousedown='onProcessClick(event)' onkeypress='return captureKeys(event)'>
    <script type='text/javascript'>
            function onTabSelected(sender, args)
            {
            }
            function updateDetailChild(jsObjP9000)
            {
                if(jsObjP9000)
                {
                }
            }
            function pageLoad()
            {
                if(typeof pageLoadP9000 == 'function')
                {
                    pageLoadP9000('');
                }
            }

            function onItemClick(itemLI, tBox, index)
            {
                if(curDataDivShow==null)
                    return false;
                if(itemLI==null || tBox==null || index==null)
                    return false;
                var tCurBox = $get(tBox);
                var tCurBoxId = $get(tBox+'_ID');
                if(tCurBox != null)
                {
                    if(curDataDivShow)
                    {
                        curDataDivShowIndex = index;
                        var item = curDataDivShow[index];
                        $get(tBox).value = item[1];
                        $get(tBox+'_ID').value = item[0];
                        $get(divShowCID).style.display ='none';
                        if(typeof postonItemClick =='function')
                        {
                            postonItemClick(itemLI, tBox, index)
                        }
                    }
                }
            }
            function onItemSearchOption(itemLI, tBox, index)
            {
            }
            function onItemMenuClick(itemLI, tBox, index)
            {
            }
            function onItemSearchOptionCheck(id, str)
            {
                var tCheckBox = $get(id);
                var tCurBox = $get(otP9000.BtnDetailP9000_IDCID);
                if(tCheckBox.checked)
                {
                     if(tCurBox.value.indexOf(str)<0)
                     {
                         tCurBox.value = tCurBox.value +'|'+str
                         gcGobalFORM.LoadConfigForm('tbl_THUCHI_PhieuChiTienMat',str,'P9000', updateFormSearch);
                     }
                }
                else
                {
                    tCurBox.value = tCurBox.value.replace('|'+str, "");
                }
            }
            function updateFormSearch(result)
            {
                $get(otP9000.holeDivSearchCID).innerHTML += result;
            }
           
            var captureKeys = function(ev) 
             {
                ev = ev || window.event;     
                kCode = ev.keyCode || ev.which;  
                // alert("123 - "+ kCode + "-" + ev);
                if (ev.ctrlKey && ev.shiftKey && kCode == 19 || ev.ctrlKey && ev.shiftKey && kCode == 83) 
                    return false;  // ctrl+alt+s
               if (ev.ctrlKey && kCode == 119) 
               { // ctrl+w
                   closeWin();  // run your own script to close the window    // doesn't work in ie, ie just closes the window
               }
               if ( (ev.ctrlKey && kCode == 10) || (ev.ctrlKey && kCode == 13))//enter firefox: enter is 13
               { // ctrl+w
                    
               }
               if ( (ev.shiftKey && kCode == 10) || (ev.shiftKey && kCode == 13))//enter firefox: enter is 13
               { // ctrl+w
                    
               }
                    
               if ( (ev.shiftKey && kCode == 73))//enter firefox: I is 78
               { // Shift+I
                   
                   return false;
               }
               if ( (ev.shiftKey && kCode == 80))//enter firefox: P is 80
               { // Shift+P
                    
                   return false;
               }
               if(typeof postCaptureKeys =='function')
                       if(postCaptureKeys(ev) == 'false') return false;
               if ( kCode == 13)
                      return false;
                return true;
            }
            function onItemClickLink(itemLI, tBox, index)
            {
                if(curDataDivShow)
                {
                    curDataDivShowIndex = index;
                    var item = curDataDivShow[index];
                    window.location = item[0];
                }
            }
            -->
    </script>
    <form runat='server' id='mainForm' method='post' autocomplete='off'>
    <asp:ScriptManager ID='gcSM' runat='server' EnablePageMethods='true' ScriptMode='Release'>
        <Services>
            <asp:ServiceReference Path='../WSASMXGobal/zgc0GobalService.asmx' />
        </Services>
    </asp:ScriptManager>
    <uc1:HeaderUserLogin ID='HeaderUserLogin' runat='server' />
    <div class='gcWraper'>
        <div class='gcMainParent'>
            <div class='gcMainIn'>
                <div runat='server' id='gcDIVShowSelect' class='gcDIVShowMathSelect'>
                </div>
                <div runat='server' id='divShow' class='gcDIVShow'>
                </div>
                <div runat='server' id='gcAccount' style='display: none !important;'>
                </div>
                <div runat='server' id='divPrint' style='display: none !important;'>
                </div>
                <%-- ----Bengin Row--- --%>
                <div class='gc1Row' style='z-index: 1; width: inherit; height: 2px;'>
                    <div class='gcHeaderStyle'>
                        <div class='gcTitleTbl' style='display: none !important;'>
                            NHẬP THÔNG TIN:</div>
                        <div class='gcNameTitle' style='display: none !important;'>
                            Bảng thông tin chức năng
                        </div>
                    </div>
                </div>
                <%-- ----End Row--- --%>
                <div class='gcClear'>
                </div>
                <%-- ----Bengin Content--- --%>
                <div class='gc1Row' style='width: 100%; height: 100%; min-width: 1080px;'>
                    <uc4:formmain ID="a123" runat="server" />
                </div>
                <%-- --End Tab Contents-- --%>
                <div class='gcClear'>
                </div>
                <div style='height: 70px;'>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
