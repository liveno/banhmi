﻿<%@ WebHandler Language="C#" Class="ReportReceiptTheoNgaytHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportReceiptTheoNgaytHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
            + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
            + "<tr>"
                     + "<td width=\"500px\"align=\"center\" ><span style='font-weight:bold;font-size:18px;'> {2} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{3}</strong></span></br><span style='font-size:12px;'> &nbsp;Địa chỉ: {4}</br></span></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>BÁO CÁO BÁN HÀNG</font></b> </br>"
                        + "<span style='font-size:14px;'>Từ {1} đến {5}</span><br/>"
                        + "</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"760px\" style=\"background:#FFFFFF\">"

                       + " <tr><td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Người lập phiếu</b><br>(Ký, họ tên)</td> "
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Kế toán</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Thủ quỹ</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Quản lý</b><br>(Ký, họ tên)</td>"
                      + "  </tr>"

                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = ResponseFormat



        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        string User = gcUtility.GetNullString(context.Request.Form["obj[User]"]);
        string ListCa = gcUtility.GetNullString(context.Request.Form["obj[ListCa]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        string _BranchId = gcUtility.GetNullString(context.Request.Form["obj[BranchId]"]);



        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"]) == "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền

            if (typeReport == "Receipt")
            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                DataTable tb;
                string[] lCa = new string[1];
                //if (!string.IsNullOrEmpty(_BranchId))
                //{
                //    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", _BranchId);
                //    tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                //    if (tb.Rows.Count > 0)
                //    {
                //        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                //        COMName = COMName.Replace("-", "</br>");
                //        COMTel = tb.Rows[0]["TEL"].ToString();
                //        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                //        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                //        lCa = gcUtility.GetNullString(tb.Rows[0]["CONFIG"]).Split('|');
                //    }
                //}
                //else
                //{
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", "1");
                    tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        //COMName = "BỆNH VIÊN BÀ RỊA</BR>CANTEEN";
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                        lCa = gcUtility.GetNullString(tb.Rows[0]["CONFIG"]).Split('|');
                    }
                //}

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

                //divBody.InnerHtml = "";
                //divHeader.InnerHtml = "";
                string name = zgc0Helper.getConfigValue("Logopath1");

                bool bEmpty = false;
                DataTable PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt04 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                if (PhieuThu_Print.Rows.Count < 1)
                {
                    PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt04 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                    bEmpty = true;
                }
                //DataTable tbKH = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM gcGobal_CUST_Customer where Id=" + PhieuThu_Print.Rows[0]["KHId"].ToString(), zgc0GlobalStr.getSqlStr());
                string LoaiKhachId = "";
                //LoaiKhachId = tbKH.Rows[0]["LoaiKhachId"].ToString();

                //----------------------------------------------------------

                string NhanVienIdHoTen = "THU NGÂN";

                string BanAn = "";

                DateTime tparam = DateTime.Parse(t1);

                DateTime tnow = DateTime.Parse(DateTime.Now.ToShortDateString());
                string tget = DateTime.Now.ToShortDateString();
                if (tparam != tnow)
                    tget = tparam.ToShortDateString();
                //if (string.IsNullOrEmpty(ListCa) || ListCa == "0")
                //{
                //    t1 = t1 + " " + lCa[0].Split('-')[0];
                //    t2 = tparam.AddDays(1).ToString("MM/dd/yyyy") + " " + lCa[lCa.Length - 2].Split('-')[1];
                //}
                //else if (int.Parse(ListCa) < lCa.Length)
                //{
                //    t1 = t1 + " " + lCa[int.Parse(ListCa) - 1].Split('-')[0];
                //    t2 = lCa[int.Parse(ListCa) - 1].Split('-').Length == 3 ?
                //        (tparam.AddDays(1).ToString("MM/dd/yyyy") + " " + lCa[int.Parse(ListCa) - 1].Split('-')[1]) :
                //        (tparam.ToString("MM/dd/yyyy") + " " + lCa[int.Parse(ListCa) - 1].Split('-')[1]);
                //}
                //else
                //{

                //}


                string SoCT = "";
                string tempHead = String.Format(headerStr,
                                SoCT,
                                t1,
                                 COMName, COMTel, COMAdd, t2, BanAn, NhanVienIdHoTen);

                string strWhere = "";

                strWhere = " (isPrgOrdered=1) and ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";

                string storewhere = "  ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";


                if (strWhere == null || strWhere.Length <= 0)
                {
                    storewhere = String.Format(" ( convert(date,NgayLap)>=N'{0}' and convert(date,NgayLap)<=N'{1}' )", t1, t2);
                    strWhere = String.Format(" ( convert(date,NgayLap)>=N'{0}' and convert(date,NgayLap)<=N'{1}' )", t1, t2);
                }
                else
                {
                    storewhere += String.Format(" AND ( convert(date,NgayLap)>=N'{0}' and convert(date,NgayLap)<=N'{1}' )", t1, t2);
                    strWhere += String.Format(" AND ( convert(date,NgayLap)>=N'{0}' and convert(date,NgayLap)<=N'{1}' )", t1, t2);
                }

                //phan quyen
                //if (HttpContext.Current.Session["gcRightGroup"] != null)
                //{
                //    string rg = HttpContext.Current.Session["gcRightGroup"].ToString();
                //    if (rg != "1")
                //    {
                //        string partcomp = HttpContext.Current.Session["gcCtyId"] + "-" + HttpContext.Current.Session["gcBranchId"] + "-" + HttpContext.Current.Session["gcDepartmentId"];
                //        if (strWhere == null || strWhere.Trim().Length < 1)
                //        {
                //            storewhere = string.Format(" isPrgPartComp='{0}'", partcomp);
                //            strWhere = string.Format(" isPrgPartComp='{0}'", partcomp);
                //        }
                //        else
                //        {
                //            storewhere += string.Format(" and isPrgPartComp='{0}'", partcomp);
                //            strWhere += string.Format(" and isPrgPartComp='{0}'", partcomp);
                //        }
                //    }
                //}



                // DM thu
                if (_Id != null && _Id.Length > 0)
                {
                    if (strWhere == null || strWhere.Length <= 0)
                        strWhere = String.Format(" ( OrderId= {0})", _Id);
                    else
                    {
                        strWhere += String.Format(" AND ( OrderId= {0})", _Id);
                    }
                }
                // Branch thu
                if (_BranchId != null && _BranchId.Length > 0)
                {
                    string partcomp = HttpContext.Current.Session["gcCtyId"] + "-" + _BranchId.ToString() + "-";
                    if (strWhere == null || strWhere.Trim().Length < 1)
                    {
                        //storewhere = string.Format(" isPrgPartComp='{0}'", partcomp);
                        strWhere = string.Format(" isPrgPartComp like '{0}%'", partcomp);
                    }
                    else
                    {
                        //storewhere += string.Format(" and isPrgPartComp='{0}'", partcomp);
                        strWhere += string.Format(" and isPrgPartComp like '{0}%'", partcomp);
                    }
                }
                //-DM
                if (Name.Contains("-"))
                {
                    string Id = Name.Substring(1, Name.Length - 1);
                    if (strWhere == null || strWhere.Length <= 0)
                        strWhere = String.Format(" ( OrderId <> {0})", Id);
                    else
                    {
                        strWhere += String.Format(" AND ( OrderId <> {0})", Id);
                    }
                }
                if (Name.Contains("+"))
                {
                    char[] slit = { '+' };
                    string[] arrId = Name.Split(slit);
                    string tmpString = "";
                    for (int m = 0; m < arrId.Length; m++)
                    {
                        string tmpId = arrId[m];
                        if (m == 0)
                            tmpString += String.Format(" ( OrderId = {0}) ", tmpId);
                        else
                            tmpString += String.Format("  or (OrderId = {0}) ", tmpId);
                    }

                    if (strWhere == null || strWhere.Length <= 0)
                        strWhere = String.Format(" ( {0} )", tmpString);
                    else
                    {
                        strWhere += String.Format(" AND ( {0} )", tmpString);
                    }
                }
                if (User != null && User.Length > 0)
                {
                    if (strWhere == null || strWhere.Length <= 0)
                        strWhere = String.Format(" ( isPrgVNKoDau LIKE N'%{0}%')", User);
                    else
                    {
                        strWhere += String.Format(" AND ( isPrgVNKoDau LIKE N'%{0}%')", User);
                    }
                }

                //strOrderBy = " order by NgayLap";

                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;
                double GiamGia = 0;
                string strTien = "";
                //double GiamGia = 0;
                strOrderBy = " order by NgayLap desc, SoCT, VatTuIdName";
                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead +
                    BuildAReport(strWhere, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty));//+ footerStr );

                double sumNH = 0;
                string string2 = BuildAReportNH(_Id, storewhere, "", ref sumNH);
                //sb.AppendLine("Thu tiền từ Ngân hàng:");
                //sb.Append(string2);
                sb.AppendLine("</br>");
                sb.Append(footerStr);

                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }

            if (typeReport == "ReceiptSUM")
            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMName = COMName.Replace("-", "</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

                //divBody.InnerHtml = "";
                //divHeader.InnerHtml = "";
                string name = zgc0Helper.getConfigValue("Logopath1");

                bool bEmpty = false;
                DataTable PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt04 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                if (PhieuThu_Print.Rows.Count < 1)
                {
                    PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt04 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                    bEmpty = true;
                }
                //DataTable tbKH = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM gcGobal_CUST_Customer where Id=" + PhieuThu_Print.Rows[0]["KHId"].ToString(), zgc0GlobalStr.getSqlStr());
                string LoaiKhachId = "";
                //LoaiKhachId = tbKH.Rows[0]["LoaiKhachId"].ToString();

                //----------------------------------------------------------

                string NhanVienIdHoTen = "THU NGÂN";

                string BanAn = "";



                string SoCT = "";
                string tempHead = String.Format(headerStr,
                                SoCT,
                                DateTime.Parse(t1).ToString("dd/MM/yyyy"),
                                 COMName, COMTel, COMAdd, BanAn, NhanVienIdHoTen);

                string strWhere = "";

                strWhere = "";

                DateTime tparam = DateTime.Parse(t1);


                t1 = t1 + " 0:00";
                t2 = t2 + " 23:59";
                if (strWhere == null || strWhere.Length <= 0)
                    strWhere = String.Format(" ( convert(date,NgayLap)>=N'{0}' and convert(date,NgayLap)<=N'{1}' )", t1, t2);
                else
                {
                    strWhere += String.Format(" AND ( convert(date,NgayLap)>=N'{0}' and convert(date,NgayLap)<=N'{1}' )", t1, t2);
                }

                //strOrderBy = " order by NgayLap";

                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;
                double GiamGia = 0;
                string strTien = "";
                //double GiamGia = 0;
                strOrderBy = " order by NgayLap desc ";

                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead +
                    BuildAReportSUM(strWhere, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty));// + footerStr);



                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }
        }
        else
            context.Response.Write("Unknown.");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    //35111299
    public string BuildAReportNH(string Id, string strWhere, string strOrderBy, ref double sumNH)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        //strOrderBy = " order by DonGia ";// Diễn giải
        string table = "";
        gcRptTable tblReport = new gcRptTable();//Hàng hóa
        //tblReport.strTblSql = @"SELECT VatTuIdName,DonGia,DonViTinhIdName, Sum(SoLuong) AS SoLuong,SUM(ThanhTien) AS ThanhTien FROM zgcl_gcGobal_STOCK_gcProduct_Output_Detail02 WHERE PhieuXuatKhoId=" + _Id + " GROUP BY VatTuIdName,DonViTinhIdName, DonGia";

        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây

        tblReport.strMainTable = "gcGobal_INCOM_SoQuyNganHang_NopTienHoacNhanChuyenKhoan";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 2;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        //tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[0];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        // tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        // tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        // tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGobal_INCOM_SoQuyNganHang_NopTienHoacNhanChuyenKhoan", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "35111299");

        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[3] = 1;

        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();//Kho Chủ

        //---------------------------------------------------


        //------------------------------------------------------------
        double sum = 0, sum1 = 0, sum2 = 0;
        for (int i = 3; i < 4; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            //if (bEmpty)
            //{
            //    sum = OldThucThu;
            //    tblReport.RowSumGobal.mValue[i] = OldThucThu.ToString();
            //}
        }
        sum1 = 0;



        //tblReport.mArrRightStyle[3] = 1; ;
        //tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextRight";

        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        //Xuất dữ liệu
        //


        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRightBold";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextRightBold";

        table = tblReport.MakeSimpleTable();
        return table;


    }
    //Bàn//Thực thu//Giờ thu
    public string BuildAReport(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "XIKE_Report_DoanhSoTrongNgay";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 6;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[2];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "XIKE_Report_DoanhSoTrongNgay", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "9021502");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        //---------------------------------------------------
        //if (LoaiKhachId == "1") 
        //    for (int i = 0; i < tblReport.cCols; i++)
        //    {
        //        for (int j = 0; j < tblReport.mData.Count; j++)
        //        {
        //            if (i == 1 || i == 5 | i == 6)
        //                tblReport.mData[j][i] = "0";
        //            if (i == 1)
        //                tblReport.mData[j][i] = "";
        //        }
        //    }
        //--------------------------------------------------



        //------------------------------------------------------------
        double sum = 0;


        if (int.Parse(HttpContext.Current.Session["gcRightGroup"] + "") == 1)
        {
            for (int i = 7; i < 8; i++)
            {
                sum = 0;
                for (int j = 0; j < tblReport.mData.Count; j++)
                {
                    string value = (string)tblReport.mData[j][i];
                    value.Trim();
                    if (value != string.Empty)
                    {
                        sum += double.Parse((string)value);
                    }
                }
                tblReport.RowSumGobal.mValue[i] = sum.ToString();

            }
        }

        //if (int.Parse(HttpContext.Current.Session["gcRightGroup"] + "") == 1)
        //{
            //Tong theo nhom và định dạng
            if (tblReport.mListItem.Count > 0)
            {
                for (int m = 0; m < tblReport.mListItem.Count; m++)
                {
                    gcRptItem Item = tblReport.mListItem[m];
                    Item.strShowName = "<b>Cộng </b>";
                    //Item.strContent = "DM " + tblReport.mData[Item.from][1];


                    double Tem3 = 0;//, temp4 = 0;
                    for (int j = 7; j < 8; j++)
                    {
                        for (int i = Item.from; i < Item.to; i++)
                        {
                            string a5 = (string)tblReport.mData[i][j];
                            Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                            Item.mValue[j] = Tem3.ToString();
                        }
                        Tem3 = 0;
                    }
                    Item.cssClass = "dhu_rpt_TextCenterBgr";
                    Item.cssClassShowName = "dhu_rpt_TextCenterBgr";
                }
            }
        //}

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }



        //tblReport.mArrRightStyle[4] = 1; ;
        //tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextRight";

        //tblReport.mArrRightStyle[7] = 1; ;
        //tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextLeft10";

        tblReport.mArrRightStyle[2] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";
        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[3] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[4] = 1; ;
        tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextRight";
        tblReport.mArrRightStyle[6] = 1; ;
        tblReport.mArrRightStyleCss[6] = "dhu_rpt_TextRight";
        tblReport.mArrRightStyle[7] = 1; ;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";
        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }



    //Bàn//Thực thu//Giờ thu
    public string BuildAReportSUM(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "gcGOBAL_AUTO_BILL_QA_INFO_SUMDAY";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 2;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[1];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_BILL_QA_INFO_SUMDAY", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "4444");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[2] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();


        //------------------------------------------------------------
        double sum = 0;
        for (int i = 2; i < 3; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();

        }

        //Tong theo nhom và định dạng
        double tienphaithu = sum;


        //dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
        string tientthu = dhuConvertNumberToString.Convert(tienphaithu);
        //In tien ra cho khach
        gcRptTailItem TitemTien = new gcRptTailItem();
        TitemTien.cssValueClass = "dhu_rpt_TextRightBold";
        TitemTien.ColSpan = tblReport.cCols;
        TitemTien.strFormName = "<span style='font-style:italic; font-weight:bold;'>" + tientthu + "./</span>";
        TitemTien.Value = "";
        tblReport.mListTailItem.Add(TitemTien);


        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }


        //for (int i = 3; i < 4; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextCenter";
        //}
        //tblReport.mArrRightStyle[3] = 1; ;
        //tblReport.mArrRightStyleCss[3] = "dhu_rpt_TextRight";

        tblReport.mArrRightStyle[2] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";
        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[3] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[4] = 1; ;
        tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextRight";
        tblReport.mArrRightStyle[6] = 1; ;
        tblReport.mArrRightStyleCss[6] = "dhu_rpt_TextRight";
        tblReport.mArrRightStyle[7] = 1; ;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRightBold";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextRightBold";
        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }

}