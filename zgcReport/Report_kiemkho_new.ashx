﻿<%@ WebHandler Language="C#" Class="ReportReceiptTheoNgaytHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportReceiptTheoNgaytHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
            + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
            + "<tr>"
                     + "<td width=\"500px\"align=\"center\" ><span style='font-weight:bold;font-size:18px;'> {1} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{2}</strong> &nbsp;Địa chỉ: {3}</br></span></td>"
                        + "<td></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>KIỂM KHO</font></b> </br>"
                        + "{0}</br>"
                        + "<span style='font-size:14px;'>{4}</span><br/>"
                        + "</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"800\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>XIN CẢM ƠN QUÝ KHÁCH!</font></b> </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" >&nbsp; </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"
                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = ResponseFormat



        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string HangHoa = gcUtility.GetNullString(context.Request.Form["obj[HangHoa]"]);
        string KhoId = gcUtility.GetNullString(context.Request.Form["obj[KhoId]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        string _BranchId = gcUtility.GetNullString(context.Request.Form["obj[BranchId]"]);


        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"]) == "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền


            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMName = COMName.Replace("-", "</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }

                Sql = String.Format("Select * from gcGobal_STOCK_List where id = {0}", KhoId.ToString());
                DataTable tb2 = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                if (tb2.Rows.Count > 0)
                {
                    string stockCode = tb2.Rows[0]["Code"].ToString().ToUpper();
                    Sql = String.Format("alter view gcGOBAL_ATUTO_CHECKSTOCK_V2014 as SELECT     a.*, case when a.Miss is null then a.ED else (a.ED + a.Miss) end as ThucTe, dbo.gcGobal_STOCK_gcProductList.Name, dbo.gcGobal_LITERAL_Unit.Name AS UName"
                                + " FROM         dbo.gcGobal_STOCK_gcStock_{0}_Today a INNER JOIN"
                                + "   dbo.gcGobal_STOCK_gcProductList ON a.ProductId = dbo.gcGobal_STOCK_gcProductList.Id INNER JOIN"
                                + "   dbo.gcGobal_LITERAL_Unit ON a.UnitId = dbo.gcGobal_LITERAL_Unit.Id", stockCode);
                    zgc0HelperSecurity.ExecuteNonQuery(Sql, zgc0GlobalStr.getSqlStr());
                }


                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

                //----------------------------------------------------------

                string NhanVienIdHoTen = "THU NGÂN";


                string strWhere = "";
                string filter = "";
                t1 = t1 + " 0:00";
                t2 = t2 + " 23:59";
                var tD = DateTime.Parse(t1);
                string tempHead = String.Format(headerStr,
                                tb2.Rows[0]["Name"].ToString(),
                                 COMName, COMTel, COMAdd, tD.ToString("dd/MM/yyyy HH:mm"));
                if (strWhere == null || strWhere.Length <= 0)
                    strWhere = String.Format(" ( mToDay=N'{0}')", t1);
                else
                {
                    strWhere += String.Format(" AND ( mToDay>=N'{0}')", t1);
                }

                if (_Id != null && _Id.Length > 0)
                {
                    if (strWhere == null || strWhere.Length <= 0)
                        strWhere = String.Format(" ( ProductId= {0})", _Id);
                    else
                    {
                        strWhere += String.Format(" AND ( ProductId= {0})", _Id);
                    }
                }

                if (Name != null && Name.Length > 0)
                {
                    if (strWhere == null || strWhere.Length <= 0)
                        strWhere = String.Format(" ( Name like N'%{0}%')", Name);
                    else
                    {
                        strWhere += String.Format(" AND ( Name like N'%{0}%')", Name);
                    }
                }
                strOrderBy = " order by Name ";
                bool bEmpty = false;
                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;
                double GiamGia = 0;
                string strTien = "";
                //double GiamGia = 0;

                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead +
                    BuildAReport(strWhere, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty) + footerStr);
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }

        }
        else
            context.Response.Write("Unknown.");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    //Bàn//Thực thu//Giờ thu
    public string BuildAReport(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "gcGOBAL_ATUTO_CHECKSTOCK_V2014";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 3;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        //tblReport.mArrPos = new int[1];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_ATUTO_CHECKSTOCK_V2014", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "15060401");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        //tblReport.mArrArgSumary[3] = 1;
        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[6] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[8] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();


        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";
        for (int i = 3; i < 11; i++) {
            tblReport.mArrRightStyle[i] = 1; ;
            tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextRight";
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRightBold";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextRightBold";
        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }

}