﻿<%@ WebHandler Language="C#" Class="BaoCaoDoanhThuChiTietHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using Vxr;
using Vxr.Core;

public class BaoCaoDoanhThuChiTietHandler : IHttpHandler, IReadOnlySessionState
{
    public void ProcessRequest(HttpContext context = null)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        string value = new StreamReader(context.Request.InputStream).ReadToEnd(); ;
        object obj = ((serializer.Deserialize(value, typeof(Dictionary<string, object>))) as Dictionary<string, object>)["obj"];
        object[] pr = new object[2];
        pr[0] = obj;
        pr[1] = "ERROR";
        Dictionary<string, object> _ip = (Dictionary<string, object>)obj;
        try
        {
            if (_ip.ContainsKey("_a"))
            {
                var a = _ip["_a"] as string;
                if (a != null && DR._a.ContainsKey(a))
                {
                    var _a = DR._a[a];
                    PR p = new PR();
                    MethodInfo m = p.GetType().GetMethod(_a[3]);
                    if (m != null)
                        m.Invoke(p, pr);// sử dụng gọi method
                    else // call extension
                    {
                        Type t = Type.GetType("Vxr.PRE");
                        if (t != null)
                        {
                            object pe = Activator.CreateInstance(t);
                            m = pe.GetType().GetMethod(_a[3]);
                            if (m != null)
                                m.Invoke(p, pr);// sử dụng gọi method
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            pr[1] = "ERROR";
        }
        System.Text.StringBuilder sb = new System.Text.StringBuilder(pr[1] + "");
        //context.Response.ContentType = "text/plain";
        PR.CheckSizeFile(context, sb, 10000000);

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}