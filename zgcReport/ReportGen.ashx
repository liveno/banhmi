﻿<%@ WebHandler Language="C#" Class="ReportGenHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportGenHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
			
			        + "<tr>"
                     + "<td>{2} </br>{4}</td>"
                       
                        + "<td>ĐT : {3}</td>"
                        + "</tr>"
                        
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"760px\" style = 'padding : 10px 0 '><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>PHIẾU THU TIỀN - NGÂN HÀNG</font></b> </br>"
                        + "<span style='font-size:14px;'>Số phiếu:{0} - Ngày: {1} </span><br/>"
                        + "</td>"
                        + "</tr>"

                         + "<tr>"
                        + "<td>Nhà cung cấp: {5}</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td>Địa chỉ: {11} </td>"
                        + "</tr>"

                         + "<tr>"
                        + "<td>Số tiền nộp: {6}</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td>Bằng chữ : ({10})</td>"
                        + "</tr>"

                         + "<tr>"
                        + "<td>Diễn giải: {7} </td>"
                        + "</tr>"

                         + "<tr>"
                        + "<td>Chi nhánh: {8} </td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td>Danh mục thu: {9} </td>"
                        + "</tr>" 
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"800\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td>Ngưởi lập phiếu</td>"
                        + "<td>Ngưởi nộp tiền</td>"
                        + "<td>Thủ quỷ</td>"
                        + "<td>Kế toán trưởng</td>"
                        + "<td>Giám đốc</td>"
                        + "</tr>"

                         + "<tr>"
                        + "<td colspan=\"5\"  height=\"60px\">&nbsp; </td>"
                        + "</tr>"
                         + "<tr>"
                        + "<td colspan=\"5\"> </td>"
                        + "</tr>"
                         + "<tr>"
                        + "<td colspan=\"5\"> </td>"
                        + "</tr>"
                         + "<tr>"
                        + "<td colspan=\"5\" style='border-bottom:1px dotted #000'>&nbsp; </td>"
                        + "</tr>"
                        
                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest (HttpContext context) {
        //context.Response.ContentType = ResponseFormat

       

        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        
        

        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"])== "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền

            if (typeReport == "Receipt")
            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
			            COMName = COMName.Replace("-"," ");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
                
                //divBody.InnerHtml = "";
                //divHeader.InnerHtml = "";
                string name = zgc0Helper.getConfigValue("Logopath1");

                bool bEmpty = false;
                DataTable PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_BANK_ADD04 where Id=" + _Id, zgc0GlobalStr.getSqlStr());

                string NCCIdName = PhieuThu_Print.Rows[0].IsNull("NCCIdTen") ? "__" : PhieuThu_Print.Rows[0]["NCCIdTen"].ToString();
                double TongTien = PhieuThu_Print.Rows[0].IsNull("TongTien") ? 0 :  (double)PhieuThu_Print.Rows[0]["TongTien"];
                string vndTongTien = dhuConvertNumberToString.Convert(TongTien);
                string DienGiai = PhieuThu_Print.Rows[0].IsNull("DienGiai") ? "__" : PhieuThu_Print.Rows[0]["DienGiai"].ToString();
                string BankIdName = PhieuThu_Print.Rows[0].IsNull("BankIdName") ? "__" : PhieuThu_Print.Rows[0]["BankIdName"].ToString();
                string OrderIdName = PhieuThu_Print.Rows[0].IsNull("OrderIdName") ? "__" : PhieuThu_Print.Rows[0]["OrderIdName"].ToString();
                string NCCId = PhieuThu_Print.Rows[0].IsNull("NCCId") ? "__" : PhieuThu_Print.Rows[0]["NCCId"].ToString();
                string NhanVienIdHoTen = "THU NGÂN";
                if (PhieuThu_Print.Rows.Count >0)
                {
                    NhanVienIdHoTen = PhieuThu_Print.Rows[0].IsNull("NhanVienIdHoTen")?  "THU NGÂN": PhieuThu_Print.Rows[0]["NhanVienIdHoTen"].ToString(); 
                }


                DataTable DiaChiNCC_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_SUPP_Supplier00 where Id=" + NCCId, zgc0GlobalStr.getSqlStr());

                string DiaChi = DiaChiNCC_Print.Rows[0].IsNull("DiaChi") ? "__" : DiaChiNCC_Print.Rows[0]["DiaChi"].ToString();
                
                string SoCT = PhieuThu_Print.Rows[0]["SoCT"].ToString();
                SoCT = SoCT.Replace("PT000", "");
                string tempHead = String.Format(headerStr,
                                SoCT, (String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now)),
                                 COMName, COMTel, COMAdd, NCCIdName, String.Format("{0:N0}", TongTien), DienGiai, BankIdName, OrderIdName, vndTongTien, DiaChi);

                
                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead + footerStr );
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }
            
            
        }
        else
            context.Response.Write("Unknown.");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string BuildAReport(string Id, string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        strOrderBy = " order by DonGia ";
        string table = "";
        gcRptTable tblReport = new gcRptTable();//Hàng hóa
        tblReport.strTblSql = @"SELECT VatTuIdName,DonGia,DonViTinhIdName, Sum(SoLuong) AS SoLuong,SUM(ThanhTien) AS ThanhTien FROM zgcl_gcGobal_STOCK_gcProduct_Input_Detail02 WHERE PhieuNhapKhoId=" + _Id + " GROUP BY VatTuIdName,DonViTinhIdName, DonGia";
        
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        
        tblReport.strMainTable = "zgcl_gcGobal_INCOM_Receipt_DetailAll";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 5;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[1];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "zgcl_gcGobal_INCOM_Receipt_DetailAll", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "351");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();//Kho Chủ

        //---------------------------------------------------
        //if (LoaiKhachId == "1") 
        //    for (int i = 0; i < tblReport.cCols; i++)
        //    {
        //        for (int j = 0; j < tblReport.mData.Count; j++)
        //        {
        //            if (i == 1 || i == 5 | i == 6)
        //                tblReport.mData[j][i] = "0";
        //            if (i == 1)
        //                tblReport.mData[j][i] = "";
        //        }
        //    }
        //--------------------------------------------------

       
        
        //------------------------------------------------------------
        double sum = 0;
        for (int i = 5; i < 6; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            if (bEmpty)
            {
                sum = OldThucThu;
                tblReport.RowSumGobal.mValue[i] = OldThucThu.ToString();
            }
        }

        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.strContent = "Khách hàng " + tblReport.mData[Item.from][1];


                double Tem3 = 0;//, temp4 = 0;
                for (int j = 5; j < 6; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                        Item.mValue[j] = Tem3.ToString();
                    }
                    Tem3 = 0;
                }
                Item.cssClass = "dhu_rpt_TextRight";
            }
        }

        //tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        //tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";

        double valuept = 0;


        double valueg = 0;

        double VATg = 0;
        if (VAT > 0)
        {
            VATg = VAT;
            gcRptTailItem item = new gcRptTailItem();
            item.ColSpan = tblReport.cCols - 1;
            item.cssValueClass = "dhu_rpt_TextRight";
            item.strFormName = "Tổng tiền hàng";
            item.Value = String.Format("{0:N0}", VATg);
            tblReport.mListTailItem.Add(item);
        }
        valuept = 0;
        if (OldPhuThu > 0)
        {

            valuept = OldPhuThu;
            gcRptTailItem itemg = new gcRptTailItem();
            itemg.ColSpan = tblReport.cCols - 1;
            itemg.cssValueClass = "dhu_rpt_TextRight";
            itemg.strFormName = "Nợ ban đầu";
            itemg.Value = String.Format("{0:N0}", (valuept));
            tblReport.mListTailItem.Add(itemg);
        }
        if (OldGiamGia > 0)
        {

            if (OldGiamGia > 0 && OldGiamGia < 100.0)
                valueg = OldGiamGia * sum / 100.0;
            else
                valueg = OldGiamGia;
            gcRptTailItem itemg = new gcRptTailItem();
            itemg.ColSpan = tblReport.cCols - 1;
            itemg.cssValueClass = "dhu_rpt_TextRight";
            itemg.strFormName = "Đã tạm ứng";
            //if (OldGiamGia > 0 && OldGiamGia < 100.0)
            //    itemg.strFormName = "Giảm giá (Discount) " + OldGiamGia + "%";
            itemg.Value = String.Format("{0:N0}", (valueg));
            tblReport.mListTailItem.Add(itemg);
        }

        gcRptTailItem item2 = new gcRptTailItem();
        item2.cssValueClass = "dhu_rpt_TextRightBold";
        item2.ColSpan = tblReport.cCols - 1;
        item2.strFormName = "Công nợ hiện tại";
        item2.Value = String.Format("{0:N0}", (VATg + valuept - valueg ));

	if (OldPhuThu > 0 || OldGiamGia > 0)
        	tblReport.mListTailItem.Add(item2);
	
	double tienphaithu =VATg + valuept - valueg ;


	//dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
    string tientthu = dhuConvertNumberToString.Convert(tienphaithu);	
	//In tien ra cho khach
	gcRptTailItem TitemTien = new gcRptTailItem();
        TitemTien.cssValueClass = "dhu_rpt_TextRightBold";
        TitemTien.ColSpan = tblReport.cCols;
        TitemTien.strFormName = "<span style='font-style:italic; font-weight:bold;'>"+tientthu +"./</span>";
        TitemTien.Value = "";
      	tblReport.mListTailItem.Add(TitemTien );

        OldThucThu = (sum + valuept - valueg + VATg);
        //for (int i = 3; i < 4; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextCenter";
        //}
        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextRight";

        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";
        
        //Xuất dữ liệu
        //
  

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRightBold";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextRightBold";
        
        table = tblReport.MakeSimpleTable();
        return table;
    }




}