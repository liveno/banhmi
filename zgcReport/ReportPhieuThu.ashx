﻿<%@ WebHandler Language="C#" Class="ReportGenHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportGenHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"760px\" style=\"background:#FFFFFF\">"
			 
			        + "<tr>"
                     + "<td><strong>{2}</strong> </br>{4}</td>"

                        + "<td>ĐT: {3}</td>"
                        + "</tr>"
                        
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"760px\" style = 'padding : 10px 0 '><b><font size=\"2.0pt\" style='font-weight:bold;font-size:16px;'>PHIẾU THU TIỀN</font></b> </br>"
                        + "<span style='font-size:10px;'>Số:<strong>{0}</strong> - Ngày: {1} </span><br/>"
                        + "</td>"
                        + "</tr>"

                         + "<tr>"
                        + "<td>Khách hàng: <strong>{5}</strong></td>"
                        + "</tr>"

                        //+ "<tr>"
                        //+ "<td>Địa chỉ: {11} </td>"
                        //+ "</tr>"

                         + "<tr>"
                        + "<td>Số tiền nộp: ({6})</td>"
                        + "</tr>"

                        //+ "<tr>"
                        //+ "<td>Bằng chữ : ({7})</td>"
                        //+ "</tr>"

                        // + "<tr>"
                        //+ "<td>Diễn giải: {7} </td>"
                        //+ "</tr>"

                        // + "<tr>"
                        //+ "<td>Chi nhánh: {8} </td>"
                        //+ "</tr>"

                        //+ "<tr>"
                        //+ "<td>Danh mục thu: {9} </td>"
                        //+ "</tr>" 
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"760px\" style=\"background:#FFFFFF\">"
                   
                       + " <tr><td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Người lập phiếu</b><br>(Ký, họ tên)</td> "
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Khách hàng</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Thủ quỹ</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Quản lý</b><br>(Ký, họ tên)</td>"
                      + "  </tr>"
                        
                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest (HttpContext context) {
        //context.Response.ContentType = ResponseFormat

       

        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        
        

        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"])== "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền

            if (typeReport == "Receipt")
            {

                /// = this.Session["gcUserName"].ToString();
                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
                
                string body = "";
                string header = "";
                string name = zgc0Helper.getConfigValue("Logopath1");

                bool bEmpty = false;
                DataTable PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt08 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                DataTable PhieuThu_PrintEmpty = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt_Detail07 where PhieuThuTienMatId=" + _Id, zgc0GlobalStr.getSqlStr());

                if (PhieuThu_PrintEmpty.Rows.Count < 1)
                {
                         bEmpty = true;
                }
                DataTable tbKH = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM gcGobal_CUST_Customer where Id=" + PhieuThu_Print.Rows[0]["KhachHangId"].ToString(), zgc0GlobalStr.getSqlStr());
                string LoaiKhachId = "";
                //LoaiKhachId = tbKH.Rows[0]["LoaiKhachId"].ToString();


               

                string strWhere = "";
                strWhere = "Id=" + _Id + " AND ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";
                double ThucThu = (PhieuThu_Print.Rows[0].IsNull("ThucThu")) ? (double)0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["ThucThu"]); 
                //(double)PhieuThu_Print.Rows[0]["ThucThu"];
                double VAT = (PhieuThu_Print.Rows[0].IsNull("VAT")) ? (double)0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["VAT"]);
                double PhuThu = 0;
                PhuThu = PhieuThu_Print.Rows[0].IsNull("PhuThu") ? 0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["PhuThu"]);
                double GiamGia = PhieuThu_Print.Rows[0].IsNull("DiscountCust") ? 0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["DiscountCust"]);
                string strTien = "";
                string strDienGiai = PhieuThu_Print.Rows[0].IsNull("DienGiai") ? "" : PhieuThu_Print.Rows[0]["DienGiai"].ToString();
                if (!bEmpty)
                {
                    strWhere = " PhieuThuTienMatId=" + _Id + " AND ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";
                    body = BuildAReport(LoaiKhachId, strWhere, strOrderBy, ref Number, ref ThucThu, ref PhuThu, ref GiamGia, ref VAT, bEmpty);
                }
                else
                {
                    body = BuildAReportEmptyDetail(strWhere, strOrderBy, ref Number, ref ThucThu, ref PhuThu, ref GiamGia, ref VAT, bEmpty);
                }

                //if (bEmpty)
                //{
                //    ThucThu = (double)PhieuThu_Print.Rows[0]["ThucThu"];
                //    strTien = ((double)PhieuThu_Print.Rows[0]["ThucThu"]).ToString();
                //}
                string DiaChi = "";
                if(tbKH.Rows.Count>0)
                    DiaChi= tbKH.Rows[0].IsNull("DiaChi") ? "" : tbKH.Rows[0]["DiaChi"].ToString();
                string inforCus = "";
                //if(DiaChi.Length>0)
                    inforCus = PhieuThu_Print.Rows[0]["KhachHangIdHoTen"].ToString() + "</br>" + DiaChi;
                StrConvert = dhuConvertNumberToString.Convert(ThucThu);
                header += String.Format(headerStr, PhieuThu_Print.Rows[0]["SoCT"], (String.Format("{0:dd/MM/yyyy hh:mm}",
                   PhieuThu_Print.Rows[0]["NgayLap"]))
                  , COMName, COMTel, COMAdd, inforCus,
                   StrConvert, "", "", "");

                if (!bEmpty)
                    if (strDienGiai.Trim().Length > 0)
                    {
                        strTien += "Nội dung: " + strDienGiai + "<br/>";
                    }

                string strThucThu = "Thực thu: " + String.Format("{0:N0}", ThucThu) + "";
                strTien += strThucThu;


                string footer = String.Format(footerStr, StrConvert, strTien);
                dhu_KT_SS_Footer dhuFooter = new dhu_KT_SS_Footer();
                //footerStr = dhuFooter.BuildATemplateDUNG();

                //if (!bEmpty)
                //    footer += String.Format(footerStr, "Ngày " + DateTime.Now.Day.ToString() + " tháng " + DateTime.Now.Month.ToString() + " năm " + DateTime.Now.Year.ToString(), "" + Dict.GetValue(dhuDictionary.NguoiLapPhieu) + "<br/>(Ký, họ tên)", "<b style='font-size:12px;'>" + "Người kiểm hàng" + "</b><br/>(Ký, họ tên)", "<b style='font-size:12px;'>" + "Thủ quỹ" + "</b><br/>(Ký, họ tên)", "<b style='font-size:12px;'>" + "Quản lý" + "</b><br/>(Ký, họ tên)", "", " ", "", "","");
                    
                //else
               //     footer += String.Format(footerStr, "Ngày " + DateTime.Now.Day.ToString() + " tháng " + DateTime.Now.Month.ToString() + " năm " + DateTime.Now.Year.ToString(), "" + Dict.GetValue(dhuDictionary.NguoiLapPhieu) + "(Ký, họ tên)", "<b style='font-size:12px;'>" + Dict.GetValue(dhuDictionary.NguoiNopTien) + "</b><br/>(Ký, họ tên)", "<b style='font-size:12px;'>" + Dict.GetValue(dhuDictionary.QuanlyBoPhan) + "</b><br/>(Ký, họ tên)", "", " ", "");


                System.Text.StringBuilder sb = new System.Text.StringBuilder(header + body );//+ footer);
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }
            
            
        }
        else
            context.Response.Write("Unknown.");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string BuildAReport(string LoaiKhachId, string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
          ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "zgcl_gcGobal_INCOM_Receipt_Detail07";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 4;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[1];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "zgcl_gcGobal_INCOM_Receipt_Detail07", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "5");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        //---------------------------------------------------
        //if (LoaiKhachId == "1")
        //    for (int i = 0; i < tblReport.cCols; i++)
        //    {
        //        for (int j = 0; j < tblReport.mData.Count; j++)
        //        {
        //            if (i == 1 || i == 5 | i == 6)
        //                tblReport.mData[j][i] = "0";
        //            if (i == 1)
        //                tblReport.mData[j][i] = "";
        //        }
        //    }
        //--------------------------------------------------

        //------------------------------------------------------------
        double sum = 0;
        for (int i = 4; i < 5; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            if (bEmpty)
            {
                sum = OldThucThu;
                tblReport.RowSumGobal.mValue[i] = OldThucThu.ToString();
            }
        }

        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.strContent = "Khách hàng " + tblReport.mData[Item.from][1];


                double Tem3 = 0;//, temp4 = 0;
                for (int j = 4; j < 5; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                        Item.mValue[j] = Tem3.ToString();
                    }
                    Tem3 = 0;
                }
                Item.cssClass = "dhu_rpt_TextRight";
            }
        }

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";

        double valuept = 0;


        double valueg = 0;

        double VATg = 0;
        if (VAT > 0)
        {
            VATg = sum * VAT / 100;
            gcRptTailItem item = new gcRptTailItem();
            item.ColSpan = tblReport.cCols - 2;
            item.cssValueClass = "dhu_rpt_TextRight";
            item.strFormName = "Tiền thuế GTGT (VAT)" + VAT + "%";
            item.Value = String.Format("{0:N0}", VATg);
            tblReport.mListTailItem.Add(item);
        }
        valuept = 0;
        if (OldPhuThu > 0)
        {

            valuept = OldPhuThu;
            gcRptTailItem itemg = new gcRptTailItem();
            itemg.ColSpan = tblReport.cCols - 1;
            itemg.cssValueClass = "dhu_rpt_TextRight";
            itemg.strFormName = "Phụ thu (Tiền dịch vụ vận chuyển)";
            itemg.Value = String.Format("{0:N0}", (valuept));
            tblReport.mListTailItem.Add(itemg);
        }
        if (OldGiamGia > 0)
        {

            if (OldGiamGia > 0 && OldGiamGia < 100.0)
                valueg = OldGiamGia * sum / 100.0;
            else
                valueg = OldGiamGia;
            gcRptTailItem itemg = new gcRptTailItem();
            itemg.ColSpan = tblReport.cCols - 1;
            itemg.cssValueClass = "dhu_rpt_TextRight";
            itemg.strFormName = "Giảm giá (Discount)";
            if (OldGiamGia > 0 && OldGiamGia < 100.0)
                itemg.strFormName = "Giảm giá (Discount) " + OldGiamGia + "%";
            itemg.Value = String.Format("{0:N0}", (valueg));
            tblReport.mListTailItem.Add(itemg);
        }
        
        if (OldThucThu <= 0)
            OldThucThu = (sum + valuept - valueg + VATg);
        gcRptTailItem itemtthu = new gcRptTailItem();
        itemtthu.ColSpan = tblReport.cCols - 1;
        itemtthu.cssValueClass = "dhu_rpt_TextRight";
        itemtthu.strFormName = "Đã thanh toán";
        itemtthu.Value = String.Format("{0:N0}", (OldThucThu));
        tblReport.mListTailItem.Add(itemtthu);
        
        
        
        gcRptTailItem item2 = new gcRptTailItem();
        item2.cssValueClass = "dhu_rpt_TextRight";
        item2.ColSpan = tblReport.cCols - 1;
        item2.strFormName = "Tổng cộng tiền thanh toán";
        item2.Value = String.Format("{0:N0}", (sum + valuept - valueg + VATg));
        //tblReport.mListTailItem.Add(item2);

        double valueSUM = sum + valuept - valueg + VATg;
        if (OldThucThu < valueSUM)
        {
            double nolai = valueSUM - OldThucThu;
            gcRptTailItem itemnolai = new gcRptTailItem();
            itemnolai.ColSpan = tblReport.cCols - 2;
            itemnolai.cssValueClass = "dhu_rpt_TextRight";
            itemnolai.strFormName = "Khách nợ";
            itemnolai.Value = String.Format("{0:N0}", (nolai));
            tblReport.mListTailItem.Add(itemnolai);
        }
       
        //for (int i = 3; i < 4; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextCenter";
        //}
        tblReport.mArrRightStyle[5] = 1; ;
        tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextRight";

        //tblReport.mArrRightStyle[2] = 1; ;
        //tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }

    public string BuildAReportEmptyDetail(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
       ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "gcGobal_REPORT_AUTOGEN_CustReceiptCharge";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 2;


        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[1];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGobal_REPORT_AUTOGEN_CustReceiptChargeBill", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "7");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[2] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //------------------------------------------------------------
        //------------------------------------------------------------

        double sum = 0;


        for (int i = 2; i < 3; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            if (bEmpty)
            {
                sum = OldThucThu;
                tblReport.RowSumGobal.mValue[i] = OldThucThu.ToString();
            }
        }

        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.strContent = "Khách hàng " + tblReport.mData[Item.from][1];


                double Tem3 = 0;//, temp4 = 0;
                for (int j = 2; j < 3; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                        Item.mValue[j] = Tem3.ToString();
                    }
                    Tem3 = 0;
                }
                Item.cssClass = "dhu_rpt_BoldRight";
            }
        }

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";

        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";

        double valuept = 0;
        if (OldPhuThu > 0)
        {

            valuept = OldPhuThu;
            gcRptTailItem itemg = new gcRptTailItem();
            itemg.ColSpan = tblReport.cCols - 2;
            itemg.cssValueClass = "dhu_rpt_BoldRight";
            itemg.strFormName = "Phụ thu ";
            itemg.Value = String.Format("{0:N0}", (valuept));
            tblReport.mListTailItem.Add(itemg);
        }

        double valueg = 0;
        if (OldGiamGia > 0)
        {

            if (OldGiamGia > 0 && OldGiamGia < 100.0)
                valueg = OldGiamGia * sum / 100.0;
            else
                valueg = OldGiamGia;
            gcRptTailItem itemg = new gcRptTailItem();
            itemg.ColSpan = tblReport.cCols - 2;
            itemg.cssValueClass = "dhu_rpt_BoldRight";
            itemg.strFormName = "Giảm giá (Discount)";
            if (OldGiamGia > 0 && OldGiamGia < 100.0)
                itemg.strFormName = "Giảm giá (Discount) " + OldGiamGia + "%";
            itemg.Value = String.Format("{0:N0}", (valueg));
            tblReport.mListTailItem.Add(itemg);
        }
        double VATg = 0;
        if (VAT > 0)
        {
            VATg = sum * VAT / 100;
            gcRptTailItem item = new gcRptTailItem();
            item.ColSpan = tblReport.cCols - 2;
            item.cssValueClass = "dhu_rpt_BoldRight";
            item.strFormName = "Tiền thuế GTGT (VAT)" + VAT + "%";
            item.Value = String.Format("{0:N0}", VATg);
            tblReport.mListTailItem.Add(item);
        }

        gcRptTailItem item2 = new gcRptTailItem();
        item2.cssValueClass = "dhu_rpt_BoldRight";
        item2.ColSpan = tblReport.cCols - 2;
        item2.strFormName = "Đã thanh toán";
        item2.Value = String.Format("{0:N0}", (sum + valuept - valueg + VATg));
        tblReport.mListTailItem.Add(item2);

        OldThucThu = (sum + valuept - valueg + VATg);
        //for (int i = 3; i < 4; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextCenter";
        //}
        for (int i = 2; i < 3; i++)
        {
            tblReport.mArrRightStyle[i] = 1; ;
            tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextRight";
        }
        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }


}