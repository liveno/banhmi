﻿<%@ WebHandler Language="C#" Class="ReportGenHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportGenHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"760px\" style=\"background:#FFFFFF\">"

                    + "<tr align='center'>"
                     + "<td><strong>{2}</strong> </br>{4}</td>"
                        + "</tr>"
                    + "<tr align='center'>"
                     
                        + "<td>ĐT: {3}</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"760px\" style = 'padding : 10px 0 '><b><font size=\"2.0pt\" style='font-weight:bold;font-size:16px;'>BÁO CÁO THU</font></b> </br>"
                        + "<span>Từ ngày {0} đến ngày {1} </span><br/>"
                        + "</td>"
                        + "</tr>"

                        // + "<tr>"
                        //+ "<td>AO: <strong>{5}</strong></td>"
                        //+ "</tr>"

                        //+ "<tr>"
        //+ "<td>Địa chỉ: {11} </td>"
        //+ "</tr>"

                        // + "<tr>"
                        //+ "<td>Số tiền nộp: ({6})</td>"
                        //+ "</tr>"

                        //+ "<tr>"
        //+ "<td>Bằng chữ : ({7})</td>"
        //+ "</tr>"

                        // + "<tr>"
        //+ "<td>Diễn giải: {7} </td>"
        //+ "</tr>"

                        // + "<tr>"
        //+ "<td>Chi nhánh: {8} </td>"
        //+ "</tr>"

                        //+ "<tr>"
        //+ "<td>Danh mục thu: {9} </td>"
        //+ "</tr>" 
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"760px\" style=\"background:#FFFFFF\">"

                       //+ " <tr><td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Thủ </b><br>(Ký, họ tên)</td> "
                      //+ "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Khách hàng</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Thủ quỹ</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">AO</b><br>(Ký, họ tên)</td>"
                      + "  </tr>"

                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = ResponseFormat



        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);



        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"]) == "true")
        {

            /// = this.Session["gcUserName"].ToString();
            string COMName = "";
            string COMTel = "";
            string COMAdd = "";
            string COMMobile = "";
            string MSHD = "";
            string Sql = "Select * from gcGobal_COMP_Branch ";
            if (HttpContext.Current.Session["gcBranchId"] != null)
            {
                Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                if (tb.Rows.Count > 0)
                {
                    COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                    COMTel = tb.Rows[0]["TEL"].ToString();
                    COMMobile = tb.Rows[0]["MOBILE"].ToString();
                    COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                }
            }

            dhuDictionary Dict = new dhuDictionary();
            dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

            string body = "";
            string header = "";
            string name = zgc0Helper.getConfigValue("Logopath1");

            bool bEmpty = false;
            string strWhere = "";
            var fromDate = DateTime.Parse(t1 + " 00:00");
            var toDate = DateTime.Parse(t2 + " 23:59");
            var strOrderBy = " order by NgayLap, NhanVienGiaoIdHoTen, VuViecNhanTienIdName ";
            strWhere = string.Format(" (PhieuNhanTienIdisPrgOrdered = 1) AND ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1)) " + 
                        " and NgayLap >= '{0} 00:00' and NgayLap <= '{1} 23:59' ", t1, t2);
            body = BuildAReport(strWhere, strOrderBy);
            header += String.Format(headerStr, fromDate.ToString("dd/MM/yyyy HH:mm"), toDate.ToString("dd/MM/yyyy HH:mm")
              , COMName, COMTel, COMAdd
               , "", "", "");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(header + body);
            context.Response.ContentType = "text/plain";
            context.Response.Write(sb.ToString());


        }
        else
            context.Response.Write("Unknown.");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string BuildAReport(string strWhere, string strOrderBy)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "zgcl_GBL_TRAINNING_THUQUY_PHIEUNHANTIEN_DETAILS02_FULL";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 6;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[2];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "zgcl_GBL_TRAINNING_THUQUY_PHIEUNHANTIEN_DETAILS02_FULL", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "15040502");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[8] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        //---------------------------------------------------
        //if (LoaiKhachId == "1")
        //    for (int i = 0; i < tblReport.cCols; i++)
        //    {
        //        for (int j = 0; j < tblReport.mData.Count; j++)
        //        {
        //            if (i == 1 || i == 5 | i == 6)
        //                tblReport.mData[j][i] = "0";
        //            if (i == 1)
        //                tblReport.mData[j][i] = "";
        //        }
        //    }
        //--------------------------------------------------

        //------------------------------------------------------------
        double sum = 0;
        for (int i = 8; i < 9; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
        }

        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                //Item.strContent = "Khách hàng " + tblReport.mData[Item.from][1];


                double Tem3 = 0;//, temp4 = 0;
                for (int j = 8; j < 9; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                        Item.mValue[j] = Tem3.ToString();
                    }
                    Tem3 = 0;
                }
                Item.cssClass = "dhu_rpt_TextRight";
            }
        }

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";

        tblReport.mArrRightStyle[8] = 1; ;
        tblReport.mArrRightStyleCss[8] = "dhu_rpt_TextRight";

        //tblReport.mArrRightStyle[2] = 1; ;
        //tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }

}