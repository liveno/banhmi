﻿<%@ WebHandler Language="C#" Class="ReportReceiptTheoNgaytHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportReceiptTheoNgaytHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"100%\" style=\"background:#FFFFFF\">"
            + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
            + "<tr>"
                     + "<td align=\"center\" width=\"100%\"  ><span style='font-weight:bold;font-size:18px;'> {2} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{3}</strong></span></br><span style='font-size:12px;'> &nbsp;Địa chỉ: {4}</br></span></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>BÁO CÁO DOANH THU</font></b> </br>"
                        + "<span style='font-size:14px;'>Từ <strong>{1}</strong> đến <strong>{5}</strong></span><br/>"
                        + "</td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'>Tổng tiền: <strong>{6}</strong></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    //string footerStr = "<table class=\"rptprintBodyTable\"  style=\"background:#FFFFFF\">"

    //                   + " <tr><td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Người lập phiếu</b><br>(Ký, họ tên)</td> "
    //                  + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Kế toán</b><br>(Ký, họ tên)</td>"
    //                  + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Thủ quỹ</b><br>(Ký, họ tên)</td>"
    //                  + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Quản lý</b><br>(Ký, họ tên)</td>"
    //                  + "  </tr>"

    //                + "</table>";
    string footerStr = "";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = ResponseFormat



        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        string User = gcUtility.GetNullString(context.Request.Form["obj[User]"]);
        string ListCa = gcUtility.GetNullString(context.Request.Form["obj[ListCa]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        string _BranchId = gcUtility.GetNullString(context.Request.Form["obj[BranchId]"]);
        //string _BranchId = "2";



        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"]) == "true")
        {
            if (typeReport == "Receipt")
            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                DataTable tb;
                string[] lCa = new string[1];
                if (!string.IsNullOrEmpty(_BranchId))
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", _BranchId);
                    tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMName = COMName.Replace("-", "</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                        lCa = gcUtility.GetNullString(tb.Rows[0]["CONFIG"]).Split('|');
                    }
                }
                else
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", "2");
                    tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMName = COMName.Replace("-", "</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                        lCa = gcUtility.GetNullString(tb.Rows[0]["CONFIG"]).Split('|');
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
                string name = zgc0Helper.getConfigValue("Logopath1");
                bool bEmpty = false;
                DateTime tparam = DateTime.Parse(t1);
                DateTime tp2 = DateTime.Parse(t2);
                DateTime tnow = DateTime.Parse(DateTime.Now.ToShortDateString());
                string tget = DateTime.Now.ToShortDateString();
                if (tparam != tnow)
                    tget = tparam.ToShortDateString();
                if(ListCa == "1"){
                    t2 = tparam.AddDays(1).ToString("MM/dd/yyyy") + " 00:00";
                    t1 = t1 + " 00:00";
                }
                else if (ListCa == "2"){
                    t2 = t1 + " 14:00:00";
                    t1 = t1 + " 00:00";
                }
                else if (ListCa == "3"){
                    t2 = tparam.AddDays(1).ToString("MM/dd/yyyy") + " 00:00";
                    t1 = t1 + " 14:00:00";
                }
                string SoCT = "";
                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;
                double GiamGia = 0;
                string strTien = "";
                string strWhere = "";
                strWhere = " (isPrgOrdered=1) and ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";
                string storewhere = "  ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";
                if (strWhere == null || strWhere.Length <= 0)
                {
                    storewhere = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
                    strWhere = String.Format("  ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
                }
                else
                {
                    storewhere += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
                    strWhere += String.Format(" AND  ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
                }
                // Branch thu
                if (_BranchId != null && _BranchId.Length > 0)
                {
                    string partcomp = HttpContext.Current.Session["gcCtyId"] + "-" + _BranchId.ToString() + "-";
                    if (strWhere == null || strWhere.Trim().Length < 1)
                    {
                        //storewhere = string.Format(" isPrgPartComp='{0}'", partcomp);
                        strWhere = string.Format(" isPrgPartComp like '{0}%'", partcomp);
                    }
                    else
                    {
                        //storewhere += string.Format(" and isPrgPartComp='{0}'", partcomp);
                        strWhere += string.Format(" and isPrgPartComp like '{0}%'", partcomp);
                    }
                }
                
                strOrderBy = " order by SoLuong desc, VatTuIdName, ThanhTien";
                string sql =  string.Format("alter view XIKE_Report_DoanhSoTrongNgayTheoHang as " + 
                            "SELECT        VatTuId, VatTuIdName, DonGia, DonViTinhId, DonViTinhIdName, " + 
                            "SUM(ISNULL(ThanhTien, 0)) AS ThanhTien, SUM(ISNULL(SoLuong, 0)) AS SoLuong " +
                            "FROM            dbo.XIKE_Report_DoanhSoTrongNgay_AnUong AS c where {0} " +
                            "GROUP BY VatTuId, VatTuIdName, DonGia, DonViTinhId, DonViTinhIdName "
                            , strWhere);
                zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());
                var doAn = BuildAReport("", strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty);
                
                string tempHead = String.Format(headerStr,
                                SoCT,
                                DateTime.Parse(t1).ToString("HH:mm dd/MM/yyyy"),
                                 COMName, COMTel, COMAdd, DateTime.Parse(t2).AddMilliseconds(-1).ToString("HH:mm dd/MM/yyyy"),
                                 ThucThu.ToString("0#,###"));
                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead + doAn);
                
                sb.Append(footerStr);
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }
        }
        else
            context.Response.Write("Unknown.");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    //Bàn//Thực thu//Giờ thu
    public string BuildAReport(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
       ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "XIKE_Report_DoanhSoTrongNgayTheoHang";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 5;

        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        //tblReport.mArrPos = new int[2];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "XIKE_Report_DoanhSoTrongNgayTheoHang", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "15040801");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[5] = 1;
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        double sum = 0;
        if (int.Parse(HttpContext.Current.Session["gcRightGroup"] + "") == 1)
        {
            for (int i = 5; i < 6; i++)
            {
                sum = 0;
                for (int j = 0; j < tblReport.mData.Count; j++)
                {
                    string value = (string)tblReport.mData[j][i];
                    value.Trim();
                    if (value != string.Empty)
                    {
                        sum += double.Parse((string)value);
                    }
                }
                tblReport.RowSumGobal.mValue[i] = sum.ToString();
            }
        }
        OldThucThu = sum;
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";
        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.mArrRightStyle[5] = 1; ;
        tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextRightBold";
        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssClassShowName = "dhu_rpt_TextCenterTotal";
        table = tblReport.MakeSimpleTable();
        return table;
    }
}