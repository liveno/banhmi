﻿<%@ WebHandler Language="C#" Class="ReportOutputHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportOutputHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
			+ "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
			+ "<tr>"
                     + "<td width=\"500px\"align=\"center\" ><span style='font-weight:bold;font-size:18px;'> {2} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{3}</strong> &nbsp;Địa chỉ: {4}</br></span></td>"
                        + "<td></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>KIỂM KHO</font></b> </br>"
                        + "<span style='font-size:14px;'>{0}</span><br/>"
                        + "</td>"
                        + "</tr>"
                        + "<tr>"
                        //+ "<td colspan=\"2\" style='font-size:12px;'> Người bán: <strong>{5}</strong> - Nhân viên: <strong>{6}</strong></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"800\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'></font></b> </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" >&nbsp; </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"
                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest (HttpContext context) {
        //context.Response.ContentType = ResponseFormat

       

        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        string HangHoa = gcUtility.GetNullString(context.Request.Form["obj[HangHoa]"]);
        string KhoId = gcUtility.GetNullString(context.Request.Form["obj[KhoId]"]);
        
        

        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"])== "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền

            
            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
			            COMName = COMName.Replace("-","</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }
                
                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

                string tempHead = "";
                //----------------------------------------------------------

                string NhanVienIdHoTen = "THU NGÂN";
                
                
                string strWhere = "";
                string filter = "";
                t1 = t1 + " 0:00";
                t2 = t2 + " 23:59";
                //if (strWhere == null || strWhere.Length <= 0)
                //    strWhere = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
                //else
                //{
                //    strWhere += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
                //}

                if (_Id != null && _Id.Length > 0)
                {
                    if (filter == null || filter.Length <= 0)
                        filter = String.Format(" ( VatTuId= {0})", _Id);
                    else
                    {
                        filter += String.Format(" AND ( VatTuId= {0})", _Id);
                    }
                }
                string storeWhere2 = strWhere;
                //--------------------------------------------------------------------------
                //
                if (Name != null && Name.Length > 0)
                {
                    if (filter == null || filter.Length <= 0)
                        filter = String.Format(" ( Name like N'%{0}%')", Name);
                    else
                    {
                        filter += String.Format(" AND ( Name like N'%{0}%')", Name);
                    }
                }
               
                
                //
                //strWhere = ""; // bỏ qua where
                //strOrderBy = " order by VatTuIdName,NgayLap, SoCT ";

                bool bEmpty = false;
                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;// PhieuThu_Print.Rows[0].IsNull("PhuThu") ? 0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["PhuThu"]);
                double GiamGia = 0;// PhieuThu_Print.Rows[0].IsNull("DiscountCust") ? 0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["DiscountCust"]);
                string table = "";
                {
                    string strOrderBy = "ORDER BY LoaiVattuId, VattuId";
                    DateTime TuNgay = DateTime.Now;
                    DateTime DenNgay = DateTime.Now;
                    //string strWhere = "(DayCheck = (SELECT Max(DayCheck) FROM gcGobal_REPORT_AUTOGEN_CheckStock))";
                    strWhere = "(DayCheck = (SELECT Max(DayCheck)  FROM gcGobal_REPORT_AUTOGEN_CheckStock)) and ( (SLHT<>0) or (SLNK<>0) or (SLXK<>0) ) ";
                    string sql = "SELECT * FROM gcGobal_STOCK_List WHERE Id =" + KhoId.ToString();
                    DataTable dtKHO = zgc0HelperSecurity.GetDataTable(sql, zgc0GlobalStr.getSqlStr());
                    string Kho = "tbl_Stock_" + dtKHO.Rows[0]["Code"].ToString();
                    string StockName = "gcGobal_STOCK_gcStock_" + dtKHO.Rows[0]["Code"].ToString();
                    string StockNameShow = "" + dtKHO.Rows[0]["Name"].ToString();

                     tempHead = String.Format(headerStr,
                                StockNameShow + " - Ngày: " + 
                                (String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now)),"",
                                 COMName, COMTel, COMAdd, "", NhanVienIdHoTen);
                    //----------------------------------------------------------------------------------
                    // change view 


                    sql = "ALTER VIEW [dbo].[gcGobal_REPORT_AUTOGEN_CheckStock01] "
                    + " AS "
                    + " SELECT     " + StockName + ".Id, " + StockName + ".DayCheck, " + StockName + ".VattuId, "
                    + "                      " + StockName + ".SLDK, " + StockName + ".SLXK, " + StockName + ".SLNK, "
                    + "                      " + StockName + ".SLHT, " + StockName + ".SLTT, " + StockName + ".DGDK, "
                    + "                      " + StockName + ".DGHT, " + StockName + ".DonviId, " + StockName + ".ExtentId, "
                    + "                      " + StockName + ".TypeId, " + StockName + ".ExpandCode, " + StockName + ".KHTS, "
                    + "                      " + StockName + ".SoTh, " + StockName + ".IsTSCD, " + StockName + ".DayInitData, "
                    + "                      dbo.gcGobal_STOCK_gcProductList.Code, dbo.gcGobal_STOCK_gcProductList.Name, dbo.gcGobal_STOCK_gcProductList.GiaMua, "
                    + "                      dbo.gcGobal_STOCK_gcProductList.GiaMoi, dbo.gcGobal_STOCK_gcProductList.MinValue, gcGobal_STOCK_gcProductList.LoaiVatTuId ,dbo.gcGobal_STOCK_gcProductList.KE+'-'+ dbo.gcGobal_STOCK_gcProductList.TANG_HANG as VITRI "
                    + " FROM         " + StockName + " INNER JOIN "
                    + "                      dbo.gcGobal_STOCK_gcProductList ON " + StockName + ".VattuId = dbo.gcGobal_STOCK_gcProductList.Id "
                    + " WHERE     ((dbo.gcGobal_STOCK_gcProductList.isPrgbUserDeleted = 0) OR "
                    + "                      (dbo.gcGobal_STOCK_gcProductList.isPrgbUserDeleted IS NULL) )   ";
                    zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());

                    sql = "ALTER VIEW gcGobal_REPORT_AUTOGEN_CheckStock AS SELECT     dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.Id, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.DayCheck,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.VattuId, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.SLDK,  "
                    + "              dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.SLXK, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.SLNK,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.SLHT, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.SLTT,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.DGDK, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.DGHT,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.DonviId, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.ExtentId,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.TypeId, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.ExpandCode,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.KHTS, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.SoTh,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.IsTSCD, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.DayInitData,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.Code, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.Name,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.GiaMua, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.GiaMoi,  "
                    + "               dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.MinValue, dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.LoaiVatTuId,  "
                    + "               dbo.gcGobal_STOCK_gcProductType.Code AS TypeCode, dbo.gcGobal_STOCK_gcProductType.Name AS TypeName,dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.VITRI "
                    + " FROM         dbo.gcGobal_REPORT_AUTOGEN_CheckStock01 LEFT OUTER JOIN "
                    + "               dbo.gcGobal_STOCK_gcProductType ON dbo.gcGobal_REPORT_AUTOGEN_CheckStock01.LoaiVatTuId = dbo.gcGobal_STOCK_gcProductType.Id  " + ((filter.Length < 2) ? "" : " where " + filter);
                    zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());

                    //----------------------------------------------------------------------------------
                    ExecProc_KhoChuyen("gcGOBAL_PROC_STOCK_CheckStock_Theongay_ChuyenData", dtKHO.Rows[0]["Code"].ToString(), t2);
                    
                    int thuhien1 = ExecProc_Kho("gcGOBAL_PROC_STOCK_CheckStock_New", dtKHO.Rows[0]["Code"].ToString());
                    if (thuhien1 != 0)
                    {
                        return;
                    }

                    if (HttpContext.Current.Session["gcRightGroup"].ToString() == "1")
                        table = BuildAReport(strWhere, strOrderBy, dtKHO.Rows[0]["Code"].ToString());
                    else
                        table = BuildAReportSeller(strWhere, strOrderBy, dtKHO.Rows[0]["Code"].ToString());
                    
                    string name = zgc0Helper.getConfigValue("Logopath");
                    string from = "", to = "";
                    to = DateTime.Now.Date.ToString("dd/MM/yyyy");

                    dhu_KT_SS_Header dhuHeader = new dhu_KT_SS_Header();
                    dhu_KT_SS_Footer dhuFooter = new dhu_KT_SS_Footer();
                   
                    headerStr = dhuHeader.BuildATemplate();
                    footerStr = dhuFooter.BuildATemplate();
                    string sqlNgay = "SELECT MAX(DayCheck) AS DayCheck FROM " + Kho;
                    DataTable dtNgay = zgc0HelperSecurity.GetDataTable(sqlNgay, zgc0GlobalStr.getSqlStr());
                    string DateNgay = "";
                    if (dtNgay.Rows.Count > 0)
                    {
                        DateNgay = ((dtNgay.Rows[0]["DayCheck"].ToString() == "") ? "" : DateTime.Parse(dtNgay.Rows[0]["DayCheck"].ToString()).ToString("dd/MM/yyyy"));
                    }
                    else
                        DateNgay = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    //divHeader.InnerHtml = String.Format(headerStr, "BÁO CÁO HIỆN TRẠNG KHO ", "<b> Kho: " + RadComboBoxTaiKhoan.Text + "</b> <br/> Ngày kiểm kho cuối cùng: " + DateNgay);//+ "<br/>Giờ " + DateTime.Now.Hour.ToString() + " phút " + DateTime.Now.Minute.ToString()
                    footerStr = String.Format(footerStr, "Ngày " + DateTime.Now.Day.ToString() + " tháng " + DateTime.Now.Month.ToString() + " năm " + DateTime.Now.Year.ToString(), "<b>" + Dict.GetValue(dhuDictionary.KeToanGhiSo) + "</b><br/>(Ký, họ tên)", "<b>" + Dict.GetValue(dhuDictionary.KeToanTruong) + "</b><br/>(Ký, họ tên)", "<b>" + Dict.GetValue(dhuDictionary.ChuNhiem) + "</b><br/>(Ký, họ tên)", "", " ", "");
                }
               
                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead + table + "</br>"  + footerStr);
                
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }
            
            
        }
        else
            context.Response.Write("Unknown.");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }


    private int ExecProc_Kho(string ProcName, string Kho)
    {
        int kq = 0;
        try
        {
            zgc0GlobalStr gb = new zgc0GlobalStr();
            //SqlConnection myCon =  zgc0HelperSecurity.getCon();
            SqlCommand myCmdTonKho = new SqlCommand(ProcName);
            myCmdTonKho.CommandType = System.Data.CommandType.StoredProcedure;
            myCmdTonKho.Parameters.Add("@StockCode", SqlDbType.NVarChar);
            myCmdTonKho.Parameters["@StockCode"].Value = Kho;
            zgc0HelperSecurity.ExecuteProcedure(myCmdTonKho, zgc0GlobalStr.getSqlStr());
        }
        catch
        {

            return kq = -1;
        }
        return kq;
    }

    private int ExecProc_KhoChuyen(string ProcName, string Kho, string t2)
    {
        int kq = 0;
        try
        {
            zgc0GlobalStr gb = new zgc0GlobalStr();
            //SqlConnection myCon =  zgc0HelperSecurity.getCon();
            SqlCommand myCmdTonKho = new SqlCommand(ProcName);
            myCmdTonKho.CommandType = System.Data.CommandType.StoredProcedure;
            myCmdTonKho.Parameters.Add("@StockCode", SqlDbType.NVarChar);
            myCmdTonKho.Parameters["@StockCode"].Value = Kho;
            myCmdTonKho.Parameters.Add("@NgayChuyen", SqlDbType.DateTime);
            myCmdTonKho.Parameters["@NgayChuyen"].Value = DateTime.Parse(t2);
            
            zgc0HelperSecurity.ExecuteProcedure(myCmdTonKho, zgc0GlobalStr.getSqlStr());
        }
        catch
        {

            return kq = -1;
        }
        return kq;
    }
    public string BuildAReportSeller(string strWhere, string strOrderBy, String codeKho)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây

        gcRptTable tblReport = new gcRptTable();

        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGobal_REPORT_AUTOGEN_CheckStock";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.strWhere = strWhere;
        tblReport.mMaxLevel = 2;
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 4;

        tblReport.bShowGroupBy = true;
        tblReport.mArrPos = new int[1];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        zgc0GlobalStr gb = new zgc0GlobalStr();
        String execStore = "Exec gcGOBAL_PROC_STOCK_CheckStock " + codeKho;
        zgc0HelperSecurity.ExecuteNonQuery(execStore, zgc0GlobalStr.getSqlStr());


        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        //tblReport.mArrPos = new int[0];
        //-----------------------------------------------------------
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_PROC_STOCK_CheckStock", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "1");

        //-----------------------------------------------------------
        tblReport.InitTable();


        //-----------------------------------------------------------
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //-----------------------------------------------------------
        tblReport.mArrArgSumary[4] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[5] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[6] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[7] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[8] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[9] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[10] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[11] = 1;// Tính tổng cột 5

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.mArrRightStyle[2] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //for (int t = 0; t < tblReport.cCols; t++)
        //{
        //    tblReport.mArrRightStyle.Add(0);
        //    tblReport.mArrRightStyleCss.Add("");
        //}
        ////tblReport.RowSumGobal.cssClass = "dhu_rpt_TextLeft";
        //for (int i = 3; i < 9; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1; ;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextRight";
        //}
        double sum = 0;


        for (int i = 0; i < tblReport.mData.Count; i++)
        {
            double SLTon = 0;
            double SLCanhBao = 0;
            double giaHH = 0;
            //So luong dau ki
            double SL = double.Parse((tblReport.mData[i][4].ToString() == "") ? "0" : tblReport.mData[i][4].ToString());
            double TriGia = double.Parse((tblReport.mData[i][9].ToString() == "") ? "0" : tblReport.mData[i][9].ToString());
            tblReport.mData[i][5] = (TriGia * SL).ToString();
            //So luong Nhap
            SL = double.Parse((tblReport.mData[i][6].ToString() == "") ? "0" : tblReport.mData[i][6].ToString());
            TriGia = double.Parse((tblReport.mData[i][9].ToString() == "") ? "0" : tblReport.mData[i][9].ToString());
            giaHH = TriGia;
            tblReport.mData[i][7] = (TriGia * SL).ToString();
            //So luong Xuat
            SL = double.Parse((tblReport.mData[i][8].ToString() == "") ? "0" : tblReport.mData[i][8].ToString());
            TriGia = double.Parse((tblReport.mData[i][9].ToString() == "") ? "0" : tblReport.mData[i][9].ToString());
            tblReport.mData[i][9] = (TriGia * SL).ToString();
            //So luong Hien tai
            SL = double.Parse((tblReport.mData[i][10].ToString() == "") ? "0" : tblReport.mData[i][10].ToString());
            TriGia = giaHH;

            tblReport.mData[i][11] = (TriGia * SL).ToString();
            SLTon = double.Parse((tblReport.mData[i][10].ToString().Trim() == "") ? "0" : tblReport.mData[i][10].ToString());
            SLCanhBao = double.Parse((tblReport.mData[i][12].ToString().Trim() == "") ? "0" : tblReport.mData[i][12].ToString());
            if (SLTon <= SLCanhBao)
            {
                tblReport.mArrCssForRow[i] = "dhu_rpt_Warning";
            }
            if (SL < 0)
            {
                tblReport.mArrCssForRow[i] = "dhu_rpt_Warning2";
            }
            for (int j = 0; j < tblReport.cCols; j++)
            {
                if (tblReport.mData[i][j].ToString() == "0")
                    tblReport.mData[i][j] = "";
            }
        }

        for (int i = 4; i < 13; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                //if (i == 5 || i == 7 || i == 9 | i == 11)
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            //if (i == 5 || i == 7 || i == 9 | i == 11)
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            sum = 0;
        }
        //Tong theo nhom và định dạng
        double ssum5 = 0, ssum7 = 0, ssum9 = 0, ssum11 = 0, ssum6 = 0, ssum8 = 0, ssum10 = 0; ;
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.cssClassShowName = "dhu_rpt_TextCenterBgr";
                Item.strContent = "  " + tblReport.mData[Item.from][1];
                //-------------------------------------------------

                Item.cssClass = "dhu_rpt_BoldRightBgr";
                double sum5 = 0, sum7 = 0, sum9 = 0, sum11 = 0, sum6 = 0, sum8 = 0, sum10 = 0;
                for (int p = Item.from; p < Item.to; p++)
                {
                    sum5 += double.Parse((tblReport.mData[p][5].ToString().Trim() == "") ? "0" : tblReport.mData[p][5].ToString());
                    sum7 += double.Parse((tblReport.mData[p][7].ToString().Trim() == "") ? "0" : tblReport.mData[p][7].ToString());
                    sum9 += double.Parse((tblReport.mData[p][9].ToString().Trim() == "") ? "0" : tblReport.mData[p][9].ToString());
                    sum11 += double.Parse((tblReport.mData[p][11].ToString().Trim() == "") ? "0" : tblReport.mData[p][11].ToString());

                    sum6 += double.Parse((tblReport.mData[p][6].ToString().Trim() == "") ? "0" : tblReport.mData[p][6].ToString());
                    sum8 += double.Parse((tblReport.mData[p][8].ToString().Trim() == "") ? "0" : tblReport.mData[p][8].ToString());
                    sum10 += double.Parse((tblReport.mData[p][10].ToString().Trim() == "") ? "0" : tblReport.mData[p][10].ToString());

                }
                Item.mValue[5] = sum5.ToString();
                Item.mValue[7] = sum7.ToString();
                Item.mValue[9] = sum9.ToString();
                Item.mValue[11] = sum11.ToString();
                Item.mValue[6] = sum6.ToString();
                Item.mValue[8] = sum8.ToString();
                Item.mValue[10] = sum10.ToString();

                ssum5 += sum5;
                ssum7 += sum7;
                ssum9 += sum9;
                ssum11 += sum11;
                ssum6 += sum6;
                ssum8 += sum8;
                ssum10 += sum10;
            }
        }



        tblReport.RowSumGobal.mValue[5] = ssum5.ToString();
        tblReport.RowSumGobal.mValue[7] = ssum7.ToString();
        tblReport.RowSumGobal.mValue[9] = ssum9.ToString();
        tblReport.RowSumGobal.mValue[11] = ssum11.ToString();

        tblReport.RowSumGobal.mValue[6] = ssum6.ToString();
        tblReport.RowSumGobal.mValue[8] = ssum8.ToString();
        tblReport.RowSumGobal.mValue[10] = ssum10.ToString();

        tblReport.SubRowSumGobal.position = 3;
        tblReport.SubRowSumGobal.showName = "Sub total";
        tblReport.SubRowSumGobal.cssClass = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.SubRowSumGobal.cssClassShowName = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.SubRowSumGobal.cssValueClass = "dhu_rpt_BoldRightBgrTOTAL";

        tblReport.RowSumGobal.showName = "Tổng cộng";
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.RowSumGobal.cssClassShowName = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_BoldRightBgrTOTAL";

        table = tblReport.MakeSimpleTable();
        return table;
    }

    public string BuildAReport(string strWhere, string strOrderBy, String codeKho)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây

        gcRptTable tblReport = new gcRptTable();

        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGobal_REPORT_AUTOGEN_CheckStock";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.strWhere = strWhere;
        tblReport.mMaxLevel = 2;
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 4;

        tblReport.bShowGroupBy = true;
        tblReport.mArrPos = new int[1];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        zgc0GlobalStr gb = new zgc0GlobalStr();
        String execStore = "Exec gcGOBAL_PROC_STOCK_CheckStock " + codeKho;
        zgc0HelperSecurity.ExecuteNonQuery(execStore, zgc0GlobalStr.getSqlStr());


        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        //tblReport.mArrPos = new int[0];
        //-----------------------------------------------------------
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_PROC_STOCK_CheckStock", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "1");

        //-----------------------------------------------------------
        tblReport.InitTable();


        //-----------------------------------------------------------
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //-----------------------------------------------------------
        tblReport.mArrArgSumary[4] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[5] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[6] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[7] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[8] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[9] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[10] = 1;// Tính tổng cột 5
        tblReport.mArrArgSumary[11] = 1;// Tính tổng cột 5

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.mArrRightStyle[2] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //for (int t = 0; t < tblReport.cCols; t++)
        //{
        //    tblReport.mArrRightStyle.Add(0);
        //    tblReport.mArrRightStyleCss.Add("");
        //}
        ////tblReport.RowSumGobal.cssClass = "dhu_rpt_TextLeft";
        //for (int i = 3; i < 9; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1; ;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextRight";
        //}
        double sum = 0;


        for (int i = 0; i < tblReport.mData.Count; i++)
        {
            double SLTon = 0;
            double SLCanhBao = 0;
            //So luong dau ki
            double SL = double.Parse((tblReport.mData[i][4].ToString() == "") ? "0" : tblReport.mData[i][4].ToString());
            double TriGia = double.Parse((tblReport.mData[i][5].ToString() == "") ? "0" : tblReport.mData[i][5].ToString());
            tblReport.mData[i][5] = (TriGia * SL).ToString();
            //So luong Nhap
            SL = double.Parse((tblReport.mData[i][6].ToString() == "") ? "0" : tblReport.mData[i][6].ToString());
            TriGia = double.Parse((tblReport.mData[i][7].ToString() == "") ? "0" : tblReport.mData[i][7].ToString());
            tblReport.mData[i][7] = (TriGia * SL).ToString();
            //So luong Xuat
            SL = double.Parse((tblReport.mData[i][8].ToString() == "") ? "0" : tblReport.mData[i][8].ToString());
            TriGia = double.Parse((tblReport.mData[i][9].ToString() == "") ? "0" : tblReport.mData[i][9].ToString());
            tblReport.mData[i][9] = (TriGia * SL).ToString();
            //So luong Hien tai
            SL = double.Parse((tblReport.mData[i][10].ToString() == "") ? "0" : tblReport.mData[i][10].ToString());
            TriGia = double.Parse((tblReport.mData[i][11].ToString() == "") ? "0" : tblReport.mData[i][11].ToString());
            tblReport.mData[i][11] = (TriGia * SL).ToString();
            SLTon = double.Parse((tblReport.mData[i][10].ToString().Trim() == "") ? "0" : tblReport.mData[i][10].ToString());
            SLCanhBao = double.Parse((tblReport.mData[i][12].ToString().Trim() == "") ? "0" : tblReport.mData[i][12].ToString());
            if (SLTon <= SLCanhBao)
            {
                tblReport.mArrCssForRow[i] = "dhu_rpt_Warning";
            }
            if (SL < 0)
            {
                tblReport.mArrCssForRow[i] = "dhu_rpt_Warning2";
            }
            for (int j = 0; j < tblReport.cCols; j++)
            {
                if (tblReport.mData[i][j].ToString() == "0")
                    tblReport.mData[i][j] = "";
            }
        }

        for (int i = 4; i < 13; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                //if (i == 5 || i == 7 || i == 9 | i == 11)
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            //if (i == 5 || i == 7 || i == 9 | i == 11)
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            sum = 0;
        }

        //Tong theo nhom và định dạng
        double ssum5 = 0, ssum7 = 0, ssum9 = 0, ssum11 = 0, ssum6 = 0, ssum8 = 0, ssum10 = 0; ;
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.cssClassShowName = "dhu_rpt_TextCenterBgr";
                Item.strContent = "  " + tblReport.mData[Item.from][1];
                //-------------------------------------------------

                Item.cssClass = "dhu_rpt_BoldRightBgr";
                double sum5 = 0, sum7 = 0, sum9 = 0, sum11 = 0, sum6 = 0, sum8 = 0, sum10 = 0;
                for (int p = Item.from; p < Item.to; p++)
                {
                    sum5 += double.Parse((tblReport.mData[p][5].ToString().Trim() == "") ? "0" : tblReport.mData[p][5].ToString());
                    sum7 += double.Parse((tblReport.mData[p][7].ToString().Trim() == "") ? "0" : tblReport.mData[p][7].ToString());
                    sum9 += double.Parse((tblReport.mData[p][9].ToString().Trim() == "") ? "0" : tblReport.mData[p][9].ToString());
                    sum11 += double.Parse((tblReport.mData[p][11].ToString().Trim() == "") ? "0" : tblReport.mData[p][11].ToString());

                    sum6 += double.Parse((tblReport.mData[p][6].ToString().Trim() == "") ? "0" : tblReport.mData[p][6].ToString());
                    sum8 += double.Parse((tblReport.mData[p][8].ToString().Trim() == "") ? "0" : tblReport.mData[p][8].ToString());
                    sum10 += double.Parse((tblReport.mData[p][10].ToString().Trim() == "") ? "0" : tblReport.mData[p][10].ToString());

                }
                Item.mValue[5] = sum5.ToString();
                Item.mValue[7] = sum7.ToString();
                Item.mValue[9] = sum9.ToString();
                Item.mValue[11] = sum11.ToString();
                Item.mValue[6] = sum6.ToString();
                Item.mValue[8] = sum8.ToString();
                Item.mValue[10] = sum10.ToString();

                ssum5 += sum5;
                ssum7 += sum7;
                ssum9 += sum9;
                ssum11 += sum11;
                ssum6 += sum6;
                ssum8 += sum8;
                ssum10 += sum10;
            }
        }



        tblReport.RowSumGobal.mValue[5] = ssum5.ToString();
        tblReport.RowSumGobal.mValue[7] = ssum7.ToString();
        tblReport.RowSumGobal.mValue[9] = ssum9.ToString();
        tblReport.RowSumGobal.mValue[11] = ssum11.ToString();

        tblReport.RowSumGobal.mValue[6] = ssum6.ToString();
        tblReport.RowSumGobal.mValue[8] = ssum8.ToString();
        tblReport.RowSumGobal.mValue[10] = ssum10.ToString();

        tblReport.SubRowSumGobal.position = 3;
        tblReport.SubRowSumGobal.showName = "Sub total";
        tblReport.SubRowSumGobal.cssClass = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.SubRowSumGobal.cssClassShowName = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.SubRowSumGobal.cssValueClass = "dhu_rpt_BoldRightBgrTOTAL";

        tblReport.RowSumGobal.showName = "Tổng cộng";
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.RowSumGobal.cssClassShowName = "dhu_rpt_BoldRightBgrTOTAL";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_BoldRightBgrTOTAL";

        table = tblReport.MakeSimpleTable();

        table += String.Format("</br><div>Tổng hàng mua vào: <strong>{0}<strong></div>", ssum6);
        table += String.Format("</br><div>Tổng hàng bán ra: <strong>{0}<strong></div>", ssum8);
        table += String.Format("</br><div>Hàng còn lại: <strong>{0}<strong></div>", ssum10);
        table += String.Format("</br><div>Lãi bán ra: <strong>{0}<strong></div>", String.Format("{0:0,0 VND}", (ssum9 - (ssum7 - ssum11))));
        return table;
    }


}