﻿<%@ WebHandler Language="C#" Class="ReportOutputHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportOutputHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
            + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
            + "<tr>"
                     + "<td width=\"500px\"align=\"center\" ><span style='font-weight:bold;font-size:18px;'> {2} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{3}</strong></span></br><span style='font-size:12px;'> &nbsp;Địa chỉ: {4}</br></span></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>BÁO CÁO TỔNG HỢP</font></b> </br>"
                        + "<span style='font-size:14px;'>{0}</span><br/>"
                        + "</td>"
                        + "</tr>"
                        + "<tr>"
        //+ "<td colspan=\"2\" style='font-size:12px;'> Người bán: <strong>{5}</strong> - Nhân viên: <strong>{6}</strong></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"800\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'></font></b> </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" >&nbsp; </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"
                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = ResponseFormat



        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        string BranchId = gcUtility.GetNullString(context.Request.Form["obj[BranchId]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);

        //string HangHoa = gcUtility.GetNullString(context.Request.Form["obj[HangHoa]"]);



        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"]) == "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền


            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                DataTable tb;
                string[] lCa = new string[1];
                if (!string.IsNullOrEmpty(BranchId))
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", BranchId);
                    tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMName = COMName.Replace("-", "</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                        lCa = gcUtility.GetNullString(tb.Rows[0]["CONFIG"]).Split('|');
                    }
                }
                else
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", "1");
                    tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        //COMName = "BỆNH VIÊN BÀ RỊA</BR>CANTEEN";
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                        lCa = gcUtility.GetNullString(tb.Rows[0]["CONFIG"]).Split('|');
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

                //divBody.InnerHtml = "";
                //divHeader.InnerHtml = "";
                string name = zgc0Helper.getConfigValue("Logopath1");

                bool bEmpty = false;
                DataTable PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt07 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                if (PhieuThu_Print.Rows.Count < 1)
                {
                    PhieuThu_Print = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM zgcl_gcGobal_INCOM_Receipt07 where Id=" + _Id, zgc0GlobalStr.getSqlStr());
                    bEmpty = true;
                }
                //DataTable tbKH = zgc0HelperSecurity.GetDataTableNew("SELECT * FROM gcGobal_CUST_Customer where Id=" + PhieuThu_Print.Rows[0]["KHId"].ToString(), zgc0GlobalStr.getSqlStr());
                string LoaiKhachId = "";
                //LoaiKhachId = tbKH.Rows[0]["LoaiKhachId"].ToString();

                //----------------------------------------------------------

                string NhanVienIdHoTen = "THU NGÂN";
                if (PhieuThu_Print.Rows.Count > 0)
                {
                    NhanVienIdHoTen = PhieuThu_Print.Rows[0].IsNull("NhanVienIdHoTen") ? "THU NGÂN" : PhieuThu_Print.Rows[0]["NhanVienIdHoTen"].ToString();
                }

                string SoCT = "";
                if (PhieuThu_Print.Rows.Count > 0)
                    SoCT = PhieuThu_Print.Rows[0].IsNull("SoCT") ? "" : PhieuThu_Print.Rows[0]["SoCT"].ToString();
                SoCT = SoCT.Replace("PT000", "");
                //---------------------------------------------
                DateTime tu1 = DateTime.Parse(t1);
                DateTime tu2 = DateTime.Parse(t2);
                String description = String.Format("Từ ngày: 00:00 {0} đến ngày 23:59 {1}", tu1.ToString("dd/MM/yyyy"), tu2.ToString("dd/MM/yyyy"));
                string tempHead = String.Format(headerStr,
                                description,
                                 DateTime.Now.ToString("dd/MM/yyyy"),
                                 COMName, COMTel, COMAdd, NhanVienIdHoTen);

                string strWhere = "";
                string strWhere1 = "";
                string reportTable = "gcGobal_Report_Brian_Receipt_SUM";
                string reportId = "23031501";
                string lS = "";
                //string lw = string.Format("convert(time(0), NgayLap) < '{0}' then "
                //            + " convert(datetime, convert(nvarchar(10), dateadd(day, -1, NgayLap), 101) + ' {1}')",
                //            "05:30:00", "23:30:00");
                t1 = t1 + " 00:00:00";
                t2 = tu2.AddDays(1).ToString("MM/dd/yyyy") + " 00:00";
                strWhere = String.Format(" (NgayLap >=N'{0}' and NgayLap <N'{1}' )", t1, t2);

                string sql = String.Format("ALTER VIEW gcGobal_Report_Brian_Receipt_SUM "
                            + " AS "
                            + " SELECT     BranchId, BranchIdName, convert(date, NgayLap) as NgayLap, "
                            + " Username + ' (' + convert(nvarchar, convert(time(0), min(NgayTam))) + ' - ' "
                            + " + convert(nvarchar, convert(time(0), max(NgayTam))) + ')' as UserName, sum(ThucThu) as ThucThu "
                            + " FROM         (select TongTien as ThucThu, NgayLap  "
                            + " , BranchId, BranchIdName, UserName, NgayLap as NgayTam "
                            + " From XIKE_Report_TongHop_Receipt WHERE (isPrgOrdered=1) "
                            + " and ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1)) "
                            + " and {0} ) as a GROUP BY  convert(date, NgayLap), BranchId, BranchIdName, UserName ",
                            strWhere);


                var t = zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());

                //
                strWhere = ""; // bỏ qua where
                strOrderBy = " order by NgayLap desc, BranchIdName";

                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;// PhieuThu_Print.Rows[0].IsNull("PhuThu") ? 0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["PhuThu"]);
                double GiamGia = 0;// PhieuThu_Print.Rows[0].IsNull("DiscountCust") ? 0 : Convert.ToDouble(PhieuThu_Print.Rows[0]["DiscountCust"]);
                string strTien = "";
                //double GiamGia = 0;

                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead +
                    (t ? BuildAReport(_Id, strWhere, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty, typeReport, reportTable, reportId) : "Error") + footerStr);
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }


        }
        else
            context.Response.Write("Unknown.");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public string BuildAReport(string Id, string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty, string typeReport, string reportTable, string reportId)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        //strOrderBy = " order by DonGia ";
        string table = "";
        gcRptTable tblReport = new gcRptTable();//Hàng hóa
        //tblReport.strTblSql = @"SELECT VatTuIdName,DonGia,DonViTinhIdName, Sum(SoLuong) AS SoLuong,SUM(ThanhTien) AS ThanhTien FROM zgcl_gcGobal_STOCK_gcProduct_Output_Detail02 WHERE PhieuXuatKhoId=" + _Id + " GROUP BY VatTuIdName,DonViTinhIdName, DonGia";

        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây

        tblReport.strMainTable = reportTable;
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 4;
        tblReport.mColMergForSubSumRowSub = 2;
        tblReport.bShowSubSumary = 1;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[2];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        // tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = true;
        tblReport.mMaxLevel = 1;
        //if (typeReport == "Receipt")
        tblReport.BuidlASimpleByDB(tblReport, reportTable, "zgcBUILDIN_CONFIG_REPORT_DETAIL", reportId);

        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[4] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();//Kho Chủ

        //---------------------------------------------------       

        double sum = 0;
        for (int i = 4; i < 5; i++)
        {
            sum = 0;
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
        }
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                //Item.strContent = "Khách hàng " + tblReport.mData[Item.from][1];


                double Tem3 = 0;//, temp4 = 0;
                for (int j = 4; j < 5; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                        Item.mValue[j] = Tem3.ToString();
                    }
                    Tem3 = 0;
                }
                Item.cssClassShowName = "dhu_rpt_TextCenterSumary";
                Item.cssClass = "dhu_rpt_TextCenterSumary";
                for (int l = 0; l < tblReport.mListItem[m].list.Count; l++)
                {
                    gcRptItem Item2 = tblReport.mListItem[m].list[l];
                    //Item.strShowName = "<b>Cộng </b>";
                    //Item.strContent = "DM " + tblReport.mData[Item.from][1];


                    double Tem32 = 0;//, temp4 = 0;
                    for (int j2 = 4; j2 < 5; j2++)
                    {
                        for (int i2 = Item2.from; i2 < Item2.to; i2++)
                        {
                            string a5 = (string)tblReport.mData[i2][j2];
                            Tem32 += double.Parse((a5.Length < 1) ? "0" : a5);
                            Item2.mValue[j2] = Tem32.ToString();
                        }
                        Tem32 = 0;
                    }

                    Item2.cssClass = "dhu_rpt_TextCenterSubTotal";
                    Item2.cssClassShowName = "dhu_rpt_TextCenterSubTotal";
                    Item2.cssValueClass = "dhu_rpt_TextCenterSubTotal";
                    Item2.strShowName = "Tong Cong";
                }

            }
        }
        //for (int j = 0; j < tblReport.mData.Count; j++)
        //{
        //    if (double.Parse(tblReport.mData[j][index == 6 ? 5 : (index == 12 ? 11 : 14)] + "") <= 0)
        //    {
        //        tblReport.mArrCssForRow[j] = "dhu_rpt_Warning2";
        //    }
        //    else {
        //        tblReport.mArrCssForRow[j] = "dhu_rpt_TextRight";
        //    }
        //}
        //if(index == 6){
        //    tblReport.mArrRightStyle[5] = 1; 
        //    tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextRightBgrXANHLO";
        //}
        //else if(index == 12){
        //    tblReport.mArrRightStyle[4] = 1; 
        //    tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextRightBgrXANHLO";
        //    tblReport.mArrRightStyle[7] = 1;
        //    tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRightBgrNew";
        //    tblReport.mArrRightStyle[10] = 1;
        //    tblReport.mArrRightStyleCss[11] = "dhu_rpt_TextRightBgrXANHLOLA";
        //    tblReport.mArrRightStyle[11] = 1; 
        //    tblReport.mArrRightStyleCss[11] = "dhu_rpt_TextRightBgrCAM";
        //}
        //else if(index == 15 ){
        //    tblReport.mArrRightStyle[5] = 1; 
        //    tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextRightBgrXANHLO";
        //    tblReport.mArrRightStyle[9] = 1;
        //    tblReport.mArrRightStyleCss[9] = "dhu_rpt_TextRightBgrNew";
        //    tblReport.mArrRightStyle[13] = 1;
        //    tblReport.mArrRightStyleCss[13] = "dhu_rpt_TextRightBgrXANHLOLA";
        //    tblReport.mArrRightStyle[14] = 1; 
        //    tblReport.mArrRightStyleCss[14] = "dhu_rpt_TextRightBgrCAM";
        //}
        //tblReport.RowSumGobal.cssClass = "dhu_rpt_TextCenterBgr";
        tblReport.SubRowSumGobal.cssClass = "dhu_rpt_TextCenterBgr";
        tblReport.SubRowSumGobal.cssValueClass = "dhu_rpt_TextCenterBgr"; tblReport.SubRowSumGobal.cssClassShowName = "dhu_rpt_TextCenterBgr";
        tblReport.SubRowSumGobal.position = 2;
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssClassShowName = "dhu_rpt_TextCenterTotal";
        table = tblReport.MakeSimpleTable();
        return table;
    }




}