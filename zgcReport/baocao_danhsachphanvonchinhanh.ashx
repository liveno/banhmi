﻿<%@ WebHandler Language="C#" Class="CongNoTongHangNhapHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class CongNoTongHangNhapHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";

    String strOrderBy;
    double Number = 0;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
			+ "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
			+ "<tr>"
                     + "<td width=\"500px\"align=\"center\" ><span style='font-weight:bold;font-size:18px;'> {2} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{3}</strong> &nbsp;Địa chỉ: {4}</br></span></td>"
                        + "<td></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>BÁO CÁO DANH SÁCH PHÂN VỐN CHI NHÁNH</font></b> </br>"
                        + "<span style='font-size:14px;'>{1} </span><br/>"
                        + "</td>"
                        + "</tr>"
                        
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"760px\" style=\"background:#FFFFFF\">"

                       + " <tr><td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Người lập phiếu</b><br>(Ký, họ tên)</td> "
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Kế toán</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Thủ quỹ</b><br>(Ký, họ tên)</td>"
                      + "   <td class=\"dhu_rpt_tpl_KTSS_Footer4\"><b style=\"font-size:12px;\">Quản lý</b><br>(Ký, họ tên)</td>"
                      + "  </tr>"

                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest (HttpContext context) {
        //context.Response.ContentType = ResponseFormat

       

        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        
        

        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"])== "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền

            if (typeReport == "Receipt")
            {

                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
			COMName = COMName.Replace("-","</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
                
                //divBody.InnerHtml = "";
                //divHeader.InnerHtml = "";
                string name = zgc0Helper.getConfigValue("Logopath1");

                bool bEmpty = false;
                string LoaiKhachId = "";
                //LoaiKhachId = tbKH.Rows[0]["LoaiKhachId"].ToString();

                //----------------------------------------------------------

                string NhanVienIdHoTen = "THU NGÂN";
                
                string BanAn = "";



                string SoCT = "";
                
                string strWhere = "";
                
                strWhere = "";// (isFinished=1) and ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";

                DateTime tparam = DateTime.Parse(t1);

                DateTime tnow = DateTime.Parse(DateTime.Now.ToShortDateString());
                string tget = DateTime.Now.ToShortDateString();
                if(tparam!=tnow)
                    tget = tparam.ToShortDateString();
                t1 = t1 + " 0:00";
                t2 = t2 + " 23:59";

                string tempHead = String.Format(headerStr,
                       SoCT,
                       (String.Format(" Từ ngày {0} đến ngày {1}", t1, t2)),
                        COMName, COMTel, COMAdd, BanAn, NhanVienIdHoTen);
                
                if (strWhere == null || strWhere.Length <= 0)
                    strWhere = String.Format(" (Id in (select distinct Id from gcGobal_INCOM_SoQuyVon_PhanBoVon_ChiNhanh where '{0}'<=NgayLap and NgayLap<='{1}') )", t1, t2);
                else
                {
                    strWhere += String.Format(" AND (Id in (select distinct Id from gcGobal_INCOM_SoQuyVon_PhanBoVon_ChiNhanh where '{0}'<=NgayLap and NgayLap<='{1}')))", t1, t2);
                }
                
//phan quyen
                if (HttpContext.Current.Session["gcRightGroup"] != null)
                {
                    string rg = HttpContext.Current.Session["gcRightGroup"].ToString();
                    if (rg != "1" )
                    {
                        string partcomp = HttpContext.Current.Session["gcCtyId"] + "-" + HttpContext.Current.Session["gcBranchId"] + "-" + HttpContext.Current.Session["gcDepartmentId"];
                        if (strWhere == null || strWhere.Trim().Length < 1)
                            strWhere = string.Format(" isPrgPartComp='{0}'", partcomp);
                        else
                            strWhere += string.Format(" and isPrgPartComp='{0}'", partcomp);
                    }
                }

                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;
                double GiamGia = 0;
                string strTien = "";
                //double GiamGia = 0;
		strOrderBy = " order by BranchIdNAME";

                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead+ 
                    BuildAReport(strWhere, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty) + footerStr );
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
            }

           
        }
        else
            context.Response.Write("Unknown.");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
    //Bàn//Thực thu//Giờ thu
    public string BuildAReport(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        //strWhere = ""; Nhân viên/Ngày tạm ứng/Số tiền/Ghi chú
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "zgcl_gcGobal_INCOM_SoQuyVon_PhanBoVon_ChiNhanh01";
        tblReport.bShowIndexRow = true;
        tblReport.mColMergForSubSumRow = 4;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[1];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên// Nhân viên
        tblReport.bShowGroupBy = true;//Công nợ
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "zgcl_gcGobal_INCOM_SoQuyVon_PhanBoVon_ChiNhanh01", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "906145");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[5] = 1;
        tblReport.mArrArgSumary[6] = 1;
        tblReport.mArrArgSumary[7] = 1;
        tblReport.mArrArgSumary[8] = 1;
        tblReport.mArrArgSumary[9] = 1;
        tblReport.mArrArgSumary[11] = 1;
        tblReport.mArrArgSumary[11] = 1;
        tblReport.mArrArgSumary[12] = 1;
        tblReport.mArrArgSumary[13] = 1;
        tblReport.mArrArgSumary[14] = 1;
        tblReport.mArrArgSumary[15] = 1;
        tblReport.mArrArgSumary[16] = 1;
       
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        //---------------------------------------------------
        //if (LoaiKhachId == "1") 
        
        //--------------------------------------------------

       
        
        //------------------------------------------------------------
        

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        
	double sum = 0;
        for (int i = 5; i < 17; i++)
        {
            sum = 0;
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                if (i == 7)
                {
                    if (HttpContext.Current.Session["gcRightGroup"] != null)
                    {
                        string rg = HttpContext.Current.Session["gcRightGroup"].ToString();
                        if (rg != "1")
                        {
                            tblReport.mData[j][i]="0";
                        }
                    }
                }
                string value = (string)tblReport.mData[j][i];
              
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();

        }
        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                //Item.strShowName = "<b>Cộng </b>";
                //Item.strContent = "DM " + tblReport.mData[Item.from][1];


                double Tem3 = 0;//, temp4 = 0;
                for (int j = 5; j < 17; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
                        Item.mValue[j] = Tem3.ToString();
                    }
                    Tem3 = 0;
                }

                // Tinh cong no
                double c5 = double.Parse((Item.mValue[5].Length < 1) ? "0" : Item.mValue[5]);
                double c10 = double.Parse((Item.mValue[10].Length < 1) ? "0" : Item.mValue[10]);
                double c11 = double.Parse((Item.mValue[11].Length < 1) ? "0" : Item.mValue[11]);
                double c12 = double.Parse((Item.mValue[12].Length < 1) ? "0" : Item.mValue[12]);
                double c13 = double.Parse((Item.mValue[13].Length < 1) ? "0" : Item.mValue[13]);
                double c14 = double.Parse((Item.mValue[14].Length < 1) ? "0" : Item.mValue[14]);
                double c15 = double.Parse((Item.mValue[15].Length < 1) ? "0" : Item.mValue[15]);
                double congno = c5 - c10-c11-c12-c13-c14-c15;
                Item.mValue[16] = congno.ToString();
                
                Item.cssClass = "dhu_rpt_TextCenterBgr";
                Item.cssClassShowName = "dhu_rpt_TextCenterBgr";
            }
        }
        
       
        //for (int i = 3; i < 4; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextCenter"; //Điện thoại
        //}


        tblReport.mArrRightStyle[5] = 1; ;
        tblReport.mArrRightStyleCss[5] = "dhu_rpt_TextCenterBgrNew";

        tblReport.mArrRightStyle[9] = 1; ;
        tblReport.mArrRightStyleCss[9] = "dhu_rpt_TextCenterBgrNew";
        tblReport.mArrRightStyle[12] = 1; ;
        tblReport.mArrRightStyleCss[12] = "dhu_rpt_TextCenterBgrNew";
        tblReport.mArrRightStyle[13] = 1; ;
        tblReport.mArrRightStyleCss[13] = "dhu_rpt_TextCenterBgrNew";

        tblReport.mArrRightStyle[4] = 1; ;
        tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextRight";

        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRightBold";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextRightBold";
        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }


}