﻿<%@ WebHandler Language="C#" Class="luongnhanvienHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class luongnhanvienHandler : IHttpHandler, IReadOnlySessionState
{
    string _Id = "-1";
    int numday = 30;
    String strOrderBy;
    double Number = 0;
    int curday = 30;
    string StrConvert = "";
    string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
			+ "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                        + "<tr>"
			+ "<tr>"
                     + "<td width=\"500px\"align=\"center\" ><span style='font-weight:bold;font-size:18px;'> {2} </span>  </br><span style='font-size:12px;'>Điện thoại: <strong>{3}</strong></span></br><span style='font-size:12px;'> &nbsp;Địa chỉ: {4}</br></span></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>BẢNG LƯƠNG NHÂN VIÊN</font></b> </br>"
                        + "<span style='font-size:14px;'>{1} </span><br/>"
                        + "</td>"
                        + "</tr>"
                        
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'></td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"

    string footerStr = "<table class=\"rptprintBodyTable\" width=\"800\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" style='font-weight:bold;font-size:16px;'>XIN CẢM ƠN QUÝ KHÁCH!</font></b> </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"

                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0pt\" >&nbsp; </br>"
                        + "<span style='font-size:14px;'> </span><br/>"
                        + "</td>"
                        + "</tr>"
                    + "</table>";
    //Nhà sàn//Bờ Sông//Bè cá//Phòng lạnh//Nhà cổ
    public void ProcessRequest (HttpContext context) {
        string typeReport = gcUtility.GetNullString(context.Request.Form["typeReport"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string t1e = gcUtility.GetNullString(context.Request.Form["obj[t1]"]);
        string t2e = gcUtility.GetNullString(context.Request.Form["obj[t2]"]);
        string Name = gcUtility.GetNullString(context.Request.Form["obj[Name]"]);
        _Id = gcUtility.GetNullString(context.Request.Form["obj[Id]"]);
        DateTime t = DateTime.Parse(t1);
        numday = 30;//
        curday = getSumMonth(t);
        string sql = string.Format(" alter view gcGOBAL_ATUTO_NHANVIEN_TINHLUONG_THEOTHANG_TAMUNG as "
                       + " SELECT     NhanVienId,  isnull(sum(isnull(SoTien, 0)), 0) as TienTamUng"
                       + " FROM         dbo.cc_GOBAL_NhanVien_TamUng " + " WHERE month(NgayTamUng)={0} and year(NgayTamUng)={1}  group by NhanVienId ", t.Month, t.Year);
        zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());

        sql = string.Format(" alter view gcGOBAL_ATUTO_NHANVIEN_TINHLUONG_THEOTHANG_PHAT as "
                       + " SELECT     NhanVienId,  isnull(sum(isnull(SoTien, 0)), 0) as TienPhat"
                       + " FROM         dbo.cc_GOBAL_NhanVien_ThuongPhat " + " WHERE month(NgayXet)={0} and year(NgayXet)={1} "
                       + " and (  ThuongPhatId in (select id from cc_GOBAL_DM_ChamCong_ThuongPhat where thuong is null or thuong = 0)  ) group by NhanVienId ", t.Month, t.Year);
        zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());

        sql = string.Format(" alter view gcGOBAL_ATUTO_NHANVIEN_TINHLUONG_THEOTHANG_THUONG as "
                       + " SELECT     NhanVienId,  isnull(sum(isnull(SoTien, 0)), 0) as TienThuong"
                       + " FROM         dbo.cc_GOBAL_NhanVien_ThuongPhat " + " WHERE month(NgayXet)={0} and year(NgayXet)={1} "
                       + "and ( ThuongPhatId in (select id from cc_GOBAL_DM_ChamCong_ThuongPhat where thuong = 1) ) group by NhanVienId ", t.Month, t.Year);
        zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());

        sql = string.Format(" alter view gcGOBAL_ATUTO_NHANVIEN_TINHLUONG_THEOTHANG_NGAYNGHI as "
                      + " SELECT     EmployeeId,  isnull(sum(isnull(Money, 0)), 0) as NgayCong, isnull(sum(isnull(Hour, 0)), 0) as GioTangCa "
                      + " FROM         dbo.gcGobal_EMPLOY_Payoff " + " WHERE month(ValidDate)={0} and year(ValidDate)={1}  group by EmployeeId ", t.Month, t.Year);
        zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());
        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"]) == "true")
        {
                string COMName = "";
                string COMTel = "";
                string COMAdd = "";
                string COMMobile = "";
                string MSHD = "";
                string Sql = "Select * from gcGobal_COMP_Branch ";
                if (HttpContext.Current.Session["gcBranchId"] != null)
                {
                    Sql = String.Format("Select * from gcGobal_COMP_Branch where id = {0}", HttpContext.Current.Session["gcBranchId"].ToString());
                    DataTable tb = zgc0HelperSecurity.GetDataTableNew(Sql, zgc0GlobalStr.getSqlStr());
                    if (tb.Rows.Count > 0)
                    {
                        COMName = tb.Rows[0]["NAME"].ToString().ToUpper();
			            COMName = COMName.Replace("-","</br>");
                        COMTel = tb.Rows[0]["TEL"].ToString();
                        COMMobile = tb.Rows[0]["MOBILE"].ToString();
                        COMAdd = tb.Rows[0]["ADDRESS"].ToString();
                    }
                }

                dhuDictionary Dict = new dhuDictionary();
                dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();
                string name = zgc0Helper.getConfigValue("Logopath1");
                bool bEmpty = false;
                string LoaiKhachId = "";
                //LoaiKhachId = tbKH.Rows[0]["LoaiKhachId"].ToString();
                string NhanVienIdHoTen = "THU NGÂN";
                string BanAn = "";
                string SoCT = "";
                string tempHead = String.Format(headerStr,
                                SoCT, 
                                (String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now)),
                                 COMName, COMTel, COMAdd, BanAn, NhanVienIdHoTen);
                string strWhere = "", strWhere2 = "";
                strWhere = String.Format(" Id={0}  and ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))",_Id);
                DateTime tparam = DateTime.Parse(t1);
                DateTime tnow = DateTime.Parse(DateTime.Now.ToShortDateString());
                string tget = DateTime.Now.ToShortDateString();
                if(tparam!=tnow)
                    tget = tparam.ToShortDateString();
                t1 = tget + " 0:00";
                t2 = tget + " 23:59";
                double ThucThu = 0;
                double VAT = 0;
                double PhuThu = 0;
                double GiamGia = 0;
                string strTien = "";
                //double GiamGia = 0;
                strOrderBy = " order by departmentIdNAME";
                string table1 = BuildAReport(strWhere, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty);
                t1 = t1e + " 0:00";
                t2 = t2e + " 23:59";
                if (strWhere2 == null || strWhere2.Length <= 0)
                    strWhere2 = String.Format(" ( NgayTamUng>=N'{0}' and NgayTamUng<=N'{1}' )", t1, t2);
                else
                {
                    strWhere2 += String.Format(" AND ( NgayTamUng>=N'{0}' and NgayTamUng<=N'{1}' )", t1, t2);
                }
                if (strWhere2 == null || strWhere2.Length <= 0)
                    strWhere2 = String.Format(" ( Id = {0} )", _Id);
                else
                {
                    strWhere2 += String.Format(" AND  ( Id = {0} )", _Id);
                }
                string table2 = BuildAReportTamUng(strWhere2, strOrderBy, ref Number, ref ThucThu,
                                            ref PhuThu, ref GiamGia, ref VAT, bEmpty);
                System.Text.StringBuilder sb = new System.Text.StringBuilder(tempHead+ table1 +"Danh sách tạm ứng tiền:</br>"+ table2 + footerStr );
                context.Response.ContentType = "text/plain";
                context.Response.Write(sb.ToString());
        }
        else
            context.Response.Write("Unknown.");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
    public int getSumMonth(DateTime t)
    {
        int num = 30;
        int month = t.Month;
        int year = t.Year;
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:   
                num = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                num = 30;
                break;
            case 2:
                num = 28;
                if(year % 400 ==0 || (year%4==0 && year%100!=0))
                    num = 29;
                break;
            default: num = 30;
                break;
        }
        return num;
    }
    //Bàn//Thực thu//Giờ thu
    public string BuildAReport(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
        ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        //strWhere = " ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))";
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "gcGOBAL_ATUTO_NHANVIEN_TINHLUONG_NHANVIEN_NGAYNGHI_PHAT_TAMUNG_THUONG";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 3;
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by// Bộ phận//Lương//Tạm ứng//Thưởng//Phạt//Còn lại//Ngày nghỉ
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        //tblReport.mArrPos = new int[1];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;//Công nợ
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_ATUTO_NHANVIEN_TINHLUONG_NHANVIEN_NGAYNGHI_PHAT_TAMUNG_THUONG", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "15052801");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[3] = 1;
        tblReport.mArrArgSumary[4] = 1;
        tblReport.mArrArgSumary[5] = 1;
        tblReport.mArrArgSumary[6] = 1;
        tblReport.mArrArgSumary[7] = 1;
        tblReport.mArrArgSumary[8] = 1;
        tblReport.mArrArgSumary[9] = 1;
        tblReport.mArrArgSumary[10] = 1;
        tblReport.mArrArgSumary[11] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        //------------------------------------------------------------
        double sum = 0;
        for (int i = 3; i < 12; i++)
        {
            sum = 0;
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            if ((sum + "").Split('.').Length > 1) sum = double.Parse((sum + "").Split('.')[0]);
            tblReport.RowSumGobal.mValue[i] = sum.ToString();

        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";
        ////Xuất dữ liệu
        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        for (int i = 3; i < 12; i++)
        {
            tblReport.mArrRightStyle[i] = 1; ;
            tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextRight";
        }
        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";
        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssClassShowName = "dhu_rpt_TextCenterTotal";
        //Xuất dữ liệu
        table = tblReport.MakeSimpleTable();
        return table;
    }

    public string BuildAReportTamUng(string strWhere, string strOrderBy, ref double StrConvert, ref double OldThucThu,
       ref double OldPhuThu, ref double OldGiamGia, ref double VAT, bool bEmpty)
    {
        dhuConvertNumberToString NumberConvert = new dhuConvertNumberToString();

        //strWhere = ""; Nhân viên/Ngày tạm ứng/Số tiền/Ghi chú
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.strMainTable = "gcGOBAL_AUTO_NHANVIEN_TAMUNG";
        tblReport.bShowIndexRow = false;
        tblReport.mColMergForSubSumRow = 3;



        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.strOrderBy = strOrderBy;
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strWhere = strWhere;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[1];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;//Công nợ
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_NHANVIEN_TAMUNG", "zgcBUILDIN_CONFIG_REPORT_DETAIL", "15052802");
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);
        tblReport.mArrArgSumary[3] = 1;
        tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[6] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[12] = 1;
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        //---------------------------------------------------
        //if (LoaiKhachId == "1") 

        //--------------------------------------------------



        //------------------------------------------------------------
        double sum = 0;
        for (int i = 3; i < 4; i++)
        {
            sum = 0;
            for (int j = 0; j < tblReport.mData.Count; j++)
            {

                string value = (string)tblReport.mData[j][i];

                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            tblReport.RowSumGobal.mValue[i] = sum.ToString();

        }

        //Tong theo nhom và định dạng
        //if (tblReport.mListItem.Count > 0)
        //{
        //    for (int m = 0; m < tblReport.mListItem.Count; m++)
        //    {
        //        gcRptItem Item = tblReport.mListItem[m];
        //        Item.strShowName = "<b>Cộng </b>";
        //        Item.strContent = "Khách hàng " + tblReport.mData[Item.from][1];


        //        double Tem3 = 0;//, temp4 = 0;
        //        for (int j = 3; j < 4; j++)
        //        {
        //            for (int i = Item.from; i < Item.to; i++)
        //            {
        //                string a5 = (string)tblReport.mData[i][j];
        //                Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);
        //                Item.mValue[j] = Tem3.ToString();
        //            }
        //            Tem3 = 0;
        //        }
        //        Item.cssClass = "dhu_rpt_TextRight";
        //    }
        //}

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }



        //for (int i = 3; i < 4; i++)
        //{
        //    tblReport.mArrRightStyle[i] = 1;
        //    tblReport.mArrRightStyleCss[i] = "dhu_rpt_TextCenter"; //Điện thoại
        //}
        tblReport.mArrRightStyle[4] = 1; ;
        tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextRight";

        tblReport.mArrRightStyle[3] = 1; ;
        tblReport.mArrRightStyleCss[2] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1; ;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssValueClass = "dhu_rpt_TextCenterTotal";
        tblReport.RowSumGobal.cssClassShowName = "dhu_rpt_TextCenterTotal";
        //Xuất dữ liệu
        //
        table = tblReport.MakeSimpleTable();
        return table;
    }


    

}