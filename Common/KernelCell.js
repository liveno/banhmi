﻿var _gbc = {
    _v: ['Cập nhật', 'Xóa'],
    _e: ['Update', 'Delete'],
    _r: {},
    _d: {},
    $gbTit: {},
    $gbHTab: {},
    $gbCTab: {},
    $gbTotal: {},
    pD: function (r) {
        if (r.hasOwnProperty("d")) {
            return r.d;
        }
        return r;
    },
    it: function (r, $el, dic, obj) {
        _gbc._r = _gbc.pD(r);
        _gbc._d = dic;
        if (_gbc._r.Result == 'OK') {
            $("#pagination").jPaginator({
                nbPages: parseInt((parseInt(_gbc._r.TotalRecordCount) - 1) / obj.mr) + 1,
                marginPx: 5,
                nbVisible: 5,
                selectedPage: obj.si,
                onPageClicked: function (a, num) {
                    obj.si = num;
                    Kernel.P(obj, function (result) { _gbc.it(result, $el, dic, obj); });
                }
            });
            _gbc.$gbCTab = $el;
            _gbc.$gbTit = $(dic.jElementTit);
            _gbc.$gbTit.html(dic.title);
            _gbc.$gbTotal = $(typeof dic.totalDiv == 'string' ? dic.totalDiv : '#gc_ShowNumObjectID');
            _gbc.$gbTotal.html((typeof dic.totalTitle == 'string' ? dic.totalTitle : 'Tổng') + ": <span style='color:red;'>" + _gbc._r.TotalRecordCount + "</span>");
            _gbc.rd();
            cur_gb_cur_ListDataShow = _gbc._r.Records;
        }
        else if (_gbc._r.Records == "Không có quyền truy cập dữ liệu") {
            showError("Bạn không còn quyền truy cập dữ liệu ở phiên giao dịch hiện tại. </br> Vui lòng đăng nhập lại (5s sau trình duyệt tự chuyển).");
            setTimeout(function () { window.location = '../default.aspx'; }, 5000);
        }
    },
    rd: function () {
        _gbc.$gbCTab.html("");
        for (var i = 0; i < _gbc._r.Records.length; i++) {
            var ri = _gbc._r.Records[i];
            var $block = $('<div />').attr('id', 'block' + i).addClass('block');
            var $item = $('<div />').attr('id', 'item' + i).addClass('item');
            var $legBottom = $('<div />').attr('id', 'legBottom' + i).addClass('LegBottom');
            var $footerItem = $('<div />').attr('id', 'footerItem' + i);
            var $btnToolbar = $('<div />').attr('id', 'btnToolbar' + i).addClass('btn-toolbar');
            var $btnGroup = $('<div />').attr('id', 'btnGroup' + i).addClass('btn-group');
            var $upPhieuNhapButton = $('<a />').attr('id', 'upPhieuNhapButton' + i).addClass('btn btn-warning btn-sm')
                    .attr('onclick', "_gbc._d['" + _gbc._d.button[0][1] + "'](this," + ri[1] + "," + i + ")")
                    .append("<i class='glyphicon glyphicon-user'></i> " + _gbc._d.button[0][0]);
            var $delPhieuNhapButton = $('<a />').attr('id', 'delPhieuNhapButton' + i).addClass('btn btn-danger btn-sm')
                    .attr('onclick', "_gbc._d['" + _gbc._d.button[1][1] + "'](this," + ri[1] + "," + i + ")")
                    .append("<i class='glyphicon glyphicon-user'></i> " + _gbc._d.button[1][0]);
            _gbc.rdC(ri, $item, i);
            _gbc.$gbCTab.append($block.append($item).append($legBottom.append($footerItem
                .append($btnToolbar.append($btnGroup.append($upPhieuNhapButton).append($delPhieuNhapButton))))));
            if (typeof _gbc._d.minHeight != 'undefined') $item.attr('style', 'min-height: ' + _gbc._d.minHeight);
        }
        _gbc.exCs();
        _gbc._d.eventItem();
    },
    rdC: function (ri, $el, i) {       // Render content of item
        var d = _gbc._d.data;
        var $ct = $('<div style="display: none"/>').attr('data-index', i);
        $el.append($ct);
        for (var j = 0; j < d.length; j++) {
            var $div = $('<' + d[j][0] + ' ' + d[j][1] + '/>');
            var $la = $('<' + d[j][2] + ' ' + d[j][3] + '/>').addClass(d[j][4]).append(d[j][6]);
            var $lab = d[j][2] != '' ? (d[j][5] != '' ? $la.attr('id', d[j][5]) : $la) : d[j][6];
            var val = _gbc.pTd(ri[d[j][12]], d[j][11], d[j][13]);
            var $te = $('<' + d[j][7] + ' ' + d[j][8] + '/>').addClass(d[j][9]).append(val);
            var $tex = d[j][7] != '' ? (d[j][10] != '' ? $te.attr('id', d[j][10]) : $te) : val;
            $el.append($div.append($lab).append($tex));
        }
    },
    pTd: function (d, td, df) {
        var r = '';
        if (td == 1 || td == 2 || td == 3 || td == 4) {
            r = d == null ? df : (d + '');
        } else if (td == 5) {
            r = d == null ? df : (typeof d == 'string' ? _parseDate(d + '').format('dd/MM/yyyy HH:mm:ss') : d.format('dd/MM/yyyy HH:mm:ss'));
        } else if (td == 6) {
            r = d == null ? df : gcFormatStr(d + '');
        } else if (td == 7) {
            r = d == null ? df : (typeof d == 'string' ? _parseDate(d + '').format('dd/MM/yyyy') : d.format('dd/MM/yyyy'));
        }
        return r;
    },
    exCs: function () {
        $(".item").focusout(function () {
            $(this).siblings().children().css("display", "none");
        });
        $(".LegBottom").hover(function () { $(this).children().css("display", "block"); },
            function () { $(this).children().css("display", "none"); });
        $(".item").hover(function () { $(this).siblings().children().css("display", "block"); },
            function () { $(this).siblings().children().css("display", "none"); });
        $(".LegBottom").children().css("display", "none");
        if (typeof gc_fn_Gobal_ConfigRightLoadListObject == 'function') { gc_fn_Gobal_ConfigRightLoadListObject(); }
        $("#gc_gobal_PagerTopID").show();
        $('#gc_gb_MessageInfomation').html("<i class='glyphicon glyphicon-ok '></i>&nbsp;<strong>Đã tải dữ liệu thành công.</strong> ");
        _gbc.$gbCTab.removeClass('loaderleft');
        modalcenter.close();
        modal.close();
    }
};