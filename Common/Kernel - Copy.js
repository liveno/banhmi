﻿var _la=0;//0:vietnamese,1:english
var cssb=["btn btn-primary btn-sm","btn btn-warning btn-sm","btn btn-info btn-sm"];
var _gb = //gobal object
{
ll: function (i, s) { switch (_la) { case 0: return s._v[i + 1]; case 1: return s._e[i + 1]; } },
_l: function (i) { switch (_la) { case 0: return _gb._v[i]; case 1: return _gb._e[i]; } },
_v: ['Thêm mới', 'Đóng cửa sổ', 'Cập nhật', 'Xóa', 'Lưu', 'Sao chép', 'Có', 'Không', 'Báo cáo'],
_e: ['AddNew', 'Close', 'Update', 'Delete', 'Save', 'Copy', 'Yes', 'No', 'Report'],
sfi: function (id) {
    $("'#" + id + "_f'").each(function () { this.reset(); });
    // setup the first input;
    for (m = 0; m < $("#" + id + " input").length; m++) {
        if ($($("#" + id + " input")[m]).css('display') != 'none') {
            $($("#" + id + "  input")[m]).focus(); $($("#" + id + "  input")[m]).select(); break;
        }
    }
},
a: function ($r, i, ii, _s) {
    /*return label for field i*/
    _gb.l(i, _s.id, _s._v, _s._e).appendTo($r);
    if (ii[3] == '5')
        _gb.i(i, _s.id, "name='datepicker'").appendTo($r);
    else {
        if (ii[4] == '1')//ref key
        {
            _gb.i(i, _s.id, "name='combo'").appendTo($r);
            _gb.ih(i, _s.id, "").appendTo($r);
        }
        else _gb.i(i, _s.id, '').appendTo($r);
    }
    return $r;
},
bfg: function (_s) {//build form
    var sr = '';
    for (i = 0; i < _s._d.length; i++) {
        var r = _s._d[i].split('|');
        for (u = 0; u < r.length; u++) {
            var ii = r[u].split(',');
            var ic = parseInt(ii[0]);
            if (sr == '') {
                if (_gb.ll(ic, _s) == 'Id')
                    sr += ("Id: {key: true, create: false,edit: false,list: false, title: '|0|', display: function (data) { return (data.record.f|1| ==null)?'':data.record.f|1|+'' }  } ").f2(_gb.ll(ic, _s), (ic <= 9) ? '0' + (ic + 1) : '' + (ic + 1));
                else
                    sr += ("f|0|: {title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':data.record.f|2|+'' }  } ").f2((ic <= 9) ? '0' + (ic - 1) : '' + (ic - 1), _gb.ll(ic, _s));
            }
            else {
                var v = new String(",f|0|: {title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':data.record.f|2|+'' }  } ").f2(((ic - 1) <= 9) ? '0' + (ic - 1) : '' + (ic - 1), _gb.ll(ic, _s), (ic <= 9) ? '0' + (ic) : '' + (ic));
                if ((ii[3] == '3' || ii[3] == '2' || ii[3] == '1') && ii[4] != '1')
                    v = new String(",f|0|: {title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':c(data.record.f|2|+'') }  } ").f2(((ic - 1) <= 9) ? '0' + (ic - 1) : '' + (ic - 1), _gb.ll(ic, _s), (ic <= 9) ? '0' + (ic) : '' + (ic));
                if (ii[3] == '5')
                    v = new String(",f|0|: {title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss') }  } ").f2(((ic - 1) <= 9) ? '0' + (ic - 1) : '' + (ic - 1), _gb.ll(ic, _s), (ic <= 9) ? '0' + (ic) : '' + (ic));
                sr += v;
            }
        }
    }
    return sr;
},
fi: function (_s) { //form input
    _s.$dw = $("<div class='" + _s.dwcss + "'></div>");
    for (i = 0; i < _s._d.length; i++) {
        var r = _s._d[i].split('|');
        var $r01 = _gb.r(i < 10 ? '0' + i : i + '', _s.id); ;
        for (u = 0; u < r.length; u++)
        { var ii = r[u].split(','); _gb.a($r01, ii[0], ii, _s); }
        $r01.appendTo(_s.$dw);
    }
    _gb.be().appendTo(_s.$dw);
    if (typeof pfi_o71 == 'function') pfi_o71(_s.$dw); //extension fi
},
ff: function (_sg, _s) {// form full, sg: show grid
    var $dall = $("<div id='" + _s.id + "' class='" + _s._sy + "'></div>");
    _gb.fc(_sg, _s).appendTo($dall);
    if ($("#gobalSaveFormInner").length <= 0)
        $('body').append($("<div id='gobalSaveFormInner' style='display:none;'></div>"));
    $("#gobalSaveFormInner").html($dall);
    //setup css
    $("#" + _s.id + " label").addClass('brlabel125');
    $("#" + _s.id + " input[name*='combo']").addClass('brwidthinput200');
    $("#" + _s.id + " input[type*='text']").addClass('brwidthinput200');
    setClassAll(_s.id);
    if (typeof psc_o71 == 'function') psc_o71(_s);
    //load data grid
    if (_sg != null && _sg != undefined && _sg)
        $("#" + _s.id + "_g").jtable('load', {
            obj: { a: 'pGet' + _s._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 25, se: ' Id desc', f: gb_strFilterGrid }
		, jtStartIndex: 0
		, jtPageSize: 300
        });
    return _s;
},
fc: function (_sg, _s) { //form control, sg: show grid
    var $fa = _gb.fa(_s.id);
    var $da = _gb.da(_s.id);
    var $db = _gb.db(_s.cssb);
    _gb.btn(_s, cssb[1], 'c', 1, _sg).appendTo($db);
    _gb.btn(_s, cssb[0], 'i', 0, _sg).appendTo($db);
    _gb.btn(_s, cssb[2], 'u', 2, _sg).appendTo($db);
    _gb.fi(_s);
    $db.appendTo(_s.$dw)
    _s.$dw.appendTo($fa);
    if (_sg != null && _sg != undefined && _sg) {
        _gb.sg($da, _s, 25);
    }
    if (typeof pfc_o71 == 'function') pfc_o71($dw); //extension fc
    $da.appendTo($fa);
    return $fa;
},
sf: function (o, s) {//set form
    for (u = 0; u < s._f.length; u++)// duyet field, field phai duoc parser tu _gb
    {
        var fel = 'f' + (parseInt(u) <= 9 ? '0' + u: u+'');
        var of = s._f[u].split(',');
        if ($('#f' + of[0] + 'o71i').length) $('#f' + of[0] + 'o71i').val((o[fel] == null) ? '' : o[fel] + '');
        if (of[3][0] == '5') if ($('#f' + of[0] + 'o71i').length) $('#f' + of[0] + 'o71i').val((o[fel] == null) ? '' : _parseDate(o[fel] + '').format('dd/MM/yyyy HH:mm:ss')); //date time
        if ((of[3][0] == '3' || of[3][0] == '2' || of[3][0] == '1') && of[3][0] != '1') if ($('#f' + of[0] + 'o71i').length) $('#f' + of[0] + 'o71i').val((o[fel] == null) ? '' : c(o[fel] + '')); //int

        if (of[3][0] == '1')
            if ($('#f' + of[0] + 'o71h').length) $('#f' + of[0] + 'o71h').val((o[fel] == null) ? '' : o[fel] + '');
    }
    return s;
},
gf: function (s) { //get form
    s._coj = new Object();
    for (u = 0; u < s._f.length; u++) {
        var of = s._f[u].split(',');
        if ($('#f' + of[0] + 'o71i').length) s._coj['f' + of[0]] = $('#f' + of[0] + 'o71i').val();
        if (of[3] == '1')
            if ($('#f' + of[0] + 'o71h').length) s._coj['f' + of[0]] = $('#f' + of[0] + 'o71h').val();
    }
    return s;
},
pr: function (s) {//push row
    for (i = 0; i < s._d.length; i++) {
        s._r.push(s._d);
        var r = s._d[i].split('|');
        for (u = 0; u < r.length; u++) s._f.push(r[u]);
    }
    return this;
},
//show and hide control
fh: function ($r) {
    var t1 = 0;
    for (u = 0; u < $r.children().length; u++)
        if ($($r.children()[u]).css('display') == 'none') t1++;
    if (t1 == $r.children().length) $($r).attr('style', 'display:none');
},
/*return input for field i,if foreign key, add service & hide input*/
i: function (i, id, n) { return $("<input id='f" + i + id + "i' type='text'  autocomplete='off' " + n + "></input>"); },
ih: function (i, id, n) { return $("<input id='f" + i + id + "h' type='text' style='display:none;' autocomplete='off' " + n + "></input>"); },
l: function (i, id, v, e) { /*return label for field i*/
    var $v = $("<label id='l" + i + id + "i'>" + ((_la == 0) ? v[parseInt(i) + 1] : e[parseInt(i) + 1]) + "</label>");
    return $v;
},
r: function (i, id) { /*return label for field i*/return $("<div class='input' id='r" + i + id + "'></div>"); },
g: function (id) { /*return label for field i*/return $("<div id='" + id + "_g'></div>"); },
be: function () {
    var $btn = $("<button style='display:none;'></button>");
    $btn.click(function (ev) { ev.preventDefault(); ev.stopPropagation(); });
    return $btn;
},
fa: function (id) { return $("<form name='multiform' action='' method='POST' id='" + id + "_f'></form>") },
da: function (id) { return $("<div id='" + id + "_i'></div>") },
db: function (css) { return $("<div class='" + css + "'></div>") },
btn: function (s, css, t, i, sg) {//default css=cssb[0],t:type:a->add, u->udpate,i->insert
    var $btn = $("<input class='" + css + "'  value='" + _gb._l(i) + "'  id='" + s.id + t + "_'></input>");
    $btn.click(function (ev) {
        _gb.gf(s)._coj.type = ((t == 'i') ? 'INSERT' : ((t == 'u') ? 'UPDATE' : (t == 'd') ? 'DELETE' : 'NONE'));
        if (typeof pad_o71 == 'function') if (pad_o71(s._coj)) return btn; //break here
        //Kernel.P(s._coj, sf_a_o71, ff_a_o71);
        $('#' + s.id + '_f').each(function () { this.reset(); });
        if (sg != null && sg != undefined && sg) {
            $("#" + _s.id + "_g").jtable('load', {
                obj: { a: 'pGet' + s._e[0], c: '', d: '', type: 'p', cl: '*', si: 1, mr: 25, se: ' Id desc', f: gb_strFilterGrid }
		        , jtStartIndex: 0
		        , jtPageSize: 300
            });
            if (typeof p_ug_o71 == 'function')
                p_ug_o71(s, t);
        }
        // setup the first input;
        _gb.sfi(s.id);
    });
    return $btn;
},
sg: function ($d, s, si) { //form service, $d: div all, s is object to build, si, page size
    _gb.g(s.id).appendTo($d);
    var sr = document.createElement('script');
    sr.type = 'text/javascript';
    sr.innerHTML = "$(document).ready(function () {"
				    + "$('#" + s.id + "_g').jtable({"
				    + "    title: '" + _gb.ll(0, s) + "',"
				    + "    paging: true,"
				    + "    pageSize: " + si + ","
				    + "    sorting: true,"
				    + "    selecting: true, "
				    + "    selectingCheckboxes: true, "
				    + "    defaultSorting: 'Id ASC',"
				    + "    recordsLoaded: function (event, data) {"
				    + "         if (typeof pgrl_" + s.id + " == 'function')"
				    + "             pgrl_" + s.id + "(data);"
				    + "    },"
				    + "    selectionChanged: function () {"
				    + "        var $selectedRows = $('#" + s.id + "_g').jtable('selectedRows');"
				    + "        if ($selectedRows.length > 0) {"
				    + "            $selectedRows.each(function () {"
				    + "                var record = $(this).data('record');"
				    + "                cur_gb_KeyObj = record.Id.toString();"
				    + "                _gb.sf(record," + s.id + ");"
				    + "                if (typeof pgrs_" + s.id + " == 'function')"
				    + "                    pgrs_" + s.id + "(record);"
				    + "            });"
				    + "        }"
				    + "    },"
				    + "    recordDeleted: function (event, data) {"
				    + "         if (typeof pgrd_" + s.id + " == 'function')"
				    + "             pgrd_" + s.id + "(data);"
				    + "    },"
				    + "    actions: {"
				    + "        listAction: '../WSASMXGobal/Kernel.asmx/Process',"
				    + "        createAction: '../WSASMX/Kernel.asmx/Process',"
				    + "        deleteAction: '../WSASMX/Kernel.asmx/Process' "
				    + "    },"
				    + "    fields: {"
				    + _gb.bfg(s)//bgf build grid field
 				    + "    }"
				    + "});"
				    + "});";
    //if (typeof p_ig_71 == 'function')
    //    p_ig_71(sr, $d, s);
    // Use any selector 
    $($d).append(sr);
},
sv: function (strId, ev, s) {
    var ev = ev || window.event;
    var keyCode = (ev == null) ? -1 : (ev.keyCode || ev.which);
    /* tab null backspace , minus*/
    if (keyCode == 9 || keyCode == 16 || keyCode == 17) {
        $('#divShowID').hide(); return;
    }
    else if (keyCode == 40) {
        gc_fn_Gobal_ProcessArrowDown(strId, ev); ev.preventDefault(); return;
    }
    else if (keyCode == 38) {
        gc_fn_Gobal_ProcessArrowUp(strId, ev); ev.preventDefault(); return;
    }
    else if (keyCode == 13) {
        if (ev.ctrlKey == 1 || ev.ctrlKey == true)
            if (typeof pe_io == 'function')
                pe_oi(strId, ev, s);
            else {
                var ctrlTmp = $get('itemid' + cur_gb_Index.toString()); if (ctrlTmp != null) ctrlTmp.onclick();
                return;
            }
    }
    cur_gb_CotrolID = strId;
    /*check have space at the end*/
    var value = $get(cur_gb_CotrolID).value;
    if (value.length > 0 && value[value.length - 1] != ' ')
        return true;
    /*end check*/
    cur_gb_FlagControlCmb = 0;
    gb_strFilter = ' '; /*reset filter*/
    if (cur_gb_CotrolID) {
        cur_gb_Index = -1;
        cur_gb_Id = '-9999';
        cur_gb_v = value;
        cur_gb_n = gb_NumComboBox;
        cur_gb_f = gb_strFilter;
        cur_gb_fid = cur_gb_CotrolID;
        cur_gb_oid = s.__id;
        $('#' + cur_gb_CotrolID).removeClass('gc-style-form-combo');
        $('#' + cur_gb_CotrolID).addClass('loader');
        //không cần truyền user vì dư thừa chúng ta có thể check dc bằng server.
        Kernel.Combo(cur_gb_Obj, showPopupDivData);
    }
}
};