﻿// open new window
function ow(link)
{
    var winSize = ",width=" + (screen.availWidth).toString() + ",height=" + (screen.availHeight).toString();

    var ControlBar="fullscreen=no,menubar=no, toolbar=no, scrollbars=yes,top=0,left=0, titlebar=no, resizable=yes,status=no,location=no ";
    window.open(link,'',ControlBar+winSize);
}
function ows(link,width,height)
{
    var winSize = ",width=" + width.toString() + ",height=" + height.toString();

    var ControlBar="fullscreen=no,menubar=no, toolbar=no, scrollbars=yes,top=0,left=0, titlebar=no, resizable=yes,status=no,location=no ";
    window.open(link,'',ControlBar+winSize);
}
function toggleDisplay(blockId, titleId)
{
    
	var block = document.getElementById(blockId);
	var title = document.getElementById(titleId);
	if (block.className.indexOf("qsfNone") == -1)
	{
		block.className = block.className + " qsfNone";
		title.className = "qsfSubtitleCollapsed";
	}
	else
	{
		block.className = block.className.replace(/qsfNone/, "");
		title.className = "qsfSubtitle";
	}
	
	return false;
}
function toggleListLab(blockId, titleId)
{
    
	var block = document.getElementById(blockId);
	var title = document.getElementById(titleId);
	if (block.className.indexOf("qsfNone") == -1)
	{
		block.className = "qsfNone";
		title.className = "qsfSubtitleCollapsed";
	}
	else
	{
		block.className = block.className.replace(/qsfNone/, "");
		title.className = "qsfSubtitle";
	}	
	return false;
	 
}
function processCode(str)
{
    for (var i = 0; i < str.length; i++) {
        str = str.replace('%', '');
    }
    if(str==null)
        return '';
    var arrId=str.split('^');
    if(arrId.length <2)
        return '';
    return   arrId[0]; 
}
function labListToShowAll(hiddenContainsListId)
{
    var lstId=document.getElementById(hiddenContainsListId);
    var arrId=lstId.value.split(';');    
    for(i=0;i<arrId.length;i++)
    {
        var divToShow=document.getElementById(arrId[i]);
        divToShow.className="";        
    }  
}
function labListToHidewAll(hiddenContainsListId)
{
    var lstId=document.getElementById(hiddenContainsListId);
    var arrId=lstId.value.split(';');    
    for(i=0;i<arrId.length;i++)
    {
        var divToShow=document.getElementById(arrId[i]);
        divToShow.className="qsfNone";        
    }
   
    
}
function Process(tail) {
}


 var hasChanges, inputs, dropdowns, editedRow;
                                    
function KeyPress(sender, args) {  
     if (args.get_keyCharacter() == sender.get_numberFormat().DecimalSeparator) {
         args.set_cancel(true);  
      }  
}  
function hov(loc,cls) 
{ 
	if(loc.className) 
		loc.className=cls; 
}
function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

 /* -------- Định dạng chuỗi của text   ----------*/
function gcFormat(strId)
{
    var txtbox = document.getElementById(strId);
    var index = getCaretStart(txtbox);
    var lB = txtbox.value.length;
    txtbox.value = gcNum(txtbox.value, ",", ".");
    var lE = txtbox.value.length;
    var newindex = index + (lE-lB);
    if(newindex<0)
        newindex = 0;
    setCaret(txtbox, newindex);
}
function gcFormatStr(str) {
    return gcNum(str, ",", ".");
}

 /* -------- Chuyển định dạng  ----------*/
function gcRev( str, t1)
{
    if (str == null)
        return str;
    if (trim(str).length < 1)
        return str;
    var idx = str.indexOf( t1 );
    while ( idx > -1 ) {
        str = str.replace( t1, "" );
        idx = str.indexOf( t1 );
    }
    return str;
}
//-----------------------------------------------
function ltrim(str) { 
	for(var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++);
	return str.substring(k, str.length);
}
function rtrim(str) {
	for(var j=str.length-1; j>=0 && isWhitespace(str.charAt(j)) ; j--) ;
	return str.substring(0,j+1);
}
function rCutChar(str) {
	for(var j=str.length-1; j>=0 && !isNumber(str.charAt(j)) ; j--) ;
	return str.substring(0,j+1);
}

function trim(str) {
	return ltrim(rtrim(str));
}
function isWhitespace(charToCheck) {
	var whitespaceChars = " \t\n\r\f";
	return (whitespaceChars.indexOf(charToCheck) != -1);
}
function isNumber(charToCheck) {
	var NumberChars = "0123456789.";
	return (NumberChars.indexOf(charToCheck) != -1);
}
//-----------------------------------------------


 /* -------- Chuyển định dạng ngay Viet -> Anh  ----------*/
 function gcDateCUnchange( str )
 {
    return str;
}
function d2e(s) {
    if (s == null )
        return "";
    if (s.length < 4)
        return "";
    var p = s.split(' ');
    var aS = p[0].split('/');
    if (aS.length != 3)
        return "";
    var str = aS[1] + '/' + aS[0] + '/' + aS[2];
    /*testing */
    var month = parseInt(aS[1], 10);
    var day = parseInt(aS[0], 10);
    var year = parseInt(aS[2], 10);
    if (month < 1 || month > 12)        str = "";
    if (day < 1 || day > 31)            str = "";
    if (year < 1900 || year > 2999)     str = "";
    if (str != "")
        if (p.length > 1)
            str = str + ' ' + p[1];
    return str;
}

function gcDateC( str )
{
    if(str==null)
        return str;
    if(str.length<4)
        return "";
    var partition = str.split( ' ' );
    var arrString = partition[0].split( '/' );
    
    if(arrString.length!=3)
        return "";
    var str = arrString[1]+'/'+arrString[0]+'/'+arrString[2];
    /*testing */
    var month = parseInt(arrString[1], 10);
    var day = parseInt(arrString[0], 10);
    var year = parseInt(arrString[2], 10);
    if(month<1 || month>12)
        str = "";
    if(day<1 || day>31)
        str = "";
    if(year<1900 || year>2999)
        str = "";
    if(str!="")
        if(partition.length>1)
            str = str +' '+ partition[1];
    return str;
}

/*    Caret Functions     */

// Get the end position of the caret in the object. Note that the obj needs to be in focus first
function getCaretEnd(obj){
	if(typeof selectionEnd != "undefined"){
		return selectionEnd;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=createTextRange();
		}
		Lp.setEndPoint("EndToEnd",M);
		var rb=Lp.text.length;
		if(rb>value.length){
			return -1;
		}
		return rb;
	}
}
// Get the start position of the caret in the object
function getCaretStart(obj){
	if(typeof selectionStart != "undefined"){
		return selectionStart;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=createTextRange();
		}
		Lp.setEndPoint("EndToStart",M);
		var rb=Lp.text.length;
		if(rb>value.length){
			return -1;
		}
		return rb;
	}
}
// sets the caret position to l in the object
function setCaret(obj,l){
	focus();
	if (setSelectionRange){
		setSelectionRange(l,l);
	}else if(createTextRange){
		m = createTextRange();		
		m.moveStart('character',l);
		m.collapse();
		m.select();
	}
}
// sets the caret selection from s to e in the object
function setSelection(obj,s,e){
	focus();
	if (setSelectionRange){
		setSelectionRange(s,e);
	}else if(createTextRange){
		m = createTextRange();		
		m.moveStart('character',s);
		m.moveEnd('character',e);
		m.select();
	}
}

 
  function focusOn(sender, args)
 {
       var textBox = sender;
       textBox.focus();
 }


 String.prototype.ra = function (s,d) {
     var t = this;
     var i = t.indexOf(s);
     while (i > -1) {
         t = t.replace(s, d);
         i = t.indexOf(s);
     }
     return t;
 };
 String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};
String.prototype.f2 = function () {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\|' + i + '\\|', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};
var modalcenter = (function () {
    var 
				method = {},
				$overlay,
				$modal,
				$content,
				$close;

    // Center the modal in the viewport
    method.center = function () {
        var top, left;

        top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
        //top = 0;
        left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

        $modal.css({
            top: top + $(window).scrollTop(),
            left: left + $(window).scrollLeft()
        });
    };

    // Open the modal
    method.open = function (settings) {
        $content.empty().append(settings.content);

        $modal.css({
            width: settings.width || 'auto',
            height: settings.height || 'auto'
        });

        method.center();
        $(window).bind('resize.modal', method.center);
        $modal.show();
        $overlay.show();
    };
    // Get element content
    method.$content = function () {
        return $content;
    }
    // Close the modal
    method.close = function () {
        $modal.hide();
        $overlay.hide();
        $content.empty();
        $(window).unbind('resize.modal');
    };

    // Generate the HTML and add it to the document
    $overlay = $('<div id="top_overlay"></div>');
    $modal = $('<div id="top_modal"></div>');
    $content = $('<div id="top_content"></div>');
    $close = $('<a id="close" href="#">close</a>');

    $modal.hide();
    $overlay.hide();
    $modal.append($content, $close);

    $(document).ready(function () {
        $('body').append($overlay, $modal);
    });

    $close.click(function (e) {
        e.preventDefault();
        method.close();
    });

    return method;
} ());

var modal = (function () {
    var 
				method = {},
				$overlay,
				$modal,
				$content,
				$close;

    // Center the modal in the viewport
    method.center = function () {
        var top, left;

        top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
        top = 0;
        left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

        $modal.css({
            top: top + $(window).scrollTop(),
            left: left + $(window).scrollLeft()
        });
    };

    // Open the modal
    method.open = function (settings) {
        $content.empty().append(settings.content);

        $modal.css({
            width: settings.width || 'auto',
            height: settings.height || 'auto'
        });

        method.center();
        $(window).bind('resize.modal', method.center);
        $modal.show();
        $overlay.show();
    };

    // Close the modal
    method.close = function () {
        $modal.hide();
        $overlay.hide();
        $content.empty();
        $(window).unbind('resize.modal');
    };

    // Get element content
    method.$content = function () {
        return $content;
    }
    // Generate the HTML and add it to the document
    $overlay = $('<div id="top_overlay"></div>');
    $modal = $('<div id="top_modal"></div>');
    $content = $('<div id="top_content"></div>');
    $close = $('<a id="close" href="#">close</a>');

    $modal.hide();
    $overlay.hide();
    $modal.append($content, $close);

    $(document).ready(function () {
        $('body').append($overlay, $modal);
    });

    $close.click(function (e) {
        e.preventDefault();
        method.close();
    });

    return method;
} ());
function showError(str) {

    var $divall = $("<div></div>");
    var $div = $("<div class='alert alert-danger'>" + str + "</div>");
    var $btnActive = $("<input type='button'  value='Quay về' id='gc_btnActiveForm' onclick='modal.close()' class='btn btn-info btn-sm' />");
    $divall.append($div);
    $divall.append($btnActive);
    modal.open({ content: $divall.html() });
    return;
}
function showErrorCenter(str) {

    var $divall = $("<div></div>");
    var $div = $("<div class='alert alert-danger'>" + str + "</div>");
    var $btnActive = $("<input type='button'  value='Quay về' id='gc_btnActiveForm' onclick='modalcenter.close()' class='btn btn-info btn-sm' />");
    $divall.append($div);
    $divall.append($btnActive);
    modalcenter.open({ content: $divall.html() });
    return;
}
/*------------------------ change format ---------------------------------- */
/* t1=[,] t2=[.] sử dụng theo số tiếng anh gcNum(str,",",".")*/
function gcNum(str, t1, t2) {
    if (str == null)
        return str;
    if (trim(str).length < 1)
        return str;

    var tmpValue = str;
    var arrString = new Array(Math.round(tmpValue.length / 3) + 2);

    tmpValue = tmpValue.replace(t1, "");

    /* -------- Xử lý các dấu chấm   ----------*/
    var idx = tmpValue.indexOf(t1);
    while (idx > -1) {
        tmpValue = tmpValue.replace(t1, "");
        idx = tmpValue.indexOf(t1);
    }
    /*-----------------------------------------*/

    var idxMinus = tmpValue.indexOf("-");
    if (idxMinus == 0)
        tmpValue = tmpValue.substring(idxMinus + 1, tmpValue.length);
    else if (idxMinus > 0)
        tmpValue = tmpValue.substring(0, idxMinus);
    /*/-----------------remove all dấu [-]-------------------*/
    /* -------- Xử lý các dấu chấm   ----------*/
    var idxM = tmpValue.indexOf("-");
    while (idxM > -1) {
        tmpValue = tmpValue.replace("-", "");
        idxM = tmpValue.indexOf("-");
    }
    /*----------------------------------------*/
    var k = 0;
    var i = 0;
    idx = tmpValue.indexOf(t2);
    var strTail = "";
    if (idx >= 0) {
        var strHead = tmpValue.substring(0, idx);

        var lH = strHead.length;
        if (lH > 3) {
            for (k = lH - 3; k > 0; k = k - 3) {
                arrString[i] = strHead.substring(k, k + 3);
                i++;
            }
            /* ------------------------------------ */
            if (k <= 0) {
                arrString[i] = strHead.substring(0, k + 3);
                i++;
            }
        } /* end lH */
        else {
            arrString[i] = strHead.substring(0, strHead.length);
            i++;
        }

        var strTail = tmpValue.substring(idx + 1, tmpValue.length);
        /* remove all comma (,) */
        var idx2 = strTail.indexOf(t2);
        while (idx2 > -1) {
            strTail = strTail.replace(t2, "");
            idx2 = strTail.indexOf(t2);
        }
    }
    else {
        var strHead = tmpValue;
        var lH = strHead.length;
        if (lH > 3) {
            for (k = lH - 3; k > 0; k = k - 3) {
                arrString[i] = strHead.substring(k, k + 3);
                i++;
            }
            /* ------------------------------------ */
            if (k <= 0) {
                arrString[i] = strHead.substring(0, k + 3);
                i++;
            }
        } /* end lH */
        else {
            arrString[i] = strHead.substring(0, strHead.length);
            i++;
        }
    }

    var j = 0; tmpValue = "";
    if (idxMinus >= 0)
        tmpValue += "-";

    /* concat Head */
    for (j = i - 1; j > 0; j--) {
        tmpValue += arrString[j];
        if (j > 0)
            tmpValue += t1;
    }
    if (i > 0)
        tmpValue += arrString[0];

    /* concar Tail */
    if (idx >= 0) {
        tmpValue += t2;
        tmpValue += strTail;
    }
    return tmpValue;
}