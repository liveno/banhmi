﻿<%@ Page Language="C#" MasterPageFile="../UserControl/Share/Site.Master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Default" %>

<%@ Register Src="~/UserControl/PagerTop.ascx" TagName="PagerTop" TagPrefix="gc_pagertop" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Style" runat="server">
    <style>
        .color-palette
        {
            height: 35px;
            line-height: 35px;
            text-align: center;
        }
        .color-palette-set
        {
            margin-bottom: 15px;
        }
        .color-palette span
        {
            display: none;
            font-size: 12px;
        }
        .color-palette:hover span
        {
            display: block;
        }
        .color-palette-box h4
        {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Quản lý bán hàng
                <%--<small>Control panel</small>--%>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Quản lý đặt hàng</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Main row -->
            <div class="row" id="FilterReceiptDay">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-body form-horizontal">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">
                                            Từ ngày</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="datepicker" id="NgayBatDauIdNew" autocomplete="off" style="background: url(&quot;../Usercontrol/images/date7.png&quot;) no-repeat scroll right 5px center rgb(255, 255, 255);
                                                padding-right: 20px; color: rgb(136, 136, 136);" class="form-control valid">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">
                                            Đến ngày</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="datepicker" id="NgayKetThucIdNew" autocomplete="off" style="background: url(&quot;../Usercontrol/images/date7.png&quot;) no-repeat scroll right 5px center rgb(255, 255, 255);
                                                padding-right: 20px; color: rgb(136, 136, 136);" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">
                                            Chi nhánh</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control gc-style-form-combo" name="combo" id="chiNhanhIdi"
                                                placeholder="Chọn danh sách..." autocomplete="off">
                                            <input type="text" style="display: none;" id="chiNhanhIdh" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">
                                            Ca</label>
                                        <div class="col-sm-8">
                                            <select id="listCa" class="form-control">
                                                <option value="1">Tất cả</option>
                                                <option value="2" selected="selected">Ca 1</option>
                                                <option value="3">Ca 2</option>
                                            </select>
                                            <input type="hidden" autocomplete="off" id="NoiDungTuKhoa" class="input-large form-control" />
                                            <input type="hidden" autocomplete="off" id="NhanVien" class="input-large form-control" />
                                            <input type="hidden" value="Kiểm kho new" id="DSChiTietTrongNgay" class="btn btn-success btn-sm" />
                                            <input type="hidden" value="Kiểm kho new" id="DSTrongNgay" class="btn btn-success btn-sm" />
                                            <input type="hidden" value="Kiểm kho new" id="DSTongTrongNgay" class="btn btn-success btn-sm" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">
                                            Loại báo cáo</label>
                                        <div class="col-sm-8">
                                            <select id="typeID" class="selectpicker show-tick form-control" data-live-search="true"
                                                name='master'>
                                                <%if (Session["gcRightGroup"] + "" == "1")
                                                  {%>
                                                <option id="Option3" value="DSTrongNgay">1. DS trong ngày</option>
                                                <option id="Option2" value="DSChiTietTrongNgay">2. DS chi tiết trong ngày</option>
                                                <option id="Option5" value="DSTongTrongNgay">3. DS tổng trong ngày</option>
                                                <%}
                                                  else
                                                  { %>
                                                <option id="Option4" value="DSTongTrongNgay">1. DS tổng trong ngày</option>
                                                <option id="Option1" value="DSChiTietTrongNgay">2. DS chi tiết trong ngày</option>
                                                <%} %>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="button" value="In báo cáo" id="InBillBtn" class="btn btn-info pull-right" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="rowLapPhieu">
                <div class="col-md-8">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" style="font-size: 17px;
                                font-weight: 600; font-weight: 600; font-family: Arial"><i>BÁNH MÌ</i></a></li>
                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" style="font-size: 17px;
                                font-weight: 600; font-weight: 600"><i>BÁNH NGỌT</i></a></li>
                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" style="font-size: 17px;
                                font-weight: 600; font-weight: 600"><i>BÁNH BAO</i></a></li>
                            <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false" style="font-size: 17px;
                                font-weight: 600; font-weight: 600"><i>NƯỚC NGỌT</i></a></li>
                        </ul>
                        <div class="tab-content" id="tabContentLeft">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <div class="row">
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_4">
                                <div class="row">
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_5">
                                <div class="row">
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                    <!-- Custom Tabs (Pulled to the right) -->
                    <div class="nav-tabs-custom">
                        <div class="box">
                            <div class="box-header with-border">
                                <button type="button" id="printBill" class="fa-print btn btn-primary large" style="font-size: 200%;">
                                    <i class="fa fa-print"></i>
                                </button>
                                <span id="tongTien" class="btn btn-danger" style="margin-left: 20px; font-size: 200%;">
                                    0 VND</span>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12" id="popupPhieuThu">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row (main row) -->
                <!-- Main row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script src="../UserControl/Plugin/hightchart/highcharts.js" type="text/javascript"></script>
    <script src="../UserControl/Plugin/hightchart/exporting.js" type="text/javascript"></script>
    <script src="banHang.js" type="text/javascript"></script>
    <script src="init.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).load(function () {
            cur_gb_Account = '<%= Session["gcUserName"] %>'; // có thời gian mã hóa tên của user để che lại
            cur_gb_GroupRight = '<%= Session["gcRightGroup"] %>';
            cur_gb_BranchId = '<%= Session["gcBranchId"] %>';
            cur_gb_AccountId = '<%= Session["gcAccountId"] %>';
            cur_gb_MaCanBoId = '<%= Session["gcMaCanBoId"] %>';
            cur_gb_CtyId = '<%= Session["gcCtyId"] %>';
            cur_gb_DepartmentId = '<%= Session["gcDepartmentId"] %>';
            cur_gb_Today = '<%= Session["curday"] %>';
            cur_gb_ElementId = '<%= Session["curday"] %>';
            cur_gb_ChucVuId = '<%= Session["gcChucVuId"] %>';
            cur_gb_KhoHangId = '<%= Session["gcKhoId"] %>';
            cur_gb_cur_ListDataShow = '';

            $("#gc_gobal_PagerTopID").hide();
            //    $('.selectpicker').selectpicker({
            //        'selectedText': 'cat'
            //    });
            setClassAll('FilterReceiptDay');
            $("#NgayBatDauIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            $("#PhieuThu").click();
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LeftSideBar" runat="server">
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../UserControl/Plugin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle"
                        alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        Alexander Pierce</p>
                    <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
                </div>
            </div>
            <!-- search form -->
            <div class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..." />
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active"><a href="/QLBanHangNew/Default.aspx#action=taoPhieu" id="PhieuThu">
                    <i class="fa fa-edit"></i><span>Phiếu thu</span></a> </li>
                <li class=""><a href="/QLBanHangNew/Default.aspx#action=baoCao" id="BaoCao"><i class="fa fa-table">
                </i><span>Báo cáo</span></a> </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
</asp:Content>
