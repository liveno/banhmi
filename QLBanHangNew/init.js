﻿

function fun_BreakControlEnter(id) {
    return false;
};
function pAsg() {
};
function FocusCmbText(strId, e) {
};
function pGe_o(sr, s) {
    var si = "";
    var allow = false;
    if (s.id == "phieuDetail") {
        si += ",d: {width: '1%',sorting: false, type: 'img',title: '', display: function (data) { var $img=$(\"<button class='btn-sm' type='button' title='Xóa'></button>\"); $img.append(\"<i class='fa fa-fw fa-trash-o'></i>\"); $img.click(function(){ del(data.record,'" + s._e[0] + "'," + s.id + "," + s._e.length + ")}); return $img; } } ";
        allow = true;
    }
    return { allow: allow, sr: si }; 
}

/************************************************************************
* Xử lý hiển thị nội dung                                               *
*************************************************************************/
function view($that, $container, o, on, title) {
    var parentWidth = $container.parent().innerWidth();
    //var $title = $('<h3 style="font-size: 18px; color: rgb(58, 135, 173);"/>').text($that.find('a').text());
    if (title == null || title == "") $('#gc_gbHTitle').text($that.find('a').text());
    else $('#gc_gbHTitle').text(title);
    $container.prepend($(on)).css({ "width": parentWidth + "px" }).show();
    var $listButton = $container.find('.inputBtnBottomV2');
    $listButton.find('.btn.btn-warning.btn-sm').hide();
    var $inputFirst = $container.find('div.input').find('input:first'); //.not('#fo28605i');
    //    $inputFirst.each(function () {
    //        if ($that.attr('id') == "fo28605i") {
    //            $that.css({ 'margin-right': '5px' });
    //        }
    //        else {
    //            $that.css({ 'margin-right': '35px' });
    //        }
    //    });
};

/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
* Các hàm ngắt kernel.js                                                *
_______________________________________________________________________*/
/************************************************************************
* Xử lý ngắt để chỉnh data trước khi gửi request lên server             *
*************************************************************************/
function pCDT(s, obj) {
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 2) < 10 ? ('0' + (s._e.length - 2)) : (s._e.length - 2))] = cur_gb_AccountId;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 5) < 10 ? ('0' + (s._e.length - 5)) : (s._e.length - 5))] = cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account;
    else if (obj.type == 'u') obj.d['f' + ((s._e.length - 6) < 10 ? ('0' + (s._e.length - 6)) : (s._e.length - 6))] = "isnull($x, '') + N'~" + (new Date()).format('dd/MM/yyyy HH:mm:ss') + " | " + cur_gb_CtyId + '-' + cur_gb_BranchId + '-' + cur_gb_DepartmentId + " | " + cur_gb_Account + "'";
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 7) < 10 ? ('0' + (s._e.length - 7)) : (s._e.length - 7))] = cur_gb_Account;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 8) < 10 ? ('0' + (s._e.length - 8)) : (s._e.length - 8))] = 0;
    if (obj.type == 'i') obj.d['f' + ((s._e.length - 13) < 10 ? ('0' + (s._e.length - 13)) : (s._e.length - 13))] = dateToString(new Date(), 'iso');
    if (obj.type == 'i') if (obj.type == 'i') obj.d['f' + ((s._e.length - 15) < 10 ? ('0' + (s._e.length - 15)) : (s._e.length - 15))] = cur_gb_AccountId;

    // Chỉnh sửa dữ liệu
    if (s.id == "phieuDetail") {
        obj.a = "AddOrUpdateReceiptDetail";
    }
};
function p_us_o(s, t, r, obj)
{
    if(s.id == "phieuDetail"){
        modal.close();
    }
}
/************************************************************************
* Xử lý khi jtable load xong dữ liệu                                    *
*************************************************************************/
function pCb() {
    //modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    if ($('#phieuDetail').length > 0) {
        request({
            a: 'GetProcedure',
            c: [" select isnull(TongTien, 0) as TongTien from gcGobal_INCOM_Receipt where id = " + cur_gb_ReceitpID]
        }, { success: function (o, ob, r) {
                //modal.close();
                if (r.Result == "OK") {
                    $("#tongTien").html(c(r.Records[0][0]) + " VND"); 
                } 
            } 
        });
    }
};
function pRm(s, obj) {
    if (s.id == "phieuDetail") {
        obj.a = "DeleteReceiptDetail";
        obj.c.PhieuThuTienMatId = cur_gb_ReceitpID;
    }
}
/************************************************************************
* Xử lý ngắt chỉnh display jtable                                       *
*************************************************************************/
function pCW(v, _s, ll, i, ii, ic, f0) {
    // Menu Student
    if (_s.id == 'studentList') {
        if (_s._e[ic + 1] == 'Name') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { $sDetail = $('<a style=\"background-color:' + data.record.f59 + ';color: black; font-size: 16px\" data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPopupStudentDetail(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        } else if (_s._e[ic + 1] == 'isPrgCreateDate') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',width: '3%', display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        } else if (_s._e[ic + 1] == 'Birthday') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy') }  } ").f2(f0, ll, ii[1]);
        }
    }
    // Menu chăm sóc 
    else if (_s.id == 'contact') {
        if (_s._e[ic + 1] == 'isPrgCreateDate') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|',display: function (data) { return (data.record.f|2| ==null)?'':(_la == 0)?_parseDate(data.record.f|2|+'').format('dd/MM/yyyy HH:mm:ss'):_parseDate(data.record.f|2|+'').format('MM/dd/yyyy HH:mm:ss') }  } ").f2(f0, ll, ii[1]);
        }
    }
    // Menu phiếu thu 
    else if (_s.id == 'pThu') {
        if (_s._e[ic + 1] == 'isPrgOrdered') {
            v = new String(",f|0|: {" + ((ii[2] == '0' || ii[2] == '2') ? 'list: false,' : '') + "title: '|1|', width: '17%', display: function (data) { $sDetail = $('<a data-id=\"' + data.record.f00 + '\">' + ((data.record.f|2| ==null)?'':data.record.f|2|+'') + '</a>'); $sDetail.click(function(ev){showPhieuThu(ev, data.record.f00, this)}); return $sDetail }  } ").f2(f0, ll, ii[1]);
        }
    }
    return v;
};
/************************************************************************
* Xử lý khi click chọn item combobox                                    *
*************************************************************************/
function postonItemClick(itemLI, tBox, index) {
    if (tBox == "flapPhieu05i") {
        $("#flapPhieu04i").val(cur_gb_DataDivShow[index][2]);
    } else if (tBox == "fphieuDetail02i") {
        if (cur_gb_DataDivShow[index][3] != null) {
            $("#fphieuDetail03i").val(1);
            $("#fphieuDetail06i").val(c(cur_gb_DataDivShow[index][3]));
            $("#fphieuDetail07i").val(c(cur_gb_DataDivShow[index][3]));
        }
        if (cur_gb_DataDivShow[index][4] != null) $("#fphieuDetail05i").val(c(cur_gb_DataDivShow[index][4]));
        if (cur_gb_DataDivShow[index][5] != null) $("#fphieuDetail05h").val(c(cur_gb_DataDivShow[index][5]));
    } else if (tBox == "hangHoaIdi" && $("#gc_gobal_ListDataID").css("display") != "none") {
        modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
        var obj = {
            a: 'pGetgcGobal_STOCK_gcProductList',
            c: '', d: '', type: 'p', cl: '*', si: 1, mr: 20, se: ' Id desc',
            f: "Id = " + cur_gb_DataDivShow[index][1] + " and ((isPrgbUserDeleted IS NULL) OR (isPrgbUserDeleted<1))"
        };
        Kernel.P(obj, function (result) { _gbc.it(result, $('#gc_gobal_ListDataID'), listProduct, obj); });
    }
    else if (tBox == "khoIdi") {
        if (cur_gb_DataDivShow[index][3] != null) {
            $("#khoIdc").val(cur_gb_DataDivShow[index][3]);
        }
    }
};
/************************************************************************
* Xử lý ngặt chỉnh obj trước khi load jtable                            *
*************************************************************************/
function pJtablCustomizeObj(obj, s) {

}
/************************************************************************
* Xử lý gán value cho 1 số input sau khi insert, update                 *
*************************************************************************/
function pAssId(s) {
    if (s.id == 'phieuDetail') {
        $("#fphieuDetail01h").val(cur_gb_ReceitpID);
        $("#fphieuDetail02h").val(cur_gb_KhoHangId);
    } else if (s.id == 'addContract') {
        $('#faddContract11i').val(0);
        $('#faddContract12i').val(0);
        $('#faddContract13i').val(0);
        $('#faddContract14i').val(0);
        $('#faddContract23i').val(0);
        $('#faddContract03i').val((new XDate()).toString("dd/MM/yyyy HH:mm:ss"));
    } else if (s.id == 'contact') {
        $('#fcontact01h').val(cur_gb_ReceitpID);
    } else if (s.id == 'dvps') {
        $('#fdvps01h').val(cur_gb_ReceitpID);
        $('#fdvps04i').val(0);
        $('#fdvps05i').val(0);
        $('#fdvps06i').val(0);
    } else if (s.id == 'pThu') {
        $('#fpThu01h').val(cur_gb_ReceitpID);
        $('#fpThu06i').val(0);
        $('#fpThu05i').val((new XDate()).toString("dd/MM/yyyy HH:mm:ss"));
    }
};
/************************************************************************
* Xử lý gửi request lên server lấy data và show report                  *
*************************************************************************/
function showReport(obj, typeReport, fNameClose, fNamePrint, url, fNameXacNhan, opt) {
    modalcenter.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $.ajax({
        type: "POST",
        url: url,
        data: { typeReport: typeReport, obj: obj },
        success: function (msg) {
            var $divall = $("<div></div>");
            var $div = $("<div class='input'></div>");
            var $btnActive = $("<input type='button'  value='Đóng' id='gc_btnActiveForm' onclick='" + fNameClose + "()' class='btn btn-info btn-sm' />");
            var $btnPrint = $("<input type='button'  value='In' id='gc_btnPrintForm' onclick='" + fNamePrint + "()'  class='btn btn-info btn-sm' />");
            $div.append($btnActive);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnPrint);
            if (fNameXacNhan != null) {
                var $btnXN = $("<input type='button'  value='Xác nhận' id='gc_btnPrintForm1' onclick='" + fNameXacNhan + "()'  class='btn btn-info btn-sm' />");
                $div.append($("<label>&nbsp;</label>"));
                $div.append($btnXN);
            }
            var $divcontent = $("<div id='gc_DivReceiptContent'></div>");
            $divcontent.html(msg);
            $divall.append($div);
            $divall.append($divcontent);
            modalcenter.close();
            modalcenter.open({ content: $divall.html() });
            if (opt != null && typeof opt.success == "function") {
                opt.success(obj, opt, modal.$content());
            }
        },
        error: function (xhr, ErrorText, thrownError) {
            $("#gc_gobal_ListDataID").text("Error" + xhr.status + xhr.responseText);
        }
    });
};
/************************************************************************
* Xử lý khi nhấn nút in trong report                                    *
*************************************************************************/
function PrintReport() {
    GobalPrintNew('gc_DivReceiptContent');
    modal.close();
};
/************************************************************************
* Xử lý gửi request lên server và show thông tin report                 *
*************************************************************************/
function reportRequest(obj, opt) {
    modal.open({ content: "<div class='loaderleft'><span style='margin-left:30px;'> Đang tải dữ liệu...<span></div>" });
    $.ajax({
        type: "POST",
        url: '../zgcReport/Report.ashx',
        crossDomain: true,
        data: JSON.stringify({ obj: obj }),
        success: function (msg) {
            modal.close();
            var $divall = $("<div></div>");
            var $div = $("<div class='input'></div>");
            var $divClear = $("<div style='clear: both; height: 20px'></div>");
            var $btnActive = $("<input type='button'  value='Đóng' id='gc_btnActiveForm' onclick='closeReport()' class='btn btn-info btn-sm' />");
            var $btnExport = $("<input type='button'  value='Xuất' id='gc_btnExportForm' onclick='convertTableToExcel()' class='btn btn-info btn-sm' />");
            var $btnPrint = $("<input type='button'  value='In' id='gc_btnPrintForm' onclick='PrintReport()'  class='btn btn-info btn-sm' />");
            var $btnNewTab = $("<a type='button' target='_blank' href='../QLHocVien/DefaultQLHocVien.aspx'  class='btn btn-info btn-sm' >New tab</a>");
            $div.append($btnActive);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnExport);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnPrint);
            $div.append($("<label>&nbsp;</label>"));
            $div.append($btnNewTab);
            var $divcontent = $("<div id='gc_DivReceiptContent'></div>");
            $divcontent.html(msg);
            $divall.append($div);
            $divall.append($divClear);
            $divall.append($divcontent);
            modal.open({ content: $divall.html() });
            if (opt != null && typeof opt.success == "function") {
                opt.success(obj, opt, modal.$content());
            }

        },
        error: function () {
            alert("Error");
        }
    });
}

/************************************************************************
* Xử lý tắt popup show report                                           *
*************************************************************************/
function closeReport() {
    modal.close();
};
/************************************************************************
* Xử lý điều kiện filter                                                *
*************************************************************************/
function selectOption(elId, filter, table, field) {
    $('#' + elId).click(function (ev) {
        test(elId, ev, filter, table, field);
    });
    $('#' + elId).keyup(function (ev) {
        if ($('#' + elId).val() == '') {
            $('#' + elId.substr(0, elId.length - 1) + 'h').val('');
        }
        if ($('#' + elId).val() == '' && $('#' + elId.substring(0, elId.length - 1) + 'h').val() == "" &&
            typeof window[elId.split("_")[0] + "_selectOption"] == 'function') {
            window[elId.split("_")[0] + "_selectOption"]();
        }
        test(elId, ev, filter, table, field);
    });
};
$(document).bind('cbox_closed', function () {
    gb_strFilterGrid = " ";
    if (cur_gb_ElementId == 'lapPhieu') {
        $("#LapPhieu").click();
    } else if (cur_gb_ElementId == 'daLap') {
        $("#DSPhieuDaLap").click();
    } else if (cur_gb_ElementId == 'chuaDuyet') {
        $("#DSPhieuChuaDuyet").click();
    } else if (cur_gb_ElementId == 'daDuyet') {
        $("#DSPhieuDaDuyet").click();
    } else if (cur_gb_ElementId == 'hoanThanh') {
        $("#DSPhieuDaHoanThanh").click();
    } else if (cur_gb_ElementId == 'choTP') {
        $("#DSPhieuChoTP").click();
    }
});


