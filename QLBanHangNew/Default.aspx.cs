using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using gcLibAdmin;
using zgc0LibAdmin;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;

public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Action"] = "QLBanHangNew";
        gcRptTable tblReport = new gcRptTable();
        this.Session["ReportObject"] = tblReport;
        string username = (this.Session["gcUserName"] == null ? "" : this.Session["gcUserName"].ToString());
        // zgc0CheckUser.Valid(this, username);
        //zgc0Login.CheckGroupRightForPage(this.Request.Url.OriginalString, (string)Session["gcRightGroup"], this);

        string _Id = (Request.QueryString["paramExportFile"] == null) ? "-1" : Request.QueryString["paramExportFile"];
        if (_Id == "ExportToWord")
        {
            ExportToWord();
        }
        if (_Id == "_FormHaveUpload")
        {
            _FormHaveUpload();
        }
        if (!Page.IsPostBack)
        {

        }
    }

    private void _FormHaveUpload()
    {
        throw new NotImplementedException();
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void ExportToWord()
    {

        string attachment = "attachment; filename=BaoCaoXuatNhapKho.xls";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.AddHeader("content-disposition", attachment);
        HttpContext.Current.Response.ContentType = "application/ms-excel; charset=UTF-8";
        StringWriter sw = new StringWriter();
        sw.WriteLine("test");

        HtmlTextWriter htw = new HtmlTextWriter(sw);
        HttpContext.Current.Response.Write(sw.ToString());
        HttpContext.Current.Response.End();
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Vehicle> GetCurrentTime(string name)
    {

        Vehicle vehi = simpleCase();
        List<Vehicle> newL = new List<Vehicle> { vehi };
        return newL;

    }

    [WebMethod]
    public static Vehicle simpleCase()
    {
        Vehicle obj = new Vehicle();
        obj.VehicleID = "KL-9876";
        obj.VehicleType = "Nissan";
        obj.VehicleOwner = "Sanjiva";
        return obj;
    }
    public class Vehicle
    {
        public string VehicleID { get; set; }
        public string VehicleType { get; set; }
        public string VehicleOwner { get; set; }
    }
}
