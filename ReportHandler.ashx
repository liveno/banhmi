﻿<%@ WebHandler Language="C#" Class="ReportHandler" %>

/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
using System.Web.SessionState;
public class ReportHandler : IHttpHandler, IReadOnlySessionState {
    
    public void ProcessRequest (HttpContext context) {
        //context.Response.ContentType = ResponseFormat

        if (context.Request.Form["v1"] == null || context.Request.Form["v1"] == null
            || context.Request.Form["v1"] == null || context.Request.Form["v1"] == null)
        {
            context.Response.Write("Error");
            return;
        }

        string v1 = gcUtility.GetNullString(context.Request.Form["v1"]);
        string v2 = gcUtility.GetNullString(context.Request.Form["v2"]);
        string t1 = gcUtility.GetNullString(context.Request.Form["t1"]);
        string t2 = gcUtility.GetNullString(context.Request.Form["t2"]);
        string code = gcUtility.GetNullString(context.Request.Form["code"]);

        if (gcUtility.GetNullString(context.Session["zgc0Login_OK"])== "true")
        {
            //Session["gcCtyId"] = ctyId;
            //Session["gcBranchId"] = branchId;
            //Session["gcDepartmentId"] = departmentId;
            //Session["gcUserInfo"] = MaCanBoId;
            //Session["gcUserName"] = txtUsername.Value;
            //Session["gcUserNameSec"] = zgc0HelperSecurity.Encript(txtUsername.Value);
            //Session["gcAccountId"] = myData.Rows[0]["Id"].ToString();//Lay ID
            //Session["gcMaCanBoId"] = myData.Rows[0]["MaCanBoId"].ToString();// Lay ID
            //Session["zgc0Login_OK"] = "true";
            //Session["gcRightGroup"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền
            //Session["gczNewGroupRight"] = myData.Rows[0]["MaQuyenId"].ToString();// Lay nhóm quyền

            System.Text.StringBuilder sb = new System.Text.StringBuilder(GetReport(v1, v2, t1, t2, code));
            context.Response.ContentType = "text/plain";
            context.Response.Write(sb.ToString());
            
            
        }
        else
            context.Response.Write("Unknown.");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string GetReport(string v1, string v2, string t1, string t2, string code)
    {
        string headerStr = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0\" style='font-weight:bold;'>BẢNG ĐIỂM HỌC VIÊN</font></b> </br>"
            //+ "<span style='font-size:14px;'>Hợp đồng số :{9} - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Số:&nbsp;&nbsp;{2} Ngày: {1} </span><br/>"
                        + "</td>"
                        + "</tr>"
                        + "<tr>"
                     + "<td width=\"500px\"><span style='font-size:12px;font-weight:bold;'>Lớp học: {0}</br>Môn:{1} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GV:{2}</br></span></td>"
                        + "<td></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;font-weight:bold;'>Phòng: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{3}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lịch học: &nbsp;<b>{4}</b></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"
        string headerStr2 = "<table class=\"rptprintBodyTable\" width=\"800px\" style=\"background:#FFFFFF\">"
                        + "<tr>"
                        + "<td align=\"center\" colspan=\"2\" width=\"400px\"><b><font size=\"4.0\" style='font-weight:bold;'>DANH SÁCH HỌC VIÊN</font></b> </br>"
            //+ "<span style='font-size:14px;'>Hợp đồng số :{9} - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Số:&nbsp;&nbsp;{2} Ngày: {1} </span><br/>"
                        + "</td>"
                        + "</tr>"
                        + "<tr>"
                     + "<td width=\"500px\"><span style='font-size:12px;font-weight:bold;'>Lớp học: {0}</br>Môn:{1} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GV:{2}</br></span></td>"
                        + "<td></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;font-weight:bold;'>Phòng: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{3}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lịch học: &nbsp;<b>{4}</b></td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td colspan=\"2\" style='font-size:12px;'> &nbsp;</td>"
                        + "</tr>"
                    + "</table>";//width=\"670px\"
        string Footer = "<br>Ngày:	.........................................  GV chấm bài: .........................................  Lập bảng:  .........................................  ";
        string id = "77";
        if (v1 == "0" || v1 == "1")
        {
            string filter = "";
            if (v2 != null && v2.Length > 0)
            {
                if (filter.Length < 1)
                    filter = " ClassId = " + v2;
                else
                    filter += " AND (ClassId = " + v2 + ")";
            }
            if(code.Length>0)
            {
                if (filter.Length < 1)
                    filter = " ClassName like  N'" + code +"%'";
                else
                    filter += " AND (ClassName like  N'" + code +"%')";
            }
            if (filter.Length < 1)
                filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            else
                filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);
            if (v1 == "0")
            {
                filter = "";
                if (filter.Length < 1)
                    filter += String.Format("   ( '{0} 0:00 '<=StartDate AND StartDate <= '{0} 23:55 ') ", DateTime.Now.ToShortDateString());
                else
                    filter += String.Format(" AND  ( '{0} 0:00'<=StartDate AND StartDate <= '{0}  23:55 ') ", DateTime.Now.ToShortDateString());
            }

            string strWhere = filter;

            id = "77";
            return BuildAReportHD(strWhere, " order by ClassId,StudentIdHoTen desc ", id) + Footer;
        }
        else if (v1 == "11")
        {
            string filter = "";
            if (filter.Length < 1)
                filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            else
                filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);
            if (v1 == "0")
            {
                filter = "";
                if (filter.Length < 1)
                    filter += String.Format("   ( '{0} 0:00 '<=StartDate AND StartDate <= '{0} 23:55 ') ", DateTime.Now.ToShortDateString());
                else
                    filter += String.Format(" AND  ( '{0} 0:00'<=StartDate AND StartDate <= '{0}  23:55 ') ", DateTime.Now.ToShortDateString());
            }

            string strWhere = filter;

            if (v2 != null && v2.Length > 0)
            {
                if (strWhere.Length < 1)
                    strWhere = " ClassId = " + v2;
                else
                    strWhere += " AND (ClassId = " + v2 + ")";
            }

            id = "77";
            return BuildAReportHD(strWhere, " order by ClassId,StudentIdHoTen desc ", id) + Footer;
        }
        else if (v1 == "8")
        {
            string filter = "";
            if (filter.Length < 1)
                filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            else
                filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);
            if (v1 == "0")
            {
                if (filter.Length < 1)
                    filter += String.Format("   ( '{0} 0:00 '<=StartDate AND StartDate <= '{0} 23:55 ') ", DateTime.Now.ToShortDateString());
                else
                    filter += String.Format(" AND  ( '{0} 0:00'<=StartDate AND StartDate <= '{0}  23:55 ') ", DateTime.Now.ToShortDateString());
            }

            string strWhere = filter;

            id = "777";
            return BuildAReportHDNV(strWhere, " order by NVTuVanIdHoTen, StudentIdHoTen desc ", id) + Footer;
        }
        else if (v1 == "9")
        {
            string filter = "";

            if (code.Length > 0)
            {
                if (filter.Length < 1)
                    filter = " ClassName like  N'" + code + "%'";
                else
                    filter += " AND (ClassName like  N'" + code + "%')";
            }

            
            //if (filter.Length < 1)
            //    filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            //else
            //    filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);
            //if (v1 == "9")
            //{
            //    if (filter.Length < 1)
            //        filter += String.Format("   ( '{0} 0:00 '<=StartDate AND StartDate <= '{0} 23:55 ') ", DateTime.Now.ToShortDateString());
            //    else
            //        filter += String.Format(" AND  ( '{0} 0:00'<=StartDate AND StartDate <= '{0}  23:55 ') ", DateTime.Now.ToShortDateString());
            //}

            string strWhere = filter;

            id = "888";
            return BuildAReportTONGHOP(strWhere, " order by ClassName desc ", id) + Footer;
        }
        else if (v1 == "12")
        {
            string filter = "";
            if (filter.Length < 1)
                filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            else
                filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);
            if (v1 == "9")
            {
                if (filter.Length < 1)
                    filter += String.Format("   ( '{0} 0:00 '<=StartDate AND StartDate <= '{0} 23:55 ') ", DateTime.Now.ToShortDateString());
                else
                    filter += String.Format(" AND  ( '{0} 0:00'<=StartDate AND StartDate <= '{0}  23:55 ') ", DateTime.Now.ToShortDateString());
            }

            string strWhere = filter;

            id = "999";
            return BuildAReportLUONGGV("", " order by Name desc ", id);
        }
        //999
        else if (v1 == "5")
        {
            string filter = "";
            if (filter.Length < 1)
                filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            else
                filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);


            string strWhere = filter;
            id = "79";
            string where = filter;

            string sql = String.Format("select * from gcGOBAL_AUTO_STYLESHEET_CLASS_Information where ClassId={0}", v2);
            DataTable myTable = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());
            if (myTable.Rows.Count > 0)
            {
                
                    string PhongIdName = myTable.Rows[0].IsNull("PhongIdName") ? "___" : myTable.Rows[0]["PhongIdName"].ToString();
                headerStr = String.Format(headerStr, myTable.Rows[0]["ClassName"].ToString(), myTable.Rows[0]["ChuongTrinhIdName"].ToString(),
                     myTable.Rows[0]["TeacherIdHoTen"].ToString(),
                     PhongIdName, myTable.Rows[0]["LichHoc"].ToString());
            }
            return headerStr + BuildAReportHDNew(where, " order by ClassId,StudentIdHoTen desc ", id) + Footer;
        }

        else if (v1 == "2" || v1 == "3")
        {
            id = "78";
            string filter = "";
            //if (filter.Length < 1)
            //    filter += String.Format("   ( '{0} '<=StartDate AND StartDate <= '{1} ') ", t1, t2);
            //else
            //    filter += String.Format(" AND  ( '{0} '<=StartDate AND StartDate <= '{1}') ", t1, t2);


            string strWhere = filter;
            string where = filter;

            if (v2 != null && v2.Length > 0)
            {
                if (where.Length < 1)
                    where = " ClassId = " + v2;
                else
                    where += " AND (ClassId = " + v2 + ")";
            }
            if (v1 == "2")
            {
                where = "";
                if (v2 != null && v2.Length > 0)
                {
                    if (where.Length < 1)
                        where = " ClassId = " + v2;
                    else
                        where += " AND (ClassId = " + v2 + ")";
                }
                string sql3 = String.Format("select * from gcGOBAL_AUTO_STYLESHEET_CLASS_InformationDUKIEN where ClassId={0}", v2);
                DataTable myTable3 = zgc0HelperSecurity.GetDataTableNew(sql3, zgc0GlobalStr.getSqlStr());
                if (myTable3.Rows.Count > 0)
                {
                    string ClassName = myTable3.Rows[0].IsNull("Name") ? "___" : myTable3.Rows[0]["Name"].ToString();
                    string ChuongTrinhIdName = myTable3.Rows[0].IsNull("ChuongTrinhIdName") ? "___" : myTable3.Rows[0]["ChuongTrinhIdName"].ToString();
                    string TeacherIdHoTen = myTable3.Rows[0].IsNull("TeacherIdHoTen") ? "___" : myTable3.Rows[0]["TeacherIdHoTen"].ToString();
                    string LichHoc = myTable3.Rows[0].IsNull("LichHoc") ? "___" : myTable3.Rows[0]["LichHoc"].ToString();
                    string PhongIdName = myTable3.Rows[0].IsNull("PhongIdName") ? "___" : myTable3.Rows[0]["PhongIdName"].ToString();
                    headerStr2 = String.Format(headerStr, ClassName,
                        ChuongTrinhIdName,
                         TeacherIdHoTen,
                         PhongIdName, LichHoc);
                }

                return headerStr2 + BuildAReportDUKIEN(where, " order by ClassId,StudentIdHoTen desc ", id) + Footer;
            }
            string sql = String.Format("select * from gcGOBAL_AUTO_STYLESHEET_CLASS_Information where ClassId={0}", v2);
            DataTable myTable = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());
            if (myTable.Rows.Count > 0)
            {
                string ClassName = myTable.Rows[0].IsNull("ClassName") ? "___" : myTable.Rows[0]["ClassName"].ToString();
                string ChuongTrinhIdName = myTable.Rows[0].IsNull("ChuongTrinhIdName") ? "___" : myTable.Rows[0]["ChuongTrinhIdName"].ToString();
                string TeacherIdHoTen = myTable.Rows[0].IsNull("TeacherIdHoTen") ? "___" : myTable.Rows[0]["TeacherIdHoTen"].ToString();
                string LichHoc = myTable.Rows[0].IsNull("LichHoc") ? "___" : myTable.Rows[0]["LichHoc"].ToString();
                string PhongIdName = myTable.Rows[0].IsNull("PhongIdName") ? "___" : myTable.Rows[0]["PhongIdName"].ToString();
                headerStr2 = String.Format(headerStr, ClassName,
                    ChuongTrinhIdName,
                     TeacherIdHoTen,
                     PhongIdName, LichHoc);
            }
            return headerStr2 + BuildAReportHDNew(where, " order by ClassId,StudentIdHoTen desc ", id) + Footer;
        }

        else if (v1 == "4")
        {
            id = "80";
            string where = "";

            where = " FromDate <= '" + DateTime.Now.ToShortDateString() + "' AND '" + DateTime.Now.ToShortDateString() + "' <= ToDate";
            string sql = String.Format("select * from gcGOBAL_AUTO_STYLESHEET_CLASS_Information where ClassId={0}", v2);

            return BuildAReportHDNew3(where, "  ", id) + Footer;
        }

        return "";

    }

    public string BuildAReportHD(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGOBAL_AUTO_STYLESHEET_CLASS_Information";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 4;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[3];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_STYLESHEET_CLASS_Information", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        tblReport.mArrArgSumary[6] = 1;
        tblReport.mArrArgSumary[7] = 1;
        tblReport.mArrArgSumary[8] = 1;
        tblReport.mArrArgSumary[9] = 1;
        tblReport.mArrArgSumary[10] = 1;
        tblReport.mArrArgSumary[11] = 1;

        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //------------------------------------------------------------
        double sum = 0;
        double dt = 0, gv = 0, dtsauKM = 0;
        string storePT = "";
        for (int j = 0; j < tblReport.mData.Count; j++)
        {
            double KM = 0.0;
            double PT = 0.0;
            string v4 = (string)tblReport.mData[j][4];
            string v5 = (string)tblReport.mData[j][5];
            string v6 = (string)tblReport.mData[j][6];
            string v7 = (string)tblReport.mData[j][7];
            string v8 = (string)tblReport.mData[j][8];
            string v9 = (string)tblReport.mData[j][9];
            string v10 = (string)tblReport.mData[j][10];
            string v11 = (string)tblReport.mData[j][11];
            string v12 = (string)tblReport.mData[j][12];
            //string v4 = (string)tblReport.mData[j][13];

            if (v6 == "") v6 = "0";
            if (v7 == "") v7 = "0";
            if (v8 == "") v8 = "0";
            if (v9 == "") v9 = "0";
            if (v10 == "") v10 = "0";
            //if (v11 == "") v11 = "0";
            //if (v12 == "") v12 = "0";
            double v11tmp = double.Parse(v10) - double.Parse(v7);
            tblReport.mData[j][11] = v11tmp.ToString();
            //double v14 = double.Parse(v6) + double.Parse(v9);
            //tblReport.mData[j][14] = v14.ToString();

            //double v15 = double.Parse(v8) + double.Parse(v10) - double.Parse(v9) + double.Parse(v7);
            //tblReport.mData[j][15] = v15.ToString();

            //double v16 = double.Parse(v11) + double.Parse(v12);
            //tblReport.mData[j][16] = v16.ToString();

            //double v17 = (v13) - (v14) - (v15);
            //tblReport.mData[j][17] = v17.ToString();

            //double dsThuc = double.Parse((string)tt) - KM + PT;
            //tblReport.mData[j][13] = dsThuc.ToString();
        }

        for (int i = 6; i < 12; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            if (i == 6)
                dt = sum;
            if (i == 7)
                gv = sum;
            if (i == 8)
                dtsauKM = sum;
            if (i == 9)
                dtsauKM = sum;
            if (i == 10)
                dtsauKM = sum;
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            sum = 0;
        }

        double tongtienkhuyenmaichokhach = 0;
        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.cssClassShowName = "dhu_rpt_TextCenterBgr";
                Item.strContent = "Lớp " + tblReport.mData[Item.from][1];
                //-------------------------------------------------
                double Tem3 = 0, temp4 = 0;
                for (int j = 6; j < 11; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);

                    }
                    Item.mValue[j] = Tem3.ToString();

                    Tem3 = 0;
                }
                Item.cssClass = "dhu_rpt_BoldRightBgr";
            }
        }

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";

        tblReport.mArrRightStyle[4] = 1;
        tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[1] = 1;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[7] = 1;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";

        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }


    public string BuildAReportLUONGGV(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGOBAL_AUTO_STYLESHEET_CLASS_TEACHER_MONEY";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 4;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[0];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_STYLESHEET_CLASS_TEACHER_MONEY", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);


        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //------------------------------------------------------------

        for (int j = 0; j < tblReport.mData.Count; j++)
        {
            double KM = 0.0;
            double PT = 0.0;
            string v4 = (string)tblReport.mData[j][4];
            string v5 = (string)tblReport.mData[j][5];
            string v6 = (string)tblReport.mData[j][6];
            string v7 = (string)tblReport.mData[j][7];
            string v8 = (string)tblReport.mData[j][8];

            //string v11 = (string)tblReport.mData[j][11];
            //string v12 = (string)tblReport.mData[j][12];
            //string v4 = (string)tblReport.mData[j][13];

            if (v6 == "") v6 = "0";

            if (v8 == "") v8 = "0";


            double dongia1tiethoc = 0.0;
            string LoaiMH = v6;
            double SS = double.Parse(v8);
            if (SS <= 15)
            {
                string sqlSub = String.Format("select * from  gcGobal_LITERAL_THULAOGIAOVIEN where code=N'{0}' AND SS={1}", LoaiMH, 15);
                DataTable tbSub = zgc0HelperSecurity.GetDataTableNew(sqlSub, zgc0GlobalStr.getSqlStr());
                if (tbSub.Rows.Count > 0)
                {
                    dongia1tiethoc = double.Parse(tbSub.Rows[0]["SOTIEN"].ToString());
                }
            }
            else if (SS <= 22)
            {
                string sqlSub = String.Format("select * from  gcGobal_LITERAL_THULAOGIAOVIEN where code=N'{0}' AND SS={1}", LoaiMH, 22);
                DataTable tbSub = zgc0HelperSecurity.GetDataTableNew(sqlSub, zgc0GlobalStr.getSqlStr());
                if (tbSub.Rows.Count > 0)
                {
                    dongia1tiethoc = double.Parse(tbSub.Rows[0]["SOTIEN"].ToString());
                }
            }
            else if (SS > 22 && SS <= 34)
            {
                string sqlSub = String.Format("select * from  gcGobal_LITERAL_THULAOGIAOVIEN where code=N'{0}' AND SS={1}", LoaiMH, 23);
                DataTable tbSub = zgc0HelperSecurity.GetDataTableNew(sqlSub, zgc0GlobalStr.getSqlStr());
                if (tbSub.Rows.Count > 0)
                {
                    dongia1tiethoc = double.Parse(tbSub.Rows[0]["SOTIEN"].ToString());
                }
            }
            else if (SS > 34)
            {
                string sqlSub = String.Format("select * from  gcGobal_LITERAL_THULAOGIAOVIEN where code=N'{0}' AND SS={1}", LoaiMH, 34);
                DataTable tbSub = zgc0HelperSecurity.GetDataTableNew(sqlSub, zgc0GlobalStr.getSqlStr());
                if (tbSub.Rows.Count > 0)
                {
                    dongia1tiethoc = double.Parse(tbSub.Rows[0]["SOTIEN"].ToString());
                }
            }

            double SoTiet = double.Parse(v7);
            //--
            double thulaogiangday = SoTiet * dongia1tiethoc;

            tblReport.mData[j][9] = dongia1tiethoc.ToString();
            double v11tmp = thulaogiangday;
            tblReport.mData[j][10] = v11tmp.ToString();

        }


        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";



        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }


    public string BuildAReportTONGHOP(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGOBAL_AUTO_STYLESHEET_CLASS_Information_SUMHOCPHI";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 4;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[0];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = false;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_STYLESHEET_CLASS_Information_SUMHOCPHI", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);


        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //------------------------------------------------------------


        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";



        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }


    public string BuildAReportHDNV(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGOBAL_AUTO_STYLESHEET_CLASS_Information";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 4;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[3];
        tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;
        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_STYLESHEET_CLASS_Information", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[6] = 1;
        tblReport.mArrArgSumary[7] = 1;
        tblReport.mArrArgSumary[8] = 1;
        tblReport.mArrArgSumary[9] = 1;
        tblReport.mArrArgSumary[10] = 1;
        tblReport.mArrArgSumary[11] = 1;

        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();
        //------------------------------------------------------------
        double sum = 0;
        double dt = 0, gv = 0, dtsauKM = 0;
        string storePT = "";
        for (int j = 0; j < tblReport.mData.Count; j++)
        {
            double KM = 0.0;
            double PT = 0.0;
            string v4 = (string)tblReport.mData[j][4];
            string v5 = (string)tblReport.mData[j][5];
            string v6 = (string)tblReport.mData[j][6];
            string v7 = (string)tblReport.mData[j][7];
            string v8 = (string)tblReport.mData[j][8];
            string v9 = (string)tblReport.mData[j][9];
            string v10 = (string)tblReport.mData[j][10];
            //string v11 = (string)tblReport.mData[j][11];
            //string v12 = (string)tblReport.mData[j][12];
            //string v4 = (string)tblReport.mData[j][13];

            if (v6 == "") v6 = "0";
            if (v7 == "") v7 = "0";
            if (v8 == "") v8 = "0";
            if (v9 == "") v9 = "0";
            if (v10 == "") v10 = "0";
            //if (v11 == "") v11 = "0";
            //if (v12 == "") v12 = "0";
            double v11tmp = double.Parse(v10) - double.Parse(v7);
            //tblReport.mData[j][11] = v11tmp.ToString();
            //double v14 = double.Parse(v6) + double.Parse(v9);
            //tblReport.mData[j][14] = v14.ToString();

            //double v15 = double.Parse(v8) + double.Parse(v10) - double.Parse(v9) + double.Parse(v7);
            //tblReport.mData[j][15] = v15.ToString();

            //double v16 = double.Parse(v11) + double.Parse(v12);
            //tblReport.mData[j][16] = v16.ToString();

            //double v17 = (v13) - (v14) - (v15);
            //tblReport.mData[j][17] = v17.ToString();

            //double dsThuc = double.Parse((string)tt) - KM + PT;
            //tblReport.mData[j][13] = dsThuc.ToString();
        }

        for (int i = 7; i < 11; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value != string.Empty)
                {
                    sum += double.Parse((string)value);
                }
            }
            //if (i == 6)
            //    dt = sum;
            if (i == 7)
                gv = sum;
            if (i == 8)
                dtsauKM = sum;
            if (i == 9)
                dtsauKM = sum;
            if (i == 10)
                dtsauKM = sum;
            tblReport.RowSumGobal.mValue[i] = sum.ToString();
            sum = 0;
        }

        double tongtienkhuyenmaichokhach = 0;
        //Tong theo nhom và định dạng
        if (tblReport.mListItem.Count > 0)
        {
            for (int m = 0; m < tblReport.mListItem.Count; m++)
            {
                gcRptItem Item = tblReport.mListItem[m];
                Item.strShowName = "<b>Cộng </b>";
                Item.cssClassShowName = "dhu_rpt_TextCenterBgr";
                Item.strContent = "Lớp " + tblReport.mData[Item.from][1];
                //-------------------------------------------------
                double Tem3 = 0, temp4 = 0;
                for (int j = 7; j < 11; j++)
                {
                    for (int i = Item.from; i < Item.to; i++)
                    {
                        string a5 = (string)tblReport.mData[i][j];
                        Tem3 += double.Parse((a5.Length < 1) ? "0" : a5);

                    }
                    Item.mValue[j] = Tem3.ToString();

                    Tem3 = 0;
                }
                Item.cssClass = "dhu_rpt_BoldRightBgr";
            }
        }

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";

        tblReport.mArrRightStyle[4] = 1;
        tblReport.mArrRightStyleCss[4] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[7] = 1;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";

        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }

    public string BuildAReportHDNew(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGOBAL_AUTO_STYLESHEET_CLASS_Information";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 0;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[0];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;

        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_STYLESHEET_CLASS_Information", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[6] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[8] = 1;
        //tblReport.mArrArgSumary[9] = 1;
        //tblReport.mArrArgSumary[10] = 1;
        //tblReport.mArrArgSumary[11] = 1;

        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        for (int i = 5; i < tblReport.cCols; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value == "0")
                {
                    tblReport.mData[j][i] = "";
                }
            }
        }
        //------------------------------------------------------------
        double sum = 0;
        double dt = 0, gv = 0, dtsauKM = 0;
        string storePT = "";

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";

        tblReport.mArrRightStyle[1] = 1;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[7] = 1;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";

        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }


    public string BuildAReportDUKIEN(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcRptTable tblReport = new gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "gcGOBAL_AUTO_STYLESHEET_CLASS_InformationDUKIEN";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 0;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[0];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;

        tblReport.BuidlASimpleByDB(tblReport, "gcGOBAL_AUTO_STYLESHEET_CLASS_InformationDUKIEN", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[6] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[8] = 1;
        //tblReport.mArrArgSumary[9] = 1;
        //tblReport.mArrArgSumary[10] = 1;
        //tblReport.mArrArgSumary[11] = 1;


        
        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        for (int i = 5; i < tblReport.cCols; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value == "0")
                {
                    tblReport.mData[j][i] = "";
                }
            }
        }
        //------------------------------------------------------------
        double sum = 0;
        double dt = 0, gv = 0, dtsauKM = 0;
        string storePT = "";

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";

        tblReport.mArrRightStyle[1] = 1;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[7] = 1;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";

        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }
    public string BuildAReportHDNew3(string strWhere, string strOrderBy, string id)
    {
        string table = "";
        //-----------------------------------------------------------
        //Cài đặt báo cáo tự động
        //mMaxLevel số mức tối đa của cây
        gcLibAdmin.gcRptTable tblReport = new gcLibAdmin.gcRptTable();
        tblReport.server = zgc0GlobalStr.getSqlStr();
        tblReport.strMainTable = "zgcl_gcGobal_TRAINNING_COURSE_CLASS04";
        tblReport.mTypeBuild = "isSimple";
        tblReport.bShowSTT = true;
        tblReport.bShowIndexRow = false;
        tblReport.strOrderBy = strOrderBy;
        //tblReport.server = zgc0Helper.getOleStr();
        tblReport.strWhere = strWhere;

        tblReport.mColMergForSubSumRow = 0;
        //add danh sách các vị trí cần group by
        //nếu không cần group by thì bỏ qua các cài đặt dưới đây
        tblReport.mArrPos = new int[0];
        //tblReport.mArrPos[0] = 1;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[1] = 2;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.mArrPos[2] = 3;//1 ở đây là cột 1, cột 0 đã chứa số thứ tự không thì chúng ta sét cột 0 vào
        //tblReport.mStrShowContent.Add("");//Tương ứng với 3 phần tử trên
        //tblReport.bShowGroupBy = true;
        //tblReport.mMaxLevel = 2;

        tblReport.BuidlASimpleByDB(tblReport, "zgcl_gcGobal_TRAINNING_COURSE_CLASS04", "zgcBUILDIN_CONFIG_REPORT_DETAIL", id);
        tblReport.InitTable();
        tblReport.mArrArgSumary = new System.Collections.Generic.List<int>();
        for (int t = 0; t < tblReport.cCols; t++)
            tblReport.mArrArgSumary.Add(0);

        //tblReport.mArrArgSumary[4] = 1;
        //tblReport.mArrArgSumary[5] = 1;
        //tblReport.mArrArgSumary[6] = 1;
        //tblReport.mArrArgSumary[7] = 1;
        //tblReport.mArrArgSumary[8] = 1;
        //tblReport.mArrArgSumary[9] = 1;
        //tblReport.mArrArgSumary[10] = 1;
        //tblReport.mArrArgSumary[11] = 1;

        //-----------------------------------------------------------
        //BuilSampleTree(tblReport.root, tblReport);
        //-----------------------------------------------------------
        tblReport.MakeGobalRowForTesting();
        tblReport.FillData();

        for (int i = 5; i < tblReport.cCols; i++)
        {
            for (int j = 0; j < tblReport.mData.Count; j++)
            {
                string value = (string)tblReport.mData[j][i];
                value.Trim();
                if (value == "0")
                {
                    tblReport.mData[j][i] = "";
                }
            }
        }
        //------------------------------------------------------------
        double sum = 0;
        double dt = 0, gv = 0, dtsauKM = 0;
        string storePT = "";

        tblReport.RowSumGobal.cssClass = "dhu_rpt_TextRight";


        ////Xuất dữ liệu

        for (int t = 0; t < tblReport.cCols; t++)
        {
            tblReport.mArrRightStyle.Add(0);
            tblReport.mArrRightStyleCss.Add("");
        }
        tblReport.RowSumGobal.cssClass = "dhu_rpt_BoldRight";

        tblReport.mArrRightStyle[1] = 1;
        tblReport.mArrRightStyleCss[1] = "dhu_rpt_TextLeft";

        tblReport.mArrRightStyle[7] = 1;
        tblReport.mArrRightStyleCss[7] = "dhu_rpt_TextRight";

        table = tblReport.MakeSimpleTable();
        //dtsauKM -= tongtienkhuyenmaichokhach;
        //table += String.Format("<div style='height:20px; background-color:#dddddd;padding:5px;font-weight:bold;font-size:16px;'> Lãi bán ra: {0} ||| Chiết khấu tính theo lãi: {1}</div>", tblReport.InterFormatDataNumBer((dt - gv).ToString(), 1), tblReport.InterFormatDataNumBer(((dt - gv) * 8.0 / 100.0).ToString(), 1));

        return table;
    }

}