﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<%@ Register Src="~/UserControl/Header.ascx" TagName="Header" TagPrefix="gc_header" %>
<%@ Register Src="~/UserControl/Footer.ascx" TagName="Footer" TagPrefix="gc_footer" %>
<%@ Register Src="~/UserControl/PagerTop.ascx" TagName="PagerTop" TagPrefix="gc_pagertop" %>
<%@ Register Src="~/UserCtrl-BanHang/MainMenu.ascx" TagName="MainMenu" TagPrefix="gc_MainMenu" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anh ngữ AMA Vũng Tàu</title>
    <!-- Le styles -->
    <link href="../UserControl/bootstrap/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../UserControl/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="../UserControl/bootstrap/assets/css/docs.css" rel="stylesheet" />
    <link href="../UserControl/bootstrap/assets/js/google-code-prettify/prettify.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../UserControl/style_home.css" />
    <link rel="stylesheet" href="../UserControl/colorbox-master/css/colorbox.css" />
    <link rel="stylesheet" type="text/css" href="../UserControl/styles-12.css" />
    <link rel="stylesheet" href="../UserControl/datepicker/css/datepicker.css" />
    <link rel="stylesheet" href="../UserControl/jPager/css/custom-pager.css" />
    <link rel="stylesheet" href="../UserControl/jPager/css/jPaginator.css" />
    <link href="../UserControl/bootstrap/assets/css/bootstrap-select.css" rel="stylesheet" />
    <!-- jTable style file -->
    <link href="../jsGobalLib/Scripts/jtable/themes/lightcolor/green/jtable.min.css"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../JSGobalLib/jquery-ui-themes-1.10.3/themes/ui-lightness/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="../JSGobalLib/jquery-ui-themes-1.10.3/themes/ui-lightness/jquery.ui.theme.css" />
    <link href="../UserControl/bootstrap/assets/css/bootstrap-multiselect.css" rel="stylesheet" />
    <style type="text/css">
        .br-product
        {
            width: 12%;
            float: left;
            min-height: 140px;
            margin: 10px; /* padding: 10px 5px; */
            border: 1px solid rgb(206, 206, 206);
            border-radius: 8px;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            overflow: hidden;
        }
        
        .br-clr-org
        {
            background: rgb(254, 255, 223);
        }
        .br-product-img
        {
            float: left;
            padding: 5px;
        }
        .br-product-img p
        {
            font-size: 20px;
            font-weight: bold;
        }
        .br-product-info h1
        {
            padding: 5px;
            font-size: 16px;
            color: black;
            min-height: 110px;
            font-weight: bold;
        }
        .itemProduct:hover
        {
            cursor: pointer;
        }
        .br-product:hover
        {
            border: 1px solid black;
        }
        .hide-important
        {
            display: none !important;
        }
        
        #assign-work .row
        {
            margin-top: 4px;
            margin-bottom: 4px;
        }
        li.active a
        {
            background-color: White !important;
            line-height: 31px !important;
        }
    </style>
    <script src="../JS/dic.js" type="text/javascript"></script>
    <script src="../Common/KernelCell.js" type="text/javascript"></script>
    <script type="text/javascript" src="../jsGobalLib/xdate.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../jsGobalLib/jquery.validate.js"></script>
    <script type="text/javascript" src="../UserControl/colorbox-master/jquery.colorbox.js"></script>
    <script type="text/javascript" src="../UserControl/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="../UserControl/jPager/js/jPaginator-min.js"></script>
    <script type="text/javascript" src="../jsGobalLib/PageInitSize.js"></script>
    <script src='../Common/gc_ShowDivData.js' type='text/javascript'></script>
    <script type="text/javascript" src="../common/gcGlobalForm.js"></script>
    <script type="text/javascript" src="../common/gcGlobal.js"></script>
    <script type="text/javascript" src="../common/Kernel.js"></script>
    <script type="text/javascript" src="../js/o.js"></script>
    <script src="../UserControl/bootstrap/assets/js/bootstrap-select.js"></script>
    <script src="../UserControl/bootstrap/assets/js/bootstap-multiselect.js"></script>
    <!-- Report js -->
    <script src="../Common/report.js"></script>
    <script src="../jsGobalLib/Scripts/jtablesite.js" type="text/javascript"></script>
    <!-- A helper library for JSON serialization -->
    <script type="text/javascript" src="../jsGobalLib/Scripts/jtable/external/json2.js"></script>
    <!-- Core jTable script file -->
    <script type="text/javascript" src="../jsGobalLib/Scripts/jtable/jquery.jtable.js"></script>
    <!-- ASP.NET Web Forms extension for jTable -->
    <script type="text/javascript" src="../jsGobalLib/Scripts/jtable/extensions/jquery.jtable.aspnetpagemethods.js"></script>
    <script src="../Common/report.js"></script>
    <script src="banHang.js"></script>
    <script src="init.js"></script>
    <script type="text/javascript">
        //---------------------------------------------
        $(window).load(function () {
            cur_gb_Account = '<%= Session["gcUserName"] %>'; // có thời gian mã hóa tên của user để che lại
            cur_gb_GroupRight = '<%= Session["gcRightGroup"] %>';
            cur_gb_BranchId = '<%= Session["gcBranchId"] %>';
            cur_gb_AccountId = '<%= Session["gcAccountId"] %>';
            cur_gb_MaCanBoId = '<%= Session["gcMaCanBoId"] %>';
            cur_gb_CtyId = '<%= Session["gcCtyId"] %>';
            cur_gb_DepartmentId = '<%= Session["gcDepartmentId"] %>';
            cur_gb_Today = '<%= Session["curday"] %>';
            cur_gb_KhoHangId = '<%= Session["gcKhoHangId"] %>';
            cur_gb_ElementId = "";
            cur_gb_ItemSelect = [];
            $("#gc_gobal_PagerTopID").hide();
            //click vao cái đầu tiên
            $("#PhieuThu").click();
            $("#banhMi").click();
            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });
            setClassAll('FilterReceiptDay');
            $("#NgayBatDauIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            $("#NgayKetThucIdNew").val(XDate.today().toString("dd/MM/yyyy"));
            //obj.t1 = gcDateC($("#NgayBatDauIdNew").val());
        });
    </script>
</head>
<body>
    <a id='URLDownloadFile' style='display: none;' href=''></a>
    <form action="ClassInfo.aspx?paramExportFile=ExportToWord" method="post" id="downloaddata"
    style="display: none; visibility: hidden">
    <input type="submit" value="download" style="display: none; visibility: hidden" />
    </form>
    <form id="loginForm" runat="server">
    <!-- Webservice Information -->
    <asp:ScriptManager ID='gcSM' runat='server' EnablePageMethods='true' ScriptMode='Release'>
        <Services>
            <asp:ServiceReference Path="~/WSASMXGobal/Kernel.asmx" />
        </Services>
    </asp:ScriptManager>
    <!-- End webservice Information -->
    <div id="container">
        <div class="container">
            <!-- gc Header form : first time is enable-->
            <gc_MainMenu:MainMenu ID='gc_parent_MenuID' runat="server" />
            <div style="clear: both;">
            </div>
            <!-- End gc Header form -->
            <!-- This contains the hidden content for inline calls -->
            <div style='display: none'>
            </div>
            <div id="FilterReceiptDay" class="content" style="padding: 0 10px 0px 10px; display: none;">
                <div class="gc_SearchForm" id="Div2" style="margin-bottom: 10px">
                    <div id="Div3" class="login_sign">
                        <div class="input">
                            <label>
                                Từ Ngày</label>
                            <input type="text" name="datepicker" id="NgayBatDauIdNew" autocomplete="off" style="background: url(&quot;../Usercontrol/images/date7.png&quot;) no-repeat scroll right 5px center rgb(255, 255, 255);
                                padding-right: 20px; color: rgb(136, 136, 136);" class="form-control valid">
                            <label>
                                Tới ngày</label>
                            <input type="text" name="datepicker" id="NgayKetThucIdNew" autocomplete="off" style="background: url(&quot;../Usercontrol/images/date7.png&quot;) no-repeat scroll right 5px center rgb(255, 255, 255);
                                padding-right: 20px; color: rgb(136, 136, 136);" class="form-control">
                            <%--<label>
                                Người bán hàng :</label>
                            <input type="text" class="brwidthinput300 input-large form-control gc-style-form-combo"
                                name="combo" id="nhanVienIdi" placeholder="Chọn danh sách..." autocomplete="off">
                            <input type="text" style="display: none;" id="nhanVienIdh" class="form-control">--%>
                            <label>
                                Nhập vào nội dung tìm kiếm</label>
                            <input type="text" autocomplete="off" id="NoiDungTuKhoa" class="input-large form-control" />
                            <label>
                                Nhân viên</label>
                            <input type="text" autocomplete="off" id="NhanVien" class="input-large form-control" />
                        </div>
                        <%--<div class="input">
                            <label>
                                Chọn hàng hóa:</label>
                            <input type="text" class="brwidthinput300 input-large form-control gc-style-form-combo"
                                name="combo" id="hangHoaIdi" placeholder="Chọn danh sách..." autocomplete="off">
                            <input type="text" style="display: none;" id="hangHoaIdh" class="form-control">
                        </div>--%>
                        <div class="input">
                            <%--<label>
                                Chọn kho</label>
                            <input type="text" class="brwidthinput300 input-large form-control gc-style-form-combo"
                                name="combo" id="khoIdi" placeholder="Chọn danh sách..." autocomplete="off">
                            <input type="text" style="display: none;" id="khoIdh" class="form-control">
                            <input type="text" style="display: none;" id="khoIdc" class="form-control">--%>
                            <label>
                                Chi nhánh</label>
                            <input type="text" class="brwidthinput300 input-large form-control gc-style-form-combo"
                                name="combo" id="chiNhanhIdi" placeholder="Chọn danh sách..." autocomplete="off">
                            <input type="text" style="display: none;" id="chiNhanhIdh" class="form-control">
                            <label>
                                Ca</label>
                            <div class="col-md-1 col-xs-6">
                                <select id="listCa" class=" form-control">
                                    <option value="1">Tất cả</option>
                                    <option value="2" selected="selected">Ca 1</option>
                                    <option value="3">Ca 2</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-12">
                                <select id="typeID" class="selectpicker show-tick form-control" data-live-search="true"
                                    name='master'>
                                    <%if (Session["gcRightGroup"] + "" == "1")
                                      {%>
                                    <option id="Option3" value="DSTrongNgay">1. DS trong ngày</option>
                                    <option id="Option2" value="DSChiTietTrongNgay">2. DS chi tiết trong ngày</option>
                                    <option id="Option5" value="DSTongTrongNgay">3. DS tổng trong ngày</option>
                                    <%}
                                      else
                                      { %>
                                    <option id="Option4" value="DSTongTrongNgay">1. DS tổng trong ngày</option>
                                    <option id="Option1" value="DSChiTietTrongNgay">2. DS chi tiết trong ngày</option>
                                    <%} %>
                                    <%--<option id="Option13" value="LocHoaDon">1. Tìm phiếu</option>
                                <option id="Option1" value="btnNewKiemKho">2. Kiểm kho - new</option>
                                <option id="Option14" value="TinhLaiKho">3. Tính lại kho</option>--%>
                                </select>
                            </div>
                            <input type="button" value="In báo cáo" id="InBillBtn" class="btn btn-primary btn-sm" />
                        </div>
                    </div>
                    <div class="inputBtnBottomV2" style="border-top: none; display: none;">
                        <input type="button" value="Tìm phiếu" id="LocHoaDon" class="btn btn-primary btn-sm" />
                        <input type="button" value="In tạm ứng " id="TamUngTrongNgay" class="btn btn-danger btn-sm" />
                        <input type="button" value="Công nợ" id="GhiNoTrongNgay" class="btn btn-danger btn-sm" />
                        <input type="button" value="In phiếu nhập theo nhà CC" id="InPhieuNhapNCC" class="btn btn-warning btn-sm" />
                        <input type="button" value="In phiếu nhập theo ngày" id="InPhieuNhap" class="btn btn-info btn-sm" />
                        <input type="button" value="In phiếu nhập theo hàng hóa" id="InPhieuNhapProduct"
                            class="btn btn-danger btn-sm" />
                        <input type="button" value="Công nợ hôm nay" id="btnThanhToan" class="btn btn-info btn-sm" />
                    </div>
                    <div class="inputBtnBottomV2" style="border-top: none; display: none;">
                        <input type="button" value="Định lượng hàng hóa" id="DinhLuongMonAn" class="btn btn-primary btn-sm" />
                        <input type="button" value="In danh sách hàng hóa" id="InDSHangHoa" class="btn btn-primary btn-sm" />
                        <input type="button" value="Chi tiết thanh toán" id="tbnChiTietThanhToan" class="btn btn-primary btn-sm" />
                        <%--<input type="button"  value="Kiểm soát hàng hóa nhập" id="InTongHangHoaNhap"  class="btn btn-info btn-sm" />--%>
                        <input type="button" value="In tổng hàng hóa xuất nhập" id="InTongHangHoaXuatNhap"
                            class="btn btn-info btn-sm" />
                        <input type="button" value="Kiểm kho" id="btnKiemKho" class="btn btn-success btn-sm" />
                        <input type="button" value="Chuyển số liệu đầu kỳ" id="btnChuyenSoLieuDK" class="btn btn-success btn-sm" />
                        <input type="button" value="Kiểm kho new" id="btnNewKiemKho" class="btn btn-success btn-sm" />
                        <input type="button" value="Kiểm kho new" id="TinhLaiKho" class="btn btn-success btn-sm" />
                        <input type="button" value="Kiểm kho new" id="DSChiTietTrongNgay" class="btn btn-success btn-sm" />
                        <input type="button" value="Kiểm kho new" id="DSTrongNgay" class="btn btn-success btn-sm" />
                        <input type="button" value="Kiểm kho new" id="DSTongTrongNgay" class="btn btn-success btn-sm" />
                        <%--<input type="button"  value="Kiểm soát hàng hóa" id="KiemSoatHangHoa"  class="btn btn-success btn-sm" />--%>
                    </div>
                    <div class="clear">
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <%--<input type="button" class="btn btn-primary btn-sm" id="InBillBtn" value="In Báo Cáo">

                        <input type="button" class="btn btn-primary btn-sm" id="reloadCV" value="Load Công việc">--%>
            </div>
            <div style="float: left; display: block; width: 100%">
                <div class="content" id="gc_gobal_MainFormID" style="width: 70%; float: left">
                    <div class="alert alert-info alert-info-gc-small" id="gc_gb_MessageInfomation" style="display: none">
                        <strong>Chú ý!</strong> Bạn phải gõ khoảng trắng vào ô text để search.</div>
                    <div id="content">
                        <div class="sidebar-content" style="float: left;">
                        </div>
                    </div>
                    <h2 id="gc_gbHTitle" style="display: none">
                    </h2>
                    <!--tabs-->
                    <gc_pagertop:PagerTop ID="gc_pagertopID" runat="server" style="display: none" />
                    <div class="container br-phanloai" id="ShowSoDoBan">
                        <a class="navbar-brand" href="#">» </a>
                        <ul class="nav nav-tabs" id="myTab">
                            <%--<li class="active"><a id="profileTab" href="#profileTabContent" data-toggle="tab">Ghi
                            phiếu</a></li>
                        <li><a id="todayTab" href="#todayTabContent" data-toggle="tab">Phiếu trong ngày</a></li>--%>
                            <li class="active" style="width: 22%"><a id="banhMi" href="#banhMiTabContent" data-toggle="tab"
                                style="background-color: rgb(0, 238, 108); font-weight: bold; font-size: 120%;
                                padding: 25px 40px 25px 40px;">BÁNH MÌ</a></li>
                            <li style="width: 22%"><a id="banhNgot" href="#banhNgotTabContent" data-toggle="tab"
                                style="background-color: red; font-weight: bold; font-size: 120%; padding: 25px 40px 25px 40px;">
                                BÁNH NGỌT</a></li>
                            <li style="width: 22%"><a id="banhBao" href="#banhBaoTabContent" data-toggle="tab"
                                style="background-color: yellow; font-weight: bold; font-size: 120%; padding: 25px 40px 25px 40px;">
                                BÁNH BAO</a></li>
                            <li style="width: 22%"><a id="nuocNgot" href="#nuocNgotTabContent" data-toggle="tab"
                                style="background-color: rgb(146, 205, 220); font-weight: bold; font-size: 120%;
                                padding: 25px 40px 25px 40px;">NƯỚC NGỌT</a></li>
                        </ul>
                        <div class="tab-content">
                            <%--<div class="tab-pane active" id="profileTabContent">
                        </div>
                        <div class="tab-pane" id="todayTabContent">
                        </div>--%>
                            <div class="tab-pane active" id="banhMiTabContent">
                            </div>
                            <div class="tab-pane" id="banhNgotTabContent">
                            </div>
                            <div class="tab-pane" id="banhBaoTabContent">
                            </div>
                            <div class="tab-pane" id="nuocNgotTabContent">
                            </div>
                        </div>
                    </div>
                    <!--/tabs-->
                    <!-- Pager at the top -->
                    <!-- End pager at the top -->
                    <div style="clear: both;">
                    </div>
                    <!-- Pager at the top -->
                    <!-- End pager at the top -->
                    <div id="gc_gobal_ListDataID">
                    </div>
                    <div style="clear: both;">
                    </div>
                    <!-- Pager at the bottom -->
                    <%-- <gc_pagerbottom:PagerBottom ID="gc_pagerbottomID" runat="server" />--%>
                    <!-- End pager at the bottom -->
                </div>
                <div style="width: 28%; float: left">
                    <input type="button" value="In hóa đơn" id="printBill" class="btn btn-primary" style="font-size: 200%;
                        width: 45%; min-height: 70px" />
                    <span id="tongTien" class="label label-danger" style="margin-left: 20px; font-size: 200%;">
                        0 VND</span>
                    <div id="popupPhieuThu">
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <!-- End gc Header form -->
        <!-- gc Search Filter : first time is enable-->
        <!-- End gc Search Filter -->
        <!-- gc Information : for notify successfull update, insert, error, first time is disable-->
        <div class="content" id="gc_gobal_InformationID">
        </div>
        <!-- End gc Information -->
        <!-- End gc Main Form -->
        <!---content-->
        <!-- End gc Main Form -->
        <div style="clear: both;">
        </div>
        <div id="report" style="display: none">
        </div>
    </div>
    <!--/.container-->
    <!--/ #container-->
    </form>
    <!-- Footer Information -->
    <gc_footer:Footer ID='gc_footer_ID' runat="server" />
    <!-- End footer Information -->
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../jsGobalLib/widgets.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script type="text/javascript" src="../jsGobalLib/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script type="text/javascript" src="../jsGobalLib/canvas-to-blob.min.js"></script>
    <!--
     <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script type="text/javascript" src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
    <script type="text/javascript" src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> 
    -->
    <script type="text/javascript" src="../UserControl/bootstrap/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../UserControl/bootstrap/assets/js/holder.js"></script>
</body>
</html>
