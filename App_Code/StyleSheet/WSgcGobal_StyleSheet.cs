
/*------------------------------------*/
/* Summary description for  WSgcGobal_StyleSheet */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
using gcLibAdmin;
using System.IO;
[WebService(Namespace = "http://www.anhxuan.com.vn/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class WSgcGobal_StyleSheet : System.Web.Services.WebService
{
    private string index;
    private List<object> list;

    public static string unicodeEncode(string str)
    {
        StringBuilder result = new StringBuilder();
        int unicode;

        for (int i = 0; i < str.Length; i++)
        {
            unicode = (int)str[i];
            if (str[i] == '\n')
            {
                result.AppendLine(@"\line");
            }
            else if (str[i] == '\r')
            {
                // ignore '\r'
            }
            else if (str[i] == '\t')
            {
                result.Append(@"\tab ");
            }
            else if (unicode <= 0xff)
            {
                if (unicode == 0x5c || unicode == 0x7b || unicode == 0x7d)
                {
                    result.Append(@"\'" + string.Format("{0:x2}", unicode));
                }
                else if (0x00 <= unicode && unicode < 0x20)
                {
                    result.Append(@"\'" + string.Format("{0:x2}", unicode));
                }
                else if (0x20 <= unicode && unicode < 0x80)
                {
                    result.Append(str[i]);
                }
                else
                { // 0x80 <= unicode <= 0xff
                    result.Append(@"\'" + string.Format("{0:x2}", unicode));
                }
            }
            else if (0xff < unicode && unicode <= 0x8000)
            {
                result.Append(@"\uc1\u" + unicode + "*");
            }
            else if (0x8000 < unicode && unicode <= 0xffff)
            {
                result.Append(@"\uc1\u" + (unicode - 0x10000) + "*");
            }
            else
            {
                result.Append(@"\uc1\u9633*");
            }
        }
        return result.ToString();
    }

    /// <summary>
    /// big5 encoding (preserve this function for failure restoration)
    /// </summary>
    /// <param name="str">string to be encoded</param>
    /// <returns>encoded string</returns>
    public static string big5Encode(string str)
    {
        string result = "";
        Encoding big5 = Encoding.GetEncoding(950);
        Encoding ascii = Encoding.ASCII;
        Byte[] buf = big5.GetBytes(str);
        Byte c;

        for (int i = 0; i < buf.Length; i++)
        {
            c = buf[i];
            if ((0x00 <= c && c < 0x20) || (0x80 <= c && c <= 0xff)
                || c == 0x5c || c == 0x7b || c == 0x7d)
            {
                result += string.Format(@"\'{0:x2}", c);
            }
            else
            {
                result += ascii.GetChars(new byte[] { c })[0];
            }
        }
        return result;
    }

};
//{0}replace
        
