using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using gcLibAdmin;
using zgc0Admin;
using System.IO;
using zgc0LibAdmin;
using System.Net;
/// <summary>
/// Summary description for AutoComplete
/// </summary>
[WebService(Namespace = "http://www.anhxuan.com.vn")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class zgc0GobalService : System.Web.Services.WebService
{
    #region
    string runSQL = "CREATE PROCEDURE [dbo].[gcGOBAL_RunSQL]"
                    + " (@sql ntext) "
                    + " AS "
                    + " BEGIN "
                    + "    BEGIN TRANSACTION "
                    + "    EXEC (@sql) "
                    + "    COMMIT TRANSACTION "
                    + " END ";
    #endregion
    #region
    string dropObject = "CREATE PROCEDURE [dbo].[gcGOBAL_DropObject]"
        + " @objname nvarchar(256) ,"
        + " @objtype int"
        + " AS"
        + " BEGIN"
        + "    BEGIN TRANSACTION"
        + "    DECLARE @sql nvarchar(2040)"
        + "    if @objtype=0"
        + "    begin"
        + "        set @sql = 'if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].['+@objname+']'') and OBJECTPROPERTY(id, N''IsView'') = 1)'+"
        + "                    ' drop view [dbo].['+@objname+']'"
        + "        EXEC (@sql)"
        + "    end"
        + "    else if @objtype=1"
        + "    begin"
        + "        set @sql = 'if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].['+@objname+']'') and OBJECTPROPERTY(id, N''IsTable'') = 1)'+"
        + "                    ' drop table [dbo].['+@objname+']'"
        + "        EXEC (@sql)"
        + "    end"
        + "    else if @objtype=2"
        + "    begin"
        + "        set @sql = 'if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].['+@objname+']'') and OBJECTPROPERTY(id, N''IsProcedure'') = 1)'+"
        + "                    ' drop procedure [dbo].['+@objname+']'"
        + "        EXEC (@sql)"
        + "    end"
        + "    else if @objtype=3"
        + "    begin"
        + "        set @sql = 'if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].['+@objname+']'') and OBJECTPROPERTY(id, N''isFunction'') = 1)'+"
        + "                    ' drop FUNCTION [dbo].['+@objname+']'"
        + "        EXEC (@sql)"
        + "    end"
        + "    COMMIT TRANSACTION"
        + " END";
    string strCreateAccount = "CREATE VIEW zgc0BUILDIN_GetAccountInfo as SELECT     dbo.tbl_TK_TaiKhoan.Id, dbo.tbl_TK_TaiKhoan.Username, dbo.tbl_TK_TaiKhoan.Password, dbo.tbl_TK_TaiKhoan.MaQuyenId,  "
                      + " dbo.tbl_TK_TaiKhoan.MaCanBoId, dbo.tbl_TK_TaiKhoan.CheckNo, dbo.tbl_TK_TaiKhoan.QuyenPC, dbo.tbl_TK_TaiKhoan.TuNgay,  "
                      + " dbo.tbl_TK_TaiKhoan.DenNgay, dbo.{0}.{1}, dbo.{0}.{2}, dbo.{0}.{3},  "
                      + " dbo.tbl_TK_Quyen.DefaultPage, dbo.tbl_TK_Quyen.Ten  as TenQuyen"
                      + " FROM         dbo.tbl_TK_TaiKhoan INNER JOIN "
                      + "                     dbo.{0} ON dbo.tbl_TK_TaiKhoan.MaCanBoId = dbo.{0}.Id INNER JOIN "
                      + "                     dbo.tbl_TK_Quyen ON dbo.tbl_TK_TaiKhoan.MaQuyenId = dbo.tbl_TK_Quyen.Id ";
    string strRowNum = "CREATE PROCEDURE [dbo].[gcGOBAL_Process_GetRowNum] "
	        + "@table nvarchar(1024), "
	        + "@sort	nvarchar(1024), "
	        + "@filter nvarchar(1024), "
	        + "@column nvarchar(1024), "
	        + "@index int, "
	        + "@SL int "
        + " as "
        + " BEGIN "
	    + "    declare @sql	nvarchar(1024) "
	    + "    exec dbo.gcGOBAL_DropObject 'zgcTempViewData', 0 "
	    + "    select @sql = 'create view zgcTempViewData as select ROW_NUMBER() OVER (ORDER BY '+ @sort +') as ROWNUM , ' + @column +  "
        + "    	        + ' from  (select top 100 percent  '+@column+' from ' + @table + ' ' "
        + "        if (( LEN(@filter) > 3)) "
        + "                select @sql = @sql + ' WHERE (' + @filter + ') ' "
        + "         "
	    + "    select @sql = @sql + ' ORDER BY ' + @sort + ' ) a ' "
	    + "    print @sql "
	    + "    exec (@sql) "
        + "	 "
	    + "    SELECT * from zgcTempViewData "
		+ "         WHERE ROWNUM between (@index-1)*@SL + 1 and @index*@SL "
        + " END";
    string logDatabase ="CREATE PROCEDURE [dbo].[gcGOBAL_LogDatabaseOperation] "
	      + "   @username nvarchar(50), "
	      + "   @canBoId int, "
	      + "   @operation nvarchar(50), "
	      + "   @tableName nvarchar(50), "
	      + "   @description nvarchar(256), "
	      + "   @time Datetime "
	      + "   ,@RemoteClientAdd	nvarchar(50) "
	      + "   ,@RemoteClientHost	nvarchar(50) "
	      + "   ,@RemoteClientPort	nvarchar(50) "
        + " AS "
        + " BEGIN "
	    + "     IF (MONTH(@time) = 1) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang1 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ " 	        VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=2) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang2 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=3) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang3 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=4) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang4 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=5) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang5 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=6) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang6 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort) "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=7) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang7 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=8) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang8 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=9) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang9 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=10) "
        + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang10 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=11) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang11 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
	    + "     ELSE IF (MONTH(@time)=12) "
	    + "     BEGIN "
		+ "         INSERT INTO tbl_Log_Thang12 (Username, CanBoId, Operation, TableName, Description, ExecutionTime, RemoteClientAdd, RemoteClientHost, RemoteClientPort)  "
		+ "         VALUES (@username, @canBoId, @operation, @tableName, @description, @time, @RemoteClientAdd, @RemoteClientHost, @RemoteClientPort) "
	    + "     END "
        + " END";
    string logError ="CREATE PROCEDURE [dbo].[gcGOBAL_LogErrorOperation] "
	    + "     @username nvarchar(50), "
	    + "     @canBoId int, "
	    + "     @operation nvarchar(50), "
	    + "     @tableName nvarchar(50), "
	    + "     @description nvarchar(256), "
	    + "     @time Datetime "
        + " AS "
        + " BEGIN "
        + "     INSERT INTO zgcBUILDIN_LogError (Username, CanBoId, Operation, TableName, Description, ExecutionTime) "
		+ "          VALUES (@username, @canBoId, @operation, @tableName, @description, @time) "
        + " END ";
    string strIdentify ="CREATE PROCEDURE [dbo].[gcGOBAL_GetIdentity] "
            + " @tablename      nvarchar(256) , "
            + " @name      nvarchar(256) , "
            + " @identity nvarchar(128) output "
            + " as  "
            + " BEGIN "
            + " DECLARE  @sysTABLE TABLE ( "
            + "     TABLE_QUALIFIER            nvarchar(250), "
            + "     TABLE_OWNER                nvarchar(250), "
            + "     TABLE_NAME                 nvarchar(250), "
            + "     COLUMN_NAME                nvarchar(250), "
            + "     DATA_TYPE                  nvarchar(250), "
            + "     TYPE_NAME                 nvarchar(250), "
            + "     PRECISION              nvarchar(250), "
            + "     LENGTH                  nvarchar(250), "
            + "     SCALE                      nvarchar(250), "
            + "     RADIX                      nvarchar(250), "
            + "     NULLABLE                    nvarchar(250), "
            + "     REMARKS                     nvarchar(250), "
            + "     COLUMN_DEF                  nvarchar(250), "
            + "     SQL_DATA_TYPE               nvarchar(250), "
            + "     SQL_DATETIME_SUB            nvarchar(250), "
            + "     CHAR_OCTET_LENGTH           nvarchar(250), "
            + "     ORDINAL_POSITION            nvarchar(250), "
            + "     IS_NULLABLE                  nvarchar(10), "
            + "     SS_DATA_TYPE   nvarchar(250) "
            + " ) "
            + " INSERT @sysTABLE (TABLE_QUALIFIER    , "
            + "     TABLE_OWNER                , "
            + "     TABLE_NAME                 , "
            + "     COLUMN_NAME                , "
            + "     DATA_TYPE                  , "
            + "     TYPE_NAME                , "
            + "     PRECISION              , "
            + "     LENGTH                  , "
            + "     SCALE                      , "
            + "     RADIX                      , "
            + "     NULLABLE                    , "
            + "     REMARKS                     , "
            + "     COLUMN_DEF                  , "
            + "     SQL_DATA_TYPE               , "
            + "     SQL_DATETIME_SUB            , "
            + "     CHAR_OCTET_LENGTH           , "
            + "     ORDINAL_POSITION            , "
            + "     IS_NULLABLE                 , "
            + "     SS_DATA_TYPE   ) "
            + " exec sp_columns @tablename "
	        + "     set @identity = '' "
	        + "     select @identity=column_name from @sysTABLE "
	        + "     where (TYPE_NAME like '%identity%' or TYPE_NAME like '%uniqueidentifier%') "
			+ "             and column_name =@name "
            + " END";
    #endregion
    public zgc0GobalService()
    {
		//Uncomment the following line if using designed components 
		//InitializeComponent(); 
	}
    static public void SetupRightFilter(ref string filter, string table)
    {
        if (HttpContext.Current.Session["gcRightGroup"] != null)
        {
            string rg = HttpContext.Current.Session["gcRightGroup"].ToString();
            if (rg != "1" && rg != "2" && rg != "999")
            {
                if (!table.Contains("LITERAL"))
                {
                    string partcomp = HttpContext.Current.Session["gcCtyId"] + "-" + HttpContext.Current.Session["gcBranchId"] + "-" + HttpContext.Current.Session["gcDepartmentId"];
                    if (filter == null || filter.Trim().Length < 1)
                        filter = string.Format(" isPrgPartComp='{0}'", partcomp);
                    else
                        filter += string.Format(" and isPrgPartComp='{0}'", partcomp);
                }
            }
        }
    }


    static public void SetupRightComboFilter(ref string filter, string table)
    {
        if (HttpContext.Current.Session["gcRightGroup"] != null)
        {
            string rg = HttpContext.Current.Session["gcRightGroup"].ToString();
            if (rg != "1" && rg != "2" && rg != "999")
            {
                if (!table.Contains("LITERAL") )
                {
                    string partcomp = HttpContext.Current.Session["gcCtyId"] + "-" + HttpContext.Current.Session["gcBranchId"] + "-" + HttpContext.Current.Session["gcDepartmentId"];
                    if (filter == null || filter.Trim().Length < 1)
                        filter = string.Format(" isPrgPartComp='{0}'", partcomp);
                    else
                        filter += string.Format(" and isPrgPartComp='{0}'", partcomp);
                }
            }
        }
    }

 
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public void SendSMS()
    {
        string content = "MAILISA - khuyen mai 20% nhan dip tet khi lam tat ca dich vu den ngày 22/02/2011- Anh Gà goi!";
        content = Server.UrlEncode(content);
        //HttpWebRequest myRequest = 
        //(HttpWebRequest)WebRequest.Create(String.Format("http://api.sms.won.vn/service/index.php?username=mailisa&password=ml@12345&content={0}&receive=84942462744&brandname=MAILISA", content));
        // We use POST ( we can also use GET )

        HttpWebRequest myRequest =
        (HttpWebRequest)WebRequest.Create(String.Format("http://api.sms.won.vn/service/index.php?username=mailisa&password=ml@12345&content={0}&receive=84918212127&brandname=MAILISA", content));

        myRequest.Method = "GET";

        // Set the content type to a FORM

        myRequest.ContentType = "application/x-www-form-urlencoded";

        // Get length of content

        //myRequest.ContentLength = buffer.Length;

        //// Get request stream

        //Stream newStream = myRequest.GetRequestStream();
        //// Send the data.

        //newStream.Write(buffer, 0, buffer.Length);

        // Close stream

        //newStream.Close();

        // Assign the response object of 'HttpWebRequest' to a 'HttpWebResponse' variable.

        HttpWebResponse myHttpWebResponse = (HttpWebResponse)myRequest.GetResponse();

        // Display the contents of the page to the console.

        Stream streamResponse = myHttpWebResponse.GetResponseStream();
        // Get stream object

        StreamReader streamRead = new StreamReader(streamResponse);
        Char[] readBuffer = new Char[256];

        string strBuff = "";
        // Read from buffer
        int count = streamRead.Read(readBuffer, 0, 256);

        while (count > 0)
        {
            // get string

            String resultData = new String(readBuffer, 0, count);
            // Write the data
            strBuff+= resultData;

            // Read from buffer

            count = streamRead.Read(readBuffer, 0, 256);
        }
        // Release the response object resources.

        streamRead.Close();

        streamResponse.Close();

        // Close response

        myHttpWebResponse.Close();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string BuildCoreJS()
    {
        DateTime t1 = DateTime.Now;
        string strError = "";
        // DataTable tblData = zgc0HelperSecurity.GetDataTableNew("select * from zgcBUILDIN_DEFINE_CONFIG_FormatPrograme", zgc0GlobalStr.getSqlStr());
        //zgc0BUILDIN_CONFIG_FORMATInner gobalFormat = new zgc0BUILDIN_CONFIG_FORMATInner(tblData.Rows[0], false);

        //-------------------------------------------------------------------------------------------

        zgc0GlobalStr strGlobal = new zgc0GlobalStr();

        string[] arrDirect = HttpContext.Current.Request.PhysicalApplicationPath.Split('\\');
        string newPath = "";
        for (int a = 0; a < arrDirect.Length - 2; a++)
            newPath += arrDirect[a] + "\\";
        newPath += "TempAdmin" + "\\";
        string tmpNewPath = newPath;
        //--------------------------------------------------------------------------------------
        newPath = HttpContext.Current.Request.PhysicalApplicationPath;

        zgcAdmin zAdmin = new zgcAdmin();
        zAdmin.server = zgc0GlobalStr.getSqlStr();
        //zAdmin.CongfigALL();// làm bằng tay

        zgcAllTableCore zTables = new zgcAllTableCore();
        zTables.server = zgc0GlobalStr.getSqlStr();

        string path = newPath + "ObjectBuildInter\\zgc0ObjectV.cs";
        zTables.Load();
        string path2 = newPath + "\\JS\\o.js";
       
        FileInfo hh = new FileInfo(path2);
        zTables.BuildCoreJS(path2);

        DateTime t2 = DateTime.Now;
        double f = (t2 - t1).Milliseconds;
        strError += "t1: " + t1.ToLongTimeString() + " \r\n ";
        strError += "t2: " + t2.ToLongTimeString() + " \r\n ";

        strError += " \r\n " + f.ToString() + " miliseconds";
        return strError;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string BuildCore()
    {
        DateTime t1 = DateTime.Now;
        string strError = "";
       // DataTable tblData = zgc0HelperSecurity.GetDataTableNew("select * from zgcBUILDIN_DEFINE_CONFIG_FormatPrograme", zgc0GlobalStr.getSqlStr());
        //zgc0BUILDIN_CONFIG_FORMATInner gobalFormat = new zgc0BUILDIN_CONFIG_FORMATInner(tblData.Rows[0], false);

        //-------------------------------------------------------------------------------------------

        zgc0GlobalStr strGlobal = new zgc0GlobalStr();

        string[] arrDirect = HttpContext.Current.Request.PhysicalApplicationPath.Split('\\');
        string newPath = "";
        for (int a = 0; a < arrDirect.Length - 2; a++)
            newPath += arrDirect[a] + "\\";
        newPath += "TempAdmin" + "\\";
        string tmpNewPath = newPath;
        //--------------------------------------------------------------------------------------
        newPath = HttpContext.Current.Request.PhysicalApplicationPath;

        zgcAdmin zAdmin = new zgcAdmin();
        zAdmin.server = zgc0GlobalStr.getSqlStr();
        //zAdmin.CongfigALL();// làm bằng tay

        zgcAllTableCore zTables = new zgcAllTableCore();
          zTables.server = zgc0GlobalStr.getSqlStr();
      
        string path = newPath + "ObjectBuildInter\\zgc0ObjectV.cs";
        zTables.Load();
        string path2 = newPath + "\\ObjectBuildInter\\D.cs";
        string path3 = newPath + "\\ObjectBuildInter\\P.cs";
        string path4 = newPath + "\\JS\\o.js";
        string path5 = newPath + "\\ObjectBuildInter\\D2.cs";

        FileInfo hh = new FileInfo(path2);
        zTables.BuildCore(path2, path3, path4, path5);
       
        DateTime t2 = DateTime.Now;
        double f = (t2 - t1).Milliseconds;
        strError += "t1: " + t1.ToLongTimeString() + " \r\n ";
        strError += "t2: " + t2.ToLongTimeString() + " \r\n ";

        strError += " \r\n " + f.ToString() + " miliseconds";
        return strError;
    }

   
    //----------------------------------------------------------------

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public void Repair()
    {
       
    }
    //----------------------------------------------------------------

   
    //---------------------------------------------------------------


    [WebMethod(EnableSession = true)]
    public string gcWritetoSession(string hoSoId)
    {
        string value = "";
        //get session
        HttpContext.Current.Session["gchoSoId"] = hoSoId;

        //kiểm tra mã cán bộ của người này thuộc phòng ban nào, hoặc lấy tên về
        //mục đích là lấy tên để load default value;
        return value;
    }

    [WebMethod(EnableSession = true)]
    public int gcGetHoSoIdFromSession(string hoSoId)
    {

        if (HttpContext.Current.Session["gchoSoId"] != null)
            return int.Parse(HttpContext.Current.Session["gchoSoId"].ToString());
        //kiểm tra mã cán bộ của người này thuộc phòng ban nào, hoặc lấy tên về
        //mục đích là lấy tên để load default value;
        else
            return -1;
    }


    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getStartButton()
    //{
    //    List<object[]> tmp = new List<object[]>();
    //    string sql = "select * from zgc_BUILD_ADMIN where TableName<>'#'";
    //    DataTable dt = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        zgc_BUILD_ADMIN admin = new zgc_BUILD_ADMIN(dt.Rows[i], false);
    //        string TableName = Convert.ToString(dt.Rows[i]["tablename"]);
    //        string Formname = Convert.ToString(dt.Rows[i]["Formname"]);

    //        string newTableName = TableName.Substring(0, 4);
    //        if (newTableName == "tbl_")
    //            newTableName = TableName.Substring(4, TableName.Length - 4);
    //        else
    //            newTableName = TableName;

    //        newTableName = Convert.ToString(dt.Rows[i]["tab"]) + newTableName;
    //        object[] a = new object[2];
    //        a[0] = newTableName + ".aspx";
    //        a[1] = Formname;
    //        tmp.Add(a);
    //    }

    //    object[] b = new object[2];
    //    b[0] = "default.aspx";
    //    b[1] = "";
    //    tmp.Add(b);
    //    return tmp;
    //}

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string SetGroupRight(string url, string value)
    {
        int Nvalue = int.Parse(( value));
        HttpContext.Current.Session["gczNewGroupRight"] = value;

        if(Session["gcUserName"]!=null && Session["gcMaCanBoId"]!=null)
        zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(),
                       int.Parse(Session["gcMaCanBoId"].ToString()), "GROUP", "gczNewGroupRight", value, DateTime.Now,
                       HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"],
                       HttpContext.Current.Request.ServerVariables["REMOTE_HOST"],
                       HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);

        if (Nvalue > 9000 || Nvalue == 10)
        {
            HttpContext.Current.Session["gczSelfInfo"] = value;
        }
        else
        {
            HttpContext.Current.Session.Remove("gczSelfInfo");
        }
        return url;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string gcGetPhongBanIdCB()
    {
        string value = "";
        //get session
        object gcMaCanBo = HttpContext.Current.Session["gcMaCanBoId"];
        if (gcMaCanBo != null)
        {
            int id = int.Parse(gcMaCanBo.ToString());
            string sql = "select * from tbl_NS_Canbo where Id=" + id.ToString();
            DataTable tb = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());
            if (tb.Rows.Count > 0)
            {
                //int MaPhongBan;
                //string TenPhongBan
                value = tb.Rows[0]["PhongBanId"].ToString();
            }
        }
        //kiểm tra mã cán bộ của người này thuộc phòng ban nào, hoặc lấy tên về
        //mục đích là lấy tên để load default value;
        return value;
    }


       

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string gcWriteKeyPB(string key)
    {
        string value = "";
        //get session
        HttpContext.Current.Session["gcPhongBanCanChuyenId"] = key;
        Session["gcPBId"] = key;
        return value;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string gcUpdateOne(string type, string id, string text)
    {
        string strReturn = "";
        //get session
        if (type == "1")
        {
            zgc0Support.UpdateOneColunm("tbl_HS_Hoso_GiayTo", "SoLuong", id, "int", text);
        }
        return strReturn;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string UpdateOneCol(string value, string id, string field, string type, string table, string user)
    {
        try
        {
            switch (type)
            {
                case "int":
                case "float":
                case "long":
                case "smallint":
                    {
                        value = value.Replace(",", "");
                        value = value.Replace('.', ',');
                        break;
                    }
            }
        }
        catch (Exception e)
        {
            string a = e.ToString();
        }
        string strReturn = "Fail";
        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
            return "Fail";

        //get session
        if (zgc0Support.UpdateOneColunm(table, field, id, type, value) > 0)
            return "Success";
        return strReturn;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public string UpdateOneColForStock(string value, string id, string field, string type, string table, string user)
    {
        try
        {
            switch (type)
            {
                case "int":
                case "float":
                case "long":
                case "smallint":
                    {
                        value = value.Replace(",", "");
                        value = value.Replace('.', ',');
                        break;
                    }
            }
        }
        catch (Exception e)
        {
            string a = e.ToString();
        }
        string strReturn = "Fail";
        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
            return "Fail";

        if (HttpContext.Current.Session["gcStock_KHO"] != null)
        {
            table = table + HttpContext.Current.Session["gcStock_KHO"].ToString();
        }
        else
            table = "gcGobal_STOCK_gcStock_KC";

        //get session
        if (zgc0Support.UpdateOneColunm(table, field, id, type, value) > 0)
            return "Success";
        return strReturn;
    }
    //SELECT name, id, xtype, status, parent_obj, crdate, type, userstat, sysstat, indexdel, refdate, version, category, cache FROM sysobjects WHERE (xtype = 'V')

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public List<object> GetResultFromBarCode(string code)
    {
        if (code.Length > 5)
        {
            code = code.Substring(0, code.Length - 1);
            if (code.Length >= 5)
                code = code.PadRight(5);
        }
        List<object> list = new List<object>();
        string sql = String.Format(" SELECT   [Id]"
          + " ,[Code]"
          + ",[Name]"
          + ",[LoaiVatTuId]"
          + ",[DonViTinhId]"
          + ",[GiaMoi]"
          + ",[LoaiVatTuIdName]"
          + ",[DonViTinhIdName]"

          + "  FROM [zgcl_gcGobal_STOCK_gcProductList03] where Code like N'%{0}%'", code);
        DataTable tblData = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());
        if (tblData.Rows.Count > 0)
        {
            list.Add(tblData.Rows[0][0].ToString());
            list.Add(tblData.Rows[0][1].ToString());
            list.Add(tblData.Rows[0][2].ToString());
            list.Add(tblData.Rows[0][3].ToString());
            list.Add(tblData.Rows[0][4].ToString());
            list.Add(tblData.Rows[0][5].ToString());
            list.Add(tblData.Rows[0][6].ToString());
            list.Add(tblData.Rows[0][7].ToString());
        }
        return list;
    }
}
