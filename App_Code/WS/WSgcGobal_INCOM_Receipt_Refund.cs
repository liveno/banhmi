
/*------------------------------------*/
/* Summary description for  gcGobal_INCOM_Receipt_Refund */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
[WebService(Namespace = "http://www.anhxuan.com.vn/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WSgcGobal_INCOM_Receipt_Refund : System.Web.Services.WebService
{

    //public List<object> AddData(object obj, object HISobj, int startIndex,
    //                                        int maximumRows, string sortExpressions, string filter, string user)
    //{
    //    List<object> list = new List<object>();
    //    try
    //    {
    //        startIndex = 0; string IdOut = "";
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preAdd(ref list, obj, ref  startIndex, ref  maximumRows, ref sortExpressions, ref filter, "preAdd_gcGobal_INCOM_Receipt_Refund");
    //        AddObjectLock((gcGobal_INCOM_Receipt_Refund)obj, ref IdOut);
    //        if (IdOut == "DONE")
    //            if (bReturn) bReturn = gcGobal_INCOM_Receipt_Refund.AddObject(obj, ref IdOut, (SqlCommand)null, zgc0GlobalStr.getSqlStr());
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "ADDDATA", "zgcl_gcGobal_INCOM_Receipt_Refund04", "AddData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        ((gcGobal_INCOM_Receipt_Refund)obj).IdOut = IdOut;



    //        if (bReturn) bReturn = gcGobal_INCOM_Receipt_RefundExt.postAdd(ref list, obj, ref startIndex, ref maximumRows, ref sortExpressions, ref filter, "postAdd_gcGobal_INCOM_Receipt_Refund");

    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_INCOM_Receipt_Refund04");
    //        list = GetDataIner(startIndex, maximumRows, sortExpressions, filter, "", user);

    //        //---------------------------------------------------------
    //        //success get data
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)getGetNewValueMaP74(user);
    //        list.Add(tail);
    //        return list;
    //    }
    //    catch (Exception e)
    //    {
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "ADDDATA", "zgcl_gcGobal_INCOM_Receipt_Refund04", "AddData - file: WSgcGobal_INCOM_Receipt_Refund.cs" + e.Message.ToString(), DateTime.Now);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail.errMsg = "Lỗi ở hàm: [AddData - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetData(int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{
    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_INCOM_Receipt_Refund04";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_INCOM_Receipt_Refund");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_INCOM_Receipt_Refund04");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_INCOM_Receipt_Refund04", "GetData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_INCOM_Receipt_Refund04 newEmployee = new zgcl_gcGobal_INCOM_Receipt_Refund04(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_INCOM_Receipt_RefundExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_INCOM_Receipt_Refund04");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    tail = (zgc0GlobalTail)getGetNewValueMaP74(user);
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    //public List<object> GetDataIner(int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_INCOM_Receipt_Refund04";
    //    string columns = "*";
    //    int bSql = -1;

    //    List<object> list = new List<object>();
    //    bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_INCOM_Receipt_Refund");
    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_INCOM_Receipt_Refund04");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_INCOM_Receipt_Refund04", "GetData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_INCOM_Receipt_Refund04 newEmployee = new zgcl_gcGobal_INCOM_Receipt_Refund04(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------
    //    zgc0GlobalTail tail = new zgc0GlobalTail();

    //    bReturn = gcGobal_INCOM_Receipt_RefundExt.preProcessCount(ref tail, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_INCOM_Receipt_Refund04");
    //    if (bReturn) tail.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tail);
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> DeleteData(int Id, string startIndex,
    //                                        int maximumRows, string sortExpressions, string filter, string user)
    //{
    //    List<object> list = new List<object>();
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    try
    //    {
    //        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        {
    //            tail.errMsg = "Không có quyền truy cập dữ liệu";
    //            list.Add(tail);
    //            return list;
    //        }
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preDelete(Id, ref startIndex, ref  maximumRows, ref  sortExpressions, ref filter, "preDelete_gcGobal_INCOM_Receipt_Refund");
    //        if (bReturn) bReturn = zgc0Support.ProcessDel(Id, "gcGobal_INCOM_Receipt_Refund");
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "DELETE", "zgcl_gcGobal_INCOM_Receipt_Refund04", "Delete - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        if (bReturn) bReturn = gcGobal_INCOM_Receipt_RefundExt.postDelete(Id, ref  startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "postDelete_gcGobal_INCOM_Receipt_Refund");
    //    }
    //    catch (Exception e)
    //    {
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "DELETE", "zgcl_gcGobal_INCOM_Receipt_Refund04", "Delete - file: WSgcGobal_INCOM_Receipt_Refund.cs" + e.Message.ToString(), DateTime.Now);
    //        tail.errMsg = "Lỗi ở hàm: [Delete - file: WSCTY_Branch.cs]." + e.Message.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //    int index = int.Parse(startIndex);
    //    zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_INCOM_Receipt_Refund");
    //    list = GetDataIner(index, maximumRows, sortExpressions, filter, "", user);

    //    //---------------------------------------------------------
    //    //success get data
    //    list.Add(tail);
    //    return list;
    //}
    //public List<object> UpdateData(object obj, object HISobj, string startIndex,
    //                                        int maximumRows, string sortExpressions, string filter, string user)
    //{
    //    try
    //    {
    //        List<object> list = new List<object>();
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preUpdate(ref list, obj, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "preUpdate_gcGobal_INCOM_Receipt_Refund");
    //        if (bReturn) bReturn = gcGobal_INCOM_Receipt_Refund.UpdateObject(obj, (SqlCommand)null, zgc0GlobalStr.getSqlStr());
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "UPDATE", "zgcl_gcGobal_INCOM_Receipt_Refund04", "UpdateData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        if (bReturn) bReturn = gcGobal_INCOM_Receipt_RefundExt.postUpdate(ref list, obj, ref  startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "postUpdate_gcGobal_INCOM_Receipt_Refund");
    //        int index = int.Parse(startIndex);



    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_INCOM_Receipt_Refund");
    //        list = GetDataIner(int.Parse(startIndex), maximumRows, sortExpressions, filter, "", user);

    //        //---------------------------------------------------------
    //        //success get data
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        list.Add(tail);
    //        return list;
    //    }
    //    catch (Exception e)
    //    {
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "UPDATE", "zgcl_gcGobal_INCOM_Receipt_Refund04", "UpdateData - file: WSgcGobal_INCOM_Receipt_Refund.cs" + e.Message.ToString(), DateTime.Now);
    //        List<object> list = new List<object>();
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail.errMsg = "Lỗi ở hàm: [UpdateData - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> ValidForm(object objjs, string startIndex,
    //        int maximumRows, string sortExpressions, string filter, int type, string user)
    //{
    //    List<object> list = new List<object>();
    //    try
    //    {
    //        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        {
    //            zgc0GlobalTail tail = new zgc0GlobalTail();
    //            tail.errMsg = "Không có quyền truy cập dữ liệu";
    //            list.Add(tail);
    //            return list;
    //        }
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_INCOM_Receipt_Refund");
    //        object HISobj = null;
    //        gcGobal_INCOM_Receipt_Refund obj = ParseObject(objjs, ref HISobj, bHISTable, type);
    //        string tmpValue = "";
    //        bool bRet = true;

    //        if (type == 1)
    //            if (zgc0CheckFunc.CheckExsist(MaCT.ToString(), "MaCT", "gcGobal_INCOM_Receipt_Refund"))
    //            {
    //                tmpValue += zgc0CheckFunc.getMsgCheckExsist("Mã chứng từ ", MaCT.ToString());
    //                bRet = false;
    //            }
    //        if (type == 0)
    //            if (zgc0CheckFunc.CheckExsist(Id.Value, MaCT.ToString(), "MaCT", "gcGobal_INCOM_Receipt_Refund"))
    //            {
    //                tmpValue += zgc0CheckFunc.getMsgCheckExsist("Mã chứng từ ", MaCT.ToString());
    //                bRet = false;
    //            }

    //        gcGobal_INCOM_Receipt_RefundExt.postCheckContrainData(obj, ref bRet, type, ref tmpValue, "postCheckContrainData_gcGobal_INCOM_Receipt_Refund");

    //        string buildStr = zgc0CheckFunc.FormatDiv(tmpValue, "Có lỗi xảy ra khi nhập liệu");
    //        if (bRet)
    //        {
    //            //type ==0 Update
    //            if (type == 0)
    //            {
    //                return UpdateData(obj, HISobj, (startIndex), maximumRows, sortExpressions, filter, user);
    //            }
    //            else
    //            {
    //                return AddData(obj, HISobj, int.Parse(startIndex), maximumRows, sortExpressions, filter, user);
    //            }
    //        }
    //        else
    //        {
    //            zgc0GlobalTail tail = new zgc0GlobalTail();
    //            tail.errMsg = buildStr;
    //            list.Add(tail);
    //            return list;
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        System.Text.StringBuilder strBuild = new System.Text.StringBuilder();
    //        strBuild.AppendLine("Có lỗi xảy ra. Vui lòng thử lại!");
    //        strBuild.AppendLine("Lỗi ở hàm: [Valid - file: gcGobal_INCOM_Receipt_Refund.cs].");
    //        strBuild.AppendLine(e.Message.ToString());
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "VALIDFORM", "gcGobal_INCOM_Receipt_Refund", "ValidForm - file: WSgcGobal_INCOM_Receipt_Refund.cs" + strBuild.ToString(), DateTime.Now);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail.errMsg = strBuild.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //}
    //public gcGobal_INCOM_Receipt_Refund ParseObject(object objOld, ref object HISobj, bool bHISTable, int type)
    //{
    //    HISobj = null;
    //    gcGobal_INCOM_Receipt_Refund obj = new gcGobal_INCOM_Receipt_Refund();
    //    Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //    dicValues = (Dictionary<string, object>)objOld;
    //    if (dicValues["Id"] != null && dicValues["Id"].ToString() == "-9999")
    //        return obj;
    //    Id = (dicValues["Id"] == null) ? (int?)null : Convert.ToInt32(dicValues["Id"]);
    //    MaCT = (dicValues["MaCT"] == null) ? (string)null : Convert.ToString(dicValues["MaCT"]);
    //    SoCT = (dicValues["SoCT"] == null) ? (string)null : Convert.ToString(dicValues["SoCT"]);
    //    NgayLap = (dicValues["NgayLap"] == null) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayLap"]);
    //    BranchId = (dicValues["BranchId"] == null) ? (int?)null : Convert.ToInt32(dicValues["BranchId"]);
    //    KHId = (dicValues["KHId"] == null) ? (int?)null : Convert.ToInt32(dicValues["KHId"]);
    //    MSHDId = (dicValues["MSHDId"] == null) ? (int?)null : Convert.ToInt32(dicValues["MSHDId"]);
    //    TongTien = (dicValues["TongTien"] == null) ? (double?)null : Convert.ToDouble(dicValues["TongTien"]);
    //    ThucThu = (dicValues["ThucThu"] == null) ? (double?)null : Convert.ToDouble(dicValues["ThucThu"]);
    //    NoLai = (dicValues["NoLai"] == null) ? (double?)null : Convert.ToDouble(dicValues["NoLai"]);
    //    DienGiai = (dicValues["DienGiai"] == null) ? (string)null : Convert.ToString(dicValues["DienGiai"]);
    //    VAT = (dicValues["VAT"] == null) ? (double?)null : Convert.ToDouble(dicValues["VAT"]);
    //    PhuThu = (dicValues["PhuThu"] == null) ? (double?)null : Convert.ToDouble(dicValues["PhuThu"]);
    //    NhanVienId = (dicValues["NhanVienId"] == null) ? (int?)null : Convert.ToInt32(dicValues["NhanVienId"]);
    //    ThanhToan = (dicValues["ThanhToan"] == null) ? (bool?)null : Convert.ToBoolean(dicValues["ThanhToan"]);
    //    isDatCoc = (dicValues["isDatCoc"] == null) ? (bool?)null : Convert.ToBoolean(dicValues["isDatCoc"]);
    //    SoTienCoc = (dicValues["SoTienCoc"] == null) ? (double?)null : Convert.ToDouble(dicValues["SoTienCoc"]);
    //    SoTienDaCoc = (dicValues["SoTienDaCoc"] == null) ? (double?)null : Convert.ToDouble(dicValues["SoTienDaCoc"]);
    //    MaPTDaCoc = (dicValues["MaPTDaCoc"] == null) ? (string)null : Convert.ToString(dicValues["MaPTDaCoc"]);
    //    isTraNo = (dicValues["isTraNo"] == null) ? (bool?)null : Convert.ToBoolean(dicValues["isTraNo"]);
    //    NgayHen = (dicValues["NgayHen"] == null) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayHen"]);
    //    SoTienNo = (dicValues["SoTienNo"] == null) ? (double?)null : Convert.ToDouble(dicValues["SoTienNo"]);
    //    SoTienNoKhachTra = (dicValues["SoTienNoKhachTra"] == null) ? (double?)null : Convert.ToDouble(dicValues["SoTienNoKhachTra"]);
    //    isFinished = (dicValues["isFinished"] == null) ? (bool?)null : Convert.ToBoolean(dicValues["isFinished"]);
    //    DiscountNgMoiGioi = (dicValues["DiscountNgMoiGioi"] == null) ? (double?)null : Convert.ToDouble(dicValues["DiscountNgMoiGioi"]);
    //    DiscountCust = (dicValues["DiscountCust"] == null) ? (double?)null : Convert.ToDouble(dicValues["DiscountCust"]);
    //    isDiscount = (dicValues["isDiscount"] == null) ? (bool?)null : Convert.ToBoolean(dicValues["isDiscount"]);
    //    StatusId = (dicValues["StatusId"] == null) ? (int?)null : Convert.ToInt32(dicValues["StatusId"]);
    //    isPrinted = (dicValues["isPrinted"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrinted"]);
    //    Keep01 = (dicValues["Keep01"] == null) ? (string)null : Convert.ToString(dicValues["Keep01"]);
    //    Keep02 = (dicValues["Keep02"] == null) ? (string)null : Convert.ToString(dicValues["Keep02"]);
    //    Keep03 = (dicValues["Keep03"] == null) ? (string)null : Convert.ToString(dicValues["Keep03"]);
    //    Keep04 = (dicValues["Keep04"] == null) ? (string)null : Convert.ToString(dicValues["Keep04"]);
    //    Space01 = (dicValues["Space01"] == null) ? (double?)null : Convert.ToDouble(dicValues["Space01"]);
    //    Space02 = (dicValues["Space02"] == null) ? (string)null : Convert.ToString(dicValues["Space02"]);
    //    Space03 = (dicValues["Space03"] == null) ? (DateTime?)null : Convert.ToDateTime(dicValues["Space03"]);
    //    SpaceId = (dicValues["SpaceId"] == null) ? (int?)null : Convert.ToInt32(dicValues["SpaceId"]);
    //    if (type != 0) isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());
    //    else isPrgAccountId = (dicValues["isPrgAccountId"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgAccountId"]);
    //    isPrgInUse = (dicValues["isPrgInUse"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgInUse"]);
    //    isPrgCreateDate = Convert.ToDateTime(DateTime.Now);
    //    isPrgWaitingConfirmStatus = (dicValues["isPrgWaitingConfirmStatus"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgWaitingConfirmStatus"]);
    //    isPrgbAdminDeleted = (dicValues["isPrgbAdminDeleted"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgbAdminDeleted"]);
    //    isPrgbUserDeleted = (dicValues["isPrgbUserDeleted"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgbUserDeleted"]);
    //    isPrgbShow = (dicValues["isPrgbShow"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgbShow"]);
    //    isPrgOrdered = (dicValues["isPrgOrdered"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgOrdered"]);
    //    isPrgVNKoDau = (string)null;//(dicValues["isPrgVNKoDau"] == null) ? (string)null : Convert.ToString(dicValues["isPrgVNKoDau"]);
    //    isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //    if (type != 0) isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //    else isPrgPartComp = (dicValues["isPrgPartComp"] == null) ? (string)null : Convert.ToString(dicValues["isPrgPartComp"]);
    //    isPrgEncriptData = (dicValues["isPrgEncriptData"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgEncriptData"]);
    //    isPrgDescriptData = (dicValues["isPrgDescriptData"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgDescriptData"]);
    //    isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //    gcGobal_INCOM_Receipt_RefundExt.postAssignment(obj, "postAssignment_gcGobal_INCOM_Receipt_Refund");

    //    return obj;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object getGetNewValueP74(string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return null;
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    string tmpValue = "";
    //    tmpValue = zgc0Support.getAutoGenCode(null, "MaCT", "zgcBUILDIN_GOBAL_AutoGenCode", "Date", "PTH", "4", "Text", "MaCT", "", "", "gcGobal_INCOM_Receipt_Refund");

    //    tail.strNewCode = tmpValue;
    //    gcGobal_INCOM_Receipt_RefundExt.PostAutoGenCode(ref tail, "PostgetGetNewValueP74");
    //    return tail;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object getGetNewValueMaP74(string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return null;
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    string tmpValue = "";
    //    tmpValue = zgc0Support.getAutoGenCode(null, "SoCT", "tbl_GloBal_AutoGenCode", "Normal", "PTH", "8", "", "", "", "", "gcGobal_INCOM_Receipt_Refund");

    //    tail.strNewCode = tmpValue;
    //    gcGobal_INCOM_Receipt_RefundExt.PostAutoGenCode(ref tail, "PostgetGetNewValueMaP74");
    //    return tail;
    //}
    //public int AddObjectLock(gcGobal_INCOM_Receipt_Refund obj, ref string IdOut)
    //{
    //    int bReturn = 1;
    //    IdOut = "";
    //    {

    //        try
    //        {
    //            string sql = "select MaCT from gcGobal_INCOM_Receipt_Refund where MaCT='" + MaCT + "'";
    //            DataTable tbl = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());

    //            if (tbl != null && tbl.Rows.Count > 0)
    //                MaCT = zgc0Support.getAutoGenCode((SqlCommand)null, "MaCT", "zgcBUILDIN_GOBAL_AutoGenCode", "Date", "PTH", "4", "Text", "MaCT", "", "", "gcGobal_INCOM_Receipt_Refund");
    //            gcGobal_INCOM_Receipt_Refund.AddObject(obj, ref IdOut, (SqlCommand)null, zgc0GlobalStr.getSqlStr());
    //        }
    //        catch (Exception e)
    //        {
    //            try
    //            {
    //            }
    //            catch (SqlException ex)
    //            {
    //                {
    //                    bReturn = -1;
    //                }
    //                zgc0HelperSecurity.NoneException(ex);
    //            }
    //            bReturn = -2;
    //            zgc0HelperSecurity.NoneException(e);
    //        }
    //    }
    //    return bReturn;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public int GetCount(string filter, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return -1;
    //    int value = -1;
    //    string view = "zgcl_gcGobal_INCOM_Receipt_Refund04";
    //    string columns = "*";
    //    bool bReturn = zgc0Project.preProcessCount(ref value, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_INCOM_Receipt_Refund04");
    //    if (bReturn) return zgc0Support.ProcessCount(filter, view, columns);
    //    return value;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getBranchIdNAME(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_COMP_Branch00";
    //        bool bCheckFilter = true;
    //        string strExtentField = "NAME";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_INCOM_Receipt_Refund");
    //        object HISobj = null;
    //        gcGobal_INCOM_Receipt_Refund obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getBranchIdNAME");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_COMP_Branch00");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getBranchIdNAME(string prefixText, int count, string strFil, gcGobal_INCOM_Receipt_Refund obj) - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getKHIdHoTen(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_CUST_Customer01";
    //        bool bCheckFilter = true;
    //        string strExtentField = "HoTen";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_INCOM_Receipt_Refund");
    //        object HISobj = null;
    //        gcGobal_INCOM_Receipt_Refund obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getKHIdHoTen");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_CUST_Customer01");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getKHIdHoTen(string prefixText, int count, string strFil, gcGobal_INCOM_Receipt_Refund obj) - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getMSHDIdName(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_INCOM_Contract00";
    //        bool bCheckFilter = true;
    //        string strExtentField = "Name";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_INCOM_Receipt_Refund");
    //        object HISobj = null;
    //        gcGobal_INCOM_Receipt_Refund obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getMSHDIdName");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_INCOM_Contract00");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getMSHDIdName(string prefixText, int count, string strFil, gcGobal_INCOM_Receipt_Refund obj) - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getNhanVienIdHoTen(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_COMP_EmployeeLife13";
    //        bool bCheckFilter = true;
    //        string strExtentField = "HoTen";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_INCOM_Receipt_Refund");
    //        object HISobj = null;
    //        gcGobal_INCOM_Receipt_Refund obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getNhanVienIdHoTen");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_COMP_EmployeeLife13");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getNhanVienIdHoTen(string prefixText, int count, string strFil, gcGobal_INCOM_Receipt_Refund obj) - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getStatusIdName(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_INCOM_Status00";
    //        bool bCheckFilter = true;
    //        string strExtentField = "Name";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_INCOM_Receipt_Refund");
    //        object HISobj = null;
    //        gcGobal_INCOM_Receipt_Refund obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getStatusIdName");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_INCOM_Status00");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getStatusIdName(string prefixText, int count, string strFil, gcGobal_INCOM_Receipt_Refund obj) - file: WSgcGobal_INCOM_Receipt_Refund.cs]." + e.Message.ToString());
    //    }
    //}
    ////----------------------------------------------------------------------------------
    //// 19-2 get BILL empty
    ////filter set from client form
    ////filter = " (isFinished is null OR isFinished = 0) ";
    ////filter = " (isPrinted is null OR isPrinted = 0) ";
    ////filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetDataToDay(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{

    //    IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

    //    //--------------------------------------------------------------------------------------
    //    if (tmpDict.ContainsKey("isFinished"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " ( isFinished = 1) ";
    //        else
    //            filter += " AND (isFinished = 1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrinted"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " ( isPrinted = 1) ";
    //        else
    //            filter += " AND (isPrinted =1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrgOrdered"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrgOrdered = 1) ";
    //        else
    //            filter += " AND (isPrgOrdered = 1) ";
    //    }

    //    //--------------------------------------------------------------------------------------

    //    if (tmpDict.ContainsKey("NgayLap"))
    //    {
    //        string t1 = DateTime.Now.ToShortDateString() + " 0:00";
    //        string t2 = DateTime.Now.ToShortDateString() + " 23:59";
    //        if (filter == null || filter.Length <= 0)
    //            filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        else
    //        {
    //            filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        }
    //    }
    //    gcGobal_INCOM_Receipt_RefundExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
    //    //--------------------------------------------------------------------------------------

    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_INCOM_Receipt_Refund04";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_INCOM_Receipt_Refund");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_INCOM_Receipt_Refund04");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_INCOM_Receipt_Refund04", "GetData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_INCOM_Receipt_Refund04 newEmployee = new zgcl_gcGobal_INCOM_Receipt_Refund04(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_INCOM_Receipt_RefundExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_INCOM_Receipt_Refund04");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    //
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    ////----------------------------------------------------------------------------------
    //// 19-2 get BILL empty
    ////filter set from client form
    ////filter = " (isFinished is null OR isFinished = 0) ";
    ////filter = " (isPrinted is null OR isPrinted = 0) ";
    ////filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetDataAllDay(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user, string t1, string t2)
    //{

    //    IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

    //    //--------------------------------------------------------------------------------------
    //    if (tmpDict.ContainsKey("isFinished"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isFinished = 1) ";
    //        else
    //            filter += " AND (isFinished = 1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrinted"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrinted = 1) ";
    //        else
    //            filter += " AND (isPrinted = 1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrgOrdered"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrgOrdered = 1) ";
    //        else
    //            filter += " AND (isPrgOrdered = 1) ";
    //    }

    //    //--------------------------------------------------------------------------------------

    //    if (tmpDict.ContainsKey("NgayLap"))
    //    {
    //        t1 = t1 + " 0:00";
    //        t2 = t2 + " 23:59";
    //        if (filter == null || filter.Length <= 0)
    //            filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        else
    //        {
    //            filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        }
    //    }
    //    gcGobal_INCOM_Receipt_RefundExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
    //    //--------------------------------------------------------------------------------------

    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_INCOM_Receipt_Refund04";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_INCOM_Receipt_Refund");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_INCOM_Receipt_Refund04");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_INCOM_Receipt_Refund04", "GetData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_INCOM_Receipt_Refund04 newEmployee = new zgcl_gcGobal_INCOM_Receipt_Refund04(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_INCOM_Receipt_RefundExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_INCOM_Receipt_Refund04");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    //
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object CreatInput(string TableID, string itemID)
    //{
    //    string IdOut = "";
    //    gcGobal_INCOM_Receipt_Refund obj = new gcGobal_INCOM_Receipt_Refund();

    //    isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //    isPrgVNKoDau = Session["gcUserName"].ToString();
    //    isPrgCreateDate = DateTime.Now;
    //    isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //    isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //    isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));


    //    gcGobal_INCOM_Receipt_RefundExt.SetNewObject(obj, "post_SetNewObject");
    //    try
    //    {
    //        gcGobal_INCOM_Receipt_Refund.AddObject(obj, ref IdOut, null, zgc0GlobalStr.getSqlStr());
    //        //sau khi thêm dữ liệu vào rồi bây giờ lấy ID của cái hàng mới nhất về.
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message, itemID = itemID };
    //    }
    //    return new { Result = "OK", ReceiptId = IdOut, itemID = itemID };
    //}

    ////----------------------------------------------------------------------------------
    //// 19-2 get BILL empty
    ////filter set from client form
    ////filter = " (isFinished is null OR isFinished = 0) ";
    ////filter = " (isPrinted is null OR isPrinted = 0) ";
    ////filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetData1Item(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{

    //    IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

    //    //--------------------------------------------------------------------------------------
    //    if (tmpDict.ContainsKey("isFinished"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isFinished is null OR isFinished = 0) ";
    //        else
    //            filter += " AND (isFinished is null OR isFinished = 0) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrinted"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrinted is null OR isPrinted = 0) ";
    //        else
    //            filter += " AND (isPrinted is null OR isPrinted = 0) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrgOrdered"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //        else
    //            filter += " AND (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //    }

    //    //--------------------------------------------------------------------------------------
    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (HttpContext.Current.Session["gcAccountId"] == null)
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }

    //    if (tmpDict.ContainsKey("NgayLap"))
    //    {
    //        string t1 = DateTime.Now.ToShortDateString() + " 0:00";
    //        string t2 = DateTime.Now.ToShortDateString() + " 23:59";
    //        if (filter == null || filter.Length <= 0)
    //            filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        else
    //        {
    //            filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        }
    //    }

    //    string gcAccountId = HttpContext.Current.Session["gcAccountId"].ToString();
    //    if (filter == null || filter.Length <= 0)
    //        filter = String.Format(" ( isPrgAccountId={0} )", gcAccountId);
    //    else
    //    {
    //        filter += String.Format(" AND ( isPrgAccountId={0} )", gcAccountId);
    //    }

    //    gcGobal_INCOM_Receipt_RefundExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
    //    //--------------------------------------------------------------------------------------


    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_INCOM_Receipt_Refund04";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_INCOM_Receipt_RefundExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_INCOM_Receipt_Refund");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_INCOM_Receipt_Refund04");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_INCOM_Receipt_Refund04", "GetData - file: WSgcGobal_INCOM_Receipt_Refund.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_INCOM_Receipt_Refund04 newEmployee = new zgcl_gcGobal_INCOM_Receipt_Refund04(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //        //--------------------------------------------------------------------------------------
    //        // if data don't have, we add 1 item empty
    //        // we should set it in extention file to get option data
    //        if (myTable.Rows.Count <= 0)
    //        {
    //            WSgcGobal_INCOM_Receipt_Refund gc = new WSgcGobal_INCOM_Receipt_Refund();
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_INCOM_Receipt_Refund04 newEmployee = new zgcl_gcGobal_INCOM_Receipt_Refund04();

    //            newEmployee.Id = -1;
    //            newEmployee.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //            newEmployee.isPrgVNKoDau = Session["gcUserName"].ToString();
    //            newEmployee.isPrgCreateDate = DateTime.Now;
    //            newEmployee.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //            newEmployee.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //            newEmployee.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

    //            gcGobal_INCOM_Receipt_RefundExt.postAssignmentEmtyData(newEmployee, "postAssignmentEmtyData_Empty");
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_INCOM_Receipt_RefundExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_INCOM_Receipt_Refund04");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    //
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    ////--------------------------------------------------------------------------------------------
    ////jQueryTableDelete
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object jQueryTableDelete(string Id, string ReceitpID, int startIndex, int maximumRows,
    //    string sortExpressions, string filter, string bSecurity, string user)
    //{
    //    int TotalRecordCount = 0;
    //    List<object> result = new List<object>();

    //    try
    //    {
    //        string sql = String.Format("Delete FROM gcGobal_INCOM_Receipt_Refund Where Id={0}", Id);
    //        zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());
    //        //---------------------------------------------------------------------
    //        // Process extension, if have detail, we update total Money,...
    //        bool bReturn = gcGobal_INCOM_Receipt_RefundExt.postjQueryTableDelete(ReceitpID, result, ref  startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "post_postjQueryTableDelete");
    //        if (!bReturn)
    //        {
    //            return new { Result = "OK", Records = result, TotalRecordCount = TotalRecordCount };
    //        }
    //        //---------------------------------------------------------------------

    //        //--------------------------------------------------------------------
    //        //get data from database after delete
    //        WSgcGobal_INCOM_Receipt_Refund obj = new WSgcGobal_INCOM_Receipt_Refund();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_INCOM_Receipt_Refund");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);
    //        //---------------------------------------------------------------------
    //        result.RemoveAt(result.Count - 1);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)result[result.Count - 1];
    //        result.RemoveAt(result.Count - 1);
    //        //update total item
    //        TotalRecordCount = tail.numObj;
    //        //---------------------------------------------------------------------
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message };
    //    }

    //    //------------------------------------------------------------------------
    //    return new { Result = "OK", Records = result, TotalRecordCount = TotalRecordCount };
    //}
    ////--------------------------------------------------------------------------------------------
    ////jQueryTableGetData
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object jQueryTableGetData(object record, int jtStartIndex, int jtPageSize)
    //{
    //    int TotalRecordCount = 0;
    //    List<object> result = new List<object>();
    //    try
    //    {
    //        Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //        dicValues = (Dictionary<string, object>)record;

    //        int startIndex = (dicValues["startIndex"] == null) ? 0 : Convert.ToInt32(dicValues["startIndex"]);
    //        int maximumRows = (dicValues["maximumRows"] == null) ? 1 : Convert.ToInt32(dicValues["maximumRows"]);
    //        string sortExpressions = (dicValues["sortExpressions"] == null) ? "" : Convert.ToString(dicValues["sortExpressions"]);
    //        string filter = (dicValues["filter"] == null) ? "" : Convert.ToString(dicValues["filter"]);
    //        string bSecurity = (dicValues["bSecurity"] == null) ? "" : Convert.ToString(dicValues["bSecurity"]);
    //        string user = (dicValues["user"] == null) ? "" : Convert.ToString(dicValues["user"]);

    //        maximumRows = jtPageSize;
    //        startIndex = jtStartIndex;
    //        if (jtStartIndex > 0)
    //            startIndex = jtPageSize / jtStartIndex;

    //        WSgcGobal_INCOM_Receipt_Refund obj = new WSgcGobal_INCOM_Receipt_Refund();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_INCOM_Receipt_Refund");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);
    //        //----------------------------------------------------------
    //        result.RemoveAt(result.Count - 1);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)result[result.Count - 1];
    //        result.RemoveAt(result.Count - 1);
    //        TotalRecordCount = tail.numObj;
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message, TotalRecordCount = TotalRecordCount };
    //    }
    //    //----------------------------------------------------------
    //    return new { Result = "OK", Records = result, TotalRecordCount = TotalRecordCount };
    //}

    ////----------------------------------------------------------------
    ////jQueryTableAddData
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object jQueryTableAddData(object record)
    //{
    //    Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //    dicValues = (Dictionary<string, object>)record;

    //    string type = (dicValues["type"] == null) ? "" : Convert.ToString(dicValues["type"]);

    //    List<object> result = new List<object>();
    //    string IdOut = "";
    //    int TotalRecordCount = 0;
    //    gcGobal_INCOM_Receipt_Refund objAdd = new gcGobal_INCOM_Receipt_Refund();

    //    try
    //    {
    //        //scan column in here
    //        if (dicValues.ContainsKey("MaCT"))
    //            objAdd.MaCT = ((dicValues["MaCT"] == null) || (dicValues["MaCT"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["MaCT"]);

    //        if (dicValues.ContainsKey("SoCT"))
    //            objAdd.SoCT = ((dicValues["SoCT"] == null) || (dicValues["SoCT"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["SoCT"]);

    //        if (dicValues.ContainsKey("NgayLap"))
    //            objAdd.NgayLap = ((dicValues["NgayLap"] == null) || (dicValues["NgayLap"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayLap"]);

    //        if (dicValues.ContainsKey("BranchId"))
    //            objAdd.BranchId = ((dicValues["BranchId"] == null) || (dicValues["BranchId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["BranchId"]);

    //        if (dicValues.ContainsKey("KHId"))
    //            objAdd.KHId = ((dicValues["KHId"] == null) || (dicValues["KHId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["KHId"]);

    //        if (dicValues.ContainsKey("MSHDId"))
    //            objAdd.MSHDId = ((dicValues["MSHDId"] == null) || (dicValues["MSHDId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["MSHDId"]);

    //        if (dicValues.ContainsKey("TongTien"))
    //            objAdd.TongTien = ((dicValues["TongTien"] == null) || (dicValues["TongTien"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["TongTien"]);

    //        if (dicValues.ContainsKey("ThucThu"))
    //            objAdd.ThucThu = ((dicValues["ThucThu"] == null) || (dicValues["ThucThu"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["ThucThu"]);

    //        if (dicValues.ContainsKey("NoLai"))
    //            objAdd.NoLai = ((dicValues["NoLai"] == null) || (dicValues["NoLai"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["NoLai"]);

    //        if (dicValues.ContainsKey("DienGiai"))
    //            objAdd.DienGiai = ((dicValues["DienGiai"] == null) || (dicValues["DienGiai"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["DienGiai"]);

    //        if (dicValues.ContainsKey("VAT"))
    //            objAdd.VAT = ((dicValues["VAT"] == null) || (dicValues["VAT"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["VAT"]);

    //        if (dicValues.ContainsKey("PhuThu"))
    //            objAdd.PhuThu = ((dicValues["PhuThu"] == null) || (dicValues["PhuThu"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["PhuThu"]);

    //        if (dicValues.ContainsKey("NhanVienId"))
    //            objAdd.NhanVienId = ((dicValues["NhanVienId"] == null) || (dicValues["NhanVienId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["NhanVienId"]);

    //        if (dicValues.ContainsKey("ThanhToan"))
    //            objAdd.ThanhToan = ((dicValues["ThanhToan"] == null) || (dicValues["ThanhToan"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["ThanhToan"]);

    //        if (dicValues.ContainsKey("isDatCoc"))
    //            objAdd.isDatCoc = ((dicValues["isDatCoc"] == null) || (dicValues["isDatCoc"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isDatCoc"]);

    //        if (dicValues.ContainsKey("SoTienCoc"))
    //            objAdd.SoTienCoc = ((dicValues["SoTienCoc"] == null) || (dicValues["SoTienCoc"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienCoc"]);

    //        if (dicValues.ContainsKey("SoTienDaCoc"))
    //            objAdd.SoTienDaCoc = ((dicValues["SoTienDaCoc"] == null) || (dicValues["SoTienDaCoc"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienDaCoc"]);

    //        if (dicValues.ContainsKey("MaPTDaCoc"))
    //            objAdd.MaPTDaCoc = ((dicValues["MaPTDaCoc"] == null) || (dicValues["MaPTDaCoc"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["MaPTDaCoc"]);

    //        if (dicValues.ContainsKey("isTraNo"))
    //            objAdd.isTraNo = ((dicValues["isTraNo"] == null) || (dicValues["isTraNo"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isTraNo"]);

    //        if (dicValues.ContainsKey("NgayHen"))
    //            objAdd.NgayHen = ((dicValues["NgayHen"] == null) || (dicValues["NgayHen"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayHen"]);

    //        if (dicValues.ContainsKey("SoTienNo"))
    //            objAdd.SoTienNo = ((dicValues["SoTienNo"] == null) || (dicValues["SoTienNo"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienNo"]);

    //        if (dicValues.ContainsKey("SoTienNoKhachTra"))
    //            objAdd.SoTienNoKhachTra = ((dicValues["SoTienNoKhachTra"] == null) || (dicValues["SoTienNoKhachTra"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienNoKhachTra"]);

    //        if (dicValues.ContainsKey("isFinished"))
    //            objAdd.isFinished = ((dicValues["isFinished"] == null) || (dicValues["isFinished"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isFinished"]);

    //        if (dicValues.ContainsKey("DiscountNgMoiGioi"))
    //            objAdd.DiscountNgMoiGioi = ((dicValues["DiscountNgMoiGioi"] == null) || (dicValues["DiscountNgMoiGioi"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["DiscountNgMoiGioi"]);

    //        if (dicValues.ContainsKey("DiscountCust"))
    //            objAdd.DiscountCust = ((dicValues["DiscountCust"] == null) || (dicValues["DiscountCust"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["DiscountCust"]);

    //        if (dicValues.ContainsKey("isDiscount"))
    //            objAdd.isDiscount = ((dicValues["isDiscount"] == null) || (dicValues["isDiscount"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isDiscount"]);

    //        if (dicValues.ContainsKey("StatusId"))
    //            objAdd.StatusId = ((dicValues["StatusId"] == null) || (dicValues["StatusId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["StatusId"]);

    //        if (dicValues.ContainsKey("isPrinted"))
    //            objAdd.isPrinted = ((dicValues["isPrinted"] == null) || (dicValues["isPrinted"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["isPrinted"]);

    //        if (dicValues.ContainsKey("Keep01"))
    //            objAdd.Keep01 = ((dicValues["Keep01"] == null) || (dicValues["Keep01"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep01"]);

    //        if (dicValues.ContainsKey("Keep02"))
    //            objAdd.Keep02 = ((dicValues["Keep02"] == null) || (dicValues["Keep02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep02"]);

    //        if (dicValues.ContainsKey("Keep03"))
    //            objAdd.Keep03 = ((dicValues["Keep03"] == null) || (dicValues["Keep03"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep03"]);

    //        if (dicValues.ContainsKey("Keep04"))
    //            objAdd.Keep04 = ((dicValues["Keep04"] == null) || (dicValues["Keep04"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep04"]);

    //        if (dicValues.ContainsKey("Space01"))
    //            objAdd.Space01 = ((dicValues["Space01"] == null) || (dicValues["Space01"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["Space01"]);

    //        if (dicValues.ContainsKey("Space02"))
    //            objAdd.Space02 = ((dicValues["Space02"] == null) || (dicValues["Space02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Space02"]);

    //        if (dicValues.ContainsKey("Space03"))
    //            objAdd.Space03 = ((dicValues["Space03"] == null) || (dicValues["Space03"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["Space03"]);

    //        if (dicValues.ContainsKey("SpaceId"))
    //            objAdd.SpaceId = ((dicValues["SpaceId"] == null) || (dicValues["SpaceId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["SpaceId"]);

    //        objAdd.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //        objAdd.isPrgVNKoDau = Session["gcUserName"].ToString();
    //        objAdd.isPrgCreateDate = DateTime.Now;
    //        objAdd.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //        objAdd.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //        objAdd.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

    //        //if (dicValues.ContainsKey("PhieuThuTienMatId"))
    //        //    objAdd.PhieuThuTienMatId = (dicValues["PhieuThuTienMatId"] == null) ? -1 : Convert.ToInt32(dicValues["PhieuThuTienMatId"]);

    //        int startIndex = (dicValues["startIndex"] == null) ? 0 : Convert.ToInt32(dicValues["startIndex"]);
    //        int maximumRows = (dicValues["maximumRows"] == null) ? 1 : Convert.ToInt32(dicValues["maximumRows"]);
    //        string sortExpressions = (dicValues["sortExpressions"] == null) ? "" : Convert.ToString(dicValues["sortExpressions"]);
    //        string filter = (dicValues["filter"] == null) ? "" : Convert.ToString(dicValues["filter"]);
    //        string bSecurity = (dicValues["bSecurity"] == null) ? "" : Convert.ToString(dicValues["bSecurity"]);
    //        string user = (dicValues["user"] == null) ? "" : Convert.ToString(dicValues["user"]);

    //        if (type.ToUpper() == "INSERT")
    //        {
    //            // tiến hành đưa số liệu vào
    //            gcGobal_INCOM_Receipt_Refund.AddObject(objAdd, ref IdOut, null, zgc0GlobalStr.getSqlStr());

    //            //----------------------------------------------------------------------------
    //            //process data after insert data
    //            gcGobal_INCOM_Receipt_RefundExt.jQueryTableAddData(IdOut, objAdd, "post_jQueryTableAddData");
    //            //----------------------------------------------------------------------------
    //        }

    //        WSgcGobal_INCOM_Receipt_Refund obj = new WSgcGobal_INCOM_Receipt_Refund();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_INCOM_Receipt_Refund");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);

    //        //----------------------------------------------------------------------------
    //        result.RemoveAt(result.Count - 1);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)result[result.Count - 1];
    //        result.RemoveAt(result.Count - 1);
    //        TotalRecordCount = tail.numObj;
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message };
    //    }
    //    //----------------------------------------------------------------------------
    //    return new { Result = "OK", Record = result[0] };
    //}

    ////----------------------------------------------------------------
    ////jQueryTableAddData
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object AddAndUpdateData(object record)
    //{
    //    Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //    dicValues = (Dictionary<string, object>)record;

    //    string type = (dicValues["type"] == null) ? "" : Convert.ToString(dicValues["type"]);

    //    List<object> result = new List<object>();
    //    string IdOut = "";
    //    int TotalRecordCount = 0;
    //    gcGobal_INCOM_Receipt_Refund objAdd = new gcGobal_INCOM_Receipt_Refund();

    //    try
    //    {
    //        //scan column in here
    //        if (dicValues.ContainsKey("MaCT"))
    //            objAdd.MaCT = ((dicValues["MaCT"] == null) || (dicValues["MaCT"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["MaCT"]);

    //        if (dicValues.ContainsKey("SoCT"))
    //            objAdd.SoCT = ((dicValues["SoCT"] == null) || (dicValues["SoCT"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["SoCT"]);

    //        if (dicValues.ContainsKey("NgayLap"))
    //            objAdd.NgayLap = ((dicValues["NgayLap"] == null) || (dicValues["NgayLap"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayLap"]);

    //        if (dicValues.ContainsKey("BranchId"))
    //            objAdd.BranchId = ((dicValues["BranchId"] == null) || (dicValues["BranchId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["BranchId"]);

    //        if (dicValues.ContainsKey("KHId"))
    //            objAdd.KHId = ((dicValues["KHId"] == null) || (dicValues["KHId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["KHId"]);

    //        if (dicValues.ContainsKey("MSHDId"))
    //            objAdd.MSHDId = ((dicValues["MSHDId"] == null) || (dicValues["MSHDId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["MSHDId"]);

    //        if (dicValues.ContainsKey("TongTien"))
    //            objAdd.TongTien = ((dicValues["TongTien"] == null) || (dicValues["TongTien"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["TongTien"]);

    //        if (dicValues.ContainsKey("ThucThu"))
    //            objAdd.ThucThu = ((dicValues["ThucThu"] == null) || (dicValues["ThucThu"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["ThucThu"]);

    //        if (dicValues.ContainsKey("NoLai"))
    //            objAdd.NoLai = ((dicValues["NoLai"] == null) || (dicValues["NoLai"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["NoLai"]);

    //        if (dicValues.ContainsKey("DienGiai"))
    //            objAdd.DienGiai = ((dicValues["DienGiai"] == null) || (dicValues["DienGiai"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["DienGiai"]);

    //        if (dicValues.ContainsKey("VAT"))
    //            objAdd.VAT = ((dicValues["VAT"] == null) || (dicValues["VAT"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["VAT"]);

    //        if (dicValues.ContainsKey("PhuThu"))
    //            objAdd.PhuThu = ((dicValues["PhuThu"] == null) || (dicValues["PhuThu"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["PhuThu"]);

    //        if (dicValues.ContainsKey("NhanVienId"))
    //            objAdd.NhanVienId = ((dicValues["NhanVienId"] == null) || (dicValues["NhanVienId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["NhanVienId"]);

    //        if (dicValues.ContainsKey("ThanhToan"))
    //            objAdd.ThanhToan = ((dicValues["ThanhToan"] == null) || (dicValues["ThanhToan"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["ThanhToan"]);

    //        if (dicValues.ContainsKey("isDatCoc"))
    //            objAdd.isDatCoc = ((dicValues["isDatCoc"] == null) || (dicValues["isDatCoc"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isDatCoc"]);

    //        if (dicValues.ContainsKey("SoTienCoc"))
    //            objAdd.SoTienCoc = ((dicValues["SoTienCoc"] == null) || (dicValues["SoTienCoc"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienCoc"]);

    //        if (dicValues.ContainsKey("SoTienDaCoc"))
    //            objAdd.SoTienDaCoc = ((dicValues["SoTienDaCoc"] == null) || (dicValues["SoTienDaCoc"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienDaCoc"]);

    //        if (dicValues.ContainsKey("MaPTDaCoc"))
    //            objAdd.MaPTDaCoc = ((dicValues["MaPTDaCoc"] == null) || (dicValues["MaPTDaCoc"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["MaPTDaCoc"]);

    //        if (dicValues.ContainsKey("isTraNo"))
    //            objAdd.isTraNo = ((dicValues["isTraNo"] == null) || (dicValues["isTraNo"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isTraNo"]);

    //        if (dicValues.ContainsKey("NgayHen"))
    //            objAdd.NgayHen = ((dicValues["NgayHen"] == null) || (dicValues["NgayHen"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayHen"]);

    //        if (dicValues.ContainsKey("SoTienNo"))
    //            objAdd.SoTienNo = ((dicValues["SoTienNo"] == null) || (dicValues["SoTienNo"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienNo"]);

    //        if (dicValues.ContainsKey("SoTienNoKhachTra"))
    //            objAdd.SoTienNoKhachTra = ((dicValues["SoTienNoKhachTra"] == null) || (dicValues["SoTienNoKhachTra"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["SoTienNoKhachTra"]);

    //        if (dicValues.ContainsKey("isFinished"))
    //            objAdd.isFinished = ((dicValues["isFinished"] == null) || (dicValues["isFinished"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isFinished"]);

    //        if (dicValues.ContainsKey("DiscountNgMoiGioi"))
    //            objAdd.DiscountNgMoiGioi = ((dicValues["DiscountNgMoiGioi"] == null) || (dicValues["DiscountNgMoiGioi"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["DiscountNgMoiGioi"]);

    //        if (dicValues.ContainsKey("DiscountCust"))
    //            objAdd.DiscountCust = ((dicValues["DiscountCust"] == null) || (dicValues["DiscountCust"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["DiscountCust"]);

    //        if (dicValues.ContainsKey("isDiscount"))
    //            objAdd.isDiscount = ((dicValues["isDiscount"] == null) || (dicValues["isDiscount"].ToString().Trim().Length <= 0)) ? (bool?)null : Convert.ToBoolean(dicValues["isDiscount"]);

    //        if (dicValues.ContainsKey("StatusId"))
    //            objAdd.StatusId = ((dicValues["StatusId"] == null) || (dicValues["StatusId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["StatusId"]);

    //        if (dicValues.ContainsKey("isPrinted"))
    //            objAdd.isPrinted = ((dicValues["isPrinted"] == null) || (dicValues["isPrinted"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["isPrinted"]);

    //        if (dicValues.ContainsKey("Keep01"))
    //            objAdd.Keep01 = ((dicValues["Keep01"] == null) || (dicValues["Keep01"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep01"]);

    //        if (dicValues.ContainsKey("Keep02"))
    //            objAdd.Keep02 = ((dicValues["Keep02"] == null) || (dicValues["Keep02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep02"]);

    //        if (dicValues.ContainsKey("Keep03"))
    //            objAdd.Keep03 = ((dicValues["Keep03"] == null) || (dicValues["Keep03"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep03"]);

    //        if (dicValues.ContainsKey("Keep04"))
    //            objAdd.Keep04 = ((dicValues["Keep04"] == null) || (dicValues["Keep04"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep04"]);

    //        if (dicValues.ContainsKey("Space01"))
    //            objAdd.Space01 = ((dicValues["Space01"] == null) || (dicValues["Space01"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["Space01"]);

    //        if (dicValues.ContainsKey("Space02"))
    //            objAdd.Space02 = ((dicValues["Space02"] == null) || (dicValues["Space02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Space02"]);

    //        if (dicValues.ContainsKey("Space03"))
    //            objAdd.Space03 = ((dicValues["Space03"] == null) || (dicValues["Space03"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["Space03"]);

    //        if (dicValues.ContainsKey("SpaceId"))
    //            objAdd.SpaceId = ((dicValues["SpaceId"] == null) || (dicValues["SpaceId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["SpaceId"]);

    //        objAdd.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //        objAdd.isPrgVNKoDau = Session["gcUserName"].ToString();
    //        objAdd.isPrgCreateDate = DateTime.Now;
    //        objAdd.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //        objAdd.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //        objAdd.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

    //        //if (dicValues.ContainsKey("PhieuThuTienMatId"))
    //        //    objAdd.PhieuThuTienMatId = (dicValues["PhieuThuTienMatId"] == null) ? -1 : Convert.ToInt32(dicValues["PhieuThuTienMatId"]);

    //        int startIndex = (dicValues["startIndex"] == null) ? 0 : Convert.ToInt32(dicValues["startIndex"]);
    //        int maximumRows = (dicValues["maximumRows"] == null) ? 1 : Convert.ToInt32(dicValues["maximumRows"]);
    //        string sortExpressions = (dicValues["sortExpressions"] == null) ? "" : Convert.ToString(dicValues["sortExpressions"]);
    //        string filter = (dicValues["filter"] == null) ? "" : Convert.ToString(dicValues["filter"]);
    //        string bSecurity = (dicValues["bSecurity"] == null) ? "" : Convert.ToString(dicValues["bSecurity"]);
    //        string user = (dicValues["user"] == null) ? "" : Convert.ToString(dicValues["user"]);

    //        if (type.ToUpper() == "INSERT")
    //        {
    //            // tiến hành đưa số liệu vào
    //            gcGobal_INCOM_Receipt_Refund.AddObject(objAdd, ref IdOut, null, zgc0GlobalStr.getSqlStr());

    //            //----------------------------------------------------------------------------
    //            //process data after insert data
    //            gcGobal_INCOM_Receipt_RefundExt.AddAndUpdateData(IdOut, objAdd, "post_AddAndUpdateData");
    //            //----------------------------------------------------------------------------
    //        }
    //        else if (type.ToUpper() == "UPDATE")
    //        {
    //            string ObjectID = (dicValues["ObjectID"] == null) ? "" : Convert.ToString(dicValues["ObjectID"]);

    //            string sql = String.Format("UPDATE gcGobal_INCOM_Receipt_Refund SET ");
    //            string where = "";

    //            //scan column update
    //            if (dicValues.ContainsKey("MaCT"))
    //                if (dicValues["MaCT"] != null && dicValues["MaCT"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" MaCT=N'{0}' ", dicValues["MaCT"].ToString());
    //                    else
    //                        where += string.Format(" ,MaCT=N'{0}' ", dicValues["MaCT"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SoCT"))
    //                if (dicValues["SoCT"] != null && dicValues["SoCT"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SoCT=N'{0}' ", dicValues["SoCT"].ToString());
    //                    else
    //                        where += string.Format(" ,SoCT=N'{0}' ", dicValues["SoCT"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NgayLap"))
    //                if (dicValues["NgayLap"] != null && dicValues["NgayLap"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NgayLap=N'{0}' ", dicValues["NgayLap"].ToString());
    //                    else
    //                        where += string.Format(" ,NgayLap=N'{0}' ", dicValues["NgayLap"].ToString());
    //                }

    //            if (dicValues.ContainsKey("BranchId"))
    //                if (dicValues["BranchId"] != null && dicValues["BranchId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" BranchId={0} ", dicValues["BranchId"].ToString());
    //                    else
    //                        where += string.Format(" ,BranchId={0} ", dicValues["BranchId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("KHId"))
    //                if (dicValues["KHId"] != null && dicValues["KHId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" KHId={0} ", dicValues["KHId"].ToString());
    //                    else
    //                        where += string.Format(" ,KHId={0} ", dicValues["KHId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("MSHDId"))
    //                if (dicValues["MSHDId"] != null && dicValues["MSHDId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" MSHDId={0} ", dicValues["MSHDId"].ToString());
    //                    else
    //                        where += string.Format(" ,MSHDId={0} ", dicValues["MSHDId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("TongTien"))
    //                if (dicValues["TongTien"] != null && dicValues["TongTien"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" TongTien={0} ", dicValues["TongTien"].ToString());
    //                    else
    //                        where += string.Format(" ,TongTien={0} ", dicValues["TongTien"].ToString());
    //                }

    //            if (dicValues.ContainsKey("ThucThu"))
    //                if (dicValues["ThucThu"] != null && dicValues["ThucThu"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" ThucThu={0} ", dicValues["ThucThu"].ToString());
    //                    else
    //                        where += string.Format(" ,ThucThu={0} ", dicValues["ThucThu"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NoLai"))
    //                if (dicValues["NoLai"] != null && dicValues["NoLai"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NoLai={0} ", dicValues["NoLai"].ToString());
    //                    else
    //                        where += string.Format(" ,NoLai={0} ", dicValues["NoLai"].ToString());
    //                }

    //            if (dicValues.ContainsKey("DienGiai"))
    //                if (dicValues["DienGiai"] != null && dicValues["DienGiai"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" DienGiai=N'{0}' ", dicValues["DienGiai"].ToString());
    //                    else
    //                        where += string.Format(" ,DienGiai=N'{0}' ", dicValues["DienGiai"].ToString());
    //                }

    //            if (dicValues.ContainsKey("VAT"))
    //                if (dicValues["VAT"] != null && dicValues["VAT"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" VAT={0} ", dicValues["VAT"].ToString());
    //                    else
    //                        where += string.Format(" ,VAT={0} ", dicValues["VAT"].ToString());
    //                }

    //            if (dicValues.ContainsKey("PhuThu"))
    //                if (dicValues["PhuThu"] != null && dicValues["PhuThu"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" PhuThu={0} ", dicValues["PhuThu"].ToString());
    //                    else
    //                        where += string.Format(" ,PhuThu={0} ", dicValues["PhuThu"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NhanVienId"))
    //                if (dicValues["NhanVienId"] != null && dicValues["NhanVienId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NhanVienId={0} ", dicValues["NhanVienId"].ToString());
    //                    else
    //                        where += string.Format(" ,NhanVienId={0} ", dicValues["NhanVienId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("ThanhToan"))
    //                if (dicValues["ThanhToan"] != null && dicValues["ThanhToan"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" ThanhToan='{0}' ", dicValues["ThanhToan"].ToString());
    //                    else
    //                        where += string.Format(" ,ThanhToan='{0}' ", dicValues["ThanhToan"].ToString());
    //                }

    //            if (dicValues.ContainsKey("isDatCoc"))
    //                if (dicValues["isDatCoc"] != null && dicValues["isDatCoc"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" isDatCoc='{0}' ", dicValues["isDatCoc"].ToString());
    //                    else
    //                        where += string.Format(" ,isDatCoc='{0}' ", dicValues["isDatCoc"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SoTienCoc"))
    //                if (dicValues["SoTienCoc"] != null && dicValues["SoTienCoc"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SoTienCoc={0} ", dicValues["SoTienCoc"].ToString());
    //                    else
    //                        where += string.Format(" ,SoTienCoc={0} ", dicValues["SoTienCoc"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SoTienDaCoc"))
    //                if (dicValues["SoTienDaCoc"] != null && dicValues["SoTienDaCoc"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SoTienDaCoc={0} ", dicValues["SoTienDaCoc"].ToString());
    //                    else
    //                        where += string.Format(" ,SoTienDaCoc={0} ", dicValues["SoTienDaCoc"].ToString());
    //                }

    //            if (dicValues.ContainsKey("MaPTDaCoc"))
    //                if (dicValues["MaPTDaCoc"] != null && dicValues["MaPTDaCoc"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" MaPTDaCoc=N'{0}' ", dicValues["MaPTDaCoc"].ToString());
    //                    else
    //                        where += string.Format(" ,MaPTDaCoc=N'{0}' ", dicValues["MaPTDaCoc"].ToString());
    //                }

    //            if (dicValues.ContainsKey("isTraNo"))
    //                if (dicValues["isTraNo"] != null && dicValues["isTraNo"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" isTraNo='{0}' ", dicValues["isTraNo"].ToString());
    //                    else
    //                        where += string.Format(" ,isTraNo='{0}' ", dicValues["isTraNo"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NgayHen"))
    //                if (dicValues["NgayHen"] != null && dicValues["NgayHen"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NgayHen=N'{0}' ", dicValues["NgayHen"].ToString());
    //                    else
    //                        where += string.Format(" ,NgayHen=N'{0}' ", dicValues["NgayHen"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SoTienNo"))
    //                if (dicValues["SoTienNo"] != null && dicValues["SoTienNo"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SoTienNo={0} ", dicValues["SoTienNo"].ToString());
    //                    else
    //                        where += string.Format(" ,SoTienNo={0} ", dicValues["SoTienNo"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SoTienNoKhachTra"))
    //                if (dicValues["SoTienNoKhachTra"] != null && dicValues["SoTienNoKhachTra"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SoTienNoKhachTra={0} ", dicValues["SoTienNoKhachTra"].ToString());
    //                    else
    //                        where += string.Format(" ,SoTienNoKhachTra={0} ", dicValues["SoTienNoKhachTra"].ToString());
    //                }

    //            if (dicValues.ContainsKey("isFinished"))
    //                if (dicValues["isFinished"] != null && dicValues["isFinished"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" isFinished='{0}' ", dicValues["isFinished"].ToString());
    //                    else
    //                        where += string.Format(" ,isFinished='{0}' ", dicValues["isFinished"].ToString());
    //                }

    //            if (dicValues.ContainsKey("DiscountNgMoiGioi"))
    //                if (dicValues["DiscountNgMoiGioi"] != null && dicValues["DiscountNgMoiGioi"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" DiscountNgMoiGioi={0} ", dicValues["DiscountNgMoiGioi"].ToString());
    //                    else
    //                        where += string.Format(" ,DiscountNgMoiGioi={0} ", dicValues["DiscountNgMoiGioi"].ToString());
    //                }

    //            if (dicValues.ContainsKey("DiscountCust"))
    //                if (dicValues["DiscountCust"] != null && dicValues["DiscountCust"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" DiscountCust={0} ", dicValues["DiscountCust"].ToString());
    //                    else
    //                        where += string.Format(" ,DiscountCust={0} ", dicValues["DiscountCust"].ToString());
    //                }

    //            if (dicValues.ContainsKey("isDiscount"))
    //                if (dicValues["isDiscount"] != null && dicValues["isDiscount"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" isDiscount='{0}' ", dicValues["isDiscount"].ToString());
    //                    else
    //                        where += string.Format(" ,isDiscount='{0}' ", dicValues["isDiscount"].ToString());
    //                }

    //            if (dicValues.ContainsKey("StatusId"))
    //                if (dicValues["StatusId"] != null && dicValues["StatusId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" StatusId={0} ", dicValues["StatusId"].ToString());
    //                    else
    //                        where += string.Format(" ,StatusId={0} ", dicValues["StatusId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("isPrinted"))
    //                if (dicValues["isPrinted"] != null && dicValues["isPrinted"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" isPrinted={0} ", dicValues["isPrinted"].ToString());
    //                    else
    //                        where += string.Format(" ,isPrinted={0} ", dicValues["isPrinted"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep01"))
    //                if (dicValues["Keep01"] != null && dicValues["Keep01"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep01=N'{0}' ", dicValues["Keep01"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep01=N'{0}' ", dicValues["Keep01"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep02"))
    //                if (dicValues["Keep02"] != null && dicValues["Keep02"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep02=N'{0}' ", dicValues["Keep02"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep02=N'{0}' ", dicValues["Keep02"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep03"))
    //                if (dicValues["Keep03"] != null && dicValues["Keep03"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep03=N'{0}' ", dicValues["Keep03"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep03=N'{0}' ", dicValues["Keep03"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep04"))
    //                if (dicValues["Keep04"] != null && dicValues["Keep04"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep04=N'{0}' ", dicValues["Keep04"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep04=N'{0}' ", dicValues["Keep04"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Space01"))
    //                if (dicValues["Space01"] != null && dicValues["Space01"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Space01={0} ", dicValues["Space01"].ToString());
    //                    else
    //                        where += string.Format(" ,Space01={0} ", dicValues["Space01"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Space02"))
    //                if (dicValues["Space02"] != null && dicValues["Space02"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Space02=N'{0}' ", dicValues["Space02"].ToString());
    //                    else
    //                        where += string.Format(" ,Space02=N'{0}' ", dicValues["Space02"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Space03"))
    //                if (dicValues["Space03"] != null && dicValues["Space03"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Space03=N'{0}' ", dicValues["Space03"].ToString());
    //                    else
    //                        where += string.Format(" ,Space03=N'{0}' ", dicValues["Space03"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SpaceId"))
    //                if (dicValues["SpaceId"] != null && dicValues["SpaceId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SpaceId={0} ", dicValues["SpaceId"].ToString());
    //                    else
    //                        where += string.Format(" ,SpaceId={0} ", dicValues["SpaceId"].ToString());
    //                }

    //            objAdd.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //            objAdd.isPrgSmField = objAdd.isPrgSmField + "|" + Session["gcUserName"].ToString() + "-" + Convert.ToDateTime(DateTime.Now);

    //            //---------------------------------------------------------------------------
    //            where += string.Format(" where Id={0} ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sql + where, zgc0GlobalStr.getSqlStr());
    //        }
    //        else if (type.ToUpper() == "DELETE")
    //        {
    //            string ObjectID = (dicValues["ObjectID"] == null) ? "" : Convert.ToString(dicValues["ObjectID"]);

    //            string sql = String.Format("Delete from gcGobal_INCOM_Receipt_Refund  ");
    //            string where = "";
    //            //---------------------------------------------------------------------------
    //            where += string.Format(" where Id={0} ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sql + where, zgc0GlobalStr.getSqlStr());

    //            sql = String.Format("Update gcGobal_INCOM_Receipt_Refund SET isPrgbUserDeleted = 1 ");
    //            where = string.Format(" where Id={0} ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sql + where, zgc0GlobalStr.getSqlStr());
    //            //----------------------------------------------------------------------------
    //            //process data after delete data
    //            gcGobal_INCOM_Receipt_RefundExt.AddAndUpdateData(ObjectID, objAdd, "post_AddAndUpdateData");
    //        }
    //        else if (type.ToUpper() == "ISPRINT")
    //        {
    //            string ObjectID = (dicValues["ObjectID"] == null) ? "" : Convert.ToString(dicValues["ObjectID"]);

    //            string where = "";


    //            //--------------------------------------------------------------------------------------
    //            if (dicValues.ContainsKey("isFinished"))
    //            {
    //                filter = " isFinished = 'true' ";
    //            }
    //            else if (dicValues.ContainsKey("isPrinted"))
    //            {
    //                filter = " isPrinted = 1 ";
    //            }
    //            else if (dicValues.ContainsKey("isPrgOrdered"))
    //            {
    //                filter = " isPrgOrdered = 1 ";
    //            }
    //            string sqlnew = String.Format("UPDATE gcGobal_INCOM_Receipt_Refund SET ") + filter;
    //            //---------------------------------------------------------------------------
    //            where += string.Format(" where Id={0}  ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sqlnew + where, zgc0GlobalStr.getSqlStr());
    //            //----------------------------------------------------------------------------
    //            //process data after print data
    //            return new { Result = "OK", Record = result, Msg = "Printed" };
    //        }
    //        WSgcGobal_INCOM_Receipt_Refund obj = new WSgcGobal_INCOM_Receipt_Refund();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_INCOM_Receipt_Refund");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);

    //        //----------------------------------------------------------------------------
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message };
    //    }
    //    //----------------------------------------------------------------------------
    //    return new { Result = "OK", Record = result };
    //}

};
//{0}replace

