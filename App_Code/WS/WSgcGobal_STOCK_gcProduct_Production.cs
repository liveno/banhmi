
/*------------------------------------*/
/* Summary description for  gcGobal_STOCK_gcProduct_Production */
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
[WebService(Namespace = "http://www.anhxuan.com.vn/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WSgcGobal_STOCK_gcProduct_Production : System.Web.Services.WebService
{

    //public List<object> AddData(object obj, object HISobj, int startIndex,
    //                                        int maximumRows, string sortExpressions, string filter, string user)
    //{
    //    List<object> list = new List<object>();
    //    try
    //    {
    //        startIndex = 0; string IdOut = "";
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preAdd(ref list, obj, ref  startIndex, ref  maximumRows, ref sortExpressions, ref filter, "preAdd_gcGobal_STOCK_gcProduct_Production");
    //        AddObjectLock((gcGobal_STOCK_gcProduct_Production)obj, ref IdOut);
    //        if (IdOut == "DONE")
    //            if (bReturn) bReturn = gcGobal_STOCK_gcProduct_Production.AddObject(obj, ref IdOut, (SqlCommand)null, zgc0GlobalStr.getSqlStr());
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "ADDDATA", "zgcl_gcGobal_STOCK_gcProduct_Production03", "AddData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        ((gcGobal_STOCK_gcProduct_Production)obj).IdOut = IdOut;



    //        if (bReturn) bReturn = gcGobal_STOCK_gcProduct_ProductionExt.postAdd(ref list, obj, ref startIndex, ref maximumRows, ref sortExpressions, ref filter, "postAdd_gcGobal_STOCK_gcProduct_Production");

    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Production03");
    //        list = GetDataIner(startIndex, maximumRows, sortExpressions, filter, "", user);

    //        //---------------------------------------------------------
    //        //success get data
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)getGetNewValueMaP170(user);
    //        list.Add(tail);
    //        return list;
    //    }
    //    catch (Exception e)
    //    {
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "ADDDATA", "zgcl_gcGobal_STOCK_gcProduct_Production03", "AddData - file: WSgcGobal_STOCK_gcProduct_Production.cs" + e.Message.ToString(), DateTime.Now);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail.errMsg = "Lỗi ở hàm: [AddData - file: WSgcGobal_STOCK_gcProduct_Production.cs]." + e.Message.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetData(int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{
    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_STOCK_gcProduct_Production03";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Production");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Production03");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Production03", "GetData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_STOCK_gcProduct_Production03 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Production03(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Production03");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    tail = (zgc0GlobalTail)getGetNewValueMaP170(user);
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    //public List<object> GetDataIner(int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_STOCK_gcProduct_Production03";
    //    string columns = "*";
    //    int bSql = -1;

    //    List<object> list = new List<object>();
    //    bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Production");
    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Production03");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Production03", "GetData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_STOCK_gcProduct_Production03 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Production03(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------
    //    zgc0GlobalTail tail = new zgc0GlobalTail();

    //    bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preProcessCount(ref tail, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Production03");
    //    if (bReturn) tail.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tail);
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> DeleteData(int Id, string startIndex,
    //                                        int maximumRows, string sortExpressions, string filter, string user)
    //{
    //    List<object> list = new List<object>();
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    try
    //    {
    //        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        {
    //            tail.errMsg = "Không có quyền truy cập dữ liệu";
    //            list.Add(tail);
    //            return list;
    //        }
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preDelete(Id, ref startIndex, ref  maximumRows, ref  sortExpressions, ref filter, "preDelete_gcGobal_STOCK_gcProduct_Production");
    //        if (bReturn) bReturn = zgc0Support.ProcessDel(Id, "gcGobal_STOCK_gcProduct_Production");
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "DELETE", "zgcl_gcGobal_STOCK_gcProduct_Production03", "Delete - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        if (bReturn) bReturn = gcGobal_STOCK_gcProduct_ProductionExt.postDelete(Id, ref  startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "postDelete_gcGobal_STOCK_gcProduct_Production");
    //    }
    //    catch (Exception e)
    //    {
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "DELETE", "zgcl_gcGobal_STOCK_gcProduct_Production03", "Delete - file: WSgcGobal_STOCK_gcProduct_Production.cs" + e.Message.ToString(), DateTime.Now);
    //        tail.errMsg = "Lỗi ở hàm: [Delete - file: WSCTY_Branch.cs]." + e.Message.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //    int index = int.Parse(startIndex);
    //    zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_STOCK_gcProduct_Production");
    //    list = GetDataIner(index, maximumRows, sortExpressions, filter, "", user);

    //    //---------------------------------------------------------
    //    //success get data
    //    list.Add(tail);
    //    return list;
    //}
    //public List<object> UpdateData(object obj, object HISobj, string startIndex,
    //                                        int maximumRows, string sortExpressions, string filter, string user)
    //{
    //    try
    //    {
    //        List<object> list = new List<object>();
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preUpdate(ref list, obj, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "preUpdate_gcGobal_STOCK_gcProduct_Production");
    //        if (bReturn) bReturn = gcGobal_STOCK_gcProduct_Production.UpdateObject(obj, (SqlCommand)null, zgc0GlobalStr.getSqlStr());
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "UPDATE", "zgcl_gcGobal_STOCK_gcProduct_Production03", "UpdateData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        if (bReturn) bReturn = gcGobal_STOCK_gcProduct_ProductionExt.postUpdate(ref list, obj, ref  startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "postUpdate_gcGobal_STOCK_gcProduct_Production");
    //        int index = int.Parse(startIndex);



    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_STOCK_gcProduct_Production");
    //        list = GetDataIner(int.Parse(startIndex), maximumRows, sortExpressions, filter, "", user);

    //        //---------------------------------------------------------
    //        //success get data
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        list.Add(tail);
    //        return list;
    //    }
    //    catch (Exception e)
    //    {
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "UPDATE", "zgcl_gcGobal_STOCK_gcProduct_Production03", "UpdateData - file: WSgcGobal_STOCK_gcProduct_Production.cs" + e.Message.ToString(), DateTime.Now);
    //        List<object> list = new List<object>();
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail.errMsg = "Lỗi ở hàm: [UpdateData - file: WSgcGobal_STOCK_gcProduct_Production.cs]." + e.Message.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> ValidForm(object objjs, string startIndex,
    //        int maximumRows, string sortExpressions, string filter, int type, string user)
    //{
    //    List<object> list = new List<object>();
    //    try
    //    {
    //        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        {
    //            zgc0GlobalTail tail = new zgc0GlobalTail();
    //            tail.errMsg = "Không có quyền truy cập dữ liệu";
    //            list.Add(tail);
    //            return list;
    //        }
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_STOCK_gcProduct_Production");
    //        object HISobj = null;
    //        gcGobal_STOCK_gcProduct_Production obj = ParseObject(objjs, ref HISobj, bHISTable, type);
    //        string tmpValue = "";
    //        bool bRet = true;

    //        if (type == 1)
    //            if (zgc0CheckFunc.CheckExsist(Ten.ToString(), "Ten", "gcGobal_STOCK_gcProduct_Production"))
    //            {
    //                tmpValue += zgc0CheckFunc.getMsgCheckExsist("Mã chứng từ  ", Ten.ToString());
    //                bRet = false;
    //            }
    //        if (type == 0)
    //            if (zgc0CheckFunc.CheckExsist(Id.Value, Ten.ToString(), "Ten", "gcGobal_STOCK_gcProduct_Production"))
    //            {
    //                tmpValue += zgc0CheckFunc.getMsgCheckExsist("Mã chứng từ  ", Ten.ToString());
    //                bRet = false;
    //            }

    //        if (zgc0CheckFunc.CheckNULL(KhoXuatId))
    //        {
    //            tmpValue += zgc0CheckFunc.getMsgNULL("Kho xuất ");
    //            bRet = false;
    //        }
    //        if (zgc0CheckFunc.CheckNULL(KhoNhapId))
    //        {
    //            tmpValue += zgc0CheckFunc.getMsgNULL("Kho nhập ");
    //            bRet = false;
    //        }
    //        gcGobal_STOCK_gcProduct_ProductionExt.postCheckContrainData(obj, ref bRet, type, ref tmpValue, "postCheckContrainData_gcGobal_STOCK_gcProduct_Production");

    //        string buildStr = zgc0CheckFunc.FormatDiv(tmpValue, "Có lỗi xảy ra khi nhập liệu");
    //        if (bRet)
    //        {
    //            //type ==0 Update
    //            if (type == 0)
    //            {
    //                return UpdateData(obj, HISobj, (startIndex), maximumRows, sortExpressions, filter, user);
    //            }
    //            else
    //            {
    //                return AddData(obj, HISobj, int.Parse(startIndex), maximumRows, sortExpressions, filter, user);
    //            }
    //        }
    //        else
    //        {
    //            zgc0GlobalTail tail = new zgc0GlobalTail();
    //            tail.errMsg = buildStr;
    //            list.Add(tail);
    //            return list;
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        System.Text.StringBuilder strBuild = new System.Text.StringBuilder();
    //        strBuild.AppendLine("Có lỗi xảy ra. Vui lòng thử lại!");
    //        strBuild.AppendLine("Lỗi ở hàm: [Valid - file: gcGobal_STOCK_gcProduct_Production.cs].");
    //        strBuild.AppendLine(e.Message.ToString());
    //        zgc0Project.LogErrorOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "VALIDFORM", "gcGobal_STOCK_gcProduct_Production", "ValidForm - file: WSgcGobal_STOCK_gcProduct_Production.cs" + strBuild.ToString(), DateTime.Now);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail.errMsg = strBuild.ToString();
    //        list.Add(tail);
    //        return list;
    //    }
    //}
    //public gcGobal_STOCK_gcProduct_Production ParseObject(object objOld, ref object HISobj, bool bHISTable, int type)
    //{
    //    HISobj = null;
    //    gcGobal_STOCK_gcProduct_Production obj = new gcGobal_STOCK_gcProduct_Production();
    //    Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //    dicValues = (Dictionary<string, object>)objOld;
    //    if (dicValues["Id"] != null && dicValues["Id"].ToString() == "-9999")
    //        return obj;
    //    Id = (dicValues["Id"] == null) ? (int?)null : Convert.ToInt32(dicValues["Id"]);
    //    Ten = (dicValues["Ten"] == null) ? (string)null : Convert.ToString(dicValues["Ten"]);
    //    SoCT = (dicValues["SoCT"] == null) ? (string)null : Convert.ToString(dicValues["SoCT"]);
    //    NgayLap = (dicValues["NgayLap"] == null) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayLap"]);
    //    NCCId = (dicValues["NCCId"] == null) ? (int?)null : Convert.ToInt32(dicValues["NCCId"]);
    //    TienThue = (dicValues["TienThue"] == null) ? (double?)null : Convert.ToDouble(dicValues["TienThue"]);
    //    TongTien = (dicValues["TongTien"] == null) ? (double?)null : Convert.ToDouble(dicValues["TongTien"]);
    //    NhanvienId = (dicValues["NhanvienId"] == null) ? (int?)null : Convert.ToInt32(dicValues["NhanvienId"]);
    //    DaTra = (dicValues["DaTra"] == null) ? (double?)null : Convert.ToDouble(dicValues["DaTra"]);
    //    NoLai = (dicValues["NoLai"] == null) ? (double?)null : Convert.ToDouble(dicValues["NoLai"]);
    //    KhoXuatId = (dicValues["KhoXuatId"] == null) ? (int?)null : Convert.ToInt32(dicValues["KhoXuatId"]);
    //    KhoNhapId = (dicValues["KhoNhapId"] == null) ? (int?)null : Convert.ToInt32(dicValues["KhoNhapId"]);
    //    DienGiai = (dicValues["DienGiai"] == null) ? (string)null : Convert.ToString(dicValues["DienGiai"]);
    //    StatusId = (dicValues["StatusId"] == null) ? (int?)null : Convert.ToInt32(dicValues["StatusId"]);
    //    Keep01 = (dicValues["Keep01"] == null) ? (string)null : Convert.ToString(dicValues["Keep01"]);
    //    Keep02 = (dicValues["Keep02"] == null) ? (string)null : Convert.ToString(dicValues["Keep02"]);
    //    Keep03 = (dicValues["Keep03"] == null) ? (string)null : Convert.ToString(dicValues["Keep03"]);
    //    Keep04 = (dicValues["Keep04"] == null) ? (string)null : Convert.ToString(dicValues["Keep04"]);
    //    Space01 = (dicValues["Space01"] == null) ? (double?)null : Convert.ToDouble(dicValues["Space01"]);
    //    Space02 = (dicValues["Space02"] == null) ? (string)null : Convert.ToString(dicValues["Space02"]);
    //    Space03 = (dicValues["Space03"] == null) ? (DateTime?)null : Convert.ToDateTime(dicValues["Space03"]);
    //    SpaceId = (dicValues["SpaceId"] == null) ? (int?)null : Convert.ToInt32(dicValues["SpaceId"]);
    //    if (type != 0) isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());
    //    else isPrgAccountId = (dicValues["isPrgAccountId"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgAccountId"]);
    //    isPrgInUse = (dicValues["isPrgInUse"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgInUse"]);
    //    isPrgCreateDate = Convert.ToDateTime(DateTime.Now);
    //    isPrgWaitingConfirmStatus = (dicValues["isPrgWaitingConfirmStatus"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgWaitingConfirmStatus"]);
    //    isPrgbAdminDeleted = (dicValues["isPrgbAdminDeleted"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgbAdminDeleted"]);
    //    isPrgbUserDeleted = (dicValues["isPrgbUserDeleted"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgbUserDeleted"]);
    //    isPrgbShow = (dicValues["isPrgbShow"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgbShow"]);
    //    isPrgOrdered = (dicValues["isPrgOrdered"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgOrdered"]);
    //    isPrgVNKoDau = (string)null;//(dicValues["isPrgVNKoDau"] == null) ? (string)null : Convert.ToString(dicValues["isPrgVNKoDau"]);
    //    isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //    if (type != 0) isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //    else isPrgPartComp = (dicValues["isPrgPartComp"] == null) ? (string)null : Convert.ToString(dicValues["isPrgPartComp"]);
    //    isPrgEncriptData = (dicValues["isPrgEncriptData"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgEncriptData"]);
    //    isPrgDescriptData = (dicValues["isPrgDescriptData"] == null) ? (int?)null : Convert.ToInt32(dicValues["isPrgDescriptData"]);
    //    isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //    gcGobal_STOCK_gcProduct_ProductionExt.postAssignment(obj, "postAssignment_gcGobal_STOCK_gcProduct_Production");

    //    return obj;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object getGetNewValueP170(string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return null;
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    string tmpValue = "";
    //    tmpValue = zgc0Support.getAutoGenCode(null, "Ten", "zgcBUILDIN_GOBAL_AutoGenCode", "Date", "PXK", "4", "Text", "SoCT", "", "", "gcGobal_STOCK_gcProduct_Production");

    //    tail.strNewCode = tmpValue;
    //    gcGobal_STOCK_gcProduct_ProductionExt.PostAutoGenCode(ref tail, "PostgetGetNewValueP170");
    //    return tail;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object getGetNewValueMaP170(string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return null;
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    string tmpValue = "";
    //    tmpValue = zgc0Support.getAutoGenCode(null, "SoCT", "zgcBUILDIN_GOBAL_AutoGenCode", "Normal", "PXK", "8", "Text", "SoCT", "", "", "gcGobal_STOCK_gcProduct_Production");

    //    tail.strNewCode = tmpValue;
    //    gcGobal_STOCK_gcProduct_ProductionExt.PostAutoGenCode(ref tail, "PostgetGetNewValueMaP170");
    //    return tail;
    //}
    //public int AddObjectLock(gcGobal_STOCK_gcProduct_Production obj, ref string IdOut)
    //{
    //    int bReturn = 1;
    //    IdOut = "";
    //    {

    //        try
    //        {
    //            string sql = "select Ten from gcGobal_STOCK_gcProduct_Production where Ten='" + Ten + "'";
    //            DataTable tbl = zgc0HelperSecurity.GetDataTableNew(sql, zgc0GlobalStr.getSqlStr());

    //            if (tbl != null && tbl.Rows.Count > 0)
    //                Ten = zgc0Support.getAutoGenCode((SqlCommand)null, "Ten", "zgcBUILDIN_GOBAL_AutoGenCode", "Date", "PXK", "4", "Text", "SoCT", "", "", "gcGobal_STOCK_gcProduct_Production");
    //            gcGobal_STOCK_gcProduct_Production.AddObject(obj, ref IdOut, (SqlCommand)null, zgc0GlobalStr.getSqlStr());
    //        }
    //        catch (Exception e)
    //        {
    //            try
    //            {
    //            }
    //            catch (SqlException ex)
    //            {
    //                {
    //                    bReturn = -1;
    //                }
    //                zgc0HelperSecurity.NoneException(ex);
    //            }
    //            bReturn = -2;
    //            zgc0HelperSecurity.NoneException(e);
    //        }
    //    }
    //    return bReturn;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public int GetCount(string filter, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return -1;
    //    int value = -1;
    //    string view = "zgcl_gcGobal_STOCK_gcProduct_Production03";
    //    string columns = "*";
    //    bool bReturn = zgc0Project.preProcessCount(ref value, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Production03");
    //    if (bReturn) return zgc0Support.ProcessCount(filter, view, columns);
    //    return value;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getNhanvienIdHoTen(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_COMP_EmployeeLife13";
    //        bool bCheckFilter = true;
    //        string strExtentField = "HoTen";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_STOCK_gcProduct_Production");
    //        object HISobj = null;
    //        gcGobal_STOCK_gcProduct_Production obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getNhanvienIdHoTen");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_COMP_EmployeeLife13");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getNhanvienIdHoTen(string prefixText, int count, string strFil, gcGobal_STOCK_gcProduct_Production obj) - file: WSgcGobal_STOCK_gcProduct_Production.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getKhoXuatIdName(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_STOCK_List00";
    //        bool bCheckFilter = true;
    //        string strExtentField = "Name";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_STOCK_gcProduct_Production");
    //        object HISobj = null;
    //        gcGobal_STOCK_gcProduct_Production obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getKhoXuatIdName");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_STOCK_List00");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getKhoXuatIdName(string prefixText, int count, string strFil, gcGobal_STOCK_gcProduct_Production obj) - file: WSgcGobal_STOCK_gcProduct_Production.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getKhoNhapIdName(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_STOCK_List00";
    //        bool bCheckFilter = true;
    //        string strExtentField = "Name";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_STOCK_gcProduct_Production");
    //        object HISobj = null;
    //        gcGobal_STOCK_gcProduct_Production obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getKhoNhapIdName");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_STOCK_List00");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getKhoNhapIdName(string prefixText, int count, string strFil, gcGobal_STOCK_gcProduct_Production obj) - file: WSgcGobal_STOCK_gcProduct_Production.cs]." + e.Message.ToString());
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object[]> getStatusIdName(string prefixText, int count, string strFil, object objjs, string user)
    //{
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //        return new List<object[]>();
    //    try
    //    {
    //        List<object[]> list = new List<object[]>();
    //        string strWhere = (strFil == null) ? null : (strFil.Trim().Length < 1) ? null : strFil;
    //        string strViewName = "zgcl_gcGobal_INCOM_Status00";
    //        bool bCheckFilter = true;
    //        string strExtentField = "Name";
    //        bool bHISTable = zgc0Support.getHISTable("gcGobal_STOCK_gcProduct_Production");
    //        object HISobj = null;
    //        gcGobal_STOCK_gcProduct_Production obj = ParseObject(objjs, ref HISobj, bHISTable, 0);
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGetCmb(ref list, ref  count, ref  strFil, ref strViewName, ref strExtentField, obj, ref bCheckFilter, prefixText, "preGetCmb_getStatusIdName");
    //        if (!bReturn) return list;
    //        zgc0GobalService.SetupRightComboFilter(ref strFil, "zgcl_gcGobal_INCOM_Status00");
    //        return zgc0Helper.gcGetObjectWS(prefixText, count, strViewName, strExtentField, strFil, obj, bCheckFilter);
    //    }
    //    catch (Exception e)
    //    {
    //        throw new Exception("Lỗi ở hàm: [getStatusIdName(string prefixText, int count, string strFil, gcGobal_STOCK_gcProduct_Production obj) - file: WSgcGobal_STOCK_gcProduct_Production.cs]." + e.Message.ToString());
    //    }
    //}
    ////----------------------------------------------------------------------------------
    //// 19-2 get BILL empty
    ////filter set from client form
    ////filter = " (isFinished is null OR isFinished = 0) ";
    ////filter = " (isPrinted is null OR isPrinted = 0) ";
    ////filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetDataToDay(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{

    //    IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

    //    //--------------------------------------------------------------------------------------
    //    if (tmpDict.ContainsKey("isFinished"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " ( isFinished = 1) ";
    //        else
    //            filter += " AND (isFinished = 1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrinted"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " ( isPrinted = 1) ";
    //        else
    //            filter += " AND (isPrinted =1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrgOrdered"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrgOrdered = 1) ";
    //        else
    //            filter += " AND (isPrgOrdered = 1) ";
    //    }

    //    //--------------------------------------------------------------------------------------

    //    if (tmpDict.ContainsKey("NgayLap"))
    //    {
    //        string t1 = DateTime.Now.ToShortDateString() + " 0:00";
    //        string t2 = DateTime.Now.ToShortDateString() + " 23:59";
    //        if (filter == null || filter.Length <= 0)
    //            filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        else
    //        {
    //            filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        }
    //    }
    //    gcGobal_STOCK_gcProduct_ProductionExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
    //    //--------------------------------------------------------------------------------------

    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_STOCK_gcProduct_Production03";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Production");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Production03");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Production03", "GetData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_STOCK_gcProduct_Production03 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Production03(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Production03");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    //
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    ////----------------------------------------------------------------------------------
    //// 19-2 get BILL empty
    ////filter set from client form
    ////filter = " (isFinished is null OR isFinished = 0) ";
    ////filter = " (isPrinted is null OR isPrinted = 0) ";
    ////filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetDataAllDay(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user, string t1, string t2)
    //{

    //    IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

    //    //--------------------------------------------------------------------------------------
    //    if (tmpDict.ContainsKey("isFinished"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isFinished = 1) ";
    //        else
    //            filter += " AND (isFinished = 1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrinted"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrinted = 1) ";
    //        else
    //            filter += " AND (isPrinted = 1) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrgOrdered"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrgOrdered = 1) ";
    //        else
    //            filter += " AND (isPrgOrdered = 1) ";
    //    }

    //    //--------------------------------------------------------------------------------------

    //    if (tmpDict.ContainsKey("NgayLap"))
    //    {
    //        t1 = t1 + " 0:00";
    //        t2 = t2 + " 23:59";
    //        if (filter == null || filter.Length <= 0)
    //            filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        else
    //        {
    //            filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        }
    //    }
    //    gcGobal_STOCK_gcProduct_ProductionExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
    //    //--------------------------------------------------------------------------------------

    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_STOCK_gcProduct_Production03";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Production");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Production03");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Production03", "GetData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_STOCK_gcProduct_Production03 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Production03(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Production03");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    //
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object CreatInput(string TableID, string itemID)
    //{
    //    string IdOut = "";
    //    gcGobal_STOCK_gcProduct_Production obj = new gcGobal_STOCK_gcProduct_Production();

    //    isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //    isPrgVNKoDau = Session["gcUserName"].ToString();
    //    isPrgCreateDate = DateTime.Now;
    //    isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //    isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //    isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));


    //    gcGobal_STOCK_gcProduct_ProductionExt.SetNewObject(obj, "post_SetNewObject");
    //    try
    //    {
    //        gcGobal_STOCK_gcProduct_Production.AddObject(obj, ref IdOut, null, zgc0GlobalStr.getSqlStr());
    //        //sau khi thêm dữ liệu vào rồi bây giờ lấy ID của cái hàng mới nhất về.
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message, itemID = itemID };
    //    }
    //    return new { Result = "OK", ReceiptId = IdOut, itemID = itemID };
    //}

    ////----------------------------------------------------------------------------------
    //// 19-2 get BILL empty
    ////filter set from client form
    ////filter = " (isFinished is null OR isFinished = 0) ";
    ////filter = " (isPrinted is null OR isPrinted = 0) ";
    ////filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public List<object> GetData1Item(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    //{

    //    IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

    //    //--------------------------------------------------------------------------------------
    //    if (tmpDict.ContainsKey("isFinished"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isFinished is null OR isFinished = 0) ";
    //        else
    //            filter += " AND (isFinished is null OR isFinished = 0) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrinted"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrinted is null OR isPrinted = 0) ";
    //        else
    //            filter += " AND (isPrinted is null OR isPrinted = 0) ";
    //    }
    //    else if (tmpDict.ContainsKey("isPrgOrdered"))
    //    {
    //        if (filter == null || filter.Length <= 0)
    //            filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //        else
    //            filter += " AND (isPrgOrdered is null OR isPrgOrdered = 0) ";
    //    }

    //    //--------------------------------------------------------------------------------------
    //    zgc0GlobalTail tailP = new zgc0GlobalTail();
    //    List<object> list = new List<object>();
    //    if (HttpContext.Current.Session["gcAccountId"] == null)
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }

    //    if (tmpDict.ContainsKey("NgayLap"))
    //    {
    //        string t1 = DateTime.Now.ToShortDateString() + " 0:00";
    //        string t2 = DateTime.Now.ToShortDateString() + " 23:59";
    //        if (filter == null || filter.Length <= 0)
    //            filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        else
    //        {
    //            filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
    //        }
    //    }

    //    string gcAccountId = HttpContext.Current.Session["gcAccountId"].ToString();
    //    if (filter == null || filter.Length <= 0)
    //        filter = String.Format(" ( isPrgAccountId={0} )", gcAccountId);
    //    else
    //    {
    //        filter += String.Format(" AND ( isPrgAccountId={0} )", gcAccountId);
    //    }

    //    gcGobal_STOCK_gcProduct_ProductionExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
    //    //--------------------------------------------------------------------------------------


    //    if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
    //    {
    //        tailP.errMsg = "Không có quyền truy cập dữ liệu";
    //        list.Add(tailP);
    //        return list;
    //    }
    //    startIndex = startIndex + 1;
    //    string view = "zgcl_gcGobal_STOCK_gcProduct_Production03";
    //    string columns = "*";
    //    int bSql = -1;
    //    bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Production");

    //    if (bReturn)
    //    {
    //        zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Production03");
    //        DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
    //        //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Production03", "GetData - file: WSgcGobal_STOCK_gcProduct_Production.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
    //        for (int m = 0; m < myTable.Rows.Count; m++)
    //        {
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_STOCK_gcProduct_Production03 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Production03(myTable.Rows[m], isHasRowNum);
    //            list.Add(newEmployee);
    //        }
    //        //--------------------------------------------------------------------------------------
    //        // if data don't have, we add 1 item empty
    //        // we should set it in extention file to get option data
    //        if (myTable.Rows.Count <= 0)
    //        {
    //            WSgcGobal_STOCK_gcProduct_Production gc = new WSgcGobal_STOCK_gcProduct_Production();
    //            bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
    //            zgcl_gcGobal_STOCK_gcProduct_Production03 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Production03();

    //            newEmployee.Id = -1;
    //            newEmployee.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //            newEmployee.isPrgVNKoDau = Session["gcUserName"].ToString();
    //            newEmployee.isPrgCreateDate = DateTime.Now;
    //            newEmployee.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //            newEmployee.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //            newEmployee.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

    //            gcGobal_STOCK_gcProduct_ProductionExt.postAssignmentEmtyData(newEmployee, "postAssignmentEmtyData_Empty");
    //            list.Add(newEmployee);
    //        }
    //    }
    //    //----------------------------------------------------------

    //    zgc0GlobalTail tailNew = new zgc0GlobalTail();
    //    bReturn = gcGobal_STOCK_gcProduct_ProductionExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Production03");
    //    if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
    //    zgc0Support.ProcessAccountRight(ref tailNew);
    //    list.Add(tailNew);

    //    //---------------------------------------------------------
    //    //success get data
    //    zgc0GlobalTail tail = new zgc0GlobalTail();
    //    //
    //    list.Add(tail);
    //    //----------------------------------------------------------
    //    return list;
    //}
    ////--------------------------------------------------------------------------------------------
    ////jQueryTableDelete
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object jQueryTableDelete(string Id, string ReceitpID, int startIndex, int maximumRows,
    //    string sortExpressions, string filter, string bSecurity, string user)
    //{
    //    int TotalRecordCount = 0;
    //    List<object> result = new List<object>();

    //    try
    //    {
    //        string sql = String.Format("Delete FROM gcGobal_STOCK_gcProduct_Production Where Id={0}", Id);
    //        zgc0HelperSecurity.ExecuteNonQuery(sql, zgc0GlobalStr.getSqlStr());
    //        //---------------------------------------------------------------------
    //        // Process extension, if have detail, we update total Money,...
    //        bool bReturn = gcGobal_STOCK_gcProduct_ProductionExt.postjQueryTableDelete(ReceitpID, result, ref  startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, "post_postjQueryTableDelete");
    //        if (!bReturn)
    //        {
    //            return new { Result = "OK", Records = result, TotalRecordCount = TotalRecordCount };
    //        }
    //        //---------------------------------------------------------------------

    //        //--------------------------------------------------------------------
    //        //get data from database after delete
    //        WSgcGobal_STOCK_gcProduct_Production obj = new WSgcGobal_STOCK_gcProduct_Production();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_STOCK_gcProduct_Production");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);
    //        //---------------------------------------------------------------------
    //        result.RemoveAt(result.Count - 1);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)result[result.Count - 1];
    //        result.RemoveAt(result.Count - 1);
    //        //update total item
    //        TotalRecordCount = tail.numObj;
    //        //---------------------------------------------------------------------
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message };
    //    }

    //    //------------------------------------------------------------------------
    //    return new { Result = "OK", Records = result, TotalRecordCount = TotalRecordCount };
    //}
    ////--------------------------------------------------------------------------------------------
    ////jQueryTableGetData
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object jQueryTableGetData(object record, int jtStartIndex, int jtPageSize)
    //{
    //    int TotalRecordCount = 0;
    //    List<object> result = new List<object>();
    //    try
    //    {
    //        Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //        dicValues = (Dictionary<string, object>)record;

    //        int startIndex = (dicValues["startIndex"] == null) ? 0 : Convert.ToInt32(dicValues["startIndex"]);
    //        int maximumRows = (dicValues["maximumRows"] == null) ? 1 : Convert.ToInt32(dicValues["maximumRows"]);
    //        string sortExpressions = (dicValues["sortExpressions"] == null) ? "" : Convert.ToString(dicValues["sortExpressions"]);
    //        string filter = (dicValues["filter"] == null) ? "" : Convert.ToString(dicValues["filter"]);
    //        string bSecurity = (dicValues["bSecurity"] == null) ? "" : Convert.ToString(dicValues["bSecurity"]);
    //        string user = (dicValues["user"] == null) ? "" : Convert.ToString(dicValues["user"]);

    //        maximumRows = jtPageSize;
    //        startIndex = jtStartIndex;
    //        if (jtStartIndex > 0)
    //            startIndex = jtPageSize / jtStartIndex;

    //        WSgcGobal_STOCK_gcProduct_Production obj = new WSgcGobal_STOCK_gcProduct_Production();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_STOCK_gcProduct_Production");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);
    //        //----------------------------------------------------------
    //        result.RemoveAt(result.Count - 1);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)result[result.Count - 1];
    //        result.RemoveAt(result.Count - 1);
    //        TotalRecordCount = tail.numObj;
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message, TotalRecordCount = TotalRecordCount };
    //    }
    //    //----------------------------------------------------------
    //    return new { Result = "OK", Records = result, TotalRecordCount = TotalRecordCount };
    //}

    ////----------------------------------------------------------------
    ////jQueryTableAddData
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object jQueryTableAddData(object record)
    //{
    //    Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //    dicValues = (Dictionary<string, object>)record;

    //    string type = (dicValues["type"] == null) ? "" : Convert.ToString(dicValues["type"]);

    //    List<object> result = new List<object>();
    //    string IdOut = "";
    //    int TotalRecordCount = 0;
    //    gcGobal_STOCK_gcProduct_Production objAdd = new gcGobal_STOCK_gcProduct_Production();

    //    try
    //    {
    //        //scan column in here
    //        if (dicValues.ContainsKey("Ten"))
    //            objAdd.Ten = ((dicValues["Ten"] == null) || (dicValues["Ten"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Ten"]);

    //        if (dicValues.ContainsKey("SoCT"))
    //            objAdd.SoCT = ((dicValues["SoCT"] == null) || (dicValues["SoCT"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["SoCT"]);

    //        if (dicValues.ContainsKey("NgayLap"))
    //            objAdd.NgayLap = ((dicValues["NgayLap"] == null) || (dicValues["NgayLap"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayLap"]);

    //        if (dicValues.ContainsKey("NCCId"))
    //            objAdd.NCCId = ((dicValues["NCCId"] == null) || (dicValues["NCCId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["NCCId"]);

    //        if (dicValues.ContainsKey("TienThue"))
    //            objAdd.TienThue = ((dicValues["TienThue"] == null) || (dicValues["TienThue"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["TienThue"]);

    //        if (dicValues.ContainsKey("TongTien"))
    //            objAdd.TongTien = ((dicValues["TongTien"] == null) || (dicValues["TongTien"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["TongTien"]);

    //        if (dicValues.ContainsKey("NhanvienId"))
    //            objAdd.NhanvienId = ((dicValues["NhanvienId"] == null) || (dicValues["NhanvienId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["NhanvienId"]);

    //        if (dicValues.ContainsKey("DaTra"))
    //            objAdd.DaTra = ((dicValues["DaTra"] == null) || (dicValues["DaTra"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["DaTra"]);

    //        if (dicValues.ContainsKey("NoLai"))
    //            objAdd.NoLai = ((dicValues["NoLai"] == null) || (dicValues["NoLai"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["NoLai"]);

    //        if (dicValues.ContainsKey("KhoXuatId"))
    //            objAdd.KhoXuatId = ((dicValues["KhoXuatId"] == null) || (dicValues["KhoXuatId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["KhoXuatId"]);

    //        if (dicValues.ContainsKey("KhoNhapId"))
    //            objAdd.KhoNhapId = ((dicValues["KhoNhapId"] == null) || (dicValues["KhoNhapId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["KhoNhapId"]);

    //        if (dicValues.ContainsKey("DienGiai"))
    //            objAdd.DienGiai = ((dicValues["DienGiai"] == null) || (dicValues["DienGiai"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["DienGiai"]);

    //        if (dicValues.ContainsKey("StatusId"))
    //            objAdd.StatusId = ((dicValues["StatusId"] == null) || (dicValues["StatusId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["StatusId"]);

    //        if (dicValues.ContainsKey("Keep01"))
    //            objAdd.Keep01 = ((dicValues["Keep01"] == null) || (dicValues["Keep01"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep01"]);

    //        if (dicValues.ContainsKey("Keep02"))
    //            objAdd.Keep02 = ((dicValues["Keep02"] == null) || (dicValues["Keep02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep02"]);

    //        if (dicValues.ContainsKey("Keep03"))
    //            objAdd.Keep03 = ((dicValues["Keep03"] == null) || (dicValues["Keep03"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep03"]);

    //        if (dicValues.ContainsKey("Keep04"))
    //            objAdd.Keep04 = ((dicValues["Keep04"] == null) || (dicValues["Keep04"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep04"]);

    //        if (dicValues.ContainsKey("Space01"))
    //            objAdd.Space01 = ((dicValues["Space01"] == null) || (dicValues["Space01"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["Space01"]);

    //        if (dicValues.ContainsKey("Space02"))
    //            objAdd.Space02 = ((dicValues["Space02"] == null) || (dicValues["Space02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Space02"]);

    //        if (dicValues.ContainsKey("Space03"))
    //            objAdd.Space03 = ((dicValues["Space03"] == null) || (dicValues["Space03"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["Space03"]);

    //        if (dicValues.ContainsKey("SpaceId"))
    //            objAdd.SpaceId = ((dicValues["SpaceId"] == null) || (dicValues["SpaceId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["SpaceId"]);

    //        objAdd.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //        objAdd.isPrgVNKoDau = Session["gcUserName"].ToString();
    //        objAdd.isPrgCreateDate = DateTime.Now;
    //        objAdd.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //        objAdd.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //        objAdd.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

    //        //if (dicValues.ContainsKey("PhieuThuTienMatId"))
    //        //    objAdd.PhieuThuTienMatId = (dicValues["PhieuThuTienMatId"] == null) ? -1 : Convert.ToInt32(dicValues["PhieuThuTienMatId"]);

    //        int startIndex = (dicValues["startIndex"] == null) ? 0 : Convert.ToInt32(dicValues["startIndex"]);
    //        int maximumRows = (dicValues["maximumRows"] == null) ? 1 : Convert.ToInt32(dicValues["maximumRows"]);
    //        string sortExpressions = (dicValues["sortExpressions"] == null) ? "" : Convert.ToString(dicValues["sortExpressions"]);
    //        string filter = (dicValues["filter"] == null) ? "" : Convert.ToString(dicValues["filter"]);
    //        string bSecurity = (dicValues["bSecurity"] == null) ? "" : Convert.ToString(dicValues["bSecurity"]);
    //        string user = (dicValues["user"] == null) ? "" : Convert.ToString(dicValues["user"]);

    //        if (type.ToUpper() == "INSERT")
    //        {
    //            // tiến hành đưa số liệu vào
    //            gcGobal_STOCK_gcProduct_Production.AddObject(objAdd, ref IdOut, null, zgc0GlobalStr.getSqlStr());

    //            //----------------------------------------------------------------------------
    //            //process data after insert data
    //            gcGobal_STOCK_gcProduct_ProductionExt.jQueryTableAddData(IdOut, objAdd, "post_jQueryTableAddData");
    //            //----------------------------------------------------------------------------
    //        }

    //        WSgcGobal_STOCK_gcProduct_Production obj = new WSgcGobal_STOCK_gcProduct_Production();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_STOCK_gcProduct_Production");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);

    //        //----------------------------------------------------------------------------
    //        result.RemoveAt(result.Count - 1);
    //        zgc0GlobalTail tail = new zgc0GlobalTail();
    //        tail = (zgc0GlobalTail)result[result.Count - 1];
    //        result.RemoveAt(result.Count - 1);
    //        TotalRecordCount = tail.numObj;
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message };
    //    }
    //    //----------------------------------------------------------------------------
    //    return new { Result = "OK", Record = result[0] };
    //}

    ////----------------------------------------------------------------
    ////jQueryTableAddData
    //[WebMethod(EnableSession = true)]
    //[ScriptMethod]
    //public object AddAndUpdateData(object record)
    //{
    //    Dictionary<string, object> dicValues = new Dictionary<string, object>();
    //    dicValues = (Dictionary<string, object>)record;

    //    string type = (dicValues["type"] == null) ? "" : Convert.ToString(dicValues["type"]);

    //    List<object> result = new List<object>();
    //    string IdOut = "";
    //    int TotalRecordCount = 0;
    //    gcGobal_STOCK_gcProduct_Production objAdd = new gcGobal_STOCK_gcProduct_Production();

    //    try
    //    {
    //        //scan column in here
    //        if (dicValues.ContainsKey("Ten"))
    //            objAdd.Ten = ((dicValues["Ten"] == null) || (dicValues["Ten"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Ten"]);

    //        if (dicValues.ContainsKey("SoCT"))
    //            objAdd.SoCT = ((dicValues["SoCT"] == null) || (dicValues["SoCT"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["SoCT"]);

    //        if (dicValues.ContainsKey("NgayLap"))
    //            objAdd.NgayLap = ((dicValues["NgayLap"] == null) || (dicValues["NgayLap"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["NgayLap"]);

    //        if (dicValues.ContainsKey("NCCId"))
    //            objAdd.NCCId = ((dicValues["NCCId"] == null) || (dicValues["NCCId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["NCCId"]);

    //        if (dicValues.ContainsKey("TienThue"))
    //            objAdd.TienThue = ((dicValues["TienThue"] == null) || (dicValues["TienThue"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["TienThue"]);

    //        if (dicValues.ContainsKey("TongTien"))
    //            objAdd.TongTien = ((dicValues["TongTien"] == null) || (dicValues["TongTien"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["TongTien"]);

    //        if (dicValues.ContainsKey("NhanvienId"))
    //            objAdd.NhanvienId = ((dicValues["NhanvienId"] == null) || (dicValues["NhanvienId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["NhanvienId"]);

    //        if (dicValues.ContainsKey("DaTra"))
    //            objAdd.DaTra = ((dicValues["DaTra"] == null) || (dicValues["DaTra"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["DaTra"]);

    //        if (dicValues.ContainsKey("NoLai"))
    //            objAdd.NoLai = ((dicValues["NoLai"] == null) || (dicValues["NoLai"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["NoLai"]);

    //        if (dicValues.ContainsKey("KhoXuatId"))
    //            objAdd.KhoXuatId = ((dicValues["KhoXuatId"] == null) || (dicValues["KhoXuatId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["KhoXuatId"]);

    //        if (dicValues.ContainsKey("KhoNhapId"))
    //            objAdd.KhoNhapId = ((dicValues["KhoNhapId"] == null) || (dicValues["KhoNhapId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["KhoNhapId"]);

    //        if (dicValues.ContainsKey("DienGiai"))
    //            objAdd.DienGiai = ((dicValues["DienGiai"] == null) || (dicValues["DienGiai"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["DienGiai"]);

    //        if (dicValues.ContainsKey("StatusId"))
    //            objAdd.StatusId = ((dicValues["StatusId"] == null) || (dicValues["StatusId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["StatusId"]);

    //        if (dicValues.ContainsKey("Keep01"))
    //            objAdd.Keep01 = ((dicValues["Keep01"] == null) || (dicValues["Keep01"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep01"]);

    //        if (dicValues.ContainsKey("Keep02"))
    //            objAdd.Keep02 = ((dicValues["Keep02"] == null) || (dicValues["Keep02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep02"]);

    //        if (dicValues.ContainsKey("Keep03"))
    //            objAdd.Keep03 = ((dicValues["Keep03"] == null) || (dicValues["Keep03"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep03"]);

    //        if (dicValues.ContainsKey("Keep04"))
    //            objAdd.Keep04 = ((dicValues["Keep04"] == null) || (dicValues["Keep04"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Keep04"]);

    //        if (dicValues.ContainsKey("Space01"))
    //            objAdd.Space01 = ((dicValues["Space01"] == null) || (dicValues["Space01"].ToString().Trim().Length <= 0)) ? (double?)null : Convert.ToDouble(dicValues["Space01"]);

    //        if (dicValues.ContainsKey("Space02"))
    //            objAdd.Space02 = ((dicValues["Space02"] == null) || (dicValues["Space02"].ToString().Trim().Length <= 0)) ? (string)null : Convert.ToString(dicValues["Space02"]);

    //        if (dicValues.ContainsKey("Space03"))
    //            objAdd.Space03 = ((dicValues["Space03"] == null) || (dicValues["Space03"].ToString().Trim().Length <= 0)) ? (DateTime?)null : Convert.ToDateTime(dicValues["Space03"]);

    //        if (dicValues.ContainsKey("SpaceId"))
    //            objAdd.SpaceId = ((dicValues["SpaceId"] == null) || (dicValues["SpaceId"].ToString().Trim().Length <= 0)) ? (int?)null : Convert.ToInt32(dicValues["SpaceId"]);

    //        objAdd.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //        objAdd.isPrgVNKoDau = Session["gcUserName"].ToString();
    //        objAdd.isPrgCreateDate = DateTime.Now;
    //        objAdd.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
    //        objAdd.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
    //        objAdd.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

    //        //if (dicValues.ContainsKey("PhieuThuTienMatId"))
    //        //    objAdd.PhieuThuTienMatId = (dicValues["PhieuThuTienMatId"] == null) ? -1 : Convert.ToInt32(dicValues["PhieuThuTienMatId"]);

    //        int startIndex = (dicValues["startIndex"] == null) ? 0 : Convert.ToInt32(dicValues["startIndex"]);
    //        int maximumRows = (dicValues["maximumRows"] == null) ? 1 : Convert.ToInt32(dicValues["maximumRows"]);
    //        string sortExpressions = (dicValues["sortExpressions"] == null) ? "" : Convert.ToString(dicValues["sortExpressions"]);
    //        string filter = (dicValues["filter"] == null) ? "" : Convert.ToString(dicValues["filter"]);
    //        string bSecurity = (dicValues["bSecurity"] == null) ? "" : Convert.ToString(dicValues["bSecurity"]);
    //        string user = (dicValues["user"] == null) ? "" : Convert.ToString(dicValues["user"]);

    //        if (type.ToUpper() == "INSERT")
    //        {
    //            // tiến hành đưa số liệu vào
    //            gcGobal_STOCK_gcProduct_Production.AddObject(objAdd, ref IdOut, null, zgc0GlobalStr.getSqlStr());

    //            //----------------------------------------------------------------------------
    //            //process data after insert data
    //            gcGobal_STOCK_gcProduct_ProductionExt.AddAndUpdateData(IdOut, objAdd, "post_AddAndUpdateData");
    //            //----------------------------------------------------------------------------
    //        }
    //        else if (type.ToUpper() == "UPDATE")
    //        {
    //            string ObjectID = (dicValues["ObjectID"] == null) ? "" : Convert.ToString(dicValues["ObjectID"]);

    //            string sql = String.Format("UPDATE gcGobal_STOCK_gcProduct_Production SET ");
    //            string where = "";

    //            //scan column update
    //            if (dicValues.ContainsKey("Ten"))
    //                if (dicValues["Ten"] != null && dicValues["Ten"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Ten=N'{0}' ", dicValues["Ten"].ToString());
    //                    else
    //                        where += string.Format(" ,Ten=N'{0}' ", dicValues["Ten"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SoCT"))
    //                if (dicValues["SoCT"] != null && dicValues["SoCT"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SoCT=N'{0}' ", dicValues["SoCT"].ToString());
    //                    else
    //                        where += string.Format(" ,SoCT=N'{0}' ", dicValues["SoCT"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NgayLap"))
    //                if (dicValues["NgayLap"] != null && dicValues["NgayLap"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NgayLap=N'{0}' ", dicValues["NgayLap"].ToString());
    //                    else
    //                        where += string.Format(" ,NgayLap=N'{0}' ", dicValues["NgayLap"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NCCId"))
    //                if (dicValues["NCCId"] != null && dicValues["NCCId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NCCId={0} ", dicValues["NCCId"].ToString());
    //                    else
    //                        where += string.Format(" ,NCCId={0} ", dicValues["NCCId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("TienThue"))
    //                if (dicValues["TienThue"] != null && dicValues["TienThue"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" TienThue={0} ", dicValues["TienThue"].ToString());
    //                    else
    //                        where += string.Format(" ,TienThue={0} ", dicValues["TienThue"].ToString());
    //                }

    //            if (dicValues.ContainsKey("TongTien"))
    //                if (dicValues["TongTien"] != null && dicValues["TongTien"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" TongTien={0} ", dicValues["TongTien"].ToString());
    //                    else
    //                        where += string.Format(" ,TongTien={0} ", dicValues["TongTien"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NhanvienId"))
    //                if (dicValues["NhanvienId"] != null && dicValues["NhanvienId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NhanvienId={0} ", dicValues["NhanvienId"].ToString());
    //                    else
    //                        where += string.Format(" ,NhanvienId={0} ", dicValues["NhanvienId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("DaTra"))
    //                if (dicValues["DaTra"] != null && dicValues["DaTra"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" DaTra={0} ", dicValues["DaTra"].ToString());
    //                    else
    //                        where += string.Format(" ,DaTra={0} ", dicValues["DaTra"].ToString());
    //                }

    //            if (dicValues.ContainsKey("NoLai"))
    //                if (dicValues["NoLai"] != null && dicValues["NoLai"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" NoLai={0} ", dicValues["NoLai"].ToString());
    //                    else
    //                        where += string.Format(" ,NoLai={0} ", dicValues["NoLai"].ToString());
    //                }

    //            if (dicValues.ContainsKey("KhoXuatId"))
    //                if (dicValues["KhoXuatId"] != null && dicValues["KhoXuatId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" KhoXuatId={0} ", dicValues["KhoXuatId"].ToString());
    //                    else
    //                        where += string.Format(" ,KhoXuatId={0} ", dicValues["KhoXuatId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("KhoNhapId"))
    //                if (dicValues["KhoNhapId"] != null && dicValues["KhoNhapId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" KhoNhapId={0} ", dicValues["KhoNhapId"].ToString());
    //                    else
    //                        where += string.Format(" ,KhoNhapId={0} ", dicValues["KhoNhapId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("DienGiai"))
    //                if (dicValues["DienGiai"] != null && dicValues["DienGiai"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" DienGiai=N'{0}' ", dicValues["DienGiai"].ToString());
    //                    else
    //                        where += string.Format(" ,DienGiai=N'{0}' ", dicValues["DienGiai"].ToString());
    //                }

    //            if (dicValues.ContainsKey("StatusId"))
    //                if (dicValues["StatusId"] != null && dicValues["StatusId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" StatusId={0} ", dicValues["StatusId"].ToString());
    //                    else
    //                        where += string.Format(" ,StatusId={0} ", dicValues["StatusId"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep01"))
    //                if (dicValues["Keep01"] != null && dicValues["Keep01"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep01=N'{0}' ", dicValues["Keep01"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep01=N'{0}' ", dicValues["Keep01"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep02"))
    //                if (dicValues["Keep02"] != null && dicValues["Keep02"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep02=N'{0}' ", dicValues["Keep02"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep02=N'{0}' ", dicValues["Keep02"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep03"))
    //                if (dicValues["Keep03"] != null && dicValues["Keep03"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep03=N'{0}' ", dicValues["Keep03"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep03=N'{0}' ", dicValues["Keep03"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Keep04"))
    //                if (dicValues["Keep04"] != null && dicValues["Keep04"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Keep04=N'{0}' ", dicValues["Keep04"].ToString());
    //                    else
    //                        where += string.Format(" ,Keep04=N'{0}' ", dicValues["Keep04"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Space01"))
    //                if (dicValues["Space01"] != null && dicValues["Space01"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Space01={0} ", dicValues["Space01"].ToString());
    //                    else
    //                        where += string.Format(" ,Space01={0} ", dicValues["Space01"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Space02"))
    //                if (dicValues["Space02"] != null && dicValues["Space02"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Space02=N'{0}' ", dicValues["Space02"].ToString());
    //                    else
    //                        where += string.Format(" ,Space02=N'{0}' ", dicValues["Space02"].ToString());
    //                }

    //            if (dicValues.ContainsKey("Space03"))
    //                if (dicValues["Space03"] != null && dicValues["Space03"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" Space03=N'{0}' ", dicValues["Space03"].ToString());
    //                    else
    //                        where += string.Format(" ,Space03=N'{0}' ", dicValues["Space03"].ToString());
    //                }

    //            if (dicValues.ContainsKey("SpaceId"))
    //                if (dicValues["SpaceId"] != null && dicValues["SpaceId"].ToString().Trim().Length > 0)
    //                {
    //                    if (where.Length <= 0)
    //                        where = string.Format(" SpaceId={0} ", dicValues["SpaceId"].ToString());
    //                    else
    //                        where += string.Format(" ,SpaceId={0} ", dicValues["SpaceId"].ToString());
    //                }

    //            objAdd.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());//isPrgAccountUpdateId;
    //            objAdd.isPrgSmField = objAdd.isPrgSmField + "|" + Session["gcUserName"].ToString() + "-" + Convert.ToDateTime(DateTime.Now);

    //            //---------------------------------------------------------------------------
    //            where += string.Format(" where Id={0} ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sql + where, zgc0GlobalStr.getSqlStr());
    //        }
    //        else if (type.ToUpper() == "DELETE")
    //        {
    //            string ObjectID = (dicValues["ObjectID"] == null) ? "" : Convert.ToString(dicValues["ObjectID"]);

    //            string sql = String.Format("Delete from gcGobal_STOCK_gcProduct_Production  ");
    //            string where = "";
    //            //---------------------------------------------------------------------------
    //            where += string.Format(" where Id={0} ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sql + where, zgc0GlobalStr.getSqlStr());

    //            sql = String.Format("Update gcGobal_STOCK_gcProduct_Production SET isPrgbUserDeleted = 1 ");
    //            where = string.Format(" where Id={0} ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sql + where, zgc0GlobalStr.getSqlStr());
    //            //----------------------------------------------------------------------------
    //            //process data after delete data
    //            gcGobal_STOCK_gcProduct_ProductionExt.AddAndUpdateData(ObjectID, objAdd, "post_AddAndUpdateData");
    //        }
    //        else if (type.ToUpper() == "ISPRINT")
    //        {
    //            string ObjectID = (dicValues["ObjectID"] == null) ? "" : Convert.ToString(dicValues["ObjectID"]);

    //            string where = "";


    //            //--------------------------------------------------------------------------------------
    //            if (dicValues.ContainsKey("isFinished"))
    //            {
    //                filter = " isFinished = 'true' ";
    //            }
    //            else if (dicValues.ContainsKey("isPrinted"))
    //            {
    //                filter = " isPrinted = 1 ";
    //            }
    //            else if (dicValues.ContainsKey("isPrgOrdered"))
    //            {
    //                filter = " isPrgOrdered = 1 ";
    //            }
    //            string sqlnew = String.Format("UPDATE gcGobal_STOCK_gcProduct_Production SET ") + filter;
    //            //---------------------------------------------------------------------------
    //            where += string.Format(" where Id={0}  ", ObjectID);
    //            zgc0HelperSecurity.ExecuteNonQuery(sqlnew + where, zgc0GlobalStr.getSqlStr());
    //            //----------------------------------------------------------------------------
    //            //process data after print data
    //            return new { Result = "OK", Record = result, Msg = "Printed" };
    //        }
    //        WSgcGobal_STOCK_gcProduct_Production obj = new WSgcGobal_STOCK_gcProduct_Production();
    //        zgc0GobalService.SetupRightFilter(ref filter, "gcGobal_STOCK_gcProduct_Production");
    //        result = GetData(startIndex, maximumRows, sortExpressions, filter, bSecurity, user);

    //        //----------------------------------------------------------------------------
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Result = "Fail", Message = ex.Message };
    //    }
    //    //----------------------------------------------------------------------------
    //    return new { Result = "OK", Record = result };
    //}

};
//{0}replace

