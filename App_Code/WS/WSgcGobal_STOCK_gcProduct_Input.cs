
/*------------------------------------*/
/* Summary description for  gcGobal_STOCK_gcProduct_Input */
using System;
using System.Globalization;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Data.Common;
using zgc0Admin;
using zgc0LibAdmin;
using System.Text;
[WebService(Namespace = "http://www.anhxuan.com.vn/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WSgcGobal_STOCK_gcProduct_Input : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public object getGetNewValueP166(string user)
    {
        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
            return null;
        zgc0GlobalTail tail = new zgc0GlobalTail();
        string tmpValue = "";
        tmpValue = zgc0Support.getAutoGenCode(null, "Ten", "zgcBUILDIN_GOBAL_AutoGenCode", "Date", "PNK", "4", "Text", "SoCT", "", "", "gcGobal_STOCK_gcProduct_Input");

        tail.strNewCode = tmpValue;
        gcGobal_STOCK_gcProduct_InputExt.PostAutoGenCode(ref tail, "PostgetGetNewValueP166");
        return tail;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public object getGetNewValueMaP166(string user)
    {
        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
            return null;
        zgc0GlobalTail tail = new zgc0GlobalTail();
        string tmpValue = "";
        tmpValue = zgc0Support.getAutoGenCode(null, "SoCT", "zgcBUILDIN_GOBAL_AutoGenCode", "Normal", "PNK", "8", "Text", "SoCT", "", "", "gcGobal_STOCK_gcProduct_Input");

        tail.strNewCode = tmpValue;
        gcGobal_STOCK_gcProduct_InputExt.PostAutoGenCode(ref tail, "PostgetGetNewValueMaP166");
        return tail;
    }
    //----------------------------------------------------------------------------------
    // 19-2 get BILL empty
    //filter set from client form
    //filter = " (isFinished is null OR isFinished = 0) ";
    //filter = " (isPrinted is null OR isPrinted = 0) ";
    //filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public List<object> GetData1Item(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    {
        IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

        //--------------------------------------------------------------------------------------
        if (tmpDict.ContainsKey("isFinished"))
        {
            if (filter == null || filter.Length <= 0)
                filter = " (isFinished is null OR isFinished = 0) ";
            else
                filter += " AND (isFinished is null OR isFinished = 0) ";
        }
        else if (tmpDict.ContainsKey("isPrinted"))
        {
            if (filter == null || filter.Length <= 0)
                filter = " (isPrinted is null OR isPrinted = 0) ";
            else
                filter += " AND (isPrinted is null OR isPrinted = 0) ";
        }
        else if (tmpDict.ContainsKey("isPrgOrdered"))
        {
            if (filter == null || filter.Length <= 0)
                filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
            else
                filter += " AND (isPrgOrdered is null OR isPrgOrdered = 0) ";
        }

        //--------------------------------------------------------------------------------------
        zgc0GlobalTail tailP = new zgc0GlobalTail();
        List<object> list = new List<object>();
        if (HttpContext.Current.Session["gcAccountId"] == null)
        {
            tailP.errMsg = "Không có quyền truy cập dữ liệu";
            list.Add(tailP);
            return list;
        }

        if (tmpDict.ContainsKey("NgayLap"))
        {
            string t1 = DateTime.Now.ToShortDateString() + " 0:00";
            string t2 = DateTime.Now.ToShortDateString() + " 23:59";
            if (filter == null || filter.Length <= 0)
                filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
            else
            {
                filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
            }
        }

        string gcAccountId = HttpContext.Current.Session["gcAccountId"].ToString();
        if (filter == null || filter.Length <= 0)
            filter = String.Format(" ( isPrgAccountId={0} )", gcAccountId);
        else
        {
            filter += String.Format(" AND ( isPrgAccountId={0} )", gcAccountId);
        }

        gcGobal_STOCK_gcProduct_InputExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
        //--------------------------------------------------------------------------------------


        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
        {
            tailP.errMsg = "Không có quyền truy cập dữ liệu";
            list.Add(tailP);
            return list;
        }
        startIndex = startIndex + 1;
        string view = "zgcl_gcGobal_STOCK_gcProduct_Input05";
        string columns = "*";
        int bSql = -1;
        bool bReturn = gcGobal_STOCK_gcProduct_InputExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Input");

        if (bReturn)
        {
            zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Input05");
            DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
            //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Input05", "GetData - file: WSgcGobal_STOCK_gcProduct_Input.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
            for (int m = 0; m < myTable.Rows.Count; m++)
            {
                bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
                zgcl_gcGobal_STOCK_gcProduct_Input05 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Input05(myTable.Rows[m], isHasRowNum);
                list.Add(newEmployee);
            }
            //--------------------------------------------------------------------------------------
            // if data don't have, we add 1 item empty
            // we should set it in extention file to get option data
            if (myTable.Rows.Count <= 0)
            {
                WSgcGobal_STOCK_gcProduct_Input gc = new WSgcGobal_STOCK_gcProduct_Input();
                bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
                zgcl_gcGobal_STOCK_gcProduct_Input05 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Input05();

                newEmployee.Id = -1;
                newEmployee.isPrgAccountId = int.Parse(Session["gcAccountId"].ToString());//obj.isPrgAccountUpdateId;
                newEmployee.isPrgVNKoDau = Session["gcUserName"].ToString();
                newEmployee.isPrgCreateDate = DateTime.Now;
                newEmployee.isPrgAccountUpdateId = int.Parse(Session["gcAccountId"].ToString());
                newEmployee.isPrgSmField = Convert.ToDateTime(DateTime.Now) + " | " + zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));
                newEmployee.isPrgPartComp = zgc0Support.getPartComp(int.Parse(Session["gcAccountId"].ToString()), int.Parse(Session["gcMaCanBoId"].ToString()));

                gcGobal_STOCK_gcProduct_InputExt.postAssignmentEmtyData(newEmployee, "postAssignmentEmtyData_Empty");
                list.Add(newEmployee);
            }
        }
        //----------------------------------------------------------

        zgc0GlobalTail tailNew = new zgc0GlobalTail();
        bReturn = gcGobal_STOCK_gcProduct_InputExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Input05");
        if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
        zgc0Support.ProcessAccountRight(ref tailNew);
        list.Add(tailNew);

        //---------------------------------------------------------
        //success get data
        zgc0GlobalTail tail = new zgc0GlobalTail();
        //
        list.Add(tail);
        //----------------------------------------------------------
        return list;
    }
    //----------------------------------------------------------------------------------
    // 19-2 get BILL empty
    //filter set from client form
    //filter = " (isFinished is null OR isFinished = 0) ";
    //filter = " (isPrinted is null OR isPrinted = 0) ";
    //filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public List<object> GetDataAllDay(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user, string t1, string t2)
    {

        IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

        //--------------------------------------------------------------------------------------
        //if (tmpDict.ContainsKey("isFinished"))
        //{
        //    if (filter == null || filter.Length <= 0)
        //        filter = " (isFinished = 1) ";
        //    else
        //        filter += " AND (isFinished = 1) ";
        //}
        //else if (tmpDict.ContainsKey("isPrinted"))
        //{
        //    if (filter == null || filter.Length <= 0)
        //        filter = " (isPrinted = 1) ";
        //    else
        //        filter += " AND (isPrinted = 1) ";
        //}
        //else if (tmpDict.ContainsKey("isPrgOrdered"))
        //{
        //    if (filter == null || filter.Length <= 0)
        //        filter = " (isPrgOrdered = 1) ";
        //    else
        //        filter += " AND (isPrgOrdered = 1) ";
        //}

        //--------------------------------------------------------------------------------------

        if (tmpDict.ContainsKey("NgayLap"))
        {
            t1 = t1 + " 0:00";
            t2 = t2 + " 23:59";
            if (filter == null || filter.Length <= 0)
                filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
            else
            {
                filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
            }
        }
        gcGobal_STOCK_gcProduct_InputExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
        //--------------------------------------------------------------------------------------

        zgc0GlobalTail tailP = new zgc0GlobalTail();
        List<object> list = new List<object>();
        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
        {
            tailP.errMsg = "Không có quyền truy cập dữ liệu";
            list.Add(tailP);
            return list;
        }
        startIndex = startIndex + 1;
        string view = "zgcl_gcGobal_STOCK_gcProduct_Input05";
        string columns = "*";
        int bSql = -1;
        bool bReturn = gcGobal_STOCK_gcProduct_InputExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Input");

        if (bReturn)
        {
            zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Input05");
            DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
            //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Input05", "GetData - file: WSgcGobal_STOCK_gcProduct_Input.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
            for (int m = 0; m < myTable.Rows.Count; m++)
            {
                bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
                zgcl_gcGobal_STOCK_gcProduct_Input05 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Input05(myTable.Rows[m], isHasRowNum);
                list.Add(newEmployee);
            }
        }
        //----------------------------------------------------------

        zgc0GlobalTail tailNew = new zgc0GlobalTail();
        bReturn = gcGobal_STOCK_gcProduct_InputExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Input05");
        if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
        zgc0Support.ProcessAccountRight(ref tailNew);
        list.Add(tailNew);

        //---------------------------------------------------------
        //success get data
        zgc0GlobalTail tail = new zgc0GlobalTail();
        //
        list.Add(tail);
        //----------------------------------------------------------
        return list;
    }
    //----------------------------------------------------------------------------------
    // 19-2 get BILL empty
    //filter set from client form
    //filter = " (isFinished is null OR isFinished = 0) ";
    //filter = " (isPrinted is null OR isPrinted = 0) ";
    //filter = " (isPrgOrdered is null OR isPrgOrdered = 0) ";
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public List<object> GetDataToDay(object tmpObj, int startIndex, int maximumRows, string sortExpressions, string filter, string bSecurity, string user)
    {

        IDictionary<string, Object> tmpDict = (IDictionary<string, Object>)tmpObj;

        //--------------------------------------------------------------------------------------
        if (tmpDict.ContainsKey("isFinished"))
        {
            if (filter == null || filter.Length <= 0)
                filter = " ( isFinished = 1) ";
            else
                filter += " AND (isFinished = 1) ";
        }
        else if (tmpDict.ContainsKey("isPrinted"))
        {
            if (filter == null || filter.Length <= 0)
                filter = " ( isPrinted = 1) ";
            else
                filter += " AND (isPrinted =1) ";
        }
        else if (tmpDict.ContainsKey("isPrgOrdered"))
        {
            if (filter == null || filter.Length <= 0)
                filter = " (isPrgOrdered = 1) ";
            else
                filter += " AND (isPrgOrdered = 1) ";
        }

        //--------------------------------------------------------------------------------------

        if (tmpDict.ContainsKey("NgayLap"))
        {
            string t1 = DateTime.Now.ToShortDateString() + " 0:00";
            string t2 = DateTime.Now.ToShortDateString() + " 23:59";
            if (filter == null || filter.Length <= 0)
                filter = String.Format(" ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
            else
            {
                filter += String.Format(" AND ( NgayLap>=N'{0}' and NgayLap<=N'{1}' )", t1, t2);
            }
        }
        gcGobal_STOCK_gcProduct_InputExt.SetParamNgayLap(ref filter, "post_SetParamNgayLap");
        //--------------------------------------------------------------------------------------

        zgc0GlobalTail tailP = new zgc0GlobalTail();
        List<object> list = new List<object>();
        if (!zgc0Login.CheckRight(HttpContext.Current.Session, HttpContext.Current.Request.UrlReferrer.OriginalString, user))
        {
            tailP.errMsg = "Không có quyền truy cập dữ liệu";
            list.Add(tailP);
            return list;
        }
        startIndex = startIndex + 1;
        string view = "zgcl_gcGobal_STOCK_gcProduct_Input05";
        string columns = "*";
        int bSql = -1;
        bool bReturn = gcGobal_STOCK_gcProduct_InputExt.preGet(ref list, ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref view, ref columns, ref bSql, "preGet_gcGobal_STOCK_gcProduct_Input");

        if (bReturn)
        {
            zgc0GobalService.SetupRightFilter(ref filter, "zgcl_gcGobal_STOCK_gcProduct_Input05");
            DataTable myTable = zgc0Support.ProcessGet(ref startIndex, ref  maximumRows, ref  sortExpressions, ref  filter, ref columns, ref bSql, view, bSecurity);
            //zgc0Project.LogDatabaseOperation(Session["gcUserName"].ToString(), int.Parse(Session["gcMaCanBoId"].ToString()), "SELECT", "zgcl_gcGobal_STOCK_gcProduct_Input05", "GetData - file: WSgcGobal_STOCK_gcProduct_Input.cs", DateTime.Now, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["REMOTE_HOST"], HttpContext.Current.Request.ServerVariables["REMOTE_PORT"]);
            for (int m = 0; m < myTable.Rows.Count; m++)
            {
                bool isHasRowNum = myTable.Columns.Contains("ROWNUM");
                zgcl_gcGobal_STOCK_gcProduct_Input05 newEmployee = new zgcl_gcGobal_STOCK_gcProduct_Input05(myTable.Rows[m], isHasRowNum);
                list.Add(newEmployee);
            }
        }
        //----------------------------------------------------------

        zgc0GlobalTail tailNew = new zgc0GlobalTail();
        bReturn = gcGobal_STOCK_gcProduct_InputExt.preProcessCount(ref tailNew, ref filter, ref view, ref columns, "preGetCountzgcl_gcGobal_STOCK_gcProduct_Input05");
        if (bReturn) tailNew.numObj = zgc0Support.ProcessCount(filter, view, columns);
        zgc0Support.ProcessAccountRight(ref tailNew);
        list.Add(tailNew);

        //---------------------------------------------------------
        //success get data
        zgc0GlobalTail tail = new zgc0GlobalTail();
        //
        list.Add(tail);
        //----------------------------------------------------------
        return list;
    }
};
//{0}replace

