using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Data.OleDb;
using System.Web;
using System.Collections.Generic;
using System.Reflection;

using zgc0LibAdmin;
namespace zgc0LibAdmin
{
    /// <summary>
    /// Summary description for zgc0Helper.
    /// </summary>
    public class gcGobal_STOCK_gcProduct_InputExt
    {
        public gcGobal_STOCK_gcProduct_InputExt()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public static bool preProcessCount(ref zgc0GlobalTail obj, ref string filter, ref string view, ref string columns, string funcname)
        {
            gcGobal_STOCK_gcProduct_InputExt tmpClass = new gcGobal_STOCK_gcProduct_InputExt();
            MethodInfo method = tmpClass.GetType().GetMethod(funcname);
            if (method != null)
            {
                object[] parammeter = new object[4]; parammeter[0] = filter; parammeter[1] = view;
                parammeter[2] = columns;
                parammeter[3] = obj;
                object vresult = method.Invoke(tmpClass, parammeter);// sử dụng gọi method
                filter = (string)parammeter[0]; view = (string)parammeter[1];
                columns = (string)parammeter[2];
                obj = (zgc0GlobalTail)parammeter[3];
                return (bool)vresult;
            }
            return true;
        }
        //postAssignmentEmtyData
        public static bool postAssignmentEmtyData(object obj, string funcname)
        {
            gcGobal_STOCK_gcProduct_InputExt tmpClass = new gcGobal_STOCK_gcProduct_InputExt();
            MethodInfo method = tmpClass.GetType().GetMethod(funcname);
            if (method != null)
            {
                object[] parammeter = new object[1];
                parammeter[0] = obj;
                object vresult = method.Invoke(tmpClass, parammeter);// sử dụng gọi method
                obj = parammeter[0];
                return (bool)vresult;
            }
            return true;
        }
        public static bool postAssignmentEmtyData_Empty(object obj)
        {
            bool vresult = true;

            string user = "crmgobal";
            zgc0GlobalTail tail = new zgc0GlobalTail();
            WSgcGobal_STOCK_gcProduct_Input receipt = new WSgcGobal_STOCK_gcProduct_Input();
            string Ten = ((zgc0GlobalTail)receipt.getGetNewValueP166(user)).strNewCode;
            string SoCT = ((zgc0GlobalTail)receipt.getGetNewValueMaP166(user)).strNewCode;

            ((zgcl_gcGobal_STOCK_gcProduct_Input05)obj).Ten = Ten;
            ((zgcl_gcGobal_STOCK_gcProduct_Input05)obj).SoCT = SoCT;
            ((zgcl_gcGobal_STOCK_gcProduct_Input05)obj).NgayLap = DateTime.Now;
            ((zgcl_gcGobal_STOCK_gcProduct_Input05)obj).isPrgOrdered = 0;

            ((zgcl_gcGobal_STOCK_gcProduct_Input05)obj).KhoNhapId = 1;
            ((zgcl_gcGobal_STOCK_gcProduct_Input05)obj).KhoXuatId = 1;

            return vresult;
        }
        public static bool PostAutoGenCode(ref zgc0GlobalTail obj, string funcname)
        {
            gcGobal_STOCK_gcProduct_InputExt tmpClass = new gcGobal_STOCK_gcProduct_InputExt();
            MethodInfo method = tmpClass.GetType().GetMethod(funcname);
            if (method != null)
            {
                object[] parammeter = new object[1]; parammeter[0] = obj;
                object vresult = method.Invoke(tmpClass, parammeter);// sử dụng gọi method
                obj = (zgc0GlobalTail)parammeter[0];
                return (bool)vresult;
            }
            return true;
        }
        public static bool preGet(ref List<object> list, ref int startIndex, ref int maximumRows,
                               ref string sortExpressions, ref string filter, ref string view,
                               ref string columns, ref int bSql, string funcname)
        {
            gcGobal_STOCK_gcProduct_InputExt tmpClass = new gcGobal_STOCK_gcProduct_InputExt();
            MethodInfo method = tmpClass.GetType().GetMethod(funcname);
            if (method != null)
            {
                object[] parammeter = new object[8];
                parammeter[0] = list;
                parammeter[1] = startIndex; parammeter[2] = maximumRows;
                parammeter[3] = sortExpressions; parammeter[4] = filter; parammeter[5] = view;
                parammeter[6] = columns; parammeter[7] = bSql;
                object vresult = method.Invoke(tmpClass, parammeter);// sử dụng gọi method
                startIndex = (int)parammeter[1]; maximumRows = (int)parammeter[2];
                sortExpressions = (string)parammeter[3]; filter = (string)parammeter[4]; view = (string)parammeter[5];
                columns = (string)parammeter[6]; bSql = (int)parammeter[7];
                list = (List<object>)parammeter[0];
                return (bool)vresult;
            }
            return true;
        }
        //SetParamNgayLap
        public static bool SetParamNgayLap(ref string obj, string funcname)
        {
            gcGobal_STOCK_gcProduct_InputExt tmpClass = new gcGobal_STOCK_gcProduct_InputExt();
            MethodInfo method = tmpClass.GetType().GetMethod(funcname);
            if (method != null)
            {
                object[] parammeter = new object[1];
                parammeter[0] = obj;
                object vresult = method.Invoke(tmpClass, parammeter);// sử dụng gọi method
                obj = (string)parammeter[0];
                return (bool)vresult;
            }
            return true;
        }
    }
}

