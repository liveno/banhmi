﻿using System;
using System.Web.Script.Services;
using System.Web.Services;
using System.Reflection;
using System.Collections.Generic;
using System.Web;
using Core;

/// <summary>
/// Summary description for Trip
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[ScriptService]
public class Kernel : WebService
{


    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public object Process(object obj, string si, string ps)
    {
        //D._a.Add("AddOrUpdateThanhLyDetail", new C { T = new[] { "P", "401", "401", "gcGobal_TSCD_THANHLY_Detail", "zgcl_gcGobal_TSCD_THANHLY_Detail02", "AddOrUpdateThanhLyDetail" } });
        //D._a.Add("AddOrUpdateNhapDetail", new C { T = new[] { "P", "395", "395", "gcGobal_TSCD_NhapTaiSan_Detail", "zgcl_gcGobal_TSCD_NhapTaiSan_Detail02", "AddOrUpdateNhapDetail" } });
        //D._a.Add("AddOrUpdateOutputDetail", new C { T = new[] { "P", "273", "273", "gcGobal_STOCK_gcProduct_Output_Detail", "zgcl_gcGobal_STOCK_gcProduct_Output_Detail02", "AddOrUpdateOutputDetail" } });
        //D._a.Add("AddOrUpdateInputDetail", new C { T = new[] { "P", "271", "271", "gcGobal_STOCK_gcProduct_Input_Detail", "zgcl_gcGobal_STOCK_gcProduct_Input_Detail02", "AddOrUpdateInputDetail" } });
        //D._a.Add("AddOrUpdatePaymentDetail", new C { T = new[] { "P", "168", "168", "gcGobal_INCOM_Payment_Detail", "zgcl_gcGobal_INCOM_Payment_Detail01", "AddOrUpdatePaymentDetail" } });
        //D._a.Add("AddOrUpdateReceiptDetail", new C { T = new[] { "P", "169", "169", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_Detail04", "AddOrUpdateReceiptDetail" } });
        //D._a.Add("AddOrUpdateRefundDetail", new C { T = new[] { "P", "172", "172", "gcGobal_INCOM_Receipt_Refund_Detail", "zgcl_gcGobal_INCOM_Receipt_Refund_Detail04", "AddOrUpdateRefundDetail" } });
        //D._a.Add("AddOrUpdateBaoTriDetail", new C { T = new[] { "P", "435", "435", "gcGobal_TSCD_BAOTRI_Detial", "zgcl_gcGobal_TSCD_BAOTRI_Detail03", "AddOrUpdateBaoTriDetail" } });
        //D._a.Add("AddOrUpdateVPPDetail", new C { T = new[] { "P", "436", "436", "GBL_VPP_YEUCAU_Details", "zgcl_GBL_VPP_YEUCAU_Details01", "AddOrUpdateBaoTriDetail" } });
        int startIndex = int.Parse(si);
        int sii = startIndex;
        if (startIndex > 0)
            sii = startIndex / int.Parse(ps);
        sii++;
        object[] pr = new object[2];
        pr[0] = obj;
        pr[1] = new { Result = "Fail", Records = (object)null, TotalRecordCount = 0 };
        Dictionary<string, object> _ip = (Dictionary<string, object>)obj;
        _ip["si"] = sii;
        _ip["mr"] = int.Parse(ps);
        if (_ip.ContainsKey("a"))
        {
            var right = (string)Session["gcRightGroup"];
            if (right != null)
            {
                var a = _ip["a"] as string;
                if (a != null && D._a.ContainsKey(a))
                {
                    //D._a["pGetGBL_TRAINING_STUDENT"] = new C { T = new[] { "P", "377", "377", "GBL_TRAINING_STUDENT", "zgcl_GBL_TRAINING_STUDENT_08FULL", "GBL_TRAINING_STUDENT" } };
                    //D._a["pGetGBL_TRAINING_STUDENT_ACTION"] = new C { T = new[] { "P", "376", "376", "GBL_TRAINING_STUDENT_ACTION", "zgcl_GBL_TRAINING_STUDENT_ACTION_FULL", "GBL_TRAINING_STUDENT_ACTION" } };
                    //D._a["pGetGBL_TRAINING_STUDENT_ENTRANCETEST"] = new C { T = new[] { "P", "351", "351", "GBL_TRAINING_STUDENT_ENTRANCETEST", "zgcl_GBL_TRAINING_STUDENT_ENTRANCETEST_FULL", "GBL_TRAINING_STUDENT_ENTRANCETEST" } };
                    //D._a["pGetgcGobal_INCOM_Receipt_Detail"] = new C { T = new[] { "P", "369", "369", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_Detail_Full", "gcGobal_INCOM_Receipt_Detail" } };
                    //D._a["pGetGBL_TRAINNING_COURSE_CLASS_STUDENT"] = new C { T = new[] { "P", "380", "380", "GBL_TRAINNING_COURSE_CLASS_STUDENT", "zgcl_GBL_TRAINNING_COURSE_CLASS_STUDENT_FULL", "GBL_TRAINNING_COURSE_CLASS_STUDENT" } };
                    //D._a["pGetGBL_TRAINNING_COURSE_CLASS"] = new C { T = new[] { "P", "360", "360", "GBL_TRAINNING_COURSE_CLASS", "AMA_XIKE_REPORT_CLASS_FULL", "GBL_TRAINNING_COURSE_CLASS" } };
                    //D._a["pGetGBL_TRAINING_STUDENT_ATTENDANCEAL"] = new C { T = new[] { "P", "413", "413", "GBL_TRAINING_STUDENT_ATTENDANCEAL", "zgcl_GBL_TRAINING_STUDENT_ATTENDANCEAL06_FULL", "GBL_TRAINING_STUDENT_ATTENDANCEAL" } };
                    //D._a["pGetGBL_TRAINNING_COURSE_CLASS_EVENT"] = new C { T = new[] { "P", "384", "384", "GBL_TRAINNING_COURSE_CLASS_EVENT", "zgcl_GBL_TRAINNING_COURSE_CLASS_EVENT04_FULL", "GBL_TRAINNING_COURSE_CLASS_EVENT" } };
                    D._a["pGetgcGobal_INCOM_Receipt_Detail"] = new C { T = new[] { "P", "74", "74", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_DetailALL", "gcGobal_INCOM_Receipt_Detail" } };
                    var _a = D._a[a];
                    P p = new P();
                    MethodInfo m = p.GetType().GetMethod(_a.T[5]);
                    if (m != null)
                        m.Invoke(p, pr);// sử dụng gọi method
                    else // call extension
                    {
                        PE pe = new PE();
                        m = pe.GetType().GetMethod(_a.T[5]);
                        if (m != null)
                            m.Invoke(pe, pr); // sử dụng gọi method
                    }
                }
            }
            else
            {
                pr[1] = new { Result = "NOSESSION", Records = "Không có quyền truy cập dữ liệu" };
            }
        }
        return pr[1];
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public object P(object obj)
    {
        //D._a.Add("AddOrUpdateThanhLyDetail", new C { T = new[] { "P", "401", "401", "gcGobal_TSCD_THANHLY_Detail", "zgcl_gcGobal_TSCD_THANHLY_Detail02", "AddOrUpdateThanhLyDetail" } });
        //D._a.Add("AddOrUpdateNhapDetail", new C { T = new[] { "P", "395", "395", "gcGobal_TSCD_NhapTaiSan_Detail", "zgcl_gcGobal_TSCD_NhapTaiSan_Detail02", "AddOrUpdateNhapDetail" } });
        //D._a.Add("AddOrUpdateOutputDetail", new C { T = new[] { "P", "273", "273", "gcGobal_STOCK_gcProduct_Output_Detail", "zgcl_gcGobal_STOCK_gcProduct_Output_Detail02", "AddOrUpdateOutputDetail" } });
        //D._a.Add("AddOrUpdateInputDetail", new C { T = new[] { "P", "271", "271", "gcGobal_STOCK_gcProduct_Input_Detail", "zgcl_gcGobal_STOCK_gcProduct_Input_Detail02", "AddOrUpdateInputDetail" } });
        //D._a.Add("AddOrUpdatePaymentDetail", new C { T = new[] { "P", "168", "168", "gcGobal_INCOM_Payment_Detail", "zgcl_gcGobal_INCOM_Payment_Detail01", "AddOrUpdatePaymentDetail" } });
        //D._a.Add("AddOrUpdateReceiptDetail", new C { T = new[] { "P", "169", "169", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_Detail04", "AddOrUpdateReceiptDetail" } });
        //D._a.Add("AddOrUpdateRefundDetail", new C { T = new[] { "P", "172", "172", "gcGobal_INCOM_Receipt_Refund_Detail", "zgcl_gcGobal_INCOM_Receipt_Refund_Detail04", "AddOrUpdateRefundDetail" } });
        //D._a.Add("AddOrUpdateBaoTriDetail", new C { T = new[] { "P", "435", "435", "gcGobal_TSCD_BAOTRI_Detial", "zgcl_gcGobal_TSCD_BAOTRI_Detail03", "AddOrUpdateBaoTriDetail" } });
        //D._a.Add("AddOrUpdateVPPDetail", new C { T = new[] { "P", "436", "436", "GBL_VPP_YEUCAU_Details", "zgcl_GBL_VPP_YEUCAU_Details01", "AddOrUpdateBaoTriDetail" } });
        D._a["pGetgcGobal_INCOM_Receipt_Detail"] = new C { T = new[] { "P", "74", "74", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_DetailALL", "gcGobal_INCOM_Receipt_Detail" } };
        object[] pr = new object[2];
        pr[0] = obj;
        pr[1] = new { Result = "Fail", Records = "", TotalRecordCount = 0 };
        Dictionary<string, object> _ip = (Dictionary<string, object>)obj;
        if (_ip.ContainsKey("a"))
        {
            var right = (string)Session["gcRightGroup"];
            if (right != null)
            {
                var a = _ip["a"] as string;
                if (a != null && D._a.ContainsKey(a))
                {
                    //D._a["pGetGBL_TRAINNING_COURSE_CLASS_EVENT"] = new C { T = new[] { "P", "384", "384", "GBL_TRAINNING_COURSE_CLASS_EVENT", "zgcl_GBL_TRAINNING_COURSE_CLASS_EVENT04_FULL", "GBL_TRAINNING_COURSE_CLASS_EVENT" } };
                    var _a = D._a[a];
                    PI pi = new PI();
                    MethodInfo m = pi.GetType().GetMethod("createAutoCode");
                    if (_ip.ContainsKey("ti") && !string.IsNullOrEmpty(_ip["ti"] + "") && a.Contains("Insert"))
                    {
                        if (m != null)
                            PI.createAutoCode(_ip);
                    }
                    P p = new P();
                    m = p.GetType().GetMethod(_a.T[5]);
                    if (m != null)
                        m.Invoke(p, pr); // sử dụng gọi method
                    else // call extension
                    {
                        PE pe = new PE();
                        m = pe.GetType().GetMethod(_a.T[5]);
                        if (m != null)
                            m.Invoke(pe, pr); // sử dụng gọi method
                    }
                    var r = (Rs)pr[1];
                    if (r != null && r.TotalRecordCount == 0 && _ip.ContainsKey("t") && !string.IsNullOrEmpty(_ip["t"] + ""))
                    {
                        m = pi.GetType().GetMethod("createDataExtra");
                        if (m != null)
                            m.Invoke(pi, pr); // sử dụng gọi method
                    }
                }
            }
            else
            {
                pr[1] = new { Result = "FAIL", Records = "Không có quyền truy cập dữ liệu" };
            }

        }
        return pr[1];
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public object U(object obj)
    {
        //D._a.Add("AddOrUpdateThanhLyDetail", new C { T = new[] { "P", "401", "401", "gcGobal_TSCD_THANHLY_Detail", "zgcl_gcGobal_TSCD_THANHLY_Detail02", "AddOrUpdateThanhLyDetail" } });
        //D._a.Add("AddOrUpdateNhapDetail", new C { T = new[] { "P", "395", "395", "gcGobal_TSCD_NhapTaiSan_Detail", "zgcl_gcGobal_TSCD_NhapTaiSan_Detail02", "AddOrUpdateNhapDetail" } });
        //D._a.Add("AddOrUpdateOutputDetail", new C { T = new[] { "P", "273", "273", "gcGobal_STOCK_gcProduct_Output_Detail", "zgcl_gcGobal_STOCK_gcProduct_Output_Detail02", "AddOrUpdateOutputDetail" } });
        //D._a.Add("AddOrUpdateInputDetail", new C { T = new[] { "P", "271", "271", "gcGobal_STOCK_gcProduct_Input_Detail", "zgcl_gcGobal_STOCK_gcProduct_Input_Detail02", "AddOrUpdateInputDetail" } });
        //D._a.Add("AddOrUpdatePaymentDetail", new C { T = new[] { "P", "168", "168", "gcGobal_INCOM_Payment_Detail", "zgcl_gcGobal_INCOM_Payment_Detail01", "AddOrUpdatePaymentDetail" } });
        //D._a.Add("AddOrUpdateReceiptDetail", new C { T = new[] { "P", "169", "169", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_Detail04", "AddOrUpdateReceiptDetail" } });
        //D._a.Add("AddOrUpdateRefundDetail", new C { T = new[] { "P", "172", "172", "gcGobal_INCOM_Receipt_Refund_Detail", "zgcl_gcGobal_INCOM_Receipt_Refund_Detail04", "AddOrUpdateRefundDetail" } });
        //D._a.Add("AddOrUpdateBaoTriDetail", new C { T = new[] { "P", "435", "435", "gcGobal_TSCD_BAOTRI_Detial", "zgcl_gcGobal_TSCD_BAOTRI_Detail03", "AddOrUpdateBaoTriDetail" } });
        //D._a.Add("AddOrUpdateVPPDetail", new C { T = new[] { "P", "436", "436", "GBL_VPP_YEUCAU_Details", "zgcl_GBL_VPP_YEUCAU_Details01", "AddOrUpdateBaoTriDetail" } });
        object[] pr = new object[2];
        pr[0] = obj;
        pr[1] = new { Result = "Fail", Records = "", TotalRecordCount = 0 };
        Dictionary<string, object> _ip = (Dictionary<string, object>)obj;

        //-------------------------------------------------------------------
        //convert object
        if (_ip.ContainsKey("id"))
        {
            int key = (int)_ip["id"];
            C[] f = D._fd[key];
            if (_ip.ContainsKey("d"))
            {
                var d = (Dictionary<string, object>)_ip["d"];
                var newd = new Dictionary<string, object>();
                for (int m = 0; m < f.Length; m++)
                {
                    string of = f[m].T[7];
                    newd[of] = d["f" + (m <= 9 ? "0" + m.ToString() : m.ToString() + "")];
                }
                _ip["d"] = newd;
            }
        }
        //------------------------------------------------------------------
        if (_ip.ContainsKey("a"))
        {
            var right = (string)Session["gcRightGroup"];
            if (right != null)
            {
                var a = _ip["a"] as string;
                if (a != null && D._a.ContainsKey(a))
                {
                    var _a = D._a[a];
                    P p = new P();
                    MethodInfo m = p.GetType().GetMethod(_a.T[5]);
                    if (m != null)
                        m.Invoke(p, pr);// sử dụng gọi method
                    else // call extension
                    {
                        PE pe = new PE();
                        m = pe.GetType().GetMethod(_a.T[5]);
                        if (m != null)
                            m.Invoke(pe, pr); // sử dụng gọi method
                    }
                }
            }
            else
            {
                pr[1] = new { Result = "FAIL", Records = "Không có quyền truy cập dữ liệu" };
            }
        }
        return pr[1];
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public object R(object obj)
    {
        //D._a.Add("AddOrUpdateThanhLyDetail", new C { T = new[] { "P", "401", "401", "gcGobal_TSCD_THANHLY_Detail", "zgcl_gcGobal_TSCD_THANHLY_Detail02", "AddOrUpdateThanhLyDetail" } });
        //D._a.Add("AddOrUpdateNhapDetail", new C { T = new[] { "P", "395", "395", "gcGobal_TSCD_NhapTaiSan_Detail", "zgcl_gcGobal_TSCD_NhapTaiSan_Detail02", "AddOrUpdateNhapDetail" } });
        //D._a.Add("AddOrUpdateOutputDetail", new C { T = new[] { "P", "273", "273", "gcGobal_STOCK_gcProduct_Output_Detail", "zgcl_gcGobal_STOCK_gcProduct_Output_Detail02", "AddOrUpdateOutputDetail" } });
        //D._a.Add("AddOrUpdateInputDetail", new C { T = new[] { "P", "271", "271", "gcGobal_STOCK_gcProduct_Input_Detail", "zgcl_gcGobal_STOCK_gcProduct_Input_Detail02", "AddOrUpdateInputDetail" } });
        //D._a.Add("AddOrUpdatePaymentDetail", new C { T = new[] { "P", "168", "168", "gcGobal_INCOM_Payment_Detail", "zgcl_gcGobal_INCOM_Payment_Detail01", "AddOrUpdatePaymentDetail" } });
        //D._a.Add("AddOrUpdateReceiptDetail", new C { T = new[] { "P", "169", "169", "gcGobal_INCOM_Receipt_Detail", "zgcl_gcGobal_INCOM_Receipt_Detail04", "AddOrUpdateReceiptDetail" } });
        //D._a.Add("AddOrUpdateRefundDetail", new C { T = new[] { "P", "172", "172", "gcGobal_INCOM_Receipt_Refund_Detail", "zgcl_gcGobal_INCOM_Receipt_Refund_Detail04", "AddOrUpdateRefundDetail" } });
        //D._a.Add("AddOrUpdateBaoTriDetail", new C { T = new[] { "P", "435", "435", "gcGobal_TSCD_BAOTRI_Detial", "zgcl_gcGobal_TSCD_BAOTRI_Detail03", "AddOrUpdateBaoTriDetail" } });
        //D._a.Add("AddOrUpdateVPPDetail", new C { T = new[] { "P", "436", "436", "GBL_VPP_YEUCAU_Details", "zgcl_GBL_VPP_YEUCAU_Details01", "AddOrUpdateBaoTriDetail" } });
        object[] pr = new object[2];
        pr[0] = obj;
        pr[1] = new { Result = "Fail", Records = "", TotalRecordCount = 0 };
        Dictionary<string, object> _ip = (Dictionary<string, object>)obj;

        //-------------------------------------------------------------------
        //convert object
        //if (_ip.ContainsKey("id"))
        //{
        //    int key = (int)_ip["id"];
        //    C[] f = D._fd[key];
        //    if (_ip.ContainsKey("d"))
        //    {
        //        var d = (Dictionary<string, object>)_ip["d"];
        //        var newd = new Dictionary<string, object>();
        //        for (int m = 0; m < f.Length; m++)
        //        {
        //            string of = f[m].T[7];
        //            newd[of] = d["f" + (m <= 9 ? "0" + m.ToString() : m.ToString() + "")];
        //        }
        //        _ip["d"] = newd;
        //    }
        //}
        //------------------------------------------------------------------
        if (_ip.ContainsKey("a"))
        {
            var right = (string)Session["gcRightGroup"];
            if (right != null)
            {
                var a = _ip["a"] as string;
                if (a != null && D._a.ContainsKey(a))
                {
                    var _a = D._a[a];
                    P p = new P();
                    MethodInfo m = p.GetType().GetMethod(_a.T[5]);
                    if (m != null)
                    {
                        m.Invoke(p, pr);// sử dụng gọi method
                        pr[1] = new { Result = "OK", Id = _ip["id"], Name = _ip["n"] as string };
                    }
                    else // call extension
                    {
                        PE pe = new PE();
                        m = pe.GetType().GetMethod(_a.T[5]);
                        if (m != null)
                            m.Invoke(pe, pr); // sử dụng gọi method
                    }
                }
            }
            else
            {
                pr[1] = new { Result = "FAIL", Records = "Không có quyền truy cập dữ liệu" };
            }
        }
        return pr[1];
    }
}
