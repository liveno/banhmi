using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using zgc0LibAdmin;

/// <summary>
/// Summary description for zgc0zgc0CheckUser
/// </summary>
public class zgc0CheckUser
{
    public zgc0CheckUser()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static bool ValidNew(Page ThePage)
    {
        return true;
    }
    public static bool Valid(Page ThePage, string usernamelog)
    {
        if (ThePage.Session["zgc0Login_OK"] == null || ThePage.Session["zgc0Login_OK"].ToString() != "true")
        {
            //if (!Undo(ThePage.Session, usernamelog))
            //{
            //    ThePage.Session["gcprePage"] = ThePage.Request.Url.OriginalString;
            //    ThePage.Response.Redirect("../Default.aspx");
            //}
        }
        if (ThePage.Session["gcUsername"] == null || ThePage.Session["zgc0Login_OK"].ToString() != "true")
        {

            //if (!Undo(ThePage.Session, usernamelog))
            //{
            //    ThePage.Session["gcprePage"] = ThePage.Request.Url.OriginalString;
            //    ThePage.Response.Redirect("../Default.aspx");
            //}
        }
        return true;
    }
    public static bool Undo(HttpSessionState s, string usernamelog)
    {
        bool bReturn = false;
        if (!bReturn)
        {
            string sql2 = String.Format("select * from zgc0UserAdminCentre where username=N'{0}'", usernamelog);
            DataTable tb = zgc0HelperSecurity.GetDataTable(sql2, zgc0GlobalStr.GetAdminSqlStr());
            if (tb.Rows.Count > 0)
            {
                HttpContext.Current.Session["gs"] = tb.Rows[0]["gs"].ToString();
                HttpContext.Current.Session["go"] = tb.Rows[0]["go"].ToString();

                string sql = String.Format(
                " select DATEDIFF(minute,ExecutionTime,'{2}'),* from"
                + " gcGobal_LOG_gcT{0}"
                + " where Username=N'{1}'"
                + " and ( ExecutionTime=(select MAX(ExecutionTime) from gcGobal_LOG_gcT{0} where Operation='LOGIN' and Username=N'{1}')"
                + "      or ExecutionTime=(select MAX(ExecutionTime) from gcGobal_LOG_gcT{0} where Operation='LOGOUT' and Username=N'{1}')"
                + "       or ExecutionTime=(select MAX(ExecutionTime) from gcGobal_LOG_gcT{0} where Operation='GROUP' and Username=N'{1}'))"
                + "      and (datepart(year,'{2}') = datepart(year,ExecutionTime) )"
                + "      and (datepart(month,'{2}') = datepart(month,ExecutionTime) )"
                + "      and (datepart(day,'{2}') = datepart(day,ExecutionTime) )"
                + "      and DATEDIFF(minute,ExecutionTime, '{2}') <2400", ((DateTime.Now.Month < 10) ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()), (usernamelog == null) ? "" : usernamelog, DateTime.Now.ToString());

                string[] operatorStr = { "|{/}|" };
                DataTable tbl = zgc0HelperSecurity.GetDataTableNew(sql, tb.Rows[0]["gs"].ToString());
                if ((tbl.Rows.Count == 1) && (tbl.Rows[0]["Operation"].ToString() == "LOGIN"))
                {

                    //phuc hoi
                    string[] arr = tbl.Rows[0]["TableName"].ToString().Split(operatorStr, StringSplitOptions.None);
                    string[] arr2 = tbl.Rows[0]["Description"].ToString().Split(operatorStr, StringSplitOptions.None);
                    for (int m = 0; m < arr.Length; m++)
                        s[arr[m]] = arr2[m];
                    bReturn = true;
                }
                else if ((tbl.Rows.Count == 2) && (tbl.Rows[0]["Operation"].ToString() == "LOGIN") && (tbl.Rows[1]["Operation"].ToString() == "GROUP"))
                {
                    //phuc hoi
                    //phuc hoi
                    string[] arr = tbl.Rows[0]["TableName"].ToString().Split(operatorStr, StringSplitOptions.None);
                    string[] arr2 = tbl.Rows[0]["Description"].ToString().Split(operatorStr, StringSplitOptions.None);
                    for (int m = 0; m < arr.Length; m++)
                        s[arr[m]] = arr2[m];
                    s[tbl.Rows[1]["TableName"].ToString()] = tbl.Rows[1]["Description"].ToString();
                    bReturn = true;
                }
                else if ((tbl.Rows.Count == 3) && (tbl.Rows[1]["Operation"].ToString() == "LOGIN") && (tbl.Rows[2]["Operation"].ToString() == "GROUP"))
                {
                    //phuc hoi
                    string[] arr = tbl.Rows[1]["TableName"].ToString().Split(operatorStr, StringSplitOptions.None);
                    string[] arr2 = tbl.Rows[1]["Description"].ToString().Split(operatorStr, StringSplitOptions.None);
                    for (int m = 0; m < arr.Length; m++)
                        s[arr[m]] = arr2[m];
                    s[tbl.Rows[2]["TableName"].ToString()] = tbl.Rows[2]["Description"].ToString();
                    bReturn = true;
                }
            }
        }
        return bReturn;
    }
}
